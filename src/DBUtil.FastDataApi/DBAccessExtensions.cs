﻿using DBUtil.FastData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil
{
    /// <summary>
    /// DBAccess扩展类
    /// </summary>
    public static class DBAccessExtensions
    {
        /// <summary>
        /// 获取实体工具
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Api FastDataApi<T>(this DBAccess db) where T : class, new()
        {
            return new Api(db);
        }
    }
}
