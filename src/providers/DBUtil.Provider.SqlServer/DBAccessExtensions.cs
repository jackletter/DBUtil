using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace DBUtil.Provider.SqlServer
{
    /// <summary>
    /// DBAccess的扩展方法
    /// </summary>
    public static class DBAccessExtensions
    {
        #region IsSqlServer IsSqlServer()
        /// <summary>
        /// 是否是SqlServer数据库
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool IsSqlServer(this DBAccess db)
        {
            return db.DBType == DBType.SQLSERVER;
        }
        #endregion

        #region SqlServer版本兼容性判断
        /* 2008
        Microsoft SQL Server 2008 R2 (RTM) - 10.50.1600.1 (X64)   
        Apr  2 2010 15:48:46   
        Copyright (c) Microsoft Corporation  
        Enterprise Edition (64-bit) on Windows NT 6.1 <X64> (Build 7601: Service Pack 1) 
        */
        /* 2012
        Microsoft SQL Server 2012 (SP1) - 11.0.3000.0 (X64) 
	        Oct 19 2012 13:38:57 
	        Copyright (c) Microsoft Corporation
	        Developer Edition (64-bit) on Windows NT 6.2 <X64> (Build 9200: ) (Hypervisor)
        */
        /* 2014
          Microsoft SQL Server 2014 (SP2) (KB3171021) - 12.0.5000.0 (X64) 
          Jun 17 2016 19:14:09 
          Copyright (c) Microsoft Corporation
          Enterprise Edition (64-bit) on Windows NT 6.3 <X64> (Build 17763: )
        */
        /*
          由上面版本的字符串,推测出其他版本的字符串如下:
          Microsoft SQL Server 2008
          Microsoft SQL Server 2008 R2
          Microsoft SQL Server 2012
          Microsoft SQL Server 2014
          Microsoft SQL Server 2016
          Microsoft SQL Server 2017
          Microsoft SQL Server 2019          
          这里只考虑2008R2及以上的
         */
        private static List<(int index, string versionstr)> Versions = new List<(int index, string versionstr)>()
        {
            (0,"Microsoft SQL Server 2008 R2"),
            (1,"Microsoft SQL Server 2012"),
            (2,"Microsoft SQL Server 2014"),
            (3,"Microsoft SQL Server 2016"),
            (4,"Microsoft SQL Server 2017"),
            (5,"Microsoft SQL Server 2019")
        };
        private static bool JudgeVersion(string version, int level)
        {
            int index = Versions.FindIndex(ver => version.Contains(ver.versionstr));
            return index >= level;
        }

        /// <summary>
        /// 是否兼容SqlServer2008 R2
        /// </summary>
        /// <returns></returns>
        public static bool IsSqlServerVersion2008R2Compatible(this DBAccess db)
        {
            return JudgeVersion(db.DBVersion, 0);
        }

        /// <summary>
        /// 是否兼容SqlServer2012
        /// </summary>
        /// <returns></returns>
        public static bool IsSqlServerVersion2012Compatible(this DBAccess db)
        {
            return JudgeVersion(db.DBVersion, 1);
        }

        /// <summary>
        /// 是否兼容SqlServer2014
        /// </summary>
        /// <returns></returns>
        public static bool IsSqlServerVersion2014Compatible(this DBAccess db)
        {
            return JudgeVersion(db.DBVersion, 2);
        }

        /// <summary>
        /// 是否兼容SqlServer2016
        /// </summary>
        /// <returns></returns>
        public static bool IsSqlServerVersion2016Compatible(this DBAccess db)
        {
            return JudgeVersion(db.DBVersion, 3);
        }
        /// <summary>
        /// 是否兼容SqlServer2017
        /// </summary>
        /// <returns></returns>
        public static bool IsSqlServerVersion2017Compatible(this DBAccess db)
        {
            return JudgeVersion(db.DBVersion, 4);
        }
        /// <summary>
        /// 是否兼容SqlServer2019
        /// </summary>
        /// <returns></returns>
        public static bool IsSqlServerVersion2019Compatible(this DBAccess db)
        {
            return JudgeVersion(db.DBVersion, 5);
        }
        #endregion
    }
}
