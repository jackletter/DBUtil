﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer
{
    public enum EnumSqlServerColumnType
    {
        /// <summary>
        /// 位类型
        /// </summary>
        Bit = 1,

        #region 数字货币
        TinyInt = 2,
        SmallInt = 3,
        Int = 4,
        BigInt = 5,

        Float = 6,
        Real = 7,

        Numeric = 8,
        Decimal = 9,

        SmallMoney = 10,
        Money = 11,
        #endregion

        #region 字符串
        Char = 12,
        VarChar = 13,
        NChar = 14,
        NVarChar = 15,
        Text = 16,
        NText = 17,

        Binary = 18,
        Image = 19,
        VarBinary = 20,
        #endregion

        #region 时间日期
        Date = 21,
        DateTime = 22,
        DateTime2 = 23,
        DateTimeOffset = 24,
        SmallDateTime = 25,
        Time = 26,
        /// <summary>
        /// 非时间,只是一个自增号
        /// </summary>
        TimeStamp = 27,
        #endregion

        Geography = 28,
        Geometry = 29,

        Hierarchyid = 30,
        SqlVariant = 31,
        UniqueIdentifier = 32,
        Xml = 33,

        Other = 34
    }
}
