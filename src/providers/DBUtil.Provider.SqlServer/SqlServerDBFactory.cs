﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data.SqlClient;

namespace DBUtil.Provider.SqlServer
{
    /// <summary>
    /// SqlServer数据库工厂类
    /// </summary>
    public class SqlServerDBFactory : IDBFactory<SqlServerAccess>
    {
        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBConn">连接字符串</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public static SqlServerAccess CreateDB(string DBConn, DBSetting Settings) => new SqlServerAccess(DBConn, Settings);

        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBType">数据库类型:SQLSERVER</param>
        /// <param name="DBConn">连接字符串:Data Source=.;Initial Catalog=test;User ID=sa;Password=xx;</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public SqlServerAccess CreateDB(string DBType, string DBConn, DBSetting Settings)
        {
            DBType = (DBType ?? "").ToUpper();
            switch (DBType)
            {
                case Consts.ServerType:
                    {
                        return CreateDB(DBConn, Settings);
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}
