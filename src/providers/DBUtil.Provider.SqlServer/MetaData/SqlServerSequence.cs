﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// 数据库序列
    /// </summary>
    public class SqlServerSequence : Sequence
    {
        /// <summary>
        /// 是否缓存
        /// </summary>
        public bool IsCached { get; set; }

        /// <summary>
        /// 缓存大小
        /// </summary>
        public int? CacheSize { get; set; }

        public string CreateSql
        {
            get
            {
                var sql = $@"
CREATE SEQUENCE [{SchemaName}].[{Name}]  
   AS {this.Type}  
    START WITH {StartValue}  
    INCREMENT BY {Increment} 
    MINVALUE {MinValue}  
    MAXVALUE {MaxValue}
    {(IsCycle ? "CYCLE" : "NO CYCLE")}
    {(IsCached ? "CACHE " + CacheSize : "NO CACHE")};
";
                return sql;
            }
        }
    }
}
