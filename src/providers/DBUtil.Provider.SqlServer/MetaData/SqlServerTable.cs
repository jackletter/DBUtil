﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer表
    /// </summary>
    public class SqlServerTable : Table
    {
        /// <summary>
        /// 表存储空间名称(文件组/分区方案或其他名称)
        /// </summary>
        public string DataSpaceName { get; set; }

        /// <summary>
        /// 表存储的空间类型
        /// </summary>
        public string DataSpaceType { set; get; }

        /// <summary>
        /// 表存储的空间类型描述
        /// </summary>
        public string DataSpaceTypeDesc { get; set; }

        /// <summary>
        /// 是否存储在文件组中
        /// </summary>
        public bool IsInFileGroup => DataSpaceType == "FG";

        /// <summary>
        /// 是否进行了分区
        /// </summary>
        public bool IsInPartitionScheme => DataSpaceType == "PS";

        /// <summary>
        /// 数据库对象占用的大小
        /// </summary>
        public string ReservedSize { get; set; }

        /// <summary>
        /// 数据占用的大小
        /// </summary>
        public string DataSize { get; set; }

        /// <summary>
        /// 索引占用的大小
        /// </summary>
        public string IndexSize { get; set; }

        /// <summary>
        /// 未使用的大小
        /// </summary>
        public string UnUsedSize { get; set; }
    }
}
