﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer文件
    /// </summary>
    public class SqlServerFile
    {
        /// <summary>
        /// 文件Id
        /// </summary>
        public int File_Id { get; set; }

        /// <summary>
        /// 文件类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 类型描述
        /// </summary>
        public string TypeDesc { get; set; }

        /// <summary>
        /// 文件组Id
        /// </summary>
        public int Data_Space_Id { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 物理文件路径
        /// </summary>
        public string Physical_Name { get; set; }

        /// <summary>
        /// 文件状态
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 文件状态描述
        /// </summary>
        public string StateDesc { set; get; }

        /// <summary>
        /// 文件当前大小(单位:8K),可使用Size_MB
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 文件当前大小(单位:M)
        /// </summary>
        public int Size_MB => Size * 8 / 1024;

        /// <summary>
        /// 限制大小(单位:8K),-1表示无限制,可使用Max_Size_Desc
        /// </summary>
        public int Max_Size { get; set; }

        /// <summary>
        /// 限制大小(单位:8K)
        /// </summary>
        public string Max_Size_Desc
        {
            get
            {
                if (Max_Size == -1) return "无限制";
                return Max_Size * 8 / 1024 + "MB";
            }
        }

        /// <summary>
        /// 增长大小(单位:8K或%),可使用Growth_Desc
        /// </summary>
        public int Growth { get; set; }

        /// <summary>
        /// 是否按照百分比增长,可使用Growth_Desc
        /// </summary>
        public int IsPercentGrowth { get; set; }

        /// <summary>
        /// 增长描述
        /// </summary>
        public string Growth_Desc
        {
            get
            {
                if (IsPercentGrowth == 1) return Growth + "%";
                return Growth * 8 / 1024 + "MB";
            }
        }

        /// <summary>
        /// 是否只读
        /// </summary>
        public int IsReadOnly { get; set; }

    }
}
