﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer索引
    /// </summary>
    public class SqlServerIndex : DBUtil.MetaData.Index
    {
        /// <summary>
        /// 索引存储空间名称(文件组/分区方案或其他名称)
        /// </summary>
        public string DataSpaceName { get; set; }

        /// <summary>
        /// 索引存储的空间类型
        /// </summary>
        public string DataSpaceType { set; get; }

        /// <summary>
        /// 索引存储的空间类型描述
        /// </summary>
        public string DataSpaceTypeDesc { get; set; }

        /// <summary>
        /// 是否存储在文件组中
        /// </summary>
        public bool IsInFileGroup => DataSpaceType == "FG";

        /// <summary>
        /// 是否进行了分区
        /// </summary>
        public bool IsInPartitionScheme => DataSpaceType == "PS";

        /// <summary>
        /// 是否是主键约束
        /// </summary>
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// 是否是唯一约束
        /// </summary>
        public bool IsUniqueKey { get; set; }
    }
}
