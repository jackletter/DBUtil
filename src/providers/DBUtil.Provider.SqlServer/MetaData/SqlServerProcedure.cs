﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer存储过程
    /// <seealso href="https://docs.microsoft.com/zh-cn/sql/relational-databases/system-stored-procedures/sp-sproc-columns-transact-sql?view=sql-server-ver15"/>
    /// </summary>
    public class SqlServerProcedure : Procedure
    {
        /// <summary>
        /// 描述
        /// </summary>
        public string Desc { get; set; }
        public class Parameter
        {
            public string ColumnName { get; set; }
            public ParameterType ColumnType { get; set; }
            public int DataType { get; set; }
            public string TypeName { get; set; }
            public int Precision { get; set; }
            public int Length { get; set; }
            public int Scale { get; set; }

            /// <summary>
            /// 感觉这个属性不准
            /// </summary>
            public bool NullAble { get; set; }
        }

        public enum ParameterType
        {
            /// <summary>
            /// 没遇到
            /// </summary>
            SQL_PARAM_TYPE_UNKNOWN = 0,

            /// <summary>
            /// 常见的输入参数
            /// </summary>
            SQL_PARAM_TYPE_INPUT = 1,

            /// <summary>
            /// 常见的输出参数
            /// </summary>
            SQL_PARAM_TYPE_OUTPUT = 2,

            /// <summary>
            /// 没遇到
            /// </summary>
            SQL_RESULT_COL = 3,

            /// <summary>
            /// 没遇到
            /// </summary>
            SQL_PARAM_OUTPUT = 4,

            /// <summary>
            /// 好像每个存储都会有这个
            /// </summary>
            SQL_RETURN_VALUE = 5
        }

        /// <summary>
        /// 参数集合
        /// </summary>
        public List<Parameter> Parameters { get; set; } = new List<Parameter>();        

    }


}
