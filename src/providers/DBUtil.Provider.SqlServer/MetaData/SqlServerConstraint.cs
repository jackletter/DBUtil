﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer约束
    /// </summary>
    public class SqlServerConstraint : Constraint
    {
        /// <summary>
        /// 是否是系统默认创建的
        /// </summary>
        public bool IsSystemNamed { get; set; }

        /// <summary>
        /// 约束定义语句,如:默认约束-(getdate()),检查约束-(len([name])&lt;(5))
        /// </summary>
        public string Definition { get; set; }
    }
}
