﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlSever函数
    /// </summary>
    public class SqlServerFunction : Function
    {
        /// <summary>
        /// 函数类型:TF/FM/IF
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 函数类型说明:SQL_TABLE_VALUED_FUNCTION/SQL_SCALAR_FUNCTION/SQL_INLINE_TABLE_VALUED_FUNCTION
        /// </summary>
        public string TypeString { get; set; }

        /// <summary>
        /// 函数描述
        /// </summary>
        public string Desc { get; set; }
    }
}
