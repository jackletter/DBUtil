﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer文件组
    /// </summary>
    public class SqlServerFileGroup
    {
        /// <summary>
        /// 文件组Id
        /// </summary>
        public int Data_Space_Id { get; set; }

        /// <summary>
        /// 文件组名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件组类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 文件组描述
        /// </summary>
        public string TypeDesc { get; set; }

        /// <summary>
        /// 是否是默认的文件组
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 是否是系统
        /// </summary>
        public bool IsSystem { get; set; }

        /// <summary>
        /// 是否是只读
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// 文件组下的文件
        /// </summary>
        public List<SqlServerFile> Files { set; get; } = new List<SqlServerFile>();
    }
}
