﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// 数据库恢复历史记录
    /// </summary>
    public class SqlServerRestoreHistory
    {
        /// <summary>
        /// 恢复id
        /// </summary>
        public int Restore_History_Id { get; set; }

        /// <summary>
        /// 恢复日期
        /// </summary>
        public DateTime Restore_Date { get; set; }

        /// <summary>
        /// 恢复到的目标数据库名称
        /// </summary>
        public string Destination_Database_Name { get; set; }

        /// <summary>
        /// 恢复使用的备份介质
        /// </summary>
        public string Physical_Device_Name { get; set; }

        /// <summary>
        /// 操作用户名称
        /// </summary>
        public string User_Name { get; set; }

        /// <summary>
        /// 对应的备份Id
        /// </summary>
        public int? Backup_Set_Id { set; get; }

        /// <summary>
        /// 备份集名称
        /// </summary>
        public string Backup_Set_Name { get; set; }

        /// <summary>
        /// 恢复类型
        /// </summary>
        public string RestoreType { get; set; }
    }
}
