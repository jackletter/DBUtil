﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer视图
    /// </summary>
    public class SqlServerView : View
    {
        /// <summary>
        /// 视图注释
        /// </summary>
        public string Desc { get; set; }
    }
}
