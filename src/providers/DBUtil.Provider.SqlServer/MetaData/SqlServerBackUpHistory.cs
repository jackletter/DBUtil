﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// 数据库备份历史记录
    /// </summary>
    public class SqlServerBackUpHistory
    {
        /// <summary>
        /// 备份ID
        /// </summary>
        public int BackUp_Set_Id { get; internal set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBase_Name { get; internal set; }

        /// <summary>
        /// 备份开始时间
        /// </summary>
        public DateTime BackUp_Start_Date { get; internal set; }

        /// <summary>
        /// 备份大小
        /// </summary>
        public string Size { get; internal set; }

        /// <summary>
        /// 备份耗时
        /// </summary>
        public string TimeToken { get; internal set; }

        /// <summary>
        /// 备份类型
        /// </summary>
        public string BackUpType { get; internal set; }

        /// <summary>
        /// 备份介质
        /// </summary>
        public string Physical_Device_Name { get; internal set; }

        /// <summary>
        /// 备份集名称
        /// </summary>
        public string Backup_Set_Name { get; internal set; }

        /// <summary>
        /// 备份用户
        /// </summary>
        public string User_Name { get; internal set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime? Expiration_Date { get; internal set; }
    }
}
