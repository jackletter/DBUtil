﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// SqlServer触发器
    /// </summary>
    public class SqlServerTrigger : Trigger
    {
        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool IsDisable { get; set; }

        /// <summary>
        /// 是否不用于复制
        /// </summary>
        public bool IsNotForReplication { get; set; }
    }
}
