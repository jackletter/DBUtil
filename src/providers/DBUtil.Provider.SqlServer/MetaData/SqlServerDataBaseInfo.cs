﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.MetaData
{
    /// <summary>
    /// 数据库整体情况
    /// </summary>
    public class SqlServerDataBaseInfo
    {
        /// <summary>
        /// 系统磁盘剩余的可用空间
        /// </summary>
        public List<(string dirver, long remainSpace_M)> DriverRemainSpaces { get; set; } = new List<(string dirver, long remainSpace_M)>();

        /// <summary>
        /// 通过sp_server_info->(Microsoft SQL Server)
        /// </summary>
        public string DBMS_NAME { get; set; }

        /// <summary>
        /// 通过sp_server_info->(Microsoft SQL Server 2014 - 12.0.5000.0)
        /// </summary>
        public string DBMS_VER { get; set; }

        /// <summary>
        /// 内部版本号 通过sp_server_info->(12.00.5000)
        /// </summary>
        public string SYS_SPROC_VERSION { get; set; }

        /// <summary>
        /// 数据库字符集/排序规则 通过sp_server_info->charset=cp936 collation=Chinese_PRC_CI_AS
        /// </summary>
        public string Collection_Seq { get; set; }

        /// <summary>
        /// 通过select @@VERSION获得
        /// </summary>
        public string FullVersion { get; set; }

        /// <summary>
        /// 数据库名称 通过exec sp_spaceused获得
        /// </summary>
        public string DBName { get; set; }

        /// <summary>
        /// 数据库大小 通过exec sp_spaceused获得
        /// </summary>
        public string DBSize { get; set; }

        /// <summary>
        /// 未分配的空间大小 通过exec sp_spaceused获得
        /// </summary>
        public string UnAllocatedSize { get; set; }

        /// <summary>
        /// 数据库对象占用的空间 通过exec sp_spaceused获得
        /// </summary>
        public string Reserved { get; set; }

        /// <summary>
        /// 数据占用的空间 通过exec sp_spaceused获得
        /// </summary>
        public string DataSize { get; set; }

        /// <summary>
        /// 索引占用空间 通过exec sp_spaceused获得
        /// </summary>
        public string IndexSize { get; set; }

        /// <summary>
        /// 已分配尚未使用的空间大小 通过exec sp_spaceused获得
        /// </summary>
        public string UnUsedSize { get; set; }

        /// <summary>
        /// 日志的大小和使用率 通过DBCC SQLPERF(LOGSPACE)获得
        /// </summary>
        public (double logsize_MB, double log_percent) LogStat { get; set; }

        /// <summary>
        /// 数据库创建时间 通过 select * from sys.databases where[name] ='test'获得
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
