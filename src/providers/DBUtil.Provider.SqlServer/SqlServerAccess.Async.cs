﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DotNetCommon;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using System.Data;

namespace DBUtil.Provider.SqlServer
{
    /// <summary>
    /// Microsoft SQL Server操作对象
    /// </summary>
    public partial class SqlServerAccess : DBAccess
    {
        #region Is系列: 判断表/视图/列/存储过程是否存在
        public async override Task<bool> IsProcedureExistAsync(string procName, CancellationToken cancellationToken = default(CancellationToken))
        {
            var sql = IsProcedureExistSql(procName);
            int r = await SelectScalarAsync<int>(sql, cancellationToken);
            return r > 0;
        }

        public async override Task<bool> IsTriggerExistAsync(string triggerName, CancellationToken cancellationToken = default(CancellationToken))
        {
            var sql = IsTriggerExistSql(triggerName);
            int r = await SelectScalarAsync<int>(sql, cancellationToken);
            return r > 0;
        }
        #endregion

        #region 使用SqlBulkCopy批量插入数据
        public override async Task BulkCopyAsync(DataTable dt, string tableName = null, int timeoutSeconds = 60 * 30, int notifyAfter = 0, Func<long, Task<bool>> callBack = null, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(tableName)) tableName = dt.TableName;
            if (string.IsNullOrWhiteSpace(tableName)) throw new Exception("必须指定要目的表名!");
            SqlBulkCopy sbc = null;
            if (IsTransaction) sbc = new SqlBulkCopy((SqlConnection)CurrentConnection, SqlBulkCopyOptions.Default, (SqlTransaction)CurrentTransaction);
            else if (IsSession) sbc = new SqlBulkCopy((SqlConnection)CurrentConnection);
            else sbc = new SqlBulkCopy((SqlConnection)GetNewConnection());

            sbc.BulkCopyTimeout = timeoutSeconds;
            sbc.DestinationTableName = tableName;
            sbc.NotifyAfter = notifyAfter;
            if (callBack != null) sbc.SqlRowsCopied += async (object sender, SqlRowsCopiedEventArgs e) =>
            {
                e.Abort = await callBack(e.RowsCopied);
            };

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sbc.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
            }
            await sbc.WriteToServerAsync(dt);
        }
        #endregion

        #region 基于数据库的Id和流水号生成器
        /// <summary>
        /// 根据指定的表名和列名生成Id
        /// </summary>
        /// <param name="tableName">指定的表名</param>
        /// <param name="colName">指定的列名</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected async override Task<long> InternalNewIdAsync(string tableName, string colName, CancellationToken cancellationToken = default(CancellationToken))
        {
            EnsureInitGenerator();
            var id = await SelectScalarAsync<long>($"exec {Settings.DBCacheGeneratorIdProcedureName} \"{tableName}\",\"{colName}\"", cancellationToken);
            return id;
        }

        /// <summary>
        /// 根据指定的表名和列名批量生成Id
        /// </summary>
        /// <param name="tableName">指定的表名</param>
        /// <param name="colName">指定的列名</param>
        /// <param name="count">生成的Id的数量</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected async override Task<List<long>> InternalNewIdsAsync(string tableName, string colName, int count, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (count < 1) throw new Exception("批量生成的数量最小为1!");
            EnsureInitGenerator();
            var id = await SelectScalarAsync<long>($"exec {Settings.DBCacheGeneratorIdProcedureName} \"{tableName}\",\"{colName}\",{count}", cancellationToken);
            var res = new List<long>();
            for (long i = id - count + 1; i <= id; i++)
            {
                res.Add(i);
            }
            return res;
        }

        /// <summary>
        /// 根据表名和列名生成流水号
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="colName">列名</param>
        /// <param name="format">流水号格式</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected async override Task<string> InternalNewSNOAsync(string tableName, string colName, SerialFormat format, CancellationToken cancellationToken = default(CancellationToken))
        {
            SerialFormat.ValidFormat(format);
            EnsureInitGenerator();
            var (likestr, snoNow, startindex) = SerialFormat.Parse(format, null, DotNetCommon.Machine.MachineIdString);
            long no = await SelectScalarAsync<long>($"exec {Settings.DBCacheGeneratorSNOProcedureName} @tablename='{tableName}',@colname='{colName}',@nowstr='{snoNow.ToString("yyyy-MM-dd HH:mm:ss")}', @likestr='{likestr}',@startindex={startindex}", cancellationToken);
            var chunk = format.Chunks.FirstOrDefault(i => i.Type == SerialFormatChunkType.SerialNo);
            var sno = likestr.Substring(0, startindex);
            var s = no.ToString();
            if (s.Length > chunk.Length)
            {
                sno += s;
            }
            else
            {
                sno += s.PadLeft(chunk.Length, '0');
            }
            return sno;
        }

        /// <summary>
        /// 根据表名和列名批量生成流水号
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="colName">列名</param>
        /// <param name="format">流水号格式</param>
        /// <param name="count">生成的流水号的数量</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected async override Task<List<string>> InternalNewSNOsAsync(string tableName, string colName, SerialFormat format, int count, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (count < 1) throw new Exception("批量生成的数量最小为1!");
            SerialFormat.ValidFormat(format);
            EnsureInitGenerator();
            var (likestr, snoNow, startindex) = SerialFormat.Parse(format, null, DotNetCommon.Machine.MachineIdString);
            long no = await SelectScalarAsync<long>($"exec {Settings.DBCacheGeneratorSNOProcedureName} @tablename='{tableName}',@colname='{colName}',@nowstr='{snoNow.ToString("yyyy-MM-dd HH:mm:ss")}', @likestr='{likestr}',@startindex={startindex},@count={count}", cancellationToken);
            var chunk = format.Chunks.FirstOrDefault(i => i.Type == SerialFormatChunkType.SerialNo);
            var res = new List<string>();
            for (long i = no - count + 1; i <= no; i++)
            {
                var sno = likestr.Substring(0, startindex);
                var s = i.ToString();
                if (s.Length > chunk.Length)
                {
                    sno += s;
                }
                else
                {
                    sno += s.PadLeft(chunk.Length, '0');
                }
                res.Add(sno);
            }
            return res;
        }
        #endregion
    }
}
