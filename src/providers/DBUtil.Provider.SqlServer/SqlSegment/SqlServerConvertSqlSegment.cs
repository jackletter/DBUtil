﻿using DBUtil.SqlSegment;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql.SqlSegment
{
    /*     
-- 字符串转小数
select convert(decimal(30,10),'123.123') -- "123.1230000000"
-- 字符串转整数
select convert(decimal,'123') -- "123"
-- 字符串转日期
select convert(datetime2,'2022-06-10 11:24:01.123') -- "2022-06-10 11:24:01.1230000"


-- 整数转字符串
select convert(varchar,123) --"123"
-- 小数转字符串
select convert(varchar,123.123) -- "123.123"
-- 日期转字符串 不带时区
select convert(varchar,getdate(),121) -- "2022-06-10 11:36:42.323"
    */

    public class SqlServerConvertSqlSegment : ConvertSqlSegment
    {
        public SqlServerConvertSqlSegment(DBAccess db) : base(db)
        {
        }

        private string ConvertTo(string input, string toType) => $"convert({toType},{input})";
        public override string ConvertDateTimeToString(string input) => ConvertTo(input, "varchar");

        public override string ConvertDecimalToString(string input) => ConvertTo(input, "varchar");

        public override string ConvertIntegerToString(string input) => ConvertTo(input, "varchar");


        public override string ConvertStringToDateTime(string input) => ConvertTo(input, "datetime2");

        public override string ConvertStringToDecimal(string input) => ConvertTo(input, "decimal(30,10)");

        public override string ConvertStringToInteger(string input) => ConvertTo(input, "decimal");
    }
}
