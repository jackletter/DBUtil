﻿using DBUtil.SqlSegment;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.SqlServer.SqlSegment
{
    /// <summary>
    /// 数据库字符串运算符
    /// </summary>
    public class SqlServerStringSqlSegment : StringSqlSegment
    {
        public SqlServerStringSqlSegment(DBAccess db) : base(db)
        {
        }

        public override string GetLength(string colName, bool null2Zero = false)
        {
            if (null2Zero) return $"len(isnull({colName},''))";
            else return $"len({colName})";
        }
        public override string Trim(string colName) => $"ltrim(rtrim({colName}))";
        public override string LeftTrim(string colName) => $"ltrim({colName})";
        public override string RightTrim(string colName) => $"rtrim({colName})";
        public override string Upper(string colName) => $"upper({colName})";
        public override string Lower(string colName) => $"lower({colName})";
        public override string ReplaceAll(string colName, string oldStr, string newStr) => $"replace({colName},'{oldStr}','{newStr}')";
        public override string SplitFirst(string input, string pattern) => $"substring({input},0,CHARINDEX({pattern},{input},0))";
        public override string SplitLast(string input, string pattern) => $"reverse(substring(reverse({input}),0,charindex({pattern},reverse({input}),0)))";
        public override string Add(string input, string input2) => $"{input}+{input2}";
        public override string SubString(string input, int start, int len) => $"substring({input},{start + 1},{len})";
    }
}
