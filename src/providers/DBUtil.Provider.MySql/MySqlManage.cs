﻿using System;
using System.Data;
using DBUtil.MetaData;
using System.Linq;
using DBUtil.Provider.MySql.MetaData;
using DotNetCommon.Extensions;
using DotNetCommon;

namespace DBUtil.Provider.MySql
{
    /// <summary>
    /// MySql操作对象
    /// </summary>
    public partial class MySqlManage : DBManage
    {
        /// <summary>
        /// 根据DBAccess创建DBManage
        /// </summary>
        /// <param name="db"></param>
        public MySqlManage(DBAccess db) : base(db) { }

        #region 修改元数据
        private Column getColumn(string tableName, string colName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            Ensure.NotNullOrEmptyOrWhiteSpace(colName);
            tableName = db.EscapeString(tableName);
            colName = db.EscapeString(colName);
            var objName = db.ParseObjectName(tableName);

            var cols = getColumns(objName);
            var column = cols.FirstOrDefault(col => col.Name.ToUpper() == colName?.ToUpper());
            return column;
        }
        public override string RenameTableSql(string oldName, string newName = null)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(oldName);
            oldName = oldName.IfNullOrEmptyUse($"{db.SchemaName}.TableName");
            var objName = db.ParseObjectName(oldName);
            if (newName.IsNullOrEmptyOrWhiteSpace())
            {
                if (objName.SegCount > 1) newName = $"{objName.SchemaName}.NewTableName";
                else newName = $"NewTableName";
            }

            var sql = $@"-- 1. 可以对多张表进行重命名
-- rename table 
-- 	old_table1 to new_table1,
-- 	old_table2 to new_table2,
-- 	old_table3 to new_table3;

-- 2. 可以交换两张表名称
-- rename table 
-- 	old_table to tmp_table,
-- 	new_table to old_table,
-- 	tmp_table to new_table;

-- 3. 可以将表从一个schema移动到另一个schema
-- rename table current_db.tbl_name to other_db.tbl_name;

-- 更多: https://dev.mysql.com/doc/refman/8.0/en/rename-table.html

rename table {objName.FullNameQuoted} to {db.QuotedName(newName)}
";
            return sql;
        }

        public override string RenameColumnSql(string tableName, string oldName, string newName = null)
        {
            var column = getColumn(tableName, oldName);

            if (column == null) throw new Exception($"表 {tableName} 中不存在列: {oldName}!");

            newName ??= "NewColumnName";
            tableName = db.QuotedName(tableName);
            var sql = $@"-- rename column(比 alter table t1 change a b varchar(50) 更便捷) 
-- 1. 重命名多列
-- alter table t1 
--     rename column b to a,
--     rename column c to d;

-- 2. 交换列名(一般我们认为不能将 表t1(a int,b int,c int) 的列a重命名为列b,但当我们需要交换列名时就可能有必要这么做了)
-- -- swap a and b
-- alter table t1 
-- 	rename column a to b,
-- 	rename column b to a;
-- -- ""rotate"" a, b, c through a cycle
-- alter table t1 
-- 	rename column a to b,
-- 	rename column b to c,
-- 	rename column c to a;

-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

alter table {db.QuotedName(tableName)} rename column {db.QuotedName(oldName)} to {db.QuotedName(newName)}
";
            return sql;
        }

        public override string UpdateTableDescriptionSql(string tableName, string description = null)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            description ??= "Description";
            return $"alter table {db.QuotedName(tableName)} comment '{description}'";
        }

        public override string DropTableSql(string tableName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            return $"drop table {db.QuotedName(tableName)}";
        }

        public override string DropTableIfExistSql(string tableName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            return $"drop table if exists {db.QuotedName(tableName)}";
        }

        public override string DropTriggerIfExistSql(string triggerName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(triggerName);
            return $"drop trigger if exists {db.QuotedName(triggerName)}";
        }

        public override string DropProcedureIfExistSql(string procedureName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(procedureName);
            return $"drop procedure if exists {db.QuotedName(procedureName)}";
        }

        public override string UpdateColumnDescriptionSql(string tableName, string colName, string description = null)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有在表 {tableName} 中找到列 {colName}!");
            description ??= "ColumnDescription";
            column.Desc = description;
            var sql = $@"-- 并没有找到仅修改列注释的语法
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

{GetModifyColumnSql(column)}
";
            return sql;
        }

        public override string AddColumnSql(string tableName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            var sql = $@"-- 参考: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html
alter table TableName add column ColumnName
   int -- 如: int
--    null -- 是否可为空 
--    unique -- 是否唯一
--    auto_increment -- 是否自增
--    default 0 -- 默认值
--    generated always as (age+1) -- stored -- 计算列定义
--    comment '列注释'
--    character set utf8mb4
--    collate utf8mb4_general_ci
";
            return sql;
        }

        public override string UpdateColumnNullAbleSql(string tableName, string colName, bool isNullAble)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有在表 {tableName} 中找到列 {colName}!");
            if (column.IsPrimaryKey && isNullAble == true) throw new Exception("主键不允许设置为空!");
            if (column.IsIdentity && isNullAble == true) throw new Exception("自增不允许设置为空!");
            column.IsNullAble = isNullAble;
            var sql = $@"-- 并没有找到仅修改列nullable的语法
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

{GetModifyColumnSql(column)}
";
            return sql;
        }

        public override string UpdateColumnUniqueSql(string tableName, string colName, bool isUnique)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有找到列{colName}");
            if (column.IsPrimaryKey && isUnique == false) throw new Exception("主键必须唯一!");
            if (column.IsIdentity && isUnique == false) throw new Exception("自增必须唯一!");
            column.IsUnique = isUnique;
            var sql = $@"-- 两种方法:
-- 1. 方法2: 通过增删唯一约束实现
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/constraint-primary-key.html
-- alter table {db.QuotedName(tableName)} add constraint {db.QuotedName(colName)} unique({db.QuotedName(colName)})
-- alter table {db.QuotedName(tableName)} drop constraint {db.QuotedName(colName)}

-- 2. 方法1: 使用 alter table ... 实现
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

{GetModifyColumnSql(column)}
";
            return sql;
        }

        public override string UpdateColumnIdentitySql(string tableName, string colName, bool isIdentity)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有在表 {tableName} 中找到列 {colName}!");
            column.IsIdentity = isIdentity;
            var sql = $@"-- 并没有找到仅修改列自增的语法
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

{GetModifyColumnSql(column)}
";
            return sql;
        }

        public override string UpdateColumnTypeSql(string tableName, string colName, string typeString = null)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有在表 {tableName} 中找到列 {colName}!");
            if (typeString.IsNotNullOrEmptyOrWhiteSpace()) column.TypeString = typeString;
            var sql = $@"-- change比modify和rename功能更多一点
-- 注意: 将 varchar(50) 改为 int 可能导致数据全变为0, 而不是报错
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

{GetModifyColumnSql(column)}
";
            return sql;
        }

        public override string UpdateColumnSql(string tableName, string colName)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有在表 {tableName} 中找到列 {colName}!");
            var sql = $@"-- change比modify和rename功能更多一点
-- 注意: 将 varchar(50) 改为 int 可能导致数据全变为0, 而不是报错
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

{GetModifyColumnSql(column)}
";
            return sql;
        }
        private string GetModifyColumnSql(Column column)
        {
            var mysqlColumn = column as MySqlColumn;
            string sql = $"alter table {db.QuotedName(column.TableName)} change {db.QuotedName(column.Name)} {db.QuotedName(column.Name)} {column.TypeString}";
            if (column.IsIdentity) sql += " auto_increment";

            if (column.IsNullAble) sql += " not null";
            else sql += " not null";

            if (column.HasDefault) sql += $" default {column.Default}";

            if (column.IsUnique) sql += " unique";
            if (mysqlColumn.IsComputed)
            {
                sql += $" generated always as {mysqlColumn.ComputedDefinition}";
                if (mysqlColumn.IsPersisted == true)
                {
                    sql += " stored";
                }
            }
            if (column.Desc.IsNotNullOrEmptyOrWhiteSpace())
                sql += $" comment '{db.EscapeString(column.Desc)}'";
            if (mysqlColumn.CHARACTER_SET_NAME.IsNotNullOrEmptyOrWhiteSpace())
                sql += $" character set {mysqlColumn.CHARACTER_SET_NAME}";
            if (mysqlColumn.COLLATION_NAME.IsNotNullOrEmptyOrWhiteSpace())
                sql += $" collate {mysqlColumn.COLLATION_NAME}";
            return sql;
        }

        public override string UpdateColumnDefaultSql(string tableName, string colName, string defaultString = null)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有在表 {tableName} 中找到列 {colName}!");
            defaultString ??= "默认值";
            var sql = $@"-- alter table tbl_name alter column col_name set default '123'
alter table tbl_name alter column col_name drop default
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

alter table {db.QuotedName(tableName)} alter column {db.QuotedName(colName)} set default '{defaultString}'
";
            return sql;
        }
        public override string RemoveColumnDefaultSql(string tableName, string colName)
        {
            var column = getColumn(tableName, colName);

            if (column == null) throw new Exception($"没有在表 {tableName} 中找到列 {colName}!");
            var sql = $@"-- alter table tbl_name alter column col_name set default '123'
alter table tbl_name alter column col_name drop default
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/alter-table.html#alter-table-redefine-column

alter table {db.QuotedName(tableName)} alter column {db.QuotedName(colName)} drop default
";
            return sql;
        }

        public override string DropColumnSql(string tableName, string columnName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            Ensure.NotNullOrEmptyOrWhiteSpace(columnName);
            return $"alter table {db.QuotedName(tableName)} drop {db.QuotedName(columnName)}";
        }

        public override string UpdateConstaintSql()
        {
            var sql = @"-- 关于约束: 可以认为mysql中的约束有六种(主要是类比sqlserver), 但mysql的元数据中仅记录了四种约束: 主键约束,唯一约束,外键约束,检查约束
--         另外两个约束""默认约束""""非空约束""是作为列的属性存在的.
-- 1. 检查约束
-- mysql 8.0.16 及以上开始支持检查约束
-- 语法: check (expr) 或 [constraint [symbol]] check (expr) [[not] enforced]
-- 解释enforced: 表示约束是否是强制性的,默认是强制性,如果写成 '... not enforced' 则是非强制性,那么即使检查失败也不会报错(一般是否强制性)
-- 检查约束分为表检查约束和列检查约束(使用: select * from information_schema.check_constraints 查看元数据)
-- 示例: create table
-- create table t1
-- (
--   check (c1 <> c2), -- 表检查约束
--   c1 int check (c1 > 10), -- 列检查约束,只能引用这一列
--   c2 int constraint c2_positive check (c2 > 0),
--   c3 int check (c3 < 100),
--   constraint c1_nonzero check (c1 <> 0),
--   check (c1 > c3)
-- );

-- 示例: alter table
-- alter table t1 add constraint t1_other_check check(c2<>c3)

-- 示例: 删除约束
-- alter table t1 drop constraint t1_other_check
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/create-table-check-constraints.html

-- 2. 外键约束
-- 语法:
-- [constraint [symbol]] foreign key
--     [index_name] (col_name, ...)
--     references tbl_name (col_name,...)
--     [on delete reference_option]
--     [on update reference_option]
-- 
-- reference_option:
--     restrict | cascade | set null | no action | set default
-- 示例: create table
-- create table product (
-- 	id int not null,
--     categoryid int not null,
--     index(id),
--     primary key(categoryid, id)
-- );
-- 
-- create table product_order (
-- 	id int primary key,
--     product_category int not null,
--     product_id int not null,
-- 
--     foreign key (product_category, product_id)
--       references product(categoryid, id)
--       on update cascade on delete restrict,
-- 
--    constraint myconstraint foreign key (product_id)
--       references product(id)
-- );

-- 示例: alter table
-- alter table product_order add constraint foreign key(product_id) references product(id)
--     on update cascade
--     on delete restrict

-- 示例: 删除约束
-- alter table product_order drop foreign key myconstraint;
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/create-table-foreign-keys.html

-- 3. 主键约束和唯一约束
-- 示例: create table
-- 下面创建后将会新建两个约束和两个索引 (索引: id,name 约束: primary,name),也就是说: 索引和约束是成对出现的(名字相同)
-- create table t3(
-- 	id int primary key,
-- 	name varchar(50) unique
-- )
-- 通过下面sql查询已创建的约束和索引
-- select * from information_schema.table_constraints tc where tc.table_schema ='test' and table_name ='t3'
-- show index from t3

-- 示例: 删除主键约束
-- alter table t3 drop constraint `primary` -- 删除主键约束,对应的主键索引也自动消失
-- -- alter table t3 drop index id -- 报错: can't drop 'id'; check that column/key exists, 不能直接删除主键索引!
-- alter table t3 drop constraint name -- 删除唯一约束或索引对应的索引或约束也一并消失
-- alter table t3 drop index name

-- 示例: alter table
-- alter table t3 add constraint `primary` primary key (id) -- 创建主键约束,自动加上索引
-- alter table t3 add constraint `name` unique(name) -- 创建唯一约束,自动加上索引
-- alter table t3 add constraint `testcols` unique(id,name) -- 两列组合成一个唯一约束
-- 思考: 从这里可以看出, 如何更改一个表的主键了吧...
-- 更多: https://dev.mysql.com/doc/refman/8.0/en/constraint-primary-key.html";
            return sql;
        }

        public override string UpdateIndexSql()
        {
            var sql = @"-- 语法:可能需要注意列长度对索引的影响
-- create [unique | fulltext | spatial] index index_name
--     [index_type]
--     on tbl_name (key_part,...)
--     [index_option]
--     [algorithm_option | lock_option] ...
-- 
-- key_part: {col_name [(length)] | (expr)} [asc | desc]
-- 
-- index_option: {
--     key_block_size [=] value
--   | index_type
--   | with parser parser_name
--   | comment 'string'
--   | {visible | invisible}
--   | engine_attribute [=] 'string'
--   | secondary_engine_attribute [=] 'string'
-- }
-- 
-- index_type:
--     using {btree | hash}
-- 
-- algorithm_option:
--     algorithm [=] {default | inplace | copy}
-- 
-- lock_option:
--     lock [=] {default | none | shared | exclusive}

-- 示例: create table
-- create table t4(
-- 	id int,
-- 	name varchar(8000) collate utf8mb4_general_ci, -- 测试列长对索引印象
-- 	name2 varchar(8000)  collate ascii_general_ci,
-- 	properties json,
-- 	index idx_name(name(200))
-- ) engine=innodb

-- 示例: alter table
-- alter table t4 add index idx_t4_name(name(768)) -- 在innodb的dynamic格式中,索引项占用字节最大3072byte,换算成utf8mb4也就是3072/4=768
-- -- alter table t4 add index idx_t4_name(name(769)) -- specified key was too long; max key length is 3072 bytes
-- alter table t4 add index idx_t4_name2(name2(3072)) -- 在innodb的dynamic格式中,索引项占用字节最大3072byte,换算成ascii也就是3072/1=3072
-- -- alter table t4 add index idx_t4_name2(name2(3073)) -- specified key was too long; max key length is 3072 bytes

-- 示例: 其他的索引
-- alter table t4 add index idx_t4_func((cast(concat(name,name2) as char(50)))) -- 使用cast是防止列定义的太长导致报错 
-- alter table t4 add index idx_t4_jsonprop((cast(properties->>'$.name' as char(50)))) -- 使用json列属性做索引

-- 示例: 删除索引
-- alter table t4 drop index idx_t4_func

-- 更多: https://dev.mysql.com/doc/refman/8.0/en/create-index.html";
            return sql;
        }

        public override string UpdateTriggerSql()
        {
            var sql = @"-- 语法:
-- create
--     [definer = user]
--     trigger [if not exists] trigger_name
--     trigger_time trigger_event
--     on tbl_name for each row
--     [trigger_order]
--     trigger_body
-- 
-- trigger_time: { before | after }
-- 
-- trigger_event: { insert | update | delete }
-- 
-- trigger_order: { follows | precedes } other_trigger_name

-- 解释trigger_order: 当在一个表上定义多个相同类型触发器的时候,他们的执行顺序. 默认follows表示按创建顺序执行,precedes表示当前新建的放前面执行

-- 示例:
-- create table student(
-- 	id int primary key,
-- 	name varchar(50)
-- );
-- create table student_msg(
-- 	id int,
-- 	name varchar(50),
-- 	msg varchar(50)
-- );
-- create trigger tri_student_before_insert before insert
-- on student for each row
-- begin
-- 	set new.name=concat(new.name,'_before_insert');
-- 	insert into student_msg values(new.id,new.name,'tri_student_before_insert');
-- end;
-- create trigger tri_student_after_update after update
-- on student for each row
-- begin
-- 	insert into student_msg values(new.id,new.name,'tri_student_after_update.new');
-- 	insert into student_msg values(old.id,old.name,'tri_student_after_update.old');
-- end;

-- 示例: 删除触发器
-- drop trigger tri_student_before_insert

-- 更多: https://dev.mysql.com/doc/refman/8.0/en/create-trigger.html";
            return sql;
        }

        public override string SetPrimaryKeySql(string tableName, params string[] colNames)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            if (colNames.IsNullOrEmpty()) colNames = new[] { "ColumnName" };
            var sql = $@"-- 设置表主键就是给表增加主键约束,mysql中约束名字一般为:`PRIMARY`
-- 注意: 需要先删除现有的主键约束: alter table {db.QuotedName(tableName)} drop constraint `PRIMARY`
alter table {db.QuotedName(tableName)} add constraint `PRIMARY` primary key({colNames.Select(i => db.QuotedName(i)).ToStringSeparated(",")})
";
            return sql;
        }

        public override string RemovePrimaryKeySql(string tableName)
        {
            Ensure.NotNullOrEmptyOrWhiteSpace(tableName);
            var sql = $@"-- 删除主键就是删除表上的主键约束(主键索引自动删除),mysql中约束名字一般为:`PRIMARY`
alter table {db.QuotedName(tableName)} drop constraint `PRIMARY`
";
            return sql;
        }

        public override string UpdateViewDescriptionSql(string viewName, string description = null)
            => $"-- mysql中的视图不支持注释";
        #endregion
    }
}