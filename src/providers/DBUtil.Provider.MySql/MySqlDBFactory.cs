﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql
{
    /// <summary>
    /// MySql数据库工厂类
    /// </summary>
    public class MySqlDBFactory : IDBFactory<MySqlAccess>
    {
        private static MySqlDBFactory _instance = new MySqlDBFactory();
        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBConn">连接字符串</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public static MySqlAccess CreateDB(string DBConn, DBSetting Settings)
        {
            if (string.IsNullOrWhiteSpace(DBConn)) return null;
            if (!DBConn.ToUpper().Contains("ALLOW USER VARIABLES") && !DBConn.ToUpper().Contains("ALLOWUSERVARIABLES"))
            {
                DBConn = DBConn.TrimEnd(';') + ";Allow User Variables=true;";
            }

            //if (!DBConn.ToUpper().Contains("ALLOW ZERO DATETIME") && !DBConn.ToUpper().Contains("ALLOWZERODATETIME"))
            //{
            //    DBConn = DBConn.TrimEnd(';') + ";Allow Zero DateTime=true;";
            //}
            if (Settings == null) Settings = DBSetting.NewInstance();
            if (Settings.Generator == null) Settings.SetIdSNOGenerator(new MySqlIdSNOGenerator());
            if (Settings.Locker == null) Settings.SetLocker(new MySqlLocker());
            return new MySqlAccess(DBConn, Settings);
        }

        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBType">数据库类型:MYSQL</param>
        /// <param name="DBConn">连接字符串:Data Source=localhost;Initial Catalog=test;User ID=root;Password=xxxx;</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public MySqlAccess CreateDB(string DBType, string DBConn, DBSetting Settings)
        {
            DBType = (DBType ?? "").ToUpper();
            switch (DBType)
            {
                case Consts.ServerType:
                    {
                        return CreateDB(DBConn, Settings);
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}

