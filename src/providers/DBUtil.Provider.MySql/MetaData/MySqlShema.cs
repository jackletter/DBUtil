﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql.MetaData
{
    /// <summary>
    /// Mysql的Schema,与database同义
    /// </summary>
    public class MySqlShema : Schema
    {
        /// <summary>
        /// 默认字符集
        /// </summary>
        public string DEFAULT_CHARACTER_SET_NAME { get; set; }

        /// <summary>
        /// 默认排序方式
        /// </summary>
        public string DEFAULT_COLLATION_NAME { get; set; }
    }
}
