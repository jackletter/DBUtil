﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql.MetaData
{
    /// <summary>
    /// MySql约束
    /// </summary>
    public class MySqlConstraint : Constraint
    {
        /// <summary>
        /// 约束内容
        /// </summary>
        public string Content { get; set; }
    }
}
