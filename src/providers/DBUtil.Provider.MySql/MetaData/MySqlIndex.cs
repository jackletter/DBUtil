﻿namespace DBUtil.Provider.MySql.MetaData
{
    /// <summary>
    /// MySql索引
    /// </summary>
    public class MySqlIndex : DBUtil.MetaData.Index
    {
        /// <summary>
        /// 取的字段前多少个字符
        /// </summary>
        public int? SubPart { get; set; }
        /// <summary>
        /// 索引的表达式, 如: alter table student add index idx_student_sub((substring(name,1,2)))
        /// </summary>
        public string Expression { get; set; }
    }
}
