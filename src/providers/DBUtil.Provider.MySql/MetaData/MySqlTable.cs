﻿using DBUtil.MetaData;
using DotNetCommon;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql.MetaData
{
    /// <summary>
    /// MySql表
    /// </summary>
    public class MySqlTable : Table
    {
        /// <summary>
        /// 使用的引擎,如:InnoDB或MyIsam
        /// </summary>
        public string Engine { get; set; }

        #region 占用空间
        /// <summary>
        /// 平均每行大小
        /// </summary>
        public long AVG_ROW_LENGTH { get; set; }

        /// <summary>
        /// 数据大小
        /// </summary>
        public long DATA_LENGTH { get; set; }

        /// <summary>
        /// 数据大小
        /// </summary>
        public string DataSize => UnitConverter.Humanize(DATA_LENGTH);

        /// <summary>
        /// 最大数据大小
        /// </summary>
        public long MAX_DATA_LENGTH { get; set; }

        /// <summary>
        /// 索引占用大小
        /// </summary>
        public long INDEX_LENGTH { get; set; }

        /// <summary>
        /// 索引占用的大小
        /// </summary>
        public string IndexSize => UnitConverter.Humanize(INDEX_LENGTH);

        /// <summary>
        /// 可用空间大小
        /// </summary>
        public long DATA_FREE { get; set; }

        /// <summary>
        /// 未使用的大小
        /// </summary>
        public string UnUsedSize => UnitConverter.Humanize(DATA_FREE);

        public string AllSize => UnitConverter.Humanize(INDEX_LENGTH + DATA_LENGTH);
        #endregion

        /// <summary>
        /// 当前自增量
        /// </summary>
        public long? AUTO_INCREMENT { get; set; }

        /// <summary>
        /// 当前自增量
        /// </summary>
        public long? Identity => AUTO_INCREMENT;

        /// <summary>
        /// 字符排序规则
        /// </summary>
        public string TABLE_COLLATION { get; set; }

        /// <summary>
        /// CREATE_OPTIONS
        /// </summary>
        public string CREATE_OPTIONS { get; set; }

        /// <summary>
        /// ROW_FORMAT
        /// </summary>
        public string ROW_FORMAT { get; set; }
    }
}
