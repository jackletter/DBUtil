﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql.MetaData
{
    /// <summary>
    /// MySql视图
    /// </summary>
    public class MySqlView : View
    {
        public string DEFINER { get; set; }
        public string SECURITY_TYPE { get; set; }
        public string VIEW_DEFINITION { get; set; }
        public string CHECK_OPTION { get; set; }
        public string CHARACTER_SET_CLIENT { get; set; }
        public string COLLATION_CONNECTION { get; set; }
    }
}
