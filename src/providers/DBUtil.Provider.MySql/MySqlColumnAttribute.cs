﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBUtil.Provider.MySql
{
    public class MySqlColumnAttribute : ColumnAttribute
    {
        public MySqlColumnAttribute() : base() { }
        public MySqlColumnAttribute(string name) : base(name) { }
    }
}
