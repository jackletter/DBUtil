﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql
{
    public enum EnumMySqlColumnType
    {
        /// <summary>
        /// 位类型
        /// </summary>
        Bit = 1,

        #region 数字
        /* 整形数字 */
        TinyInt = 2,
        TinyIntUnSigned = 3,
        SmallInt = 4,
        SmallIntUnSigned = 5,
        MediumInt = 6,
        MediumIntUnSigned = 7,
        Int = 8,
        IntUnSigned = 9,
        BigInt = 10,
        BigIntUnSigned = 11,

        /* 小数 */
        /// <summary>
        /// 浮点小数
        /// </summary>
        Float = 12,
        /// <summary>
        /// 双精度浮点小数
        /// </summary>
        Double = 13,
        /// <summary>
        /// 定点小数
        /// </summary>
        Decimal = 14,
        #endregion

        #region 字符串
        Char = 15,
        VarChar = 16,
        TinyText = 17,
        Text = 18,
        MediumText = 19,
        LongText = 20,
        #endregion

        #region 二进制
        Binary = 21,
        VarBinary = 22,
        TinyBlob = 23,
        Blob = 24,
        MediumBlob = 25,
        LongBlob = 26,
        #endregion        

        #region 日期时间
        Year = 27,
        Date = 28,
        Time = 29,
        DateTime = 30,
        TimeStamp = 31,
        #endregion

        #region 其他
        Enum = 32,
        Set = 33,
        Json = 34,
        #endregion

        #region Spatial
        Geometry = 35,
        Point = 36,
        LineString = 37,
        Polygon = 38,
        GeometryCollection = 39,
        MultiPoint = 40,
        MultiLineString = 41,
        MultiPolygon = 42
        #endregion
    }
}
