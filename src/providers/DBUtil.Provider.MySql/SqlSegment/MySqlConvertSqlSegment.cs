﻿using DBUtil.SqlSegment;

namespace DBUtil.Provider.MySql.SqlSegment
{
    /*
-- 字符串转二进制 "123abcefgh"(0x31 32 33 61 62 63 65 66 67 68)
select convert ('123abcefgh',binary)
-- 数字转字符串 "123.253"
select convert (123.253,char)
-- bool 转字符串 "1"
select convert (true,char)
-- 二进制转字符 "9"
select convert (b'00111001',char)
-- 日期转字符串 "2022-06-08 20:07:49"
select convert (now(),char)
-- 字符串转日期 "2022-06-08 20:07:49"
select convert ('2022-06-08 20:07:49',datetime)
-- 字符串转date "2022-06-08"
select convert ('2022-06-08 20:07:49',date)
-- 字符串转数字 "10"
select convert ('10.123456',decimal)
-- 字符串转数字 "10.123456"
select convert ('1234567890123.123456',decimal(30,15))
-- mysql 8.0.17 才有
-- select convert ('10.1234',float)
-- select convert ('10.1234',double)

-- 1234567890124.1234560000
select 1+convert ('1234567890123.123456',decimal(30,15))     
    */

    /// <summary>
    /// 数据类型转换
    /// </summary>
    public class MySqlConvertSqlSegment : ConvertSqlSegment
    {
        public MySqlConvertSqlSegment(DBAccess db) : base(db) { }
        private string ConvertTo(string objectName, string toType) => $"convert({objectName},{toType})";

        public override string ConvertDateTimeToString(string objectName) => ConvertTo(objectName, "char");
        public override string ConvertDecimalToString(string objectName) => ConvertTo(objectName, "char");
        public override string ConvertIntegerToString(string objectName) => ConvertTo(objectName, "char");
        public override string ConvertStringToDateTime(string objectName) => ConvertTo(objectName, "datetime(6)");
        public override string ConvertStringToDecimal(string objectName) => ConvertTo(objectName, "decimal(30,15)");
        public override string ConvertStringToInteger(string objectName) => ConvertTo(objectName, "decimal");
        public override string ConvertAllToString(string objectName) => ConvertTo(objectName, "char");
    }
}
