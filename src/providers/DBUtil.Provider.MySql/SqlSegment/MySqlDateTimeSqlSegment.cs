﻿using DBUtil.SqlSegment;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.MySql.SqlSegment
{
    public class MySqlDateTimeSqlSegment : DateTimeSqlSegment
    {
        public MySqlDateTimeSqlSegment(DBAccess db) : base(db)
        {
        }

        public override string Current => "now()";
        public override string DefaultDateTimeType => "datetime";
        #region 当前时间加上 年/月/天/小时/分钟/秒
        public override string GetCurrentAddYear(int year) => $"date_add(now(),interval {year} year)";
        public override string GetCurrentAddMonth(int month) => $"date_add(now(),interval {month} month)";
        public override string GetCurrentAddDay(int day) => $"date_add(now(),interval {day} day)";
        public override string GetCurrentAddHour(int hour) => $"date_add(now(),interval {hour} hour)";
        public override string GetCurrentAddMinute(int minute) => $"date_add(now(),interval {minute} minute)";
        public override string GetCurrentAddSecond(int second) => $"date_add(now(),interval {second} second)";
        #endregion
    }
}
