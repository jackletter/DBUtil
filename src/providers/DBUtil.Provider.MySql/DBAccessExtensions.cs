﻿using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using System.Linq;

namespace DBUtil.Provider.MySql
{
    /// <summary>
    /// DBAccess的扩展方法
    /// </summary>
    public static class DBAccessExtensions
    {
        #region IsMySql IsMySql()
        /// <summary>
        /// 是否是MySql数据库
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool IsMySql(this DBAccess db)
        {
            return db.DBType == DBType.MYSQL;
        }
        #endregion

        #region MySql版本判断
        /*
        可能版本字符串列表
        5.7.25-log
        5.7.32-log
        8.0.16
        8.0.25        
         */

        private static (int first, int second, int third) ParseVersion(string version)
        {
            var arr = version.IfNullUse("").SplitAndTrimTo<string>(".");
            if (arr.Count < 3)
            {
                return (0, 0, 0);
            }
            arr = arr.Take(3).ToList();
            if (int.TryParse(arr[0], out int first))
            {
                if (int.TryParse(arr[1], out int second))
                {
                    var ints = "";
                    foreach (var i in arr[2])
                    {
                        if ((int)i >= (int)'0' && (int)i <= (int)'9')
                        {
                            ints += i;
                            continue;
                        }
                        break;
                    }
                    if (int.TryParse(ints, out int third))
                    {
                        return (first, second, third);
                    }
                }
            }
            return (0, 0, 0);
        }

        /// <summary>
        /// 是否兼容MySql8
        /// </summary>
        /// <returns></returns>
        public static bool IsMySql8Compatible(this DBAccess db)
        {
            var (first, second, third) = ParseVersion(db.DBVersion);
            return first >= 8;
        }

        /// <summary>
        /// 是否兼容MySql5.7
        /// </summary>
        /// <returns></returns>
        public static bool IsMySql5_7Compatible(this DBAccess db)
        {
            var (first, second, third) = ParseVersion(db.DBVersion);
            return first > 5 || (first == 5 && second >= 7);
        }

        /// <summary>
        /// 是否兼容MySql5.6
        /// </summary>
        /// <returns></returns>
        public static bool IsMySql5_6Compatible(this DBAccess db)
        {
            var (first, second, third) = ParseVersion(db.DBVersion);
            return first > 5 || (first == 5 && second >= 6);
        }

        /// <summary>
        /// 是否兼容指定版本的MySql
        /// </summary>
        /// <param name="db"></param>
        /// <param name="versionNum">版本号（如: 8.0.25、5.7.26、5.7）</param>
        /// <returns></returns>
        public static bool IsMySqlCompatible(this DBAccess db, string versionNum)
        {
            var (first1, second1, third1) = ParseVersion(versionNum);
            var (first2, second2, third2) = ParseVersion(db.DBVersion);
            if (first2 < first1) return false;
            if (first1 == first2 && second2 < second1) return false;
            if (first1 == first2 && second2 == second1 && third2 < third1) return false;
            return true;
        }

        #endregion
    }
}
