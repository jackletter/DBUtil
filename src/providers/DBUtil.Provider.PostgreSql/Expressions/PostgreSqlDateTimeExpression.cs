﻿using System;
using System.Collections.Generic;
using System.Text;
using DBUtil.Expressions;

namespace DBUtil.Provider.PostgreSql.Expressions
{
    /// <summary>
    /// PostgresSql中的日期时间运算符
    /// </summary>
    public class PostgreSqlDateTimeExpression : DateTimeExpression
    {
        /// <summary>
        /// 默认实例
        /// </summary>
        public static PostgreSqlDateTimeExpression Default = new PostgreSqlDateTimeExpression();

        /// <summary>
        /// 默认的日期时间类型
        /// </summary>
        public override string DefaultDateTimeType => "timestamp";

        /// <summary>
        /// 获取当前时间
        /// </summary>
        public override string Current => "now()";

        /// <summary>
        /// 当前时间加上指定的天数
        /// </summary>
        /// <param name="day">加上的天数</param>
        public override string GetCurrentAddDay(long day) => $"now()::timestamp + '{day} day'";

        /// <summary>
        /// 当前时间加上指定的小时数
        /// </summary>
        /// <param name="hour">加上的小时数</param>
        public override string GetCurrentAddHour(long hour) => $"now()::timestamp + '{hour} hour'";

        /// <summary>
        /// 当前时间加上指定的分钟数
        /// </summary>
        /// <param name="minute">加上的分钟数</param>
        /// <returns></returns>
        public override string GetCurrentAddMinute(long minute) => $"now()::timestamp + '{minute} minute'";

        /// <summary>
        /// 当前时间加上指定的秒数
        /// </summary>
        /// <param name="second">加上的秒数</param>
        /// <returns></returns>
        public override string GetCurrentAddSecond(long second) => $"now()::timestamp + '{second} second'";

        public override string GetFilterString(string colName, DateTime? start, DateTime? end, bool includeStart = true, bool includeEnd = false)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取时间的字符串表示
        /// </summary>
        /// <param name="datetime">指定的时间</param>
        /// <returns></returns>
        public override string GetStringInSql(DateTime datetime) => $"'{datetime.ToString("yyyy-MM-dd HH:mm:ss")}'";
    }
}
