﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DBUtil.PostgreSql.MetaData;
using DBUtil.MetaData;
using System.Linq;
using DBUtil.Generators;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;
using Npgsql;
using DBUtil.Expressions;
using DotNetCommon;
using DBUtil.Util;

namespace DBUtil.Provider.PostgreSql
{
    /// <summary>
    /// PostgreSql操作对象
    /// </summary>
    public partial class PostgreSqlAccess : DBAccess
    {
        public override DBManage Manage => throw new NotImplementedException();
        #region 创建IDbCommand
        /// <summary>
        /// 创建IDbCommand
        /// </summary>
        /// <returns></returns>
        protected override IDbCommand CreateCommand()
        {
            return new NpgsqlCommand();
        }
        #endregion

        #region 创建IDataAdapter(根据命令对象)
        /// <summary>
        /// 创建IDataAdapter
        /// </summary>
        /// <param name="cmd">命令对象</param>
        /// <returns></returns>
        protected override IDataAdapter CreateAdapter(IDbCommand cmd)
        {
            return new NpgsqlDataAdapter(cmd as NpgsqlCommand);
        }
        #endregion

        #region 创建参数 CreatePara
        /// <summary>
        /// 创建参数
        /// </summary>
        /// <returns></returns>
        public override IDataParameter CreatePara()
        {
            return new NpgsqlParameter();
        }
        #endregion

        #region 初始化
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="DBConn">连接字符串</param>
        /// <param name="Settings">设置</param>
        public PostgreSqlAccess(string DBConn, DBSetting Settings)
        {
            this.Settings = Settings;
            this.DBConn = DBConn;
            this.Conn = new NpgsqlConnection(DBConn);
            InitServerInfo();
        }

        private void InitServerInfo()
        {
            this.ParaPrefix = ":";
            this.DBType = "POSTGRESQL";
        }
        #endregion

        #region GetSqlForPageSize
        /// <summary>获得分页的查询语句
        /// </summary>
        /// <param name="selectSql">查询sql如:select id,name from person where age>10</param>
        /// <param name="strOrder">排序字句如:order by id</param>
        /// <param name="PageSize">页面大小,如:10</param>
        /// <param name="PageIndex">页面索引从1开始,如:1</param>
        /// <returns>返回示例:select id,name from person where age>10 order by id limit 10 offset 0</returns>
        public override string GetSqlForPageSize(string selectSql, string strOrder, int PageSize, int PageIndex)
        {
            string sql = string.Format("{0} {1} limit {2} offset{3}", selectSql, strOrder, PageSize, (PageIndex - 1) * PageSize);
            return sql;
        }
        #endregion

        #region Judge系列: 判断表/视图/列/存储过程是否存在

        /// <summary>
        /// 判断存储过程是否存在
        /// </summary>
        /// <param name="procName">存储过程名称</param>
        /// <returns></returns>
        public override bool JudgeProcedureExist(string procName)
        {
            var sql = $@"
select
count(1)
from information_schema.routines
where routine_catalog = '{DataBase}'
and routine_type = 'PROCEDURE'";
            int r = SelectScalar<int>(sql);
            return r > 0;
        }
        #endregion

        #region 基于数据库的Id和流水号生成器

        public override long NewId(string tableName, string colName)
        {
            throw new NotImplementedException();
        }

        public override List<long> NewIds(string tableName, string colName, int count)
        {
            throw new NotImplementedException();
        }

        public override void ResetId(string tableName, string colName)
        {
            throw new NotImplementedException();
        }

        public override string NewSNO(string tableName, string colName, SerialFormat format)
        {
            throw new NotImplementedException();
        }

        public override List<string> NewSNOs(string tableName, string colName, SerialFormat format, int count)
        {
            throw new NotImplementedException();
        }

        public override void ResetSNO(string tableName, string colName)
        {
            throw new NotImplementedException();
        }

        public override bool JudgeTriggerExist(string triggerName)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region 运算符
        /// <summary>
        /// 数据库日期时间运算符
        /// </summary>
        public override DateTimeExpression Expression_DateTime { get; }
        #endregion

        #region 获取刚插入的自增id的Sql语句
        /// <summary>
        /// 获取刚插入的自增id的Sql语句
        /// </summary>
        /// <param name="sequenceName">序列名称</param>
        /// <returns></returns>
        public override string GetInsertedId(string sequenceName = null)
        {
            return $"select last_value from {SqlUtil.DealInjectSql(sequenceName)};";
        }
        #endregion
    }
}
