﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.PostgreSql
{
    /// <summary>
    /// PostgreSql数据库工厂类
    /// </summary>
    public class PostgreSqlDBFactory : IDBFactory<PostgreSqlAccess>
    {
        private static PostgreSqlDBFactory _instance = new PostgreSqlDBFactory();
        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBConn">连接字符串</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public static PostgreSqlAccess CreateIDB(string DBConn, DBSetting Settings)
        {
            return new PostgreSqlAccess(DBConn, Settings);
        }

        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBType">数据库类型:POSTGRESQL</param>
        /// <param name="DBConn">连接字符串:Server=localhost;Port=5432;UserId=postgres;Password=123456;Database=test;</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public PostgreSqlAccess CreateDB(string DBType, string DBConn, DBSetting Settings)
        {
            DBType = (DBType ?? "").ToUpper();
            switch (DBType)
            {
                case "POSTGRESQL":
                    {
                        return CreateIDB(DBConn, Settings);
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}
