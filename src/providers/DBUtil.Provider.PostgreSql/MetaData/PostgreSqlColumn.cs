﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DBUtil.PostgreSql.MetaData
{
    /// <summary>
    /// PostgreSql列
    /// </summary>
    public class PostgreSqlColumn : Column
    {
        /// <summary>
        /// 列长
        /// </summary>
        public int Length { get; set; }
    }
}
