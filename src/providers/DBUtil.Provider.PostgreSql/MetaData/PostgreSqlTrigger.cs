﻿using DBUtil.MetaData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.PostgreSql.MetaData
{
    /// <summary>
    /// PostgreSql触发器
    /// </summary>
    public class PostgreSqlTrigger : Trigger
    {
        /// <summary>
        /// 触发器执行的函数
        /// </summary>
        public string ActionStatement{ get; set; }
    }
}
