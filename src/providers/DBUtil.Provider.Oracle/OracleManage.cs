﻿using DBUtil.MetaData;
using DotNetCommon.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Index = DBUtil.MetaData.Index;

namespace DBUtil.Provider.Oracle
{
    public class OracleManage : DBManage
    {
        /// <summary>
        /// 根据DBAccess创建DBManage
        /// </summary>
        /// <param name="db"></param>
        public OracleManage(DBAccess db) : base(db) { }

        public override Result AddColumn(string tableName, Column column)
        {
            throw new NotImplementedException();
        }

        public override Result CreateTable(Table table)
        {
            throw new NotImplementedException();
        }

        public override Result DropColumn(string tableName, string columnName)
        {
            throw new NotImplementedException();
        }

        public override Result DropProcedureIfExist(string triggerName)
        {
            throw new NotImplementedException();
        }

        public override Result DropTableIfExist(string tableName)
        {
            throw new NotImplementedException();
        }

        public override Result DropTriggerIfExist(string triggerName)
        {
            throw new NotImplementedException();
        }

        public override string GenerateCode(string tableName, string shemaName)
        {
            throw new NotImplementedException();
        }

        public override string GenerateCreateTableSql(string tableName, string shemaName)
        {
            throw new NotImplementedException();
        }

        public override void GenerateCreateTableSqlFile(List<Table> tables, string fileAbsPath, bool includeInsertSql = false)
        {
            throw new NotImplementedException();
        }

        public override (long count, string sql, long total) GenerateInsertSql(string schemaName, string tableName, int limitCount = 0, string filter = null, params IDataParameter[] paras)
        {
            throw new NotImplementedException();
        }

        public override long GenerateInsertSqlFile(string schemaName, string tableName, string fileAbsPath, int limitCount = 0, string filter = null, params IDataParameter[] paras)
        {
            throw new NotImplementedException();
        }

        public override long GetCount(string tableName, string schemaName)
        {
            throw new NotImplementedException();
        }

        public override Result RenameColumn(string tableName, string oldName, string newName)
        {
            throw new NotImplementedException();
        }

        public override Result RenameTable(string oldName, string newName)
        {
            throw new NotImplementedException();
        }

        public override List<Function> ShowFunctions()
        {
            throw new NotImplementedException();
        }

        public override List<Procedure> ShowProcedures()
        {
            throw new NotImplementedException();
        }

        public override List<Sequence> ShowSequences()
        {
            throw new NotImplementedException();
        }

        public override List<Column> ShowTableColumns(string tableName, string schemaName)
        {
            throw new NotImplementedException();
        }

        public override List<DBUtil.MetaData.Constraint> ShowTableConstraints(string tableName, string schemaName)
        {
            throw new NotImplementedException();
        }

        public override Table ShowTableDetail(string tableName, string schemaName)
        {
            throw new NotImplementedException();
        }

        public override List<Index> ShowTableIndexes(string tableName, string schemaName)
        {
            throw new NotImplementedException();
        }

        public override List<Table> ShowTables(bool isDetail = false)
        {
            throw new NotImplementedException();
        }

        public override List<Trigger> ShowTableTriggers(string tableName, string schemaName)
        {
            throw new NotImplementedException();
        }

        public override View ShowViewDetail(string viewName, string schemaName)
        {
            throw new NotImplementedException();
        }

        public override List<View> ShowViews()
        {
            throw new NotImplementedException();
        }

        public override (string tableName, string schemaName, string dbName) SplitTableName(string tableName)
        {
            throw new NotImplementedException();
        }

        public override void TruncateTable(string tableName, string shemaName)
        {
            throw new NotImplementedException();
        }

        public override Result UpdateColumnDescription(string tableName, string colName, string desc)
        {
            throw new NotImplementedException();
        }

        public override Result UpdateTableDescription(string tableName, string desc)
        {
            throw new NotImplementedException();
        }

        public override Result UpdateViewDescription(string tableName, string desc)
        {
            throw new NotImplementedException();
        }
    }
}
