﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.Provider.Oracle
{
    /// <summary>
    /// Oracle数据库工厂类
    /// </summary>
    public class OracleDBFactory : IDBFactory<OracleAccess>
    {
        private static OracleDBFactory _instance = new OracleDBFactory();
        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBConn">连接字符串</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public static OracleAccess CreateDB(string DBConn,DBSetting Settings)
        {
            return new OracleAccess(DBConn,Settings);
        }

        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="DBType">数据库类型:Oracle</param>
        /// <param name="DBConn">连接字符串:Data Source=localhost/ORCL;Password=123456;User ID=sys;DBA Privilege=SYSDBA;</param>
        /// <param name="Settings">设置</param>
        /// <returns></returns>
        public OracleAccess CreateDB(string DBType, string DBConn, DBSetting Settings)
        {
            DBType = (DBType ?? "").ToUpper();
            switch (DBType)
            {
                case "ORACLE":
                    {
                        return CreateDB(DBConn,Settings);
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}
