﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DBUtil.Provider.Oracle.MetaData;
using DBUtil.MetaData;
using System.Linq;
using DBUtil.Generators;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
using DBUtil.Expressions;
using DotNetCommon;
using DBUtil.Util;

namespace DBUtil.Provider.Oracle
{
    /// <summary>
    /// Microsoft SQL Server操作对象
    /// </summary>
    public class OracleAccess : DBAccess
    {
        #region 初始化
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="DBConn">连接字符串</param>
        /// <param name="Settings">设置</param>
        public OracleAccess(string DBConn, DBSetting Settings)
        {
            this.Settings = Settings;
            this.DBConn = DBConn;
            this.Conn = new OracleConnection(DBConn);
            InitServerInfo();
        }
        #endregion

        public override DBManage Manage => throw new NotImplementedException();

        public override IDataParameter CreatePara()
        {
            throw new NotImplementedException();
        }

        public override string GetSqlForPageSize(string selectSql, string strOrder, int PageSize, int PageIndex)
        {
            throw new NotImplementedException();
        }

        public override bool JudgeProcedureExist(string procName)
        {
            throw new NotImplementedException();
        }

        public override bool JudgeTriggerExist(string triggerName)
        {
            throw new NotImplementedException();
        }

        public override long NewId(string tableName, string colName)
        {
            throw new NotImplementedException();
        }

        public override List<long> NewIds(string tableName, string colName, int count)
        {
            throw new NotImplementedException();
        }

        public override string NewSNO(string tableName, string colName, SerialFormat format)
        {
            throw new NotImplementedException();
        }

        public override List<string> NewSNOs(string tableName, string colName, SerialFormat format, int count)
        {
            throw new NotImplementedException();
        }

        public override void ResetId(string tableName, string colName)
        {
            throw new NotImplementedException();
        }

        public override void ResetSNO(string tableName, string colName)
        {
            throw new NotImplementedException();
        }

        protected override IDataAdapter CreateAdapter(IDbCommand cmd)
        {
            throw new NotImplementedException();
        }

        protected override IDbCommand CreateCommand()
        {
            throw new NotImplementedException();
        }

        private void InitServerInfo()
        {
            this.ParaPrefix = ":";
            this.DBType = "ORACLE";
        }

        #region 获取刚插入的自增id的Sql语句
        /// <summary>
        /// 获取刚插入的自增id的Sql语句
        /// </summary>
        /// <param name="sequenceName">序列名称</param>
        /// <returns></returns>
        public override string GetInsertedId(string sequenceName = null)
        {
            return $"select {SqlUtil.DealInjectSql(sequenceName)}.currval from dual;";
        }
        #endregion
    }
}
