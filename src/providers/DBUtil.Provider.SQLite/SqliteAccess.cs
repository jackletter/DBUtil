﻿using DBUtil.SqlSegment;
using DotNetCommon;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DBUtil.Provider.SQLite
{
    /// <summary>
    /// Sqlite操作对象
    /// </summary>
    public partial class SqliteAccess : DBAccess
    {
        public override DBManage Manage => throw new NotImplementedException();

        public override DbParameter CreatePara()
        {
            throw new NotImplementedException();
        }

        public override string GetLastInsertedId()
        {
            return $"select last_insert_rowid();";
        }

        public override string GetSqlForPageSize(string selectSql, string strOrder, int PageSize, int PageIndex)
        {
            throw new NotImplementedException();
        }

        public override bool IsProcedureExist(string procName, string schemaName = null)
        {
            throw new NotImplementedException();
        }

        public override Task<bool> IsProcedureExistAsync(string procName, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override bool IsSupportSequence()
        {
            throw new NotImplementedException();
        }

        public override bool IsTriggerExist(string triggerName, string schemaName = null)
        {
            throw new NotImplementedException();
        }

        public override Task<bool> IsTriggerExistAsync(string triggerName, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override string LastInsertedId()
        {
            throw new NotImplementedException();
        }

        public override string NextSequenceValue(string name)
        {
            throw new NotImplementedException();
        }

        protected override DataAdapter CreateAdapter(DbCommand cmd)
        {
            throw new NotImplementedException();
        }

        protected override DbCommand CreateCommand()
        {
            throw new NotImplementedException();
        }

        protected override DateTimeSqlSegment GetDateTimeSqlSegment()
        {
            throw new NotImplementedException();
        }

        protected override StringSqlSegment GetStringSqlSegment()
        {
            throw new NotImplementedException();
        }

        protected override bool InitDBCacheGenerator()
        {
            throw new NotImplementedException();
        }

        protected override long InternalNewId(string tableName, string colName)
        {
            throw new NotImplementedException();
        }

        protected override Task<long> InternalNewIdAsync(string tableName, string colName, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override List<long> InternalNewIds(string tableName, string colName, int count)
        {
            throw new NotImplementedException();
        }

        protected override Task<List<long>> InternalNewIdsAsync(string tableName, string colName, int count, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override string InternalNewSNO(string tableName, string colName, SerialFormat format)
        {
            throw new NotImplementedException();
        }

        protected override Task<string> InternalNewSNOAsync(string tableName, string colName, SerialFormat format, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override List<string> InternalNewSNOs(string tableName, string colName, SerialFormat format, int count)
        {
            throw new NotImplementedException();
        }

        protected override Task<List<string>> InternalNewSNOsAsync(string tableName, string colName, SerialFormat format, int count, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}
