﻿using DotNetCommon;
using System.Threading;
using System.Threading.Tasks;

namespace DBUtil
{
    public interface IIdSNOGenerator
    {
        long NewId(DBAccess db, string tableName, string colName);
        long[] NewIds(DBAccess db, string tableName, string colName, int count);

        Task<long> NewIdAsync(DBAccess db, string tableName, string colName, CancellationToken cancellationToken = default);
        Task<long[]> NewIdsAsync(DBAccess db, string tableName, string colName, int count, CancellationToken cancellationToken = default);

        string NewSNO(DBAccess db, string tableName, string colName, SerialFormat serialFormat);
        string[] NewSNOs(DBAccess db, string tableName, string colName, SerialFormat serialFormat, int count);

        Task<string> NewSNOAsync(DBAccess db, string tableName, string colName, SerialFormat serialFormat, CancellationToken cancellationToken = default);
        Task<string[]> NewSNOsAsync(DBAccess db, string tableName, string colName, SerialFormat serialFormat, int count, CancellationToken cancellationToken = default);
    }
}