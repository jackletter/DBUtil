﻿namespace DBUtil
{
    /// <summary>
    /// 创建数据库访问对象的工厂接口
    /// </summary>
    /// <typeparam name="T">要创建的数据库访问对象类型</typeparam>
    public interface IDBFactory<out T> where T : DBAccess
    {
        /// <summary>
        /// 创建数据库访问对象
        /// </summary>
        /// <param name="DBType">数据库类型</param>
        /// <param name="DBConn">链接字符串</param>
        /// <param name="Settings">使用的设置</param>
        /// <returns></returns>
        T CreateDB(string DBType, string DBConn, DBSetting Settings);
    }
}
