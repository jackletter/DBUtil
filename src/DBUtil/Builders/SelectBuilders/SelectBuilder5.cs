//auto-generated
using DotNetCommon;
using DotNetCommon.Data;
using DotNetCommon.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DBUtil.Builders
{
    public class SelectBuilder<T, T2, T3, T4, T5> : SelectBuilderBase
        where T : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
    {
        internal SelectBuilder(SelectBuilder<T, T2, T3, T4> selectBuilder, (Expression<Func<T, T2, T3, T4, T5, bool>> expression, EnumJoinType type) join, string alias) : base(selectBuilder.db)
        {
            FromJoins = (selectBuilder.FromJoins ?? []).AddFluent(new FromJoin { Expression = join.expression, Type = join.type });
            EntityAliases = selectBuilder.EntityAliases.ToList().AddFluent(new EntityAlias { EntityInfo = db.GetEntityInfoInternal<T5>(), Alias = alias });
        }

        #region AsTable
        public override SelectBuilder<T, T2, T3, T4, T5> AsTable(Func<string, string> func) => base.AsTable(func) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> AsTableIf(bool condition, Func<string, string> func) => base.AsTableIf(condition, func) as SelectBuilder<T, T2, T3, T4, T5>;
        #endregion

        #region 复写 CommandTimeout
        public override SelectBuilder<T, T2, T3, T4, T5> CommandTimeout(int timeoutSeconds)
            => base.CommandTimeout(timeoutSeconds) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> CommandTimeoutIf(bool condition, int timeoutSeconds) => base.CommandTimeoutIf(condition, timeoutSeconds) as SelectBuilder<T, T2, T3, T4, T5>;
        #endregion

        #region Alias
        public new SelectBuilder<T, T2, T3, T4, T5> Alias(string alias) => base.Alias(alias) as SelectBuilder<T, T2, T3, T4, T5>;
        #endregion

        #region 过滤
        public SelectBuilder<T, T2, T3, T4, T5> Where(Expression<Func<T, T2, T3, T4, T5, bool>> expression) => base.Where(expression) as SelectBuilder<T, T2, T3, T4, T5>;
        public SelectBuilder<T, T2, T3, T4, T5> WhereIf(bool condition, Expression<Func<T, T2, T3, T4, T5, bool>> expression) => base.WhereIf(condition, expression) as SelectBuilder<T, T2, T3, T4, T5>;
        public SelectBuilder<T, T2, T3, T4, T5> Where(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, bool>> expression)
        {
            Ensure.NotNull(expression, nameof(expression));
            return base.Where(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)])) as SelectBuilder<T, T2, T3, T4, T5>;
        }
        public SelectBuilder<T, T2, T3, T4, T5> WhereIf(bool condition, Expression<Func<IMultiTable<T, T2, T3, T4, T5>, bool>> expression) => condition ? Where(expression) : this;
        #endregion

        #region Distinct
        public new SelectBuilder<T, T2, T3, T4, T5> Distinct(bool isDistinct = true) => base.Distinct(isDistinct) as SelectBuilder<T, T2, T3, T4, T5>;
        #endregion

        #region 排序
        /// <summary>
        /// 排序,如: select.Order("age desc,id")
        /// </summary>
        public new SelectBuilder<T, T2, T3, T4, T5> Order(string orderSeg) => base.Order(orderSeg) as SelectBuilder<T, T2, T3, T4, T5>;

        public SelectBuilder<T, T2, T3, T4, T5> OrderBy(Expression<Func<T, T2, T3, T4, T5, object>> expression) => base.OrderBy(expression) as SelectBuilder<T, T2, T3, T4, T5>;
        public SelectBuilder<T, T2, T3, T4, T5> OrderBy(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, object>> expression) => base.OrderBy(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)])) as SelectBuilder<T, T2, T3, T4, T5>;
        public SelectBuilder<T, T2, T3, T4, T5> OrderByIf(bool condition, Expression<Func<T, T2, T3, T4, T5, object>> expression) => condition ? OrderBy(expression) : this;
        public SelectBuilder<T, T2, T3, T4, T5> OrderByIf(bool condition, Expression<Func<IMultiTable<T, T2, T3, T4, T5>, object>> expression) => condition ? OrderBy(expression) : this;

        public SelectBuilder<T, T2, T3, T4, T5> OrderByDesc(Expression<Func<T, T2, T3, T4, T5, object>> expression) => base.OrderByDesc(expression) as SelectBuilder<T, T2, T3, T4, T5>;
        public SelectBuilder<T, T2, T3, T4, T5> OrderByDesc(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, object>> expression) => base.OrderByDesc(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)])) as SelectBuilder<T, T2, T3, T4, T5>;
        public SelectBuilder<T, T2, T3, T4, T5> OrderByDescIf(bool condition, Expression<Func<T, T2, T3, T4, T5, object>> expression) => condition ? OrderByDesc(expression) : this;
        public SelectBuilder<T, T2, T3, T4, T5> OrderByDescIf(bool condition, Expression<Func<IMultiTable<T, T2, T3, T4, T5>, object>> expression) => condition ? OrderByDesc(expression) : this;
        #endregion

        #region 分组
        public GroupBuilder<T, T2, T3, T4, T5, TGroupKey> GroupBy<TGroupKey>(Expression<Func<T, T2, T3, T4, T5, TGroupKey>> expression)
        {
            return new GroupBuilder<T, T2, T3, T4, T5, TGroupKey>(this, expression);
        }
        #endregion

        #region 分页
        public new SelectBuilder<T, T2, T3, T4, T5> Page(int pageIndex, int pageSize) => base.Page(pageIndex, pageSize) as SelectBuilder<T, T2, T3, T4, T5>;
        public new SelectBuilder<T, T2, T3, T4, T5> Limit(int size) => base.Limit(size) as SelectBuilder<T, T2, T3, T4, T5>;
        public new SelectBuilder<T, T2, T3, T4, T5> Limit(int startIndex, int size) => base.Limit(startIndex, size) as SelectBuilder<T, T2, T3, T4, T5>;
        #endregion

        #region ToSql
        /// <summary>
        /// 专供 builder 解析使用, 提供外部已有 midValues
        /// </summary>
        internal string ToSqlFirstOrDefaultInternal(LambdaExpression expression, Dictionary<ParameterExpression, object> midValues, IEnumerable<KeyValuePair<ParameterExpression, string>> aliases)
        {
            this.Limit(1);
            return base.ToSql(EnumSelectToSql.ToList, expression, midValues, aliases);
        }
        /// <summary>
        /// 专供 builder 解析使用, 提供外部已有 midValues
        /// </summary>
        internal string ToSqlInternal(EnumSelectToSql enumSelectToSql, LambdaExpression expression, Dictionary<ParameterExpression, object> midValues, IEnumerable<KeyValuePair<ParameterExpression, string>> aliases)
        {
            return base.ToSql(enumSelectToSql, expression, midValues, aliases);
        }

        public string ToSqlFirstOrDefault<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToSqlFirstOrDefault(expression);
        public string ToSqlFirstOrDefault<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToSqlFirstOrDefault(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string ToSql() => base.ToSqlList(null);
        public string ToSqlList<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToSqlList(expression);
        public string ToSqlList<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToSqlList(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string ToSqlPage<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToSqlPage(expression);
        public string ToSqlPage<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToSqlPage(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string ToSqlPage<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression, int pageIndex, int pageSize) => Page(pageIndex, pageSize).ToSqlPage(expression);
        public string ToSqlPage<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression, int pageIndex, int pageSize) => Page(pageIndex, pageSize).ToSqlPage(expression);

        public string ToSqlMax<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToSql(EnumSelectToSql.Max, expression);
        public string ToSqlMax<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToSql(EnumSelectToSql.Max, BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string ToSqlMin<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToSql(EnumSelectToSql.Min, expression);
        public string ToSqlMin<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToSql(EnumSelectToSql.Min, BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string ToSqlAvg<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToSql(EnumSelectToSql.Avg, expression);
        public string ToSqlAvg<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToSql(EnumSelectToSql.Avg, BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string ToSqlSum<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToSql(EnumSelectToSql.Sum, expression);
        public string ToSqlSum<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToSql(EnumSelectToSql.Sum, BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        #endregion

        #region 聚合 & 聚合sql
        #region 聚合Sql
        public string MaxSql<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.MaxSql<TKey>(expression);
        public string MaxSql<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.MaxSql<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string MinSql<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.MinSql<TKey>(expression);
        public string MinSql<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.MinSql<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string SumSql<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.SumSql<TKey>(expression);
        public string SumSql<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.SumSql<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public string AvgSql<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.AvgSql<TKey>(expression);
        public string AvgSql<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.AvgSql<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        #endregion

        #region 聚合同步
        public TKey Max<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.Max<TKey>(expression);
        public TKey Max<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.Max<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public TKey Min<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.Min<TKey>(expression);
        public TKey Min<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.Min<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public TKey Sum<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.Sum<TKey>(expression);
        public TKey Sum<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.Sum<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public TKey Avg<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => base.Avg<TKey>(expression);
        public TKey Avg<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => base.Avg<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        #endregion

        #region 聚合异步
        public async Task<TKey> MaxAsync<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => await base.MaxAsync<TKey>(expression);
        public async Task<TKey> MaxAsync<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => await base.MaxAsync<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public async Task<TKey> MinAsync<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => await base.MinAsync<TKey>(expression);
        public async Task<TKey> MinAsync<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => await base.MinAsync<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public async Task<TKey> SumAsync<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => await base.SumAsync<TKey>(expression);
        public async Task<TKey> SumAsync<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => await base.SumAsync<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public async Task<TKey> AvgAsync<TKey>(Expression<Func<T, T2, T3, T4, T5, TKey>> expression) => await base.AvgAsync<TKey>(expression);
        public async Task<TKey> AvgAsync<TKey>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TKey>> expression) => await base.AvgAsync<TKey>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        #endregion
        #endregion

        #region 执行
        #region 同步
        //FirstOrDefault
        public Dto FirstOrDefault<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.FirstOrDefault<Dto>(expression);
        public Dto FirstOrDefault<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.FirstOrDefault<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));

        //ToList
        public List<Dto> ToList<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToList<Dto>(expression);
        public List<Dto> ToList<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToList<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));

        //ToPage
        public Page<Dto> ToPage<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => base.ToPage<Dto>(expression);
        public Page<Dto> ToPage<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => base.ToPage<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public Page<Dto> ToPage<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression, int pageIndex, int pageSize) => base.ToPage<Dto>(expression, pageIndex, pageSize);
        public Page<Dto> ToPage<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression, int pageIndex, int pageSize) => base.ToPage<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]), pageIndex, pageSize);
        #endregion        

        #region 异步
        //FirstOrDefaultAsync
        public async Task<Dto> FirstOrDefaultAsync<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => await base.FirstOrDefaultAsync<Dto>(expression);
        public async Task<Dto> FirstOrDefaultAsync<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => await base.FirstOrDefaultAsync<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));

        //ToListAsync
        public async Task<List<Dto>> ToListAsync<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => await base.ToListAsync<Dto>(expression);
        public async Task<List<Dto>> ToListAsync<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => await base.ToListAsync<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));

        //ToPageAsync
        public async Task<Page<Dto>> ToPageAsync<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression) => await base.ToPageAsync<Dto>(expression);
        public async Task<Page<Dto>> ToPageAsync<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression) => await base.ToPageAsync<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]));
        public async Task<Page<Dto>> ToPageAsync<Dto>(Expression<Func<T, T2, T3, T4, T5, Dto>> expression, int pageIndex, int pageSize) => await base.ToPageAsync<Dto>(expression, pageIndex, pageSize);
        public async Task<Page<Dto>> ToPageAsync<Dto>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, Dto>> expression, int pageIndex, int pageSize) => await base.ToPageAsync<Dto>(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)]), pageIndex, pageSize);
        #endregion
        #endregion

        #region WhereSeg
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSeg<TAny>(Expression<Func<TAny, bool>> filter)
            => base.WhereSeg(filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSegIf<TAny>(bool condition, Expression<Func<TAny, bool>> filter)
            => base.WhereSegIf(condition, filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSeg<TAny, TAny2>(Expression<Func<TAny, TAny2, bool>> filter)
            => base.WhereSeg(filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSegIf<TAny, TAny2>(bool condition, Expression<Func<TAny, TAny2, bool>> filter)
            => base.WhereSegIf(condition, filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSeg<TAny, TAny2, TAny3>(Expression<Func<TAny, TAny2, TAny3, bool>> filter)
             => base.WhereSeg(filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSegIf<TAny, TAny2, TAny3>(bool condition, Expression<Func<TAny, TAny2, TAny3, bool>> filter)
            => base.WhereSegIf(condition, filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSeg<TAny, TAny2, TAny3, TAny4>(Expression<Func<TAny, TAny2, TAny3, TAny4, bool>> filter)
            => base.WhereSeg(filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereSegIf<TAny, TAny2, TAny3, TAny4>(bool condition, Expression<Func<TAny, TAny2, TAny3, TAny4, bool>> filter)
             => base.WhereSegIf(condition, filter) as SelectBuilder<T, T2, T3, T4, T5>;
        #endregion
        #region Where
        public override SelectBuilder<T, T2, T3, T4, T5> Where(string filter) => base.Where(filter) as SelectBuilder<T, T2, T3, T4, T5>;
        public override SelectBuilder<T, T2, T3, T4, T5> WhereIf(bool condition, string filter) => base.WhereIf(condition, filter) as SelectBuilder<T, T2, T3, T4, T5>;
        #endregion

        #region Join
        public SelectBuilder<T, T2, T3, T4, T5, T6> LeftJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp) where T6 : class, new()
        {
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.LeftJoin), "t6");
        }
        public SelectBuilder<T, T2, T3, T4, T5, T6> LeftJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp, string alias) where T6 : class, new()
        {
            if (alias.IsNullOrEmptyOrWhiteSpace()) alias = "t6";
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.LeftJoin), alias);
        }
        public SelectBuilder<T, T2, T3, T4, T5, T6> RightJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp) where T6 : class, new()
        {
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.RightJoin), "t6");
        }
        public SelectBuilder<T, T2, T3, T4, T5, T6> RightJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp, string alias) where T6 : class, new()
        {
            if (alias.IsNullOrEmptyOrWhiteSpace()) alias = "t6";
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.RightJoin), alias);
        }
        public SelectBuilder<T, T2, T3, T4, T5, T6> InnerJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp) where T6 : class, new()
        {
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.InnerJoin), "t6");
        }
        public SelectBuilder<T, T2, T3, T4, T5, T6> InnerJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp, string alias) where T6 : class, new()
        {
            if (alias.IsNullOrEmptyOrWhiteSpace()) alias = "t6";
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.InnerJoin), alias);
        }
        public SelectBuilder<T, T2, T3, T4, T5, T6> CrossJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp) where T6 : class, new()
        {
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.CrossJoin), "t6");
        }
        public SelectBuilder<T, T2, T3, T4, T5, T6> CrossJoin<T6>(Expression<Func<T, T2, T3, T4, T5, T6, bool>> joinExp, string alias) where T6 : class, new()
        {
            if (alias.IsNullOrEmptyOrWhiteSpace()) alias = "t6";
            return new SelectBuilder<T, T2, T3, T4, T5, T6>(this, (joinExp, EnumJoinType.CrossJoin), alias);
        }
        #endregion

        #region 转到insert
        public InsertFromSelectBuilder<TInsert> AsInsert<TInsert>(Expression<Func<T, T2, T3, T4, T5, TInsert>> expression) where TInsert : class, new()
        {
            var names = ExpressionHelper.GetInitOrReturnPropNames(expression).ToArray();
            if (names.IsNullOrEmpty()) throw new Exception($"不能在 AsInsert 方法中使用 \"i=>i\" 这种形式的表达式!");
            return new InsertFromSelectBuilder<TInsert>(db, this, expression, names);
        }
        public InsertFromSelectBuilder<TInsert> AsInsert<TInsert>(Expression<Func<IMultiTable<T, T2, T3, T4, T5>, TInsert>> expression) where TInsert : class, new()
        {
            var names = ExpressionHelper.GetInitOrReturnPropNames(BuilderHelper.ReplaceIMultiTable(expression, [typeof(T), typeof(T2), typeof(T3), typeof(T4), typeof(T5)])).ToArray();
            if (names.IsNullOrEmpty()) throw new Exception($"不能在 AsInsert 方法中使用 \"i=>i\" 这种形式的表达式!");
            return new InsertFromSelectBuilder<TInsert>(db, this, expression, names);
        }
        #endregion
    }

    public interface IMultiTable<T, T2, T3, T4, T5>
        where T : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
    {
        public T t { get; }
        public T2 t2 { get; }
        public T3 t3 { get; }
        public T4 t4 { get; }
        public T5 t5 { get; }
    }

    /// <summary>
    /// 分组查询构建器
    /// </summary>
    public class GroupBuilder<T, T2, T3, T4, T5, TGroupKey> : GroupBuilderBase
        where T : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
    {
        internal GroupBuilder(SelectBuilder<T, T2, T3, T4, T5> selectBuilder, Expression<Func<T, T2, T3, T4, T5, TGroupKey>> groupExpression) : base(selectBuilder, groupExpression) { }

        #region 过滤
        public GroupBuilder<T, T2, T3, T4, T5, TGroupKey> Having(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, bool>> expression)
        {
            Ensure.NotNull(expression, nameof(expression));
            havings.Add(expression);
            return this;
        }
        public GroupBuilder<T, T2, T3, T4, T5, TGroupKey> HavingIf(bool condition, Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, bool>> expression)
            => condition ? Having(expression) : this;

        public GroupBuilder<T, T2, T3, T4, T5, TGroupKey> HavingRaw(string filter)
        {
            Ensure.NotNull(filter, nameof(filter));
            havings.Add(filter);
            return this;
        }
        public GroupBuilder<T, T2, T3, T4, T5, TGroupKey> HavingRawIf(bool condition, string filter)
            => condition ? HavingRaw(filter) : this;
        #endregion

        #region 分页
        public override GroupBuilder<T, T2, T3, T4, T5, TGroupKey> Page(int pageIndex, int pageSize)
            => base.Page(pageIndex, pageSize) as GroupBuilder<T, T2, T3, T4, T5, TGroupKey>;
        public override GroupBuilder<T, T2, T3, T4, T5, TGroupKey> Limit(int size)
            => base.Limit(size) as GroupBuilder<T, T2, T3, T4, T5, TGroupKey>;
        public override GroupBuilder<T, T2, T3, T4, T5, TGroupKey> Limit(int startIndex, int size)
            => base.Limit(size, startIndex) as GroupBuilder<T, T2, T3, T4, T5, TGroupKey>;
        #endregion

        #region ToSql
        public string ToSqlFirstOrDefault<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => base.ToSqlFirstOrDefault(expression);
        public string ToSqlList<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => base.ToSqlList(expression);
        public string ToSqlPage<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => base.ToSqlPage(expression);
        public string ToSqlPage<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression, int pageIndex, int pageSize)
            => base.ToSqlPage(expression, pageIndex, pageSize);
        #endregion

        #region 排序
        public GroupBuilder<T, T2, T3, T4, T5, TGroupKey> OrderBy(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, object>> expression)
            => base.OrderBy(expression) as GroupBuilder<T, T2, T3, T4, T5, TGroupKey>;

        public GroupBuilder<T, T2, T3, T4, T5, TGroupKey> OrderByDesc(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, object>> expression)
            => base.OrderByDesc(expression) as GroupBuilder<T, T2, T3, T4, T5, TGroupKey>;

        /// <summary>
        /// 排序,如: select.Order("age desc,id")
        /// </summary>
        public override GroupBuilder<T, T2, T3, T4, T5, TGroupKey> Order(string orderSeg)
            => base.Order(orderSeg) as GroupBuilder<T, T2, T3, T4, T5, TGroupKey>;
        #endregion

        #region 执行
        #region 同步
        public Dto FirstOrDefault<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => base.FirstOrDefault<Dto>(expression);
        public List<Dto> ToList<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => base.ToList<Dto>(expression);
        public Page<Dto> ToPage<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => base.ToPage<Dto>(expression);
        public Page<Dto> ToPage<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression, int pageIndex, int pageSize)
            => base.ToPage<Dto>(expression, pageIndex, pageSize);
        #endregion
        #region 异步
        public async Task<Dto> FirstOrDefaultAsnc<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => await base.FirstOrDefaultAsnc<Dto>(expression);
        public async Task<List<Dto>> ToListAsync<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => await base.ToListAsync<Dto>(expression);
        public async Task<Page<Dto>> ToPageAsync<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression)
            => await base.ToPageAsync<Dto>(expression);
        public async Task<Page<Dto>> ToPageAsync<Dto>(Expression<Func<IGroupFilter<T, T2, T3, T4, T5, TGroupKey>, Dto>> expression, int pageIndex, int pageSize)
            => await base.ToPageAsync<Dto>(expression, pageIndex, pageSize);
        #endregion
        #endregion
    }

    public interface IGroupFilter<T, T2, T3, T4, T5, TGroupKey>
        where T : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
    {
        public TGroupKey Key { get; }
        public int Length { get; }

        #region 聚合函数 sum max min avg
        //https://dev.mysql.com/doc/refman/8.0/en/aggregate-functions.html
        public TKey Max<TKey>(Func<T, T2, T3, T4, T5, TKey> expression);
        public TKey Min<TKey>(Func<T, T2, T3, T4, T5, TKey> expression);
        public TKey Sum<TKey>(Func<T, T2, T3, T4, T5, TKey> expression);
        public TKey Avg<TKey>(Func<T, T2, T3, T4, T5, TKey> expression);
        #endregion

        #region 字符串 Join
        /// <summary>
        /// string_agg(sqlserver/postgresql) group_concat(mysql)
        /// <list type="bullet">
        /// <item>mysql: <seealso href="https://dev.mysql.com/doc/refman/8.0/en/aggregate-functions.html#function_group-concat"/></item>
        /// <item>sqlserver: <seealso href="https://learn.microsoft.com/zh-cn/sql/t-sql/functions/string-agg-transact-sql?view=sql-server-ver15"/></item>
        /// </list>
        /// </summary>
        /// <remarks>注意: 仅mysql支持 distinct</remarks>
        public string Join(Func<T, T2, T3, T4, T5, string> expression, string separator, Func<T, T2, T3, T4, T5, object> orderBy, bool desc, bool isDistinct);
        /// <summary>
        /// string_agg(sqlserver/postgresql) group_concat(mysql)
        /// <list type="bullet">
        /// <item>mysql: <seealso href="https://dev.mysql.com/doc/refman/8.0/en/aggregate-functions.html#function_group-concat"/></item>
        /// <item>sqlserver: <seealso href="https://learn.microsoft.com/zh-cn/sql/t-sql/functions/string-agg-transact-sql?view=sql-server-ver15"/></item>
        /// </list>
        /// </summary>
        public string Join(Func<T, T2, T3, T4, T5, string> expression, string separator, Func<T, T2, T3, T4, T5, object> orderBy, bool desc);
        /// <summary>
        /// string_agg(sqlserver/postgresql) group_concat(mysql)
        /// <list type="bullet">
        /// <item>mysql: <seealso href="https://dev.mysql.com/doc/refman/8.0/en/aggregate-functions.html#function_group-concat"/></item>
        /// <item>sqlserver: <seealso href="https://learn.microsoft.com/zh-cn/sql/t-sql/functions/string-agg-transact-sql?view=sql-server-ver15"/></item>
        /// </list>
        /// </summary>
        public string Join(Func<T, T2, T3, T4, T5, string> expression, string separator);
        #endregion
    }
}