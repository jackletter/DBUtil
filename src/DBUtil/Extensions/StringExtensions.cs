﻿namespace DBUtil.Extensions
{
    internal static class StringExtensions
    {
        /// <summary>
        /// 处理括号是否加在两端
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        internal static string WrapBracket(this string str)
        {
            //todo: 性能优化
            if (str == null) return str;
            //忽略两端空格的影响
            var tmp = str.Trim();
            //当表达式已有且仅有一对"()"在两端时,直接返回
            if (tmp.StartsWith("(") && tmp.IndexOf(')', 1) == tmp.Length - 1) return str;
            //当表达式已有且仅有一对"''"在两端时,直接返回
            if (tmp.StartsWith("'") && tmp.IndexOf('\'', 1) == tmp.Length - 1) return str;
            //当有空格时 直接加上"()"
            if (tmp.Contains(' ')) return $"({str})";
            return str;
        }
    }
}
