﻿using DotNetCommon.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace DBUtil
{
    /// <summary>
    /// 封装了 DbDataReader 提供便捷方法(可以不考虑 <c>if(reader.Read())</c>)
    /// </summary>
    public class DBUtilDataReader
    {
        public DbDataReader RawReader { get; set; }
        public DBAccess db { get; set; }

        /// <summary>
        /// 读取一行第一列的值
        /// </summary>
        public T ReadScalar<T>()
        {
            if (!RawReader.Read())
            {
                object tmp = null;
                return tmp.To<T>();
            }
            var func = db.GetReaderDeserialization<T>(RawReader);
            var obj = func(RawReader);
            return obj;
        }

        /// <summary>
        /// 读取一行第一列的值
        /// </summary>
        public async Task<T> ReadScalarAsync<T>()
        {
            if (!await RawReader.ReadAsync())
            {
                object tmp = null;
                return tmp.To<T>();
            }
            var func = db.GetReaderDeserialization<T>(RawReader);
            var obj = func(RawReader);
            await RawReader.NextResultAsync();
            return obj;
        }

        /// <summary>
        /// 读取所有行
        /// </summary>
        public List<T> ReadList<T>()
        {
            if (!RawReader.Read())
            {
                return [];
            }
            var func = db.GetReaderDeserialization<T>(RawReader);
            var list = new List<T>();
            do
            {
                list.Add(func(RawReader));
            } while (RawReader.Read());
            return list;
        }

        /// <summary>
        /// 读取所有行
        /// </summary>
        public async Task<List<T>> ReadListAsync<T>()
        {
            if (!await RawReader.ReadAsync())
            {
                return [];
            }
            var func = db.GetReaderDeserialization<T>(RawReader);
            var list = new List<T>();
            do
            {
                list.Add(func(RawReader));
            } while (await RawReader.ReadAsync());
            return list;
        }

        /// <summary>
        /// 读取一行
        /// </summary>
        public T ReadOne<T>()
        {
            if (!RawReader.Read())
            {
                if (typeof(T).IsValueType && !typeof(T).IsNullable()) throw new Exception($"未查询到({typeof(T).GetClassFullName()}类型)数据!");
                return default;
            }
            var func = db.GetReaderDeserialization<T>(RawReader);
            var obj = func(RawReader);
            return obj;
        }

        /// <summary>
        /// 读取一行
        /// </summary>
        public object ReadOne(Type type)
        {
            if (!RawReader.Read())
            {
                if (type.IsValueType && !type.IsNullable()) throw new Exception($"未查询到({type.GetClassFullName()}类型)数据!");
                return default;
            }
            var func = db.GetReaderDeserialization(type, RawReader);
            var obj = func(RawReader);
            return obj;
        }

        /// <summary>
        /// 读取一行
        /// </summary>
        public async Task<T> ReadOneAsync<T>()
        {
            if (!await RawReader.ReadAsync())
            {
                if (typeof(T).IsValueType && !typeof(T).IsNullable()) throw new Exception($"未查询到({typeof(T).GetClassFullName()}类型)数据!");
                return default;
            }
            var func = db.GetReaderDeserialization<T>(RawReader);
            var obj = func(RawReader);
            return obj;
        }

        /// <summary>
        /// 读取一行
        /// </summary>
        public async Task<object> ReadOneAsync(Type type)
        {
            if (!await RawReader.ReadAsync())
            {
                if (type.IsValueType && !type.IsNullable()) throw new Exception($"未查询到({type.GetClassFullName()}类型)数据!");
                return default;
            }
            var func = db.GetReaderDeserialization(type, RawReader);
            var obj = func(RawReader);
            return obj;
        }
    }
}
