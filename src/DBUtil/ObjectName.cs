﻿namespace DBUtil
{
    /// <summary>
    /// 表示数据库对象名称,适用多种场合,如：<br />
    /// sqlserver：testdb.dbo.table1<br/>
    /// mysql：testdb.table1<br/>
    /// </summary>
    public class ObjectName
    {
        /// <summary>
        /// 数据库名称，如：testdb
        /// </summary>
        public string DataBaseName { get; set; }
        /// <summary>
        /// 模式名称，如：dbo、testdb(mysql)
        /// </summary>
        /// <remarks>注意：对于mysql来说数据库名称和schema名称是同一个意思</remarks>
        public string SchemaName { get; set; }
        /// <summary>
        /// 对象名称，如：table1
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数据库名称，如：[testdb]、`testdb`
        /// </summary>
        public string DataBaseNameQuoted { get; set; }
        /// <summary>
        /// 模式名称，如：[dbo]、`testdb`(mysql)
        /// </summary>
        /// <remarks>注意：对于mysql来说数据库名称和schema名称是同一个意思</remarks>
        public string SchemaNameQuoted { get; set; }
        /// <summary>
        /// 对象名称，如：[table1]、`table1`
        /// </summary>
        public string NameQuoted { get; set; }

        /// <summary>
        /// 数据库内的名称,不含数据库名，如：
        /// <list type="number">
        /// <item>sqlserver：dbo.table1，而不是 testdb.dbo.table1</item>
        /// <item>mysql：table1，而不是 testdb.table1</item>
        /// </list>
        /// </summary>
        public string NormalName { get; set; }
        /// <summary>
        /// 数据库内的名称,不含数据库名，如：
        /// <list type="number">
        /// <item>sqlserver：[dbo].[table1]，而不是 [testdb].[dbo].[table1]</item>
        /// <item>mysql：`table1`，而不是 `testdb`.`table1`</item>
        /// </list>
        /// </summary>
        public string NormalNameQuoted { get; set; }

        /// <summary>
        /// 全名称，含数据库名，如：
        /// <list type="number">
        /// <item>sqlserver：testdb.dbo.table1</item>
        /// <item>mysql：testdb.table1</item>
        /// </list>
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 全名称，含数据库名，如：
        /// <list type="number">
        /// <item>sqlserver：[testdb].[dbo].[table1]</item>
        /// <item>mysql：`testdb`.`table1`</item>
        /// </list>
        /// </summary>
        public string FullNameQuoted { get; set; }

        /// <summary>
        /// 名称的分段数,如: testdb.dbo.testtbl:3, testdb.testtbl:2
        /// </summary>
        public int SegCount { get; set; }

        /// <summary>
        /// 原来的名字
        /// </summary>
        public string OriginalName { get; set; }
    }
}
