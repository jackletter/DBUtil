﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.SqlSegment
{
    public abstract class ConvertSqlSegment
    {
        public DBAccess db { get; }

        public ConvertSqlSegment(DBAccess db)
        {
            this.db = db;
        }
        /// <summary>
        /// 整形转字符串(包括 byte/sbyte/short/ushort/int/unit/long/ulong)
        /// <list type="bullet">
        /// mysql: ConvertIntegerToString("age") => convert(age,char)
        /// </list>
        /// </summary>
        public abstract string ConvertIntegerToString(string objectName);

        /// <summary>
        /// 小数转字符串(包括 float double decimal)
        /// <list type="bullet">
        /// <item>mysql: ConvertDecimalToString("price") => convert(price,char)</item>
        /// </list>
        /// </summary>
        public abstract string ConvertDecimalToString(string objectName);

        /// <summary>
        /// 日期时间转字符串(包括 datetime/datetimeoffset/dateonly/timeonly)
        /// <list type="bullet">
        /// <item>mysql: ConvertDateTimeToString("birth") => convert(birth,char)</item>
        /// </list>
        /// </summary>
        public abstract string ConvertDateTimeToString(string objectName);

        /// <summary>
        /// 字符串转整形
        /// <list type="bullet">
        /// <item>mysql: ConvertStringToInteger("age") => convert(age,decimal)</item>
        /// </list>
        /// </summary>
        public abstract string ConvertStringToInteger(string objectName);

        /// <summary>
        /// 字符串转小数
        /// <list type="bullet">
        /// <item>mysql: ConvertStringToDecimal("price") => convert(price,decimal(30,15))</item>
        /// </list>
        /// </summary>
        public abstract string ConvertStringToDecimal(string objectName);

        /// <summary>
        /// 字符串转日期时间
        /// <list type="bullet">
        /// <item>mysql: ConvertStringToDateTime("birth") => convert(birth,datetime(6))</item>
        /// </list>
        /// </summary>
        public abstract string ConvertStringToDateTime(string objectName);

        public abstract string ConvertAllToString(string objectName);
    }
}
