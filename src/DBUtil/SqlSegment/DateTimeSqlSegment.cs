﻿namespace DBUtil.SqlSegment
{
    /// <summary>
    /// 数据库日期时间运算符sql，示例:
    /// <list type="number">
    /// <item>sqlserver: DateTimeSqlSegment.DefaultDateTimeType => "datetime2"</item>
    /// <item>sqlserver: DateTimeSqlSegment.Current => "GetDate()"</item>
    /// </list>
    /// </summary>
    public abstract class DateTimeSqlSegment
    {
        public DBAccess db { get; }

        public DateTimeSqlSegment(DBAccess db)
        {
            this.db = db;
        }

        /// <summary>
        /// 默认的日期时间类型，示例：
        /// <list type="number">
        /// <item>sqlserver: "datetime2"</item>
        /// <item>mysql: "datetime"</item>
        /// </list>
        /// </summary>
        public abstract string DefaultDateTimeType { get; }

        /// <summary>
        /// 获取当前时间，示例：
        /// <list type="number">
        /// <item>sqlserver: "getdate()"</item>
        /// <item>mysql: "now()"</item>
        /// </list>
        /// </summary>
        public abstract string Current { get; }

        #region 当前时间加上 年/月/天/小时/分钟/秒
        /// <summary>
        /// 当前时间加上指定的年数，示例：
        /// <list type="number">
        /// <item>sqlserver: GetCurrentAddYear(3) => "dateadd(year,3,getdate())"</item>
        /// </list>
        /// </summary>
        public abstract string GetCurrentAddYear(int year);

        /// <summary>
        /// 当前时间加上指定的月数，示例：
        /// <list type="number">
        /// <item>sqlserver: GetCurrentAddMonth(3) => "dateadd(month,3,getdate())"</item>
        /// </list>
        /// </summary>
        public abstract string GetCurrentAddMonth(int day);

        /// <summary>
        /// 当前时间加上指定的天数，示例：
        /// <list type="number">
        /// <item>sqlserver: GetCurrentAddDay(3) => "dateadd(day,3,getdate())"</item>
        /// </list>
        /// </summary>
        public abstract string GetCurrentAddDay(int day);

        /// <summary>
        /// 当前时间加上指定的小时数，示例：
        /// <list type="number">
        /// <item>sqlserver: GetCurrentAddHour(3) => "dateadd(hour,3,getdate())"</item>
        /// </list>
        /// </summary>
        /// <param name="hour">加上的小时数</param>
        public abstract string GetCurrentAddHour(int hour);

        /// <summary>
        /// 当前时间加上指定的分钟数，示例：
        /// <list type="number">
        /// <item>sqlserver: GetCurrentAddMinute(3) => "dateadd(minute,3,getdate())"</item>
        /// </list>
        /// </summary>
        /// <param name="minute">加上的分钟数</param>
        /// <returns></returns>
        public abstract string GetCurrentAddMinute(int minute);

        /// <summary>
        /// 当前时间加上指定的秒数，示例：
        /// <list type="number">
        /// <item>sqlserver: GetCurrentAddSecond(3) => "dateadd(second,3,getdate())"</item>
        /// </list>
        /// </summary>
        /// <param name="second">加上的秒数</param>
        /// <returns></returns>
        public abstract string GetCurrentAddSecond(int second);
        #endregion
    }
}
