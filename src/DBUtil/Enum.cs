﻿namespace DBUtil
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DBType
    {
        SQLSERVER = 0,
        MYSQL = 1,
        ORACLE = 2,
        POSTGRESQL = 3,
        SQLITE = 4,
    }

    public enum EnumInsertToSql
    {
        /// <summary>
        /// 执行并返回受影响的行数
        /// </summary>
        ExecuteAffrows,
        /// <summary>
        /// 执行并返回自增主键
        /// </summary>
        ExecuteIdentity,
        /// <summary>
        /// 执行并返回新增的数据
        /// </summary>
        ExecuteInserted
    }

    /// <summary>
    /// insert构建器类型
    /// </summary>
    public enum EnumInsertBuilderType
    {
        InsertByDictionary,
        InsertByEntity,
        InsertByDto,
    }

    internal enum EnumSetIgnoreType
    {
        SetColumn,
        IgnoreColumn,
        OnlyColumn
    }

    public enum EnumDeleteBuilderType
    {
        Delete,
        DeleteByPrimary
    }

    public enum EnumUpdateBuilderType
    {
        UpdateByDictionary,
        UpdateByEntity,
        UpdateByDto,
        UpdateByExpression
    }

    public enum EnumSelectToSql
    {
        ToList,
        ToPage,
        Count,
        Max,
        Min,
        Sum,
        Avg,

        ToSelectClause,
    }

    public enum EnumJsonDataType
    {
        String,
        Number,
        Null,
        Bool,
        Object,
        Array,
    }

    /// <summary>
    /// sql中目标位置需要的json类型,如:
    /// <list type="bullet">
    /// <item>null: doc[null], value(null)</item>
    /// <item>number: doc[cast(1 as json)], value(1)</item>
    /// <item>bool: doc[cast(true as json)], value(true)</item>
    /// <item>string: doc['"tom"'], value('tom')</item>
    /// <item>arr/obj: doc['[1,2]'], value(cast('[1,2]' as json))</item>
    /// </list>
    /// </summary>
    /// <remarks>
    /// 注意: 函数或sql的某个地方有声明接受的是 json_doc 或 val, 如 mysql 中:
    /// <list type="bullet">
    /// <item>JSON_ARRAY([val[, val] ...])</item>
    /// <item>JSON_ARRAY_APPEND(json_doc, path, val[, path, val] ...)</item>
    /// <item>JSON_OVERLAPS(json_doc1, json_doc2)</item>
    /// <item>set ext(json column)=json_doc</item>
    /// </list>
    /// 最好按照函数声明的类型传递, 否则会发生意外, 如:
    /// <list type="bullet">
    /// <item>正常: select json_array_append('[1,2]','$',cast('{"name":"ko"}' as json))</item>
    /// <item>意外: 追加的变成了字符串 select json_array_append('[1,2]','$','{"name":"ko"}')</item>
    /// </list>
    /// </remarks>
    public enum EnumJsonAcceptAsType
    {
        Value,
        Doc,
        /// <summary>
        /// 供 db.Insert()...ToDataTable() 使用
        /// </summary>
        DataTableValue,
    }

    public enum EnumJoinType
    {
        LeftJoin,
        RightJoin,
        InnerJoin,
        CrossJoin,
    }

    public enum EnumTreeSpreedMode
    {
        Both,
        Sub,
        Parent,
    }

    internal enum EnumLikeMode
    {
        Start,
        End,
        Both,
    }
}
