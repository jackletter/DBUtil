﻿using System;

namespace DBUtil
{
    /// <summary>
    /// 表示可直接拼接到sql中的字符串
    /// </summary>
    /// <remarks>
    /// 应用场景:
    /// <list type="number">
    /// <item>set [Name]=[Name]+2</item>
    /// <item>set [CreateTime]=now()</item>
    /// </list>
    /// </remarks>
    public struct RawString
    {
        public string String { get; }
        public RawString(string str)
        {
            String = str ?? string.Empty;
        }

        public override int GetHashCode()
        {
            return String.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var b = obj is not null;
            b = b && obj is RawString;
            return b && String == obj.ToString();
        }

        public override string ToString()
        {
            return String;
        }
    }
}
