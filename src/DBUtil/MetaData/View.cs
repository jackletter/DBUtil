﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 视图
    /// </summary>
    public class View
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 表所属架构/模式
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// 视图名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 视图创建sql语句
        /// </summary>
        public string CreateSql { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }

        /// <summary>
        /// 列集合
        /// </summary>
        public List<Column> Columns { set; get; }
    }
}
