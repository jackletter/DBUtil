﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 约束,注意: 每一行代表xx约束使用的一列(unique约束中),所以可能存在多行都是同一个约束的情况
    /// </summary>
    public class Constraint
    {
        /// <summary>
        /// 约束所在的架构名称
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// 所属表名称
        /// </summary>
        public string TableName { set; get; }

        /// <summary>
        /// 约束名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 约束类型
        /// </summary>
        public EnumConstraintType Type { set; get; }

        /// <summary>
        /// 约束列,多列用英文逗号分隔
        /// </summary>
        public string ColumnName { set; get; }

        /// <summary>
        /// 引用的表名称(外键约束用)
        /// </summary>
        public string ReferenceTableName { get; set; }

        /// <summary>
        /// 引用的表的列明(外键约束用)
        /// </summary>
        public string ReferenceColumnName { get; set; }

        /// <summary>
        /// 删除时的级联处理(外键约束用)
        /// </summary>
        public string Delete_Action { set; get; }

        /// <summary>
        /// 更新时的级联处理(外键约束用)
        /// </summary>
        public string Update_Action { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }
    }
}
