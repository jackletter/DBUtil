﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 触发器
    /// </summary>
    public class Trigger
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 触发器创建sql语句
        /// </summary>
        public string CreateSql { set; get; }

        /// <summary>
        /// 所属表名称
        /// </summary>
        public string TableName { set; get; }

        /// <summary>
        /// 是否update语句触发
        /// </summary>
        public bool IsUpdate { set; get; }

        /// <summary>
        /// 是否delete语句触发
        /// </summary>
        public bool IsDelete { set; get; }
        /// <summary>
        /// 是否插入语句触发
        /// </summary>
        public bool IsInsert { set; get; }
        /// <summary>
        /// 是否在语句执行后触发
        /// </summary>
        public bool IsAfter { set; get; }
        /// <summary>
        /// 是否替代触发源sql语句
        /// </summary>
        public bool IsInsteadof { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }
    }
}
