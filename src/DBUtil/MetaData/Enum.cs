﻿using System;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 数据库约束类型
    /// </summary>
    public enum EnumConstraintType
    {
        /// <summary>
        /// 检查约束
        /// </summary>
        Check,
        /// <summary>
        /// 唯一值约束(sqlserver中没有这种约束类型,唯一性是直接定义在列属性上的),mysql中是约束+索引实现
        /// </summary>
        Unique,
        /// <summary>
        /// 主键约束
        /// </summary>
        PrimaryKey,
        /// <summary>
        /// 外键约束
        /// </summary>
        ForeignKey,
        /// <summary>
        /// 默认值约束
        /// </summary>
        DefaultValue,
        /// <summary>
        /// 非空约束
        /// </summary>
        NotNull,
    }

    /// <summary>
    /// 获取表详情数据类型
    /// </summary>
    [Flags]
    public enum EnumTableDetailType
    {
        All = 1,
        Column = 2,
        Index = 4,
        Trigger = 8,
        Constraint = 16,
    }
}
