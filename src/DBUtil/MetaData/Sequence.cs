﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 序列
    /// </summary>
    public class Sequence
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 所属架构/模式
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// 序列名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数据类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 最小值
        /// </summary>
        public long MinValue { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public long MaxValue { get; set; }

        /// <summary>
        /// 起始值
        /// </summary>
        public long StartValue { get; set; }

        /// <summary>
        /// 增量
        /// </summary>
        public long Increment { get; set; }

        /// <summary>
        /// 当前值
        /// </summary>
        public long CurrentValue { get; set; }

        /// <summary>
        /// 是否循环
        /// </summary>
        public bool IsCycle { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }
    }
}
