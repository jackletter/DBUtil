﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 索引,注意: 每一行代表xx索引使用的一列(联合索引中),所以可能存在多行都是同一个索引的情况
    /// </summary>
    public class Index
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 索引所在的架构名称
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// 索引所在的表名称
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 索引作用的列名称(多列使用英文逗号分隔)
        /// </summary>
        public string ColumnName { set; get; }

        /// <summary>
        /// 是否是聚集索引
        /// </summary>
        public bool IsClustered { set; get; }

        /// <summary>
        /// 索引类型
        /// </summary>
        public string IndexType { set; get; }

        /// <summary>
        /// 索引是否要求唯一值
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// 索引是否允许空值
        /// </summary>
        public bool IsNullAble { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public string Desc { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }

        /// <summary>
        /// 联合索引时的排序值
        /// </summary>
        public int Seq_in_index { get; set; }
    }
}
