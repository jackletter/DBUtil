﻿using DotNetCommon.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 表
    /// </summary>
    /// <remarks>sqlserver没有表级别的排序规则</remarks>
    public class Table
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 表所属架构/模式
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 完全限定名
        /// </summary>
        public string FullNameQuoted { get; set; }

        /// <summary>
        /// 表注释
        /// </summary>
        public string Desc { get; set; }

        /// <summary>
        /// 主键,如: "student_id,book_id"
        /// </summary>
        public string PrimaryKey { set; get; }

        /// <summary>
        /// 主键列
        /// </summary>
        public List<Column> PrimaryKeyColumns { set; get; }

        /// <summary>
        /// 列集合
        /// </summary>
        public List<Column> Columns { set; get; }

        /// <summary>
        /// 触发器集合
        /// </summary>
        public List<Trigger> Triggers { set; get; }

        /// <summary>
        /// 约束集合
        /// </summary>
        public List<Constraint> Constraints { set; get; }

        /// <summary>
        /// 索引集合
        /// </summary>
        public List<Index> Indexes { set; get; }

        /// <summary>
        /// 当前多少行数据
        /// </summary>
        public long RowCount { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }
    }
}
