﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 存储过程
    /// </summary>
    public class Procedure
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 表所属架构/模式
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// 存储过程名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 存储过程创建sql语句
        /// </summary>
        public string CreateSql { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }
    }
}
