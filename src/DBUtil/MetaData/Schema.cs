﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 架构/模式
    /// </summary>
    public class Schema
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 架构/模式名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 拥有者名称
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }

    }
}
