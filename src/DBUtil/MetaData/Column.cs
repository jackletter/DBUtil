﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBUtil.MetaData
{
    /// <summary>
    /// 列
    /// </summary>
    public class Column
    {
        /// <summary>
        /// 表名称
        /// </summary>
        public string TableName { set; get; }

        /// <summary>
        /// 列名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 列描述
        /// </summary>
        public string Desc { set; get; }

        /// <summary>
        /// 是否参与到了主键
        /// </summary>
        public bool IsPrimaryKey { set; get; }

        /// <summary>
        /// 列类型码,使用时将它转化为具体的数据库列类型
        /// </summary>
        public int TypeNum { set; get; }

        /// <summary>
        /// 详细类型
        /// </summary>
        public string TypeString { set; get; }

        /// <summary>
        /// 是否可为空
        /// </summary>
        public bool IsNullAble { set; get; }

        /// <summary>
        /// 是否自增
        /// </summary>
        public bool IsIdentity { set; get; }

        /// <summary>
        /// 自增起始值
        /// </summary>
        public int IdentityStart { set; get; }

        /// <summary>
        /// 自增增量
        /// </summary>
        public int IdentityIncre { set; get; }

        /// <summary>
        /// 是否是唯一的
        /// </summary>
        public bool IsUnique { set; get; }

        /// <summary>
        /// 是否有默认值
        /// </summary>
        public bool HasDefault { set; get; }

        /// <summary>
        /// 默认值
        /// </summary>
        public string Default { set; get; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { set; get; }

        /// <summary>
        /// 上次更新时间
        /// </summary>
        public DateTime? LastUpdate { set; get; }

        #region 计算列属性
        /// <summary>
        /// 是否是计算列
        /// </summary>
        public bool IsComputed { get; set; }

        /// <summary>
        /// 计算列的定义
        /// </summary>
        public string ComputedDefinition { get; set; }

        /// <summary>
        /// 如果是计算列的话,是否是持久化存储的
        /// </summary>
        public bool? IsPersisted { get; set; }
        #endregion

    }
}
