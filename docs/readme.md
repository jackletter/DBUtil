# DBUtil

一款轻量化操作db的类库.

特点: 类DBHelper设计, 比dapper略重, 比EntityFramework/freesql/sqlsugar轻.

## 1. 安装包(以mysql为例)
```
dotnet add package DBUtil
dotnet add package DBUtil.Provider.MySql
```

## 2. 创建DBAccess
```csharp
using DBUtil;

DBAccess db = DBFactory.CreateDB("MySql", "Server=127.0.0.1;Database=test;Uid=root;Pwd=123456;");
```

## 3. 插入数据
**无实体插入:**
```csharp
var insert = db.Insert("t_user", new[]{ new Dictionary<string, object>
{
	{"name","刘备" },
	{"age",40 },
	{"birth",DateTime.Parse("1900-01-02") },
	{"addr","桃园" },
}, new Dictionary<string, object>
{
	{"name","关羽" },
	{"age",38 },
	{"birth",DateTime.Parse("1902-01-02") },
	{"addr","桃园" },
} });
sql = insert.ToSql();
sql.ShouldBe(@"insert into t_user(name,age,birth,addr) values
('刘备',40,'1900-01-02','桃园'),
('关羽',38,'1902-01-02','桃园');");
insert.ExecuteAffrows();
```

**有实体插入:**
```csharp
var insert = db.Insert<UserEntity>().SetEntity([new UserEntity()
{
	Id = 2,
	Age = 20,
	Name = "小红",
	Addr = "天明路",
	Birth = DateTime.Parse("1996-01-02")
}, new UserEntity
{
	Id = 3,
	Age = 22,
	Name = "小刚",
	Addr = "天明路",
	Birth = DateTime.Parse("1996-01-02")
} ]);
sql = insert.ToSql();
sql.ShouldBe(@"insert into t_user(id,name,age,addr,birth) values
(2,'小红',20,'天明路','1996-01-02'),
(3,'小刚',22,'天明路','1996-01-02');");
```

## 4. 更新数据
**无实体更新:**
```csharp
var sql = db.Update("t_user", new { Name = "小明", Age = 20, Addr = "天明路", Birth = DateTime.Parse("2023-02-02 01:02:03.123456") }.ToDictionary())
	.Where("id=1")
	.ToSql();
sql.ShouldBe(@"update t_user set 
    Name = '小明',
    Age = 20,
    Addr = '天明路',
    Birth = '2023-02-02 01:02:03.123456'
where id=1;");
```

**有实体更新**
```csharp
var update = db.Update<UserEntity>().SetDto(new
{
	Id = 1,
	Name = "小明",
	Addr = "天命路",
});
var sql = update.ToSql();
sql.ShouldBe(@"update t_user set
    name = '小明',
    addr = '天命路'
where id = 1;");
```

## 5. 删除数据
**无实体删除:**
```csharp
var sql = "";
sql = db.Delete("t_user").Where("id=1").ToSql();
sql.ShouldBe("delete from t_user where id=1;");
```

**有实体删除:**
```csharp
var sql = db.Delete<UserEntity>().Where(i => i.Id == 1).ToSql();
sql.ShouldBe("delete from t_user where id = 1;");
```


## 6. 查询数据
```csharp
//多种快速查询形式
var count = db.SelectScalar<int>("select count(1) from t_user");
var dic = db.SelectDictionary("select * from t_user limit 1");
var table = db.SelectDataTable("select * from t_user");
var user = db.SelectModel<UserEntity>("select * from t_user limit 1");
var user = db.SelectModel<(id)>("select * from t_user limit 1");
var data = db.SelectModel<(int id, string name, int? age)>("select id,name,age");
data.id.ShouldBe(1);
```