﻿using DBUtil;
using DBUtil.Annotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace ConsoleTests
{
    internal class Class1
    {
        private static DBAccess db = null;
        static Class1()
        {
            DBUtil.DBFactory.AddDBSupport<DBUtil.Provider.MySql.MySqlDBFactory>();
            db = DBFactory.CreateDB(DBType.MYSQL, "Server=127.0.0.1;Database=test;Uid=root;Pwd=123456;AllowLoadLocalInfile=true;SslMode=none;AllowPublicKeyRetrieval=True;Charset=utf8mb4;", DBSetting.NewInstance().SetSqlMonitor(arg =>
            {
                Console.WriteLine(arg);
                return Task.CompletedTask;
            }));
        }
        public static void Main(string[] args)
        {
            //NormalTest();

            Console.WriteLine("ok");
            Console.ReadLine();
        }

        #region model NormalUser
        [Table("t_user")]
        public class NormalUser
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("age")]
            public int? Age { get; set; }
            [Column("addr")]
            public string Addr { get; set; }
            [Column("birth")]
            public DateTime? Birth { get; set; }
        }
        #endregion

        public static void NormalTest()
        {
        }
    }
}
