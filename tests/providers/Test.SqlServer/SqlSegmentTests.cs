﻿using DBUtil;
using DBUtil.Provider.SqlServer;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.SqlServer
{
    [TestFixture]
    public class SqlSegmentTests : TestBase
    {
        #region DateTimeSqlSegment
        [Test]
        public void TestDateTimeSqlSegment_FilterString()
        {
            var filter = db.DateTimeSqlSegment.GetFilterString("birth", null, null);
            Assert.IsTrue(filter == "");
            var start = DateTime.Parse("1995-02-03");
            var end = DateTime.Parse("2000-01-01");
            filter = db.DateTimeSqlSegment.GetFilterString("birth", start, end);
            Assert.IsTrue(filter == $" and birth >='{start.ToString("yyyy-MM-dd HH:mm:ss.fff")}' and birth <'{end.ToString("yyyy-MM-dd HH:mm:ss.fff")}'");
            filter = db.DateTimeSqlSegment.GetFilterString("birth", start, null);
            Assert.IsTrue(filter == $" and birth >='{start.ToString("yyyy-MM-dd HH:mm:ss.fff")}'");
            filter = db.DateTimeSqlSegment.GetFilterString("birth", null, end);
            Assert.IsTrue(filter == $" and birth <'{end.ToString("yyyy-MM-dd HH:mm:ss.fff")}'");
            filter = db.DateTimeSqlSegment.GetFilterString("birth", start, end, false, true);
            Assert.IsTrue(filter == $" and birth >'{start.ToString("yyyy-MM-dd HH:mm:ss.fff")}' and birth <='{end.ToString("yyyy-MM-dd HH:mm:ss.fff")}'");
        }

        [Test]
        public void TestDateTimeSqlSegment()
        {
            db.DateTimeSqlSegment.Current.ShouldBe("GetDate()");
            db.DateTimeSqlSegment.DefaultDateTimeType.ShouldBe("datetime2");
            db.DateTimeSqlSegment.GetStringInSql(DateTime.Parse("2022-02-05")).ShouldBe("'2022-02-05 00:00:00.000'");
            db.DateTimeSqlSegment.GetCurrentAddYear(3).ShouldBe("DATEADD(year,3,GetDate())");
            db.DateTimeSqlSegment.GetCurrentAddMonth(3).ShouldBe("DATEADD(month,3,GetDate())");
            db.DateTimeSqlSegment.GetCurrentAddDay(3).ShouldBe("DATEADD(day,3,GetDate())");
            db.DateTimeSqlSegment.GetCurrentAddHour(3).ShouldBe("DATEADD(hour,3,GetDate())");
            db.DateTimeSqlSegment.GetCurrentAddMinute(3).ShouldBe("DATEADD(minute,3,GetDate())");
            db.DateTimeSqlSegment.GetCurrentAddSecond(3).ShouldBe("DATEADD(second,3,GetDate())");
        }
        #endregion

        #region StringSqlSegment
        [Test]
        public void TestStringSqlSegment_FilterString()
        {
            db.StringSqlSegment.GetLength("name").ShouldBe("len(name)");
            db.StringSqlSegment.GetLength("name", true).ShouldBe("len(ISNULL(name,''))");
            db.StringSqlSegment.Trim("name").ShouldBe("LTrim(RTrim(name))");
            db.StringSqlSegment.LeftTrim("name").ShouldBe("LTrim(name)");
            db.StringSqlSegment.RightTrim("name").ShouldBe("RTrim(name)");

            db.StringSqlSegment.BeforeString("name", "-").ShouldBe("IIF((charindex('-',name)>0),(SUBSTRING(name,0,(charindex('-',name)))),'')");
            db.StringSqlSegment.BeforeString("name", "-", true).ShouldBe("IIF((charindex('-',name)>0),(SUBSTRING(name,0,(charindex('-',name)+len('-')))),'')");

            db.StringSqlSegment.AfterString("name", "-").ShouldBe("IIF((charindex('-',name)>0),(SUBSTRING(name,(charindex('-',name)+len('-')),LEN(name))),'')");
            db.StringSqlSegment.AfterString("name", "-", true).ShouldBe("IIF((charindex('-',name)>0),(SUBSTRING(name,(charindex('-',name)),LEN(name))),'')");

            db.StringSqlSegment.ReplaceAll("name", "+", "-").ShouldBe("REPLACE(name,'+','-')");
            db.StringSqlSegment.Upper("name").ShouldBe("Upper(name)");
            db.StringSqlSegment.Lower("name").ShouldBe("Lower(name)");

            //测试BeforeString、AfterString
            db.Manage.DropTableIfExist("test");
            db.ExecuteSql("create table test(id int,name varchar(50))");
            db.Insert("test", new { id = 1, name = "0123-456" }.ToDictionary());

            var dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.AfterString("name", "-")} from test");
            dic["nameext"].ShouldBe("456");
            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.AfterString("name", "-", true)} from test");
            dic["nameext"].ShouldBe("-456");

            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.BeforeString("name", "-")} from test");
            dic["nameext"].ShouldBe("0123");
            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.BeforeString("name", "-", true)} from test");
            dic["nameext"].ShouldBe("0123-");

            //非正常情况 不存在指定的字符
            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.AfterString("name", "+")} from test");
            dic["nameext"].ShouldBe("");
            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.AfterString("name", "+", true)} from test");
            dic["nameext"].ShouldBe("");

            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.BeforeString("name", "+")} from test");
            dic["nameext"].ShouldBe("");
            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.BeforeString("name", "+", true)} from test");
            dic["nameext"].ShouldBe("");

            //非正常情况 列值为null
            db.Manage.DropTableIfExist("test");
            db.ExecuteSql("create table test(id int,name varchar(50))");
            db.Insert("test", new { id = 1, name = new RawString("null") }.ToDictionary());

            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.AfterString("name", "+")} from test");
            dic["nameext"].ShouldBe("");
            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.AfterString("name", "+", true)} from test");
            dic["nameext"].ShouldBe("");

            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.BeforeString("name", "+")} from test");
            dic["nameext"].ShouldBe("");
            dic = db.SelectDictionary($"select id,name,nameext={db.StringSqlSegment.BeforeString("name", "+", true)} from test");
            dic["nameext"].ShouldBe("");
        }
        #endregion
    }
}
