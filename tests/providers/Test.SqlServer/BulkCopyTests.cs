﻿using DBUtil.Provider.SqlServer;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Test.SqlServer
{
    [TestFixture]
    internal class BulkCopyTests : TestBase
    {
        /// <summary>
        /// <seealso href="https://blog.csdn.net/u010476739/article/details/123335909"/>
        /// </summary>
        [Test]
        public void Copy()
        {
            db.Manage.DropTableIfExist("test");
            db.ExecuteSql(@"create table test(
    id int identity(1,1) primary key,
    name varchar(50),
    addr varchar(500) default('默认地址'),
    age int not null,
    score int check(score>0)
)");
            //triger
            if (db.IsTableExist("t_test"))
            {
                db.Manage.DropTable("t_test");
            }
            db.ExecuteSql("create table t_test(id int identity(1,1) not null,name varchar(50))");
            if (db.IsTriggerExist("tri_test_after_insert"))
            {
                db.Manage.DropTrigger("tri_test_after_insert");
            }
            db.ExecuteSql(@"
create trigger tri_test_after_insert on test after insert as 
begin 
	insert into t_test(name) select name from inserted order by inserted.id;
end");

            var dt = new DataTable("test");
            dt.Columns.Add("id");
            dt.Columns.Add("name");
            dt.Columns.Add("addr");
            dt.Columns.Add("age");
            dt.Columns.Add("score");
            for (var i = 0; i < 5; i++)
            {
                var row = dt.NewRow();
                row["id"] = i;
                row["score"] = i + 1;
                row["name"] = "name" + i;
                row["age"] = i + 10;
                dt.Rows.Add(row);
            }
            var conn = new SqlConnection(db.DBConn);
            //conn.Open();
            //var tran = conn.BeginTransaction();
            var sbc = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity | SqlBulkCopyOptions.KeepNulls | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.CheckConstraints | SqlBulkCopyOptions.UseInternalTransaction, null);
            sbc.BatchSize = 2;
            sbc.BulkCopyTimeout = 60 * 30;//半小时
            sbc.DestinationTableName = dt.TableName;

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sbc.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
            }


            conn.Open();
            sbc.WriteToServer(dt);
            conn.Close();
        }
    }
}
