﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using DBUtil;
using DBUtil.Provider.SqlServer;
using DotNetCommon.Extensions;
using Newtonsoft.Json;
using NUnit.Framework;
using Shouldly;

namespace Test.SqlServer
{
    [TestFixture]
    public sealed class CommonTests : TestBase
    {

        #region 测试RunInTransaction

        [Test]
        public void RunInTransactionTest()
        {
            //嵌套事务 事务隔离级别相同
            db.RunInTransaction(() =>
            {
                db.IsTran.ShouldBe(true);
                db.RunInTransaction(() =>
                {
                    db.IsTran.ShouldBe(true);
                });
            });

            //嵌套事务 内层隔离事务级别低
            db.RunInTransaction(() =>
            {
                db.IsTran.ShouldBe(true);
                db.RunInTransaction(() =>
                {
                    db.IsTran.ShouldBe(true);
                }, System.Data.IsolationLevel.ReadUncommitted);
            }, System.Data.IsolationLevel.ReadCommitted);

            var exp = Should.Throw<Exception>(() =>
              {
                  //内层隔离事务级别高,导致异常
                  db.RunInTransaction(() =>
                    {
                        db.IsTran.ShouldBe(true);
                        db.RunInTransaction(() =>
                        {
                            db.IsTran.ShouldBe(true);
                        }, System.Data.IsolationLevel.Serializable);
                    }, System.Data.IsolationLevel.ReadCommitted);
              });
            exp.Message.ShouldContain("已经在事务中,并且当前的事务隔离级别(ReadCommitted)小于新指定的隔离级别(Serializable)!");
        }

        [Test]
        public async Task RunInTransactionTestAsync()
        {
            //嵌套事务 事务隔离级别相同
            await db.RunInTransactionAsync(async () =>
            {
                db.IsTran.ShouldBe(true);
                await db.RunInTransactionAsync(async () =>
                {
                    db.IsTran.ShouldBe(true);
                });
            });

            //嵌套事务 内层隔离事务级别低
            await db.RunInTransactionAsync(async () =>
            {
                db.IsTran.ShouldBe(true);
                await db.RunInTransactionAsync(async () =>
                {
                    db.IsTran.ShouldBe(true);
                }, System.Data.IsolationLevel.ReadUncommitted);
            }, System.Data.IsolationLevel.ReadCommitted);

            var exp = Should.Throw<Exception>(async () =>
            {
                //内层隔离事务级别高,导致异常
                await db.RunInTransactionAsync(async () =>
                {
                    db.IsTran.ShouldBe(true);
                    await db.RunInTransactionAsync(async () =>
                    {
                        db.IsTran.ShouldBe(true);
                    }, System.Data.IsolationLevel.Serializable);
                }, System.Data.IsolationLevel.ReadCommitted);
            });
            exp.Message.ShouldContain("已经在事务中,并且当前的事务隔离级别(ReadCommitted)小于新指定的隔离级别(Serializable)!");
        }
        #endregion

        #region 测试OpenTest
        [Test]
        public void OpenTest()
        {
            var res = db.OpenTest();
            res.ShouldNotBeNull();
            res.Success.ShouldBe(true);
        }
        #endregion

        #region 测试sqlserver版本检查
        [Test]
        public void SqlServerDetectTest()
        {
            //测试机器为sqlserver2014
            Assert.IsTrue(db.IsSqlServer());
            Assert.IsTrue(db.IsSqlServerVersion2008R2Compatible());
            Assert.IsTrue(db.IsSqlServerVersion2012Compatible());
            Assert.IsTrue(db.IsSqlServerVersion2014Compatible());
            Assert.IsFalse(db.IsSqlServerVersion2016Compatible());
            Assert.IsFalse(db.IsSqlServerVersion2017Compatible());
            Assert.IsFalse(db.IsSqlServerVersion2019Compatible());
        }
        #endregion

        #region 测试创建新的DBAccess
        [Test]
        public void CreateNewDBTest()
        {
            var db2 = db.CreateNewDB();
            Assert.IsNotNull(db2);
            Assert.AreNotEqual(db, db2);
            Assert.AreEqual(db.DBType, db2.DBType);
            Assert.AreEqual(db.DBConn, db2.DBConn);
            Assert.AreNotEqual(db.Settings, db2.Settings);
        }
        #endregion

        #region 测试获取默认Schema
        [Test]
        public void DefaultSchemaTest()
        {
            var defaultSchema = db.DefaultSchema;
            Assert.IsTrue(defaultSchema == "dbo");
        }
        #endregion

        #region 测试表名解析

        [Test]
        public void AnalysisIdentifierNames()
        {
            var objectName = db.ParseObjectName(" dbo.table1  ");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[dbo].[table1]");

            objectName = db.ParseObjectName("testdb.dbo.table1");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1]");

            objectName = db.ParseObjectName("[dbo].[table1]");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[dbo].[table1]");

            objectName = db.ParseObjectName("[testdb].[dbo].[table1]");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1]");

            objectName = db.ParseObjectName("[testdb].dbo.[table1]");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1]");

            objectName = db.ParseObjectName("[testdb].\"dbo\".\"table1.lp[]\"");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1.lp[]");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1.lp[]");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1.lp[]]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1.lp[]");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1.lp[]]");
        }

        [Test]
        public void AnalysisIdentifierNamesWithSchema()
        {
            var objectName = db.ParseObjectName(" dbo.table1  ", "dbo");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[dbo].[table1]");

            objectName = db.ParseObjectName(" testdbo.table1  ", "[dbo]");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[dbo].[table1]");
        }

        [Test]
        public void AnalysisIdentifierNamesWithDataBase()
        {
            var objectName = db.ParseObjectName(" dbo.table1  ", null, "testdb");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1]");

            objectName = db.ParseObjectName(" db.testdbo.table1  ", "dbo", "testdb");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1]");
        }

        [Test]
        public void AnalysisIdentifierNamesWithDefaultDataBase()
        {
            var objectName = db.ParseObjectName(" dbo.table1  ", null, "testdb", null, "testdbdefault");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1]");

            objectName = db.ParseObjectName(" db.testdbo.table1  ", "dbo", "testdb", "schemadefault", "testdbdefault");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "dbo");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdb");
            Assert.IsTrue(objectName.NormalName == "dbo.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[dbo].[table1]");
            Assert.IsTrue(objectName.FullName == "testdb.dbo.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdb].[dbo].[table1]");

            objectName = db.ParseObjectName(" table1  ", null, null, "schemadefault", "testdbdefault");
            Assert.IsTrue(objectName != null);
            Assert.IsTrue(objectName.SchemaName == "schemadefault");
            Assert.IsTrue(objectName.Name == "table1");
            Assert.IsTrue(objectName.DataBaseName == "testdbdefault");
            Assert.IsTrue(objectName.NormalName == "schemadefault.table1");
            Assert.IsTrue(objectName.NormalNameQuoted == "[schemadefault].[table1]");
            Assert.IsTrue(objectName.FullName == "testdbdefault.schemadefault.table1");
            Assert.IsTrue(objectName.FullNameQuoted == "[testdbdefault].[schemadefault].[table1]");
        }
        #endregion
    }
}
