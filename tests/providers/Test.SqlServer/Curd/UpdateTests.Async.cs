﻿using DBUtil;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Threading.Tasks;

namespace Test.SqlServer.Curd.Update
{

    [TestFixture]
    public sealed class UpdateTestsAsync : TestBase
    {

        [Test]
        public async Task UpdateTest()
        {
            //准备表
            if (db.IsTableExist("test"))
            {
                db.Manage.DropTable("test");
            }
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            db.Insert("test", new { id = 1, name = "小明" }.ToDictionary(), new { id = 2, name = "小红" }.ToDictionary());
            //单个更新数据
            var res = await db.UpdateAsync("test", new { name = "小明2" }.ToDictionary(), "and id=1");
            Assert.IsTrue(res == 1);
            var model = db.SelectDictionary("select * from test");
            model["name"].ShouldBe("小明2");

            res = await db.UpdateAsync("test", new { name = new RawString("name+'2'") }.ToDictionary(), "and id=1");
            Assert.IsTrue(res == 1);
            model = db.SelectDictionary("select * from test");
            model["name"].ShouldBe("小明22");

            res = await db.UpdateAsync("test", new { name = "小明3" }.ToDictionary(), $"and id={db.ParaPrefix}p_id", new { p_id = 1 }.ToDictionary());
            Assert.IsTrue(res == 1);
            model = db.SelectDictionary("select * from test");
            model["name"].ShouldBe("小明3");

            res = await db.UpdateAsync("test", new { name = "大明" }.ToDictionary(), new { id = 1 }.ToDictionary());
            Assert.IsTrue(res == 1);
            model = db.SelectDictionary("select * from test");
            model["name"].ShouldBe("大明");

            res = await db.UpdateAsync("test", new { name = "好的", id = 3 }.ToDictionary(), " and id=1");
            Assert.IsTrue(res == 1);
            model = db.SelectDictionary("select * from test where id=3");
            model["name"].ShouldBe("好的");

            res = await db.UpdateAsync("test", new { name = "封装", id = 1 }.ToDictionary(), "and id=@id", new { id = 3 }.ToDictionary());
            Assert.IsTrue(res == 1);
            model = db.SelectDictionary("select * from test");
            model["name"].ShouldBe("封装");

            //批量更新数据
            res = await db.UpdateBatchAsync(new UpdateBatchItem[] {
                UpdateBatchItem.Create("test", new { name = "小明批量修改" }.ToDictionary(), "and id=1"),
                UpdateBatchItem.Create("test", new { name = "小红批量修改" }.ToDictionary(), "and id=2"),
                UpdateBatchItem.Create("test", new { name = new RawString("name+'haha'") }.ToDictionary(), "and id=2")
            });
            Assert.IsTrue(res == 3);
            var result = db.SelectModel<Person>("select * from test order by id desc");
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Name == "小红批量修改haha");

            res = await db.UpdateBatchAsync(new UpdateBatchItem[] {
                UpdateBatchItem.Create("test", new { name = "小红" }.ToDictionary(), new { id = 1 }.ToDictionary()),
                UpdateBatchItem.Create("test", new { name = "小明" }.ToDictionary(), new { id = 2 }.ToDictionary())
            });
            Assert.IsTrue(res == 2);

            res = await db.UpdateBatchAsync(new UpdateBatchItem[]{
                UpdateBatchItem.Create("test", new{name = "xiaohong",id = 4}.ToDictionary(), "and id=1")
            });
            Assert.IsTrue(res == 1);
            model = db.SelectDictionary("select * from test where id=4");
            model["name"].ShouldBe("xiaohong");

            res = await db.UpdateBatchAsync(new UpdateBatchItem[] {
                UpdateBatchItem.Create("test", new { name = "小明批量修改" }.ToDictionary(), "and id=4", null),
                UpdateBatchItem.Create("test", new { name = "小红批量修改" }.ToDictionary(), "and id=2", new { id = 2 }.ToDictionary()),
                UpdateBatchItem.Create("test", new { name = new RawString("name+'haha2'") }.ToDictionary(), " and id=2", null)
            });
            Assert.IsTrue(res == 3);
            result = db.SelectModel<Person>("select * from test order by id desc");
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Name == "小明批量修改");

            result = db.SelectModel<Person>("select * from test where id=2");
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Name == "小红批量修改haha2");
        }

        [Test]
        public async Task UpdateNullTest()
        {
            //准备表
            if (db.IsTableExist("test"))
            {
                db.Manage.DropTable("test");
            }
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            db.Insert("test", new { id = 1, name = "小明" }.ToDictionary(), new { id = 2, name = "小红" }.ToDictionary());
            var dic = new Dictionary<string, object>();
            dic.Add("name", null);
            await db.UpdateAsync("test", dic, "and id=1");
            var model = db.SelectDictionary("select * from test");
            model["id"].ShouldBe(1);
            model["name"].ShouldBe(DBNull.Value);
        }
    }
}
