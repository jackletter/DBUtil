﻿using DBUtil;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Test.SqlServer.Curd.InsertOrUpdate
{
    [TestFixture]
    public sealed class UpdateOrInsertTestsAsync : TestBase
    {

        [Test]
        public async Task Test()
        {
            //准备表
            if (db.IsTableExist("test"))
            {
                db.Manage.DropTable("test");
            }
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            db.Insert("test", new { id = 1, name = "小明" }.ToDictionary(), new { id = 2, name = "小红" }.ToDictionary());

            //基础调用
            var insertDic = new Dictionary<string, object>();
            insertDic.Add("id", 3);
            insertDic.Add("name", "小刚insert");
            var updateDic = new Dictionary<string, object>();
            updateDic.Add("name", "小刚update");
            var res = await db.UpdateOrInsertAsync("test", insertDic, updateDic, "and id=@id", new { id = 3 }.ToDictionary());
            Assert.IsTrue(res == 1);
            var person = db.SelectModel<Person>("select * from test where id=@id", new { id = 3 }.ToDictionary());
            Assert.IsTrue(person.Id == 3);
            Assert.IsTrue(person.Name == "小刚insert");
            res = await db.UpdateOrInsertAsync("test", insertDic, updateDic, "and id=@id", new { id = 3 }.ToDictionary());
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 3 }.ToDictionary());
            Assert.IsTrue(person.Id == 3);
            Assert.IsTrue(person.Name == "小刚update");

            //不使用参数
            updateDic["name"] = "小刚2";
            res = await db.UpdateOrInsertAsync("test", insertDic, updateDic, "and id=3", null);
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 3 }.ToDictionary());
            Assert.IsTrue(person.Id == 3);
            Assert.IsTrue(person.Name == "小刚2");

            //使用匿名对象作为参数
            updateDic["name"] = "小刚3";
            res = await db.UpdateOrInsertAsync("test", insertDic, updateDic, new { id = 3 }.ToDictionary());
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 3 }.ToDictionary());
            Assert.IsTrue(person.Id == 3);
            Assert.IsTrue(person.Name == "小刚3");

            //使用匿名对象插入
            var updateObj = new
            {
                name = "小军"
            }.ToDictionary();
            var insertObj = new
            {
                id = 4,
                name = "小军new"
            }.ToDictionary();
            res = await db.UpdateOrInsertAsync("test", insertObj, updateObj, new { id = 4 }.ToDictionary());
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 4 }.ToDictionary());
            Assert.IsTrue(person.Id == 4);
            Assert.IsTrue(person.Name == "小军new");
            res = await db.UpdateOrInsertAsync("test", insertObj, updateObj, new { id = 4 }.ToDictionary());
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 4 }.ToDictionary());
            Assert.IsTrue(person.Id == 4);
            Assert.IsTrue(person.Name == "小军");

            //使用匿名对象结合过滤sql插入
            updateObj = new
            {
                name = "张三"
            }.ToDictionary();
            insertObj = new
            {
                id = 5,
                name = "张三new"
            }.ToDictionary();
            res = await db.UpdateOrInsertAsync("test", insertObj, updateObj, "and id=5");
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 5 }.ToDictionary());
            Assert.IsTrue(person.Id == 5);
            Assert.IsTrue(person.Name == "张三new");
            res = await db.UpdateOrInsertAsync("test", insertObj, updateObj, new { id = 5 }.ToDictionary());
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 5 }.ToDictionary());
            Assert.IsTrue(person.Id == 5);
            Assert.IsTrue(person.Name == "张三");

            //混合使用匿名对象和过滤sql及参数化对象
            res = await db.UpdateOrInsertAsync("test", new { id = 6, name = "李四" }.ToDictionary(), new { name = "李四update" }.ToDictionary(), "and id=@id", new { id = 6 }.ToDictionary());
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 6 }.ToDictionary());
            Assert.IsTrue(person.Id == 6);
            Assert.IsTrue(person.Name == "李四");
            res = await db.UpdateOrInsertAsync("test", new { id = 6, name = "李四" }.ToDictionary(), new { name = "李四update" }.ToDictionary(), "and id=@id", new { id = 6 }.ToDictionary());
            Assert.IsTrue(res == 1);
            person = db.SelectModel<Person>("select * from test where id=@id", new { id = 6 }.ToDictionary());
            Assert.IsTrue(person.Id == 6);
            Assert.IsTrue(person.Name == "李四update");

        }
    }
}
