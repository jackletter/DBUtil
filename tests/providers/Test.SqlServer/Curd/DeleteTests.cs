﻿using DBUtil;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DotNetCommon.Extensions;
using NUnit.Framework;

namespace Test.SqlServer.Curd.Delete
{
    [TestFixture]
    public class DeleteTests : TestBase
    {
        [Test]
        public void DeleteTest()
        {
            //准备数据
            if (db.IsTableExist("test")) db.Manage.DropTable("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50),birth datetime)");
            var now = DateTime.Now;
            var res = db.Insert("test",
                new { id = 1, name = "小明", birth = now }.ToDictionary(),
                new { id = 2, name = "小花", birth = now }.ToDictionary(),
                new { id = 3, name = "小刚" }.ToDictionary());
            Assert.IsTrue(res == 3);
            //测试单个删除
            res = db.Delete("test", new { id = 2 }.ToDictionary());
            Assert.IsTrue(res == 1);
            res = db.Delete("test", "and id=3");
            Assert.IsTrue(res == 1);
            res = db.Delete("test", "and name=@name", new { name = "小明" }.ToDictionary());
            Assert.IsTrue(res == 1);
            res = db.SelectScalar<int>("select count(1) from test");
            Assert.IsTrue(res == 0);

            //准备数据
            res = db.Insert("test",
                new { id = 1, name = "小明", birth = now }.ToDictionary(),
                new { id = 2, name = "小花", birth = now }.ToDictionary(),
                new { id = 3, name = "小刚" }.ToDictionary());
            Assert.IsTrue(res == 3);

            res = db.Delete("test", "and id=@id", new { id = 1 }.ToDictionary());
            Assert.IsTrue(res == 1);

            //批量删除
            res = db.DeleteBatch(
                DeleteBatchItem.Create("test", new { id = 2 }.ToDictionary()),
                DeleteBatchItem.Create("test", new { id = 3 }.ToDictionary())
            );
            Assert.IsTrue(res == 2);
            res = db.Insert("test",
                new { id = 1, name = "小明", birth = now }.ToDictionary(),
                new { id = 2, name = "小花", birth = now }.ToDictionary(),
                new { id = 3, name = "小刚" }.ToDictionary()
            );
            Assert.IsTrue(res == 3);

            res = db.DeleteBatch(
                DeleteBatchItem.Create("test", new { id = 1 }.ToDictionary()),
                DeleteBatchItem.Create("test", new { id = 2 }.ToDictionary()),
                DeleteBatchItem.Create("test", new { id = 3 }.ToDictionary())
            );
            Assert.IsTrue(res == 3);

            res = db.Insert("test",
                new { id = 1, name = "小明", birth = now }.ToDictionary(),
                new { id = 2, name = "小花", birth = now }.ToDictionary(),
                new { id = 3, name = "小刚" }.ToDictionary()
            );
            Assert.IsTrue(res == 3);
            res = db.DeleteBatch(
                DeleteBatchItem.Create("test", new { id = 1 }.ToDictionary()),
                DeleteBatchItem.Create("test", new { id = 2 }.ToDictionary()),
                DeleteBatchItem.Create("test", new { id = 3 }.ToDictionary())
            );
            Assert.IsTrue(res == 3);

            res = db.Insert("test",
               new { id = 1, name = "小明", birth = now }.ToDictionary(),
               new { id = 2, name = "小花", birth = now }.ToDictionary(),
               new { id = 3, name = "小刚" }.ToDictionary());
            Assert.IsTrue(res == 3);
            res = db.DeleteBatch(
                DeleteBatchItem.Create("test", "and id=1"),
                DeleteBatchItem.Create("test", "and id=2")
            );
            Assert.IsTrue(res == 2);

            res = db.DeleteBatch(DeleteBatchItem.Create("test", "and id=@id", new { id = 3 }.ToDictionary()));
            Assert.IsTrue(res == 1);
            res = db.SelectScalar<int>("select count(1) from test");
            Assert.IsTrue(res == 0);
        }
    }
}
