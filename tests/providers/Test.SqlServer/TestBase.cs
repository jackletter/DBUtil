﻿using DBUtil;
using System;
using System.Text;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Microsoft.SqlServer.Types;

namespace Test.SqlServer
{
    [TestFixture]
    public class TestBase
    {
        protected DBAccess db = null;
        public TestBase()
        {
            DBUtil.DBFactory.AddDBSupport<DBUtil.Provider.SqlServer.SqlServerDBFactory>();
            //sqlserver 2014
            //db = DBFactory.CreateDB("SQLSERVER", "Data Source=192.168.0.19;Initial Catalog=test;User ID=sa;Password=123456;Pooling=True;Max Pool Size=20000;Encrypt=True; TrustServerCertificate=True;");
            db = DBFactory.CreateDB("SQLSERVER", "Data Source=192.168.252.129;Initial Catalog=test;User ID=sa;Password=123456;Pooling=True;Max Pool Size=20000;Encrypt=True; TrustServerCertificate=True;");
            //db = DBFactory.CreateDB("SQLSERVER", "Data Source=192.168.174.128;Initial Catalog=test;User ID=sa;Password=!ajoaiu5&%alsdijfaGHUh;Pooling=True;Max Pool Size=20000;");
            //db = DBFactory.CreateDB("SQLSERVER", "Data Source=172.16.0.16;Initial Catalog=test;User ID=sa;Password=Bc191202;Pooling=True;Max Pool Size=20000;");
            //db = DBFactory.CreateIDB("SQLSERVER", "Data Source=.;Initial Catalog=FileStreamDB;User ID=sa;Password=123456;");
            //db = DBFactory.CreateIDB("SQLSERVER", "Data Source=.;Initial Catalog=testpartition;User ID=sa;Password=123456;");
        }

        /// <summary>
        /// 准备数据库列类型
        /// </summary>
        public void PrepareColumnTypes()
        {
            db.Manage.DropTableIfExist("test");
            db.ExecuteSql(@"create table test(
    col_bigint bigint primary key,
    col_binary binary(50),
    col_bit bit,
    col_char char(50),
    col_date date,
    col_datetime datetime,
    col_datetime2 datetime2(7),
    col_datetimeoffset datetimeoffset,
    col_decimal decimal,
    col_float float,
    col_geography geography,
    col_geometry geometry,
    col_hierarchyid hierarchyid,
    col_image image,
    col_int int,
    col_money money,
    col_nchar nchar(50),
    col_ntext ntext,
    col_numeric numeric(10,2),
    col_nvarchar nvarchar(50),
    col_real real,
    col_smalldatetime smalldatetime,
    col_smallint smallint,
    col_smallmoney smallmoney,
    col_sql_variant sql_variant,
    col_text text,
    col_time time(7),
    col_timestamp timestamp,
    col_tinyint tinyint,
    col_uniqueidentifier uniqueidentifier,
    col_varbinary varbinary(max),
    col_varchar varchar(50),
    col_xml xml
)");
            var res = db.Insert("test",
                new
                {
                    col_bigint = 1,
                    col_binary = Encoding.UTF8.GetBytes("jack"),
                    col_bit = 0,
                    col_char = "jack haha",
                    col_date = DateTime.Now,
                    col_datetime = DateTime.Now,
                    col_datetime2 = DateTime.Now,
                    col_datetimeoffset = DateTime.Now,
                    col_decimal = 15.2,
                    col_float = 152.3,
                    col_geography = new RawString("geography::STGeomFromText('POLYGON ((-122.358 47.653, -122.348 47.649, -122.348 47.658, -122.358 47.658, -122.358 47.653))', 4326)"),
                    col_geometry = new RawString("geometry::STGeomFromText('POLYGON ((0 0, 150 0, 150 150, 0 150, 0 0))', 4326)"),
                    col_hierarchyid = "/1/2/",
                    col_image = Encoding.UTF8.GetBytes("abc"),
                    col_int = 15,
                    col_money = 30.23,
                    col_nchar = "kolp",
                    col_ntext = "小明啊",
                    col_numeric = 15.6542,
                    col_nvarchar = "和奇偶就氨基酸的",
                    col_real = 15.63,
                    col_smalldatetime = DateTime.Now,
                    col_smallint = 45,
                    col_smallmoney = 452.363,
                    col_sql_variant = "string in sql_variant",
                    col_text = "col_text",
                    col_time = DateTime.Now,
                    col_timestamp = new RawString("default"),
                    col_tinyint = 15,
                    col_uniqueidentifier = Guid.NewGuid().ToString(),
                    col_varbinary = Encoding.UTF8.GetBytes("jiko"),
                    col_varchar = "kosadas",
                    col_xml = "<person name='小明'>info</person>"
                }.ToDictionary());
            Assert.IsTrue(res == 1);
        }
    }

    public class Test
    {
        public long col_bigint { get; set; }
        public byte[] col_binary { get; set; }
        public bool? col_bit { get; set; }
        public string col_char { get; set; }
#if NET6_0_OR_GREATER
        public DateOnly? col_date { get; set; }
#else
        public DateTime? col_date { get; set; }
#endif
        public DateTime? col_datetime { get; set; }
        public DateTime? col_datetime2 { get; set; }
        public DateTime? col_datetimeoffset { get; set; }
        public decimal? col_decimal { get; set; }
        public double? col_float { get; set; }
        public SqlGeography col_geography { get; set; }
        public SqlGeometry col_geometry { get; set; }
        public SqlHierarchyId? col_hierarchyid { get; set; }
        public byte[] col_image { get; set; }
        public int? col_int { get; set; }
        public decimal? col_money { get; set; }
        public string col_nchar { get; set; }
        public string col_ntext { get; set; }
        public double? col_numeric { get; set; }
        public string col_nvarchar { get; set; }
        public double? col_real { get; set; }
        public DateTime? col_smalldatetime { get; set; }
        public short? col_smallint { get; set; }
        public decimal? col_smallmoney { get; set; }
        public object col_sql_variant { get; set; }
        public string col_text { get; set; }
#if NET6_0_OR_GREATER
        public TimeOnly? col_time { get; set; }
#else
        //public TimeSpan? col_time { get; set; }
        public DateTime? col_time { get; set; }
#endif
        public byte[] col_timestamp { get; set; }
        public sbyte? col_tinyint { get; set; }
        public Guid? col_uniqueidentifier { get; set; }
        public string col_varbinary { get; set; }
        public string col_varchar { get; set; }
        public string col_xml { get; set; }
    }
}
