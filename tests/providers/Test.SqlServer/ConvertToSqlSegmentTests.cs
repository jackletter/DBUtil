﻿using DBUtil;
using DotNetCommon.Data;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.SqlServer
{
    [TestFixture]
    public sealed class ConvertToSqlSegmentTests : TestBase
    {
        public enum EnumTest
        {
            Open, Close, Dispose
        }

        [Test]
        public void ConvertType()
        {
            Result<string> res = null;

            res = db.ConvertToSqlSegment(null);
            res.Data.ShouldBe("null");

            res = db.ConvertToSqlSegment(DBNull.Value);
            res.Data.ShouldBe("null");

            #region 基础数字类型
            byte col_byte = 26;
            res = db.ConvertToSqlSegment(col_byte);
            res.Data.ShouldBe("26");

            sbyte col_sbyte = 26;
            res = db.ConvertToSqlSegment(col_sbyte);
            res.Data.ShouldBe("26");

            short col_short = 26;
            res = db.ConvertToSqlSegment(col_short);
            res.Data.ShouldBe("26");

            ushort col_ushort = 26;
            res = db.ConvertToSqlSegment(col_ushort);
            res.Data.ShouldBe("26");

            int col_int = 26;
            res = db.ConvertToSqlSegment(col_int);
            res.Data.ShouldBe("26");

            uint col_uint = 26;
            res = db.ConvertToSqlSegment(col_uint);
            res.Data.ShouldBe("26");

            long col_long = 26;
            res = db.ConvertToSqlSegment(col_long);
            res.Data.ShouldBe("26");

            ulong col_ulong = 26;
            res = db.ConvertToSqlSegment(col_ulong);
            res.Data.ShouldBe("26");

            float col_float = 26.1234f;
            res = db.ConvertToSqlSegment(col_float);
            res.Data.ShouldBe("26.1234");

            double col_double = 26.1234;
            res = db.ConvertToSqlSegment(col_double);
            res.Data.ShouldBe("26.1234");

            decimal col_decimal = 26.1234m;
            res = db.ConvertToSqlSegment(col_decimal);
            res.Data.ShouldBe("26.1234");
            #endregion

            char col_char = 'c';
            res = db.ConvertToSqlSegment(col_char);
            res.Data.ShouldBe("'c'");

            string col_string = "小明";
            res = db.ConvertToSqlSegment(col_string);
            res.Data.ShouldBe("'小明'");

            bool col_bool = true;
            res = db.ConvertToSqlSegment(col_bool);
            res.Data.ShouldBe("1");
            col_bool = false;
            res = db.ConvertToSqlSegment(col_bool);
            res.Data.ShouldBe("0");

            RawString col_rawString = new RawString("count(score)");
            res = db.ConvertToSqlSegment(col_rawString);
            res.Data.ShouldBe("count(score)");

            EnumTest col_enum = EnumTest.Dispose;
            res = db.ConvertToSqlSegment(col_enum);
            res.Data.ShouldBe("2");

            Guid col_guid = new Guid("a66cc049-edee-426d-9aa2-dea498149b8d");
            res = db.ConvertToSqlSegment(col_guid);
            res.Data.ShouldBe("'a66cc049-edee-426d-9aa2-dea498149b8d'");

            DateTime col_datetime = DateTime.Parse("2022-02-03 18:50:12.123");
            res = db.ConvertToSqlSegment(col_datetime);
            res.Data.ShouldBe("'2022-02-03 18:50:12.123'");

            DateTimeOffset col_dateTimeOffset = DateTimeOffset.Parse("2022-02-03 18:50:12.123 +08:00");
            res = db.ConvertToSqlSegment(col_dateTimeOffset);
            res.Data.ShouldBe("'2022-02-03 18:50:12.123 +08:00'");

            TimeSpan col_timeSpan = new TimeSpan(0, 1, 2, 3, 123);
            res = db.ConvertToSqlSegment(col_timeSpan);
            res.Data.ShouldBe("'01:02:03.1230000'");

            byte[] col_byteArr = Encoding.UTF8.GetBytes("小明");
            res = db.ConvertToSqlSegment(col_byteArr);
            res.Data.ShouldBe("0x" + col_byteArr.ToHex());
        }
    }
}
