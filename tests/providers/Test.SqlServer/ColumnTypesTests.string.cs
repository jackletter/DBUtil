﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.SqlServer
{
    [TestFixture]
    public class ColumnTypesTestsString : TestBase
    {
        [Test]
        public void Test()
        {
            db.Manage.DropTableIfExist("test");
            db.ExecuteSql(@"create table test(t_char10 char(10),t_varchar10 varchar(10),t_nchar10 nchar(10),t_nvarchar10 nvarchar(10))");
            db.ExecuteSql("insert into test(t_char10,t_varchar10,t_nchar10,t_nvarchar10) values('abc   ','abc   ','abc   ','abc   ')");
            db.SelectDataReader(reader =>
            {
                reader.Read();
                var t_char10 = reader.GetString(0);
                var t_varchar10 = reader.GetString(1);
                var t_nchar10 = reader.GetString(2);
                var t_nvarchar10 = reader.GetString(3);

                t_char10.ShouldBe("abc       ");
                t_varchar10.ShouldBe("abc   ");
                t_nchar10.ShouldBe("abc       ");
                t_nvarchar10.ShouldBe("abc   ");
            }, "select * from test");
        }

    }
}
