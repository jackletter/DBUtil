﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotNetCommon.Extensions;

namespace Test.SqlServer
{
    /// <summary>
    /// 分布式锁测试
    /// </summary>
    [TestFixture]
    public sealed class DistributeLockTests : TestBase
    {
        internal class MoneyEntity
        {
            public int Id { get; set; }
            public long Money { get; set; }
        }

        #region 同步锁
        [Test]
        public void TestLock()
        {
            #region 准备数据
            if (db.IsTableExist("test"))
            {
                db.Manage.DropTable("test");
            }
            db.ExecuteSql(@"
create table test(
 id int primary key,
 money bigint
)");
            db.ExecuteSql("insert into test values(1,100)");
            #endregion
            var lockstr = "book_update_1001";

            var tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Run(() =>
                {
                    db.RunInLock(lockstr, () =>
                    {
                        Console.WriteLine($"获取到锁:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}-{Thread.CurrentThread.ManagedThreadId}");
                        var model = db.SelectModel<MoneyEntity>("select * from test");
                        long money = model.Money;
                        Thread.Sleep(500);
                        money += 50;
                        db.ExecuteSql($"update test set money={money}");
                    });
                    Console.WriteLine($"已释放锁:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}-{Thread.CurrentThread.ManagedThreadId}");
                }));
            }
            Task.WaitAll(tasks.ToArray());
            var row = db.SelectModel<MoneyEntity>("select * from test");
            long money2 = row.Money;
            Assert.IsTrue(money2 == 600);
        }

        [Test]
        public void TestGetLock()
        {
            var lockstr = "lockstring_test";
            var autoreset = new AutoResetEvent(false);
            var task = Task.Run(() =>
              {

                  db.RunInLock(lockstr, () =>
                  {
                      // 锁和事务的叠加使用
                      //db.RunInTransaction(() =>
                      //{
                      autoreset.Set();
                      Thread.Sleep(3000);
                      //});
                  });
              });
            autoreset.WaitOne();
            var dics = db.SelectDictionaryList("select * from sys.dm_tran_locks where resource_type='APPLICATION' and resource_description like '%dbutil:runinlock%' and resource_database_id = DB_ID()");
            dics.Count.ShouldBeGreaterThanOrEqualTo(1);
            dics.Where(i => i["resource_description"].ToString().Contains("dbutil:runinlock:lockstring_test")).Count().ShouldBe(1);

            task.Wait();

            //验证是否已释放
            dics = db.SelectDictionaryList("select * from sys.dm_tran_locks where resource_type='APPLICATION' and resource_description like '%dbutil:runinlock%' and resource_database_id = DB_ID()");
            dics.Where(i => i["resource_description"].ToString().Contains("dbutil:runinlock:lockstring_test")).Count().ShouldBe(0);
        }

        [Test]
        public void TestLockReturn()
        {
            var lockstr = "lockstring_test";
            int counter = 0;
            var list = new List<int>();
            var tasks = new List<Task>();
            for (var i = 0; i < 10; i++)
            {
                var task = Task.Run(() =>
                {
                    var result = db.RunInLock(lockstr, () =>
                    {
                        counter = counter + 1;
                        return counter;
                    });
                    list.Add(result);
                });
                tasks.Add(task);
            }
            Task.WaitAll(tasks.ToArray());

            list.ShouldBe(new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        }

        /// <summary>
        /// 测试超时失败情况
        /// </summary>
        [Test]
        public void TestTimeout()
        {
            var lockstr = "lockstring_test";

            var task1 = Task.Run(async () =>
              {
                  db.RunInLock(lockstr, () =>
                  {
                      Console.WriteLine($"task1 获取锁: {DateTime.Now.ToCommonStampString()}");
                      Thread.Sleep(5 * 1000);
                  });
                  Console.WriteLine($"task1 释放锁: {DateTime.Now.ToCommonStampString()}");
              });
            var task2 = Task.Run(async () =>
            {
                await Task.Delay(1000);
                try
                {
                    db.RunInLock(lockstr, () =>
                    {
                        Console.WriteLine($"task2 获取锁: {DateTime.Now.ToCommonStampString()}");
                        Thread.Sleep(5000);
                    }, 2);
                    Console.WriteLine($"task2 释放锁: {DateTime.Now.ToCommonStampString()}");
                }
                catch (Exception ex)
                {
                    ex.Message.ShouldContain("获取锁[lockstring_test]超时");
                }
            });
            Task.WaitAll(task1, task2);
        }
        #endregion

        #region 异步锁
        [Test]
        public async Task TestLockAsync()
        {
            #region 准备数据
            if (db.IsTableExist("test"))
            {
                db.Manage.DropTable("test");
            }
            db.ExecuteSql(@"
create table test(
 id int primary key,
 money bigint
)");
            db.ExecuteSql("insert into test values(1,100)");
            #endregion
            var lockstr = "book_update_1001";

            var tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Run(async () =>
                {
                    await db.RunInLockAsync(lockstr, async () =>
                    {
                        Console.WriteLine($"获取到锁:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}-{Thread.CurrentThread.ManagedThreadId}");
                        var model = await db.SelectModelAsync<MoneyEntity>("select * from test");
                        long money = model.Money;
                        await Task.Delay(500);
                        money += 50;
                        await db.ExecuteSqlAsync($"update test set money={money}");
                    });
                    Console.WriteLine($"已释放锁:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}-{Thread.CurrentThread.ManagedThreadId}");
                }));
            }
            Task.WaitAll(tasks.ToArray());
            var row = db.SelectModel<MoneyEntity>("select * from test");
            long money2 = row.Money;
            Assert.IsTrue(money2 == 600);
        }

        [Test]
        public async Task TestGetLockAsync()
        {
            var lockstr = "lockstring_test";
            var autoreset = new AutoResetEvent(false);
            var task = Task.Run(async () =>
             {
                 await db.RunInLockAsync(lockstr, async () =>
                 {
                     autoreset.Set();
                     await Task.Delay(3000);
                 });
             });
            autoreset.WaitOne();
            var dics = db.SelectDictionaryList("select * from sys.dm_tran_locks where resource_type='APPLICATION' and resource_description like '%dbutil:runinlock%' and resource_database_id = DB_ID()");
            dics.Count.ShouldBeGreaterThanOrEqualTo(1);
            dics.Where(i => i["resource_description"].ToString().Contains("dbutil:runinlock:lockstring_test")).Count().ShouldBe(1);

            task.Wait();

            //验证是否已释放
            dics = db.SelectDictionaryList("select * from sys.dm_tran_locks where resource_type='APPLICATION' and resource_description like '%dbutil:runinlock%' and resource_database_id = DB_ID()");
            dics.Where(i => i["resource_description"].ToString().Contains("dbutil:runinlock:lockstring_test")).Count().ShouldBe(0);
        }

        [Test]
        public async Task TestLockReturnAsync()
        {
            var lockstr = "lockstring_test";
            int counter = 0;
            var list = new List<int>();
            var tasks = new List<Task>();
            for (var i = 0; i < 10; i++)
            {
                var task = Task.Run(async () =>
                {
                    var result = await db.RunInLockAsync(lockstr, async () =>
                    {
                        counter = counter + 1;
                        return counter;
                    });
                    list.Add(result);
                });
                tasks.Add(task);
            }
            Task.WaitAll(tasks.ToArray());

            list.ShouldBe(new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        }

        /// <summary>
        /// 测试超时失败情况
        /// </summary>
        [Test]
        public void TestTimeoutAsync()
        {
            var lockstr = "lockstring_test";

            var task1 = Task.Run(async () =>
            {
                await db.RunInLockAsync(lockstr, async () =>
                {
                    Console.WriteLine($"task1 获取锁: {DateTime.Now.ToCommonStampString()}");
                    Thread.Sleep(5 * 1000);
                });
                Console.WriteLine($"task1 释放锁: {DateTime.Now.ToCommonStampString()}");
            });
            var task2 = Task.Run(async () =>
            {
                await Task.Delay(1000);
                try
                {
                    await db.RunInLockAsync(lockstr, async () =>
                    {
                        Console.WriteLine($"task2 获取锁: {DateTime.Now.ToCommonStampString()}");
                        Thread.Sleep(5000);
                    }, 2);
                    Console.WriteLine($"task2 释放锁: {DateTime.Now.ToCommonStampString()}");
                }
                catch (Exception ex)
                {
                    ex.Message.ShouldContain("获取锁[lockstring_test]超时");
                }
            });
            Task.WaitAll(task1, task2);
        }
        #endregion
    }
}
