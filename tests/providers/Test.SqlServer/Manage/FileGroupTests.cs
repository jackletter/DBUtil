﻿using DBUtil.Provider.SqlServer.MetaData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.SqlServer.Manage
{
    [TestFixture]
    internal class FileGroupTests : TestBase
    {
        [Test]
        public void ShowTableFileGroup()
        {
            //自行准备数据库的文件组
            //假设当前数据库已有文件组"ByIdGroup1"
            db.Manage.DropTableIfExist("test2");
            db.ExecuteSql(@"create table test2(id int,name varchar(50) not null) on ByIdGroup1");
            var detail = db.Manage.ShowTableDetail("test2", "dbo") as SqlServerTable;
            Assert.IsTrue(detail.DataSpaceName == "ByIdGroup1");
            Assert.IsTrue(detail.Indexes.Count == 1);
            Assert.IsTrue(detail.Indexes[0].IndexType == "HEAP");
            Assert.IsTrue(((SqlServerIndex)detail.Indexes[0]).DataSpaceName == "ByIdGroup1");
            db.ExecuteSql("alter table test2 add constraint  pri_test2_name primary key (name)");
            detail = db.Manage.ShowTableDetail("test2", "dbo") as SqlServerTable;
            Assert.IsTrue(detail.DataSpaceName == "ByIdGroup1");
            Assert.IsTrue(detail.Indexes.Count == 1);
            Assert.IsTrue(detail.Indexes[0].IndexType == "CLUSTERED");
            Assert.IsTrue(((SqlServerIndex)detail.Indexes[0]).DataSpaceName == "ByIdGroup1");
            //测试获取当前表是否已分区
            var table = db.Manage.ShowTableDetail("ordersheet", "dbo");

            int i = 0;

        }
    }
}
