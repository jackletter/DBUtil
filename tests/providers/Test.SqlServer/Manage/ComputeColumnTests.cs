﻿using DBUtil.Provider.SqlServer.MetaData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.SqlServer.Manage
{
    [TestFixture]
    internal class ComputeColumnTests : TestBase
    {
        [Test]
        public void ComputedColumnTest()
        {
            db.Manage.DropTableIfExist("testcom");
            db.ExecuteSql(@"create table testcom(
        id int primary key,
        name varchar(50),
        firstname varchar(50),
        lastname varchar(50),
        fullname as firstname+lastname persisted,
        leng as len(name)
        )");
            var table = db.Manage.ShowTableDetail("testcom", "dbo");
            var col_fullname = table.Columns.FirstOrDefault(col => col.Name == "fullname") as SqlServerColumn;
            Assert.IsNotNull(col_fullname);
            Assert.IsTrue(col_fullname.IsComputed);
            Assert.IsTrue(col_fullname.ComputedDefinition == "([firstname]+[lastname])");
            Assert.IsNotNull(col_fullname.IsPersisted);
            Assert.IsTrue(col_fullname.IsPersisted.Value);
        }
    }
}
