﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Test.SqlServer.Manage
{
    [TestFixture]
    internal class GeneInsertSqlTests : TestBase
    {
        [Test]
        public void Test()
        {
            PrepareColumnTypes();
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = Path.Combine(path, "out.sql");
            var count = db.Manage.GenerateInsertSqlFile("dbo", "test", path);
            Console.WriteLine($"生成的sql文件:{path}");
            Assert.IsTrue(count == 1);
        }

        [Test]
        public void GeneInsertSqlStringTest()
        {
            db.Manage.DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            db.Insert("test", new { id = 1, name = "i'am xiaoming,what's it?" }.ToDictionary());
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = Path.Combine(path, "out.sql");
            db.Manage.GenerateInsertSqlFile("dbo", "test", path);
        }
    }
}
