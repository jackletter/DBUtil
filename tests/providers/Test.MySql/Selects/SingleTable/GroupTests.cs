﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.MySql.Selects.SingleTable
{
    [TestFixture]
    internal class GroupTests : TestBase
    {
        [Table("t_user")]
        public class TUserEntity
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int Age { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }

            [Column("create_time")]
            public DateTime? CreateTime { get; set; }
        }

        [Test]
        public void Test()
        {
            string sql = "";
            sql = db.Select<TUserEntity>()
                .Where(i => i.Id > 10)
                .GroupBy(i => i.Name)
                .Having(i => i.Length > 1)
                .OrderBy(i => i.Key)
                .Limit(10)
                .ToSqlList(i => new { Name = i.Key, Count = i.Length });
            sql.ShouldBe(@"select t.name `Name`,count(1) `Count`
from t_user t
where t.id > 10
group by t.name
having count(1) > 1
order by t.name
limit 10;");

            sql = db.Select<TUserEntity>()
                .Where(i => i.Id > 10)
                .GroupBy(i => new { i.Name, i.Age })
                .Having(i => i.Length > 1)
                .OrderBy(i => i.Key.Age)
                .Limit(10)
                .ToSqlList(i => new { Name = i.Key.Name, Age = i.Key.Age, Count = i.Length });
            sql.ShouldBe(@"select t.name `Name`,t.age `Age`,count(1) `Count`
from t_user t
where t.id > 10
group by t.name,t.age
having count(1) > 1
order by t.age
limit 10;");

            sql = db.Select<TUserEntity>()
                .GroupBy(i => i.Name)
                .Having(i => i.Length > 1 && i.Max(k => k.Age) > 40)
                .ToSqlList(i => new { Name = i.Key, Count = i.Length, MaxAge = i.Max(j => j.Age) });
            sql.ShouldBe(@"select t.name `Name`,count(1) `Count`,max(t.age) `MaxAge`
from t_user t
group by t.name
having (count(1) > 1) and (max(t.age) > 40);");

        }

        [Test]
        public void Test2()
        {
            string sql = string.Empty;
            sql = db.Select<TUserEntity>()
                .Where(i => i.Id > 10)
                .GroupBy(i => i.Name)
                .Having(i => i.Length > 1)
                .OrderBy(i => i.Key)
                .Limit(10)
                .ToSqlPage(i => new { Name = i.Key, Count = i.Length });
            sql.ShouldBe(@"select t.name `Name`,count(1) `Count`
from t_user t
where t.id > 10
group by t.name
having count(1) > 1
order by t.name
limit 10;
select count(1)
from t_user t
where t.id > 10
group by t.name
having count(1) > 1
order by t.name;");

            sql = db.Select<TUserEntity>()
                .Where(i => i.Id > 10)
                .GroupBy(i => i.Name)
                .Having(i => i.Length > 1)
                .OrderBy(i => i.Key)
                .Limit(10)
                .ToSqlCount();
            sql.ShouldBe(@"select count(1)
from t_user t
where t.id > 10
group by t.name
having count(1) > 1
order by t.name
limit 10;");

        }

        public class PersonBasicType
        {
            public int Id { get; set; }
            public int ClassId { get; set; }
            public short Age_short { get; set; }
            public short? Age_short_null { get; set; }
            public ushort Age_ushort { get; set; }
            public ushort? Age_ushort_null { get; set; }
            public int Age_int { get; set; }
            public int? Age_int_null { get; set; }
            public uint Age_uint { get; set; }
            public uint? Age_uint_null { get; set; }
            public long Age_long { get; set; }
            public long? Age_long_null { get; set; }
            public ulong Age_ulong { get; set; }
            public ulong? Age_ulong_null { get; set; }
            public float Age_float { get; set; }
            public float? Age_float_null { get; set; }
            public double Age_double { get; set; }
            public double? Age_double_null { get; set; }
            public decimal Age_decimal { get; set; }
            public decimal? Age_decimal_null { get; set; }

            public DateTime Age_DateTime { get; set; }
            public DateTime? Age_DateTime_null { get; set; }
            public DateOnly Age_DateOnly { get; set; }
            public DateOnly? Age_DateOnly_null { get; set; }
            public TimeSpan Age_TimeSpan { get; set; }
            public TimeSpan? Age_TimeSpan_null { get; set; }
            public DateTimeOffset Age_DateTimeOffset { get; set; }
            public DateTimeOffset? Age_DateTimeOffset_null { get; set; }
        }
        [Test]
        public void TestAggregateMax()
        {
            string sql = "";
            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Max(j => j.Age_short) > 10
                    && i.Max(j => j.Age_ushort) > 10
                    && i.Max(j => j.Age_int) > 10
                    && i.Max(j => j.Age_uint) > 10
                    && i.Max(j => j.Age_long) > 10
                    && i.Max(j => j.Age_ulong) > 10
                    && i.Max(j => j.Age_float) > 10
                    && i.Max(j => j.Age_double) > 10
                    && i.Max(j => j.Age_decimal) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Max(j => j.Age_short),
                    Age_ushort = i.Max(j => j.Age_ushort),
                    Age_int = i.Max(j => j.Age_int),
                    Age_uint = i.Max(j => j.Age_uint),
                    Age_long = i.Max(j => j.Age_long),
                    Age_ulong = i.Max(j => j.Age_ulong),
                    Age_float = i.Max(j => j.Age_float),
                    Age_double = i.Max(j => j.Age_double),
                    Age_decimal = i.Max(j => j.Age_decimal),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,max(t.`Age_short`) `Age_short`,max(t.`Age_ushort`) `Age_ushort`,max(t.`Age_int`) `Age_int`,max(t.`Age_uint`) `Age_uint`,max(t.`Age_long`) `Age_long`,max(t.`Age_ulong`) `Age_ulong`,max(t.`Age_float`) `Age_float`,max(t.`Age_double`) `Age_double`,max(t.`Age_decimal`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (max(t.`Age_short`) > 10)) and (max(t.`Age_ushort`) > 10)) and (max(t.`Age_int`) > 10)) and (max(t.`Age_uint`) > 10)) and (max(t.`Age_long`) > 10)) and (max(t.`Age_ulong`) > 10)) and (max(t.`Age_float`) > 10)) and (max(t.`Age_double`) > 10)) and (max(t.`Age_decimal`) > 10);");

            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Max(j => j.Age_short_null.Value) > 10
                    && i.Max(j => j.Age_ushort_null.Value) > 10
                    && i.Max(j => j.Age_int_null.Value) > 10
                    && i.Max(j => j.Age_uint_null.Value) > 10
                    && i.Max(j => j.Age_long_null.Value) > 10
                    && i.Max(j => j.Age_ulong_null.Value) > 10
                    && i.Max(j => j.Age_float_null.Value) > 10
                    && i.Max(j => j.Age_double_null.Value) > 10
                    && i.Max(j => j.Age_decimal_null.Value) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Max(j => j.Age_short_null.Value),
                    Age_ushort = i.Max(j => j.Age_ushort_null.Value),
                    Age_int = i.Max(j => j.Age_int_null.Value),
                    Age_uint = i.Max(j => j.Age_uint_null.Value),
                    Age_long = i.Max(j => j.Age_long_null.Value),
                    Age_ulong = i.Max(j => j.Age_ulong_null.Value),
                    Age_float = i.Max(j => j.Age_float_null.Value),
                    Age_double = i.Max(j => j.Age_double_null.Value),
                    Age_decimal = i.Max(j => j.Age_decimal_null.Value),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,max(t.`Age_short_null`) `Age_short`,max(t.`Age_ushort_null`) `Age_ushort`,max(t.`Age_int_null`) `Age_int`,max(t.`Age_uint_null`) `Age_uint`,max(t.`Age_long_null`) `Age_long`,max(t.`Age_ulong_null`) `Age_ulong`,max(t.`Age_float_null`) `Age_float`,max(t.`Age_double_null`) `Age_double`,max(t.`Age_decimal_null`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (max(t.`Age_short_null`) > 10)) and (max(t.`Age_ushort_null`) > 10)) and (max(t.`Age_int_null`) > 10)) and (max(t.`Age_uint_null`) > 10)) and (max(t.`Age_long_null`) > 10)) and (max(t.`Age_ulong_null`) > 10)) and (max(t.`Age_float_null`) > 10)) and (max(t.`Age_double_null`) > 10)) and (max(t.`Age_decimal_null`) > 10);");
        }

        [Test]
        public void TestAggregateMin()
        {
            string sql = "";
            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Min(j => j.Age_short) > 10
                    && i.Min(j => j.Age_ushort) > 10
                    && i.Min(j => j.Age_int) > 10
                    && i.Min(j => j.Age_uint) > 10
                    && i.Min(j => j.Age_long) > 10
                    && i.Min(j => j.Age_ulong) > 10
                    && i.Min(j => j.Age_float) > 10
                    && i.Min(j => j.Age_double) > 10
                    && i.Min(j => j.Age_decimal) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Min(j => j.Age_short),
                    Age_ushort = i.Min(j => j.Age_ushort),
                    Age_int = i.Min(j => j.Age_int),
                    Age_uint = i.Min(j => j.Age_uint),
                    Age_long = i.Min(j => j.Age_long),
                    Age_ulong = i.Min(j => j.Age_ulong),
                    Age_float = i.Min(j => j.Age_float),
                    Age_double = i.Min(j => j.Age_double),
                    Age_decimal = i.Min(j => j.Age_decimal),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,min(t.`Age_short`) `Age_short`,min(t.`Age_ushort`) `Age_ushort`,min(t.`Age_int`) `Age_int`,min(t.`Age_uint`) `Age_uint`,min(t.`Age_long`) `Age_long`,min(t.`Age_ulong`) `Age_ulong`,min(t.`Age_float`) `Age_float`,min(t.`Age_double`) `Age_double`,min(t.`Age_decimal`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (min(t.`Age_short`) > 10)) and (min(t.`Age_ushort`) > 10)) and (min(t.`Age_int`) > 10)) and (min(t.`Age_uint`) > 10)) and (min(t.`Age_long`) > 10)) and (min(t.`Age_ulong`) > 10)) and (min(t.`Age_float`) > 10)) and (min(t.`Age_double`) > 10)) and (min(t.`Age_decimal`) > 10);");

            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Min(j => j.Age_short_null.Value) > 10
                    && i.Min(j => j.Age_ushort_null.Value) > 10
                    && i.Min(j => j.Age_int_null.Value) > 10
                    && i.Min(j => j.Age_uint_null.Value) > 10
                    && i.Min(j => j.Age_long_null.Value) > 10
                    && i.Min(j => j.Age_ulong_null.Value) > 10
                    && i.Min(j => j.Age_float_null.Value) > 10
                    && i.Min(j => j.Age_double_null.Value) > 10
                    && i.Min(j => j.Age_decimal_null.Value) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Min(j => j.Age_short_null.Value),
                    Age_ushort = i.Min(j => j.Age_ushort_null.Value),
                    Age_int = i.Min(j => j.Age_int_null.Value),
                    Age_uint = i.Min(j => j.Age_uint_null.Value),
                    Age_long = i.Min(j => j.Age_long_null.Value),
                    Age_ulong = i.Min(j => j.Age_ulong_null.Value),
                    Age_float = i.Min(j => j.Age_float_null.Value),
                    Age_double = i.Min(j => j.Age_double_null.Value),
                    Age_decimal = i.Min(j => j.Age_decimal_null.Value),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,min(t.`Age_short_null`) `Age_short`,min(t.`Age_ushort_null`) `Age_ushort`,min(t.`Age_int_null`) `Age_int`,min(t.`Age_uint_null`) `Age_uint`,min(t.`Age_long_null`) `Age_long`,min(t.`Age_ulong_null`) `Age_ulong`,min(t.`Age_float_null`) `Age_float`,min(t.`Age_double_null`) `Age_double`,min(t.`Age_decimal_null`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (min(t.`Age_short_null`) > 10)) and (min(t.`Age_ushort_null`) > 10)) and (min(t.`Age_int_null`) > 10)) and (min(t.`Age_uint_null`) > 10)) and (min(t.`Age_long_null`) > 10)) and (min(t.`Age_ulong_null`) > 10)) and (min(t.`Age_float_null`) > 10)) and (min(t.`Age_double_null`) > 10)) and (min(t.`Age_decimal_null`) > 10);");
        }

        [Test]
        public void TestAggregateSum()
        {
            string sql = "";
            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Sum(j => j.Age_short) > 10
                    && i.Sum(j => j.Age_ushort) > 10
                    && i.Sum(j => j.Age_int) > 10
                    && i.Sum(j => j.Age_uint) > 10
                    && i.Sum(j => j.Age_long) > 10
                    && i.Sum(j => j.Age_ulong) > 10
                    && i.Sum(j => j.Age_float) > 10
                    && i.Sum(j => j.Age_double) > 10
                    && i.Sum(j => j.Age_decimal) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Sum(j => j.Age_short),
                    Age_ushort = i.Sum(j => j.Age_ushort),
                    Age_int = i.Sum(j => j.Age_int),
                    Age_uint = i.Sum(j => j.Age_uint),
                    Age_long = i.Sum(j => j.Age_long),
                    Age_ulong = i.Sum(j => j.Age_ulong),
                    Age_float = i.Sum(j => j.Age_float),
                    Age_double = i.Sum(j => j.Age_double),
                    Age_decimal = i.Sum(j => j.Age_decimal),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,sum(t.`Age_short`) `Age_short`,sum(t.`Age_ushort`) `Age_ushort`,sum(t.`Age_int`) `Age_int`,sum(t.`Age_uint`) `Age_uint`,sum(t.`Age_long`) `Age_long`,sum(t.`Age_ulong`) `Age_ulong`,sum(t.`Age_float`) `Age_float`,sum(t.`Age_double`) `Age_double`,sum(t.`Age_decimal`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (sum(t.`Age_short`) > 10)) and (sum(t.`Age_ushort`) > 10)) and (sum(t.`Age_int`) > 10)) and (sum(t.`Age_uint`) > 10)) and (sum(t.`Age_long`) > 10)) and (sum(t.`Age_ulong`) > 10)) and (sum(t.`Age_float`) > 10)) and (sum(t.`Age_double`) > 10)) and (sum(t.`Age_decimal`) > 10);");

            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Sum(j => j.Age_short_null.Value) > 10
                    && i.Sum(j => j.Age_ushort_null.Value) > 10
                    && i.Sum(j => j.Age_int_null.Value) > 10
                    && i.Sum(j => j.Age_uint_null.Value) > 10
                    && i.Sum(j => j.Age_long_null.Value) > 10
                    && i.Sum(j => j.Age_ulong_null.Value) > 10
                    && i.Sum(j => j.Age_float_null.Value) > 10
                    && i.Sum(j => j.Age_double_null.Value) > 10
                    && i.Sum(j => j.Age_decimal_null.Value) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Sum(j => j.Age_short_null.Value),
                    Age_ushort = i.Sum(j => j.Age_ushort_null.Value),
                    Age_int = i.Sum(j => j.Age_int_null.Value),
                    Age_uint = i.Sum(j => j.Age_uint_null.Value),
                    Age_long = i.Sum(j => j.Age_long_null.Value),
                    Age_ulong = i.Sum(j => j.Age_ulong_null.Value),
                    Age_float = i.Sum(j => j.Age_float_null.Value),
                    Age_double = i.Sum(j => j.Age_double_null.Value),
                    Age_decimal = i.Sum(j => j.Age_decimal_null.Value),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,sum(t.`Age_short_null`) `Age_short`,sum(t.`Age_ushort_null`) `Age_ushort`,sum(t.`Age_int_null`) `Age_int`,sum(t.`Age_uint_null`) `Age_uint`,sum(t.`Age_long_null`) `Age_long`,sum(t.`Age_ulong_null`) `Age_ulong`,sum(t.`Age_float_null`) `Age_float`,sum(t.`Age_double_null`) `Age_double`,sum(t.`Age_decimal_null`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (sum(t.`Age_short_null`) > 10)) and (sum(t.`Age_ushort_null`) > 10)) and (sum(t.`Age_int_null`) > 10)) and (sum(t.`Age_uint_null`) > 10)) and (sum(t.`Age_long_null`) > 10)) and (sum(t.`Age_ulong_null`) > 10)) and (sum(t.`Age_float_null`) > 10)) and (sum(t.`Age_double_null`) > 10)) and (sum(t.`Age_decimal_null`) > 10);");
        }

        [Test]
        public void TestAggregateAvg()
        {
            string sql = "";
            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Avg(j => j.Age_short) > 10
                    && i.Avg(j => j.Age_ushort) > 10
                    && i.Avg(j => j.Age_int) > 10
                    && i.Avg(j => j.Age_uint) > 10
                    && i.Avg(j => j.Age_long) > 10
                    && i.Avg(j => j.Age_ulong) > 10
                    && i.Avg(j => j.Age_float) > 10
                    && i.Avg(j => j.Age_double) > 10
                    && i.Avg(j => j.Age_decimal) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Avg(j => j.Age_short),
                    Age_ushort = i.Avg(j => j.Age_ushort),
                    Age_int = i.Avg(j => j.Age_int),
                    Age_uint = i.Avg(j => j.Age_uint),
                    Age_long = i.Avg(j => j.Age_long),
                    Age_ulong = i.Avg(j => j.Age_ulong),
                    Age_float = i.Avg(j => j.Age_float),
                    Age_double = i.Avg(j => j.Age_double),
                    Age_decimal = i.Avg(j => j.Age_decimal),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,avg(t.`Age_short`) `Age_short`,avg(t.`Age_ushort`) `Age_ushort`,avg(t.`Age_int`) `Age_int`,avg(t.`Age_uint`) `Age_uint`,avg(t.`Age_long`) `Age_long`,avg(t.`Age_ulong`) `Age_ulong`,avg(t.`Age_float`) `Age_float`,avg(t.`Age_double`) `Age_double`,avg(t.`Age_decimal`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (avg(t.`Age_short`) > 10)) and (avg(t.`Age_ushort`) > 10)) and (avg(t.`Age_int`) > 10)) and (avg(t.`Age_uint`) > 10)) and (avg(t.`Age_long`) > 10)) and (avg(t.`Age_ulong`) > 10)) and (avg(t.`Age_float`) > 10)) and (avg(t.`Age_double`) > 10)) and (avg(t.`Age_decimal`) > 10);");

            sql = db.Select<PersonBasicType>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Length > 1
                    && i.Avg(j => j.Age_short_null.Value) > 10
                    && i.Avg(j => j.Age_ushort_null.Value) > 10
                    && i.Avg(j => j.Age_int_null.Value) > 10
                    && i.Avg(j => j.Age_uint_null.Value) > 10
                    && i.Avg(j => j.Age_long_null.Value) > 10
                    && i.Avg(j => j.Age_ulong_null.Value) > 10
                    && i.Avg(j => j.Age_float_null.Value) > 10
                    && i.Avg(j => j.Age_double_null.Value) > 10
                    && i.Avg(j => j.Age_decimal_null.Value) > 10
                    )
                .ToSqlList(i => new
                {
                    Name = i.Key,
                    Count = i.Length,
                    Age_short = i.Avg(j => j.Age_short_null.Value),
                    Age_ushort = i.Avg(j => j.Age_ushort_null.Value),
                    Age_int = i.Avg(j => j.Age_int_null.Value),
                    Age_uint = i.Avg(j => j.Age_uint_null.Value),
                    Age_long = i.Avg(j => j.Age_long_null.Value),
                    Age_ulong = i.Avg(j => j.Age_ulong_null.Value),
                    Age_float = i.Avg(j => j.Age_float_null.Value),
                    Age_double = i.Avg(j => j.Age_double_null.Value),
                    Age_decimal = i.Avg(j => j.Age_decimal_null.Value),
                });
            sql.ShouldBe(@"select t.`ClassId` `Name`,count(1) `Count`,avg(t.`Age_short_null`) `Age_short`,avg(t.`Age_ushort_null`) `Age_ushort`,avg(t.`Age_int_null`) `Age_int`,avg(t.`Age_uint_null`) `Age_uint`,avg(t.`Age_long_null`) `Age_long`,avg(t.`Age_ulong_null`) `Age_ulong`,avg(t.`Age_float_null`) `Age_float`,avg(t.`Age_double_null`) `Age_double`,avg(t.`Age_decimal_null`) `Age_decimal`
from `PersonBasicType` t
group by t.`ClassId`
having (((((((((count(1) > 1) and (avg(t.`Age_short_null`) > 10)) and (avg(t.`Age_ushort_null`) > 10)) and (avg(t.`Age_int_null`) > 10)) and (avg(t.`Age_uint_null`) > 10)) and (avg(t.`Age_long_null`) > 10)) and (avg(t.`Age_ulong_null`) > 10)) and (avg(t.`Age_float_null`) > 10)) and (avg(t.`Age_double_null`) > 10)) and (avg(t.`Age_decimal_null`) > 10);");
        }

        [Test]
        public void TestDateFormat()
        {
            var sql = db.Select<PersonBasicType>()
                  .GroupBy(i => i.ClassId)
                  .Having(i => i.Max(i => i.Age_DateOnly) > DateOnly.Parse("2023-01-02"))
                  .ToSqlList(i => new
                  {
                      ClassId = i.Key,
                      Age_DateOnly_Max = i.Max(j => j.Age_DateOnly),
                      Age_DateOnly_Min = i.Min(j => j.Age_DateOnly),
                      Age_DateOnly_Max_Null = i.Max(j => j.Age_DateOnly_null.Value),
                      Age_DateOnly_Min_Null = i.Min(j => j.Age_DateOnly_null.Value),

                      Age_DateTime_Max = i.Max(j => j.Age_DateTime),
                      Age_DateTime_Min = i.Min(j => j.Age_DateTime),
                      Age_DateTime_Max_Null = i.Max(j => j.Age_DateTime_null.Value),
                      Age_DateTime_Min_Null = i.Min(j => j.Age_DateTime_null.Value),

                      Age_DateTimeOffset_Max = i.Max(j => j.Age_DateTimeOffset),
                      Age_DateTimeOffset_Min = i.Min(j => j.Age_DateTimeOffset),
                      Age_DateTimeOffset_Max_Null = i.Max(j => j.Age_DateTimeOffset_null.Value),
                      Age_DateTimeOffset_Min_Null = i.Min(j => j.Age_DateTimeOffset_null.Value),

                      Age_TimeSpan_Max = i.Max(j => j.Age_TimeSpan),
                      Age_TimeSpan_Min = i.Min(j => j.Age_TimeSpan),
                      Age_TimeSpan_Max_Null = i.Max(j => j.Age_TimeSpan_null.Value),
                      Age_TimeSpan_Min_Null = i.Min(j => j.Age_TimeSpan_null.Value),
                  });
            sql.ShouldBe(@"select t.`ClassId`,max(t.`Age_DateOnly`) `Age_DateOnly_Max`,min(t.`Age_DateOnly`) `Age_DateOnly_Min`,max(t.`Age_DateOnly_null`) `Age_DateOnly_Max_Null`,min(t.`Age_DateOnly_null`) `Age_DateOnly_Min_Null`,max(t.`Age_DateTime`) `Age_DateTime_Max`,min(t.`Age_DateTime`) `Age_DateTime_Min`,max(t.`Age_DateTime_null`) `Age_DateTime_Max_Null`,min(t.`Age_DateTime_null`) `Age_DateTime_Min_Null`,max(t.`Age_DateTimeOffset`) `Age_DateTimeOffset_Max`,min(t.`Age_DateTimeOffset`) `Age_DateTimeOffset_Min`,max(t.`Age_DateTimeOffset_null`) `Age_DateTimeOffset_Max_Null`,min(t.`Age_DateTimeOffset_null`) `Age_DateTimeOffset_Min_Null`,max(t.`Age_TimeSpan`) `Age_TimeSpan_Max`,min(t.`Age_TimeSpan`) `Age_TimeSpan_Min`,max(t.`Age_TimeSpan_null`) `Age_TimeSpan_Max_Null`,min(t.`Age_TimeSpan_null`) `Age_TimeSpan_Min_Null`
from `PersonBasicType` t
group by t.`ClassId`
having max(t.`Age_DateOnly`) > '2023-01-02';");
        }

        public class PersonTestAggregateString
        {
            public int Id { get; set; }
            public int ClassId { get; set; }
            [Column("name")]
            public string Name { get; set; }
        }
        [Test]
        public void TestAggregateString()
        {
            string sql = "";
            sql = db.Select<PersonTestAggregateString>()
                .GroupBy(i => i.ClassId)
                .Having(i => i.Join(j => j.Name, ",", j => j.Id, true).Length > 0)
                .ToSqlList(i => new
                {
                    ClassId = i.Key,
                    Name1 = i.Join(j => j.Name, ","),
                    Name2 = i.Join(j => j.Name, ",", j => j.Id, true),
                    Name3 = i.Join(j => j.Name, ",", j => new { j.Id, j.Name }, true),
                    Name4 = i.Join(j => j.Name, ",", j => new { j.Id, j.Name }, true, true),
                });
            sql.ShouldBe("""
                select t.`ClassId`,group_concat(t.name separator ',') `Name1`,group_concat(t.name order by t.`Id` desc separator ',') `Name2`,group_concat(t.name order by t.`Id`,t.name desc separator ',') `Name3`,group_concat(distinct t.name order by t.`Id`,t.name desc separator ',') `Name4`
                from `PersonTestAggregateString` t
                group by t.`ClassId`
                having (length((group_concat(t.name order by t.`Id` desc separator ',')))) > 0;
                """);
        }

    }
}
