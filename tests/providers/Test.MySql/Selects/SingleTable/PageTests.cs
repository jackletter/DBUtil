﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.MySql.Selects.SingleTable
{
    [TestFixture]
    internal class PageTests : TestBase
    {
        [Table("t_person")]
        public class Person
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("score")]
            public float Score { get; set; }
        }

        [Test]
        public void Test()
        {
            string sql = string.Empty;
            sql = db.Select<Person>().Where(i => i.Score > 90).ToSqlPage();
            sql.ShouldBe(@"select t.id `Id`,t.name `Name`,t.score `Score`
from t_person t
where t.score > 90;
select count(1)
from t_person t
where t.score > 90;");

            sql = db.Select<Person>().Where(i => i.Score > 90).ToSqlPage(i => new
            {
                Id = i.Id,
                Name = i.Name,
                Score = i.Score,
                Rank = i.Score > 90
            });
            sql.ShouldBe(@"select t.id `Id`,t.name `Name`,t.score `Score`,t.score > 90 `Rank`
from t_person t
where t.score > 90;
select count(1)
from t_person t
where t.score > 90;");

            sql = db.Select<Person>().Limit(11, 10).Where(i => i.Score > 90).ToSqlPage();
            sql.ShouldBe(@"select t.id `Id`,t.name `Name`,t.score `Score`
from t_person t
where t.score > 90
limit 11,10;
select count(1)
from t_person t
where t.score > 90;");
            sql = db.Select<Person>().Limit(11, 10).Where(i => i.Score > 90).ToSqlPage(i => new
            {
                Id = i.Id,
                Name = i.Name,
                Score = i.Score,
                Rank = i.Score > 90
            });
            sql.ShouldBe(@"select t.id `Id`,t.name `Name`,t.score `Score`,t.score > 90 `Rank`
from t_person t
where t.score > 90
limit 11,10;
select count(1)
from t_person t
where t.score > 90;");
        }
    }
}
