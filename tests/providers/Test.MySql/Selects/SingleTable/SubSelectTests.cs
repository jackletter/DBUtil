﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects.SingleTable
{
    [TestFixture]
    internal class SubSelectTests : TestBase
    {
        [Table("test")]
        public class PersonEntity
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }

        [Test]
        public void SimpleSubSelect()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,name varchar(50),age int)");
            db.ExecuteSql("""
                insert into test(name,age) values
                ('jim',20),
                ('tom',30),
                ('imi',18),
                ('lisa',24);
                """);
            var select = db.Select<PersonEntity>().Alias("t")
                .Where(i => i.Age > db.Select<PersonEntity>().Alias("t2").Where(i => i.Id == 1).FirstOrDefault(i => i.Age));
            var sql = select.ToSqlList();
            sql.ShouldBe("""
                select t.`Id`,t.`Name`,t.`Age`
                from test t
                where t.`Age` > (select t2.`Age`
                from test t2
                where t2.`Id` = 1
                limit 1);
                """);
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("[{\"Id\":2,\"Name\":\"tom\",\"Age\":30},{\"Id\":4,\"Name\":\"lisa\",\"Age\":24}]");
        }

        [Test]
        public void ComplexSubSelect()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,name varchar(50),age int)");
            db.ExecuteSql("""
                insert into test(name,age) values
                ('jim',20),
                ('tom',30),
                ('imi',18),
                ('lisa',24);
                """);
            var select = db.Select<PersonEntity>().Alias("t")
                .Where(i => i.Age > db.Select<PersonEntity>().Alias("t2").Where(i => i.Id == 1).FirstOrDefault(i => i.Age))
                .Where(i => i.Name.Length > db.Select<PersonEntity>().Alias("t3").Where(i => i.Id == 1).FirstOrDefault(i => i.Name).Length);
            var sql = select.ToSqlList();
            sql.ShouldBe("""
                select t.`Id`,t.`Name`,t.`Age`
                from test t
                where (t.`Age` > (select t2.`Age`
                from test t2
                where t2.`Id` = 1
                limit 1)) and (length(t.`Name`) > (length((select t3.`Name`
                from test t3
                where t3.`Id` = 1
                limit 1))));
                """);
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("[{\"Id\":4,\"Name\":\"lisa\",\"Age\":24}]");
        }

        [Test]
        public void ThreeNestedTest()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,name varchar(50),age int)");
            db.ExecuteSql("""
                insert into test(name,age) values
                ('jim',20),
                ('tom',30),
                ('imi',18),
                ('lisa',24);
                """);
            var select = db.Select<PersonEntity>().Alias("t")
                .Where(i => i.Age >
                    db.Select<PersonEntity>().Alias("t2").Where(i => i.Name ==
                        db.Select<PersonEntity>().Alias("t3").Where(i => i.Id == 1)
                        .FirstOrDefault(i => i.Name))
                    .FirstOrDefault(i => i.Age));
            var sql = select.ToSqlList();
            sql.ShouldBe("""
                select t.`Id`,t.`Name`,t.`Age`
                from test t
                where t.`Age` > ((select t2.`Age`
                from test t2
                where t2.`Name` = (select t3.`Name`
                from test t3
                where t3.`Id` = 1
                limit 1)
                limit 1));
                """);
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("[{\"Id\":2,\"Name\":\"tom\",\"Age\":30},{\"Id\":4,\"Name\":\"lisa\",\"Age\":24}]");
        }

        [Test]
        public void TestSubList()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,name varchar(50),age int)");
            db.ExecuteSql("""
                insert into test(name,age) values
                ('jim',18),
                ('tom',30),
                ('imi',18),
                ('lilei',18),
                ('tony',23),
                ('lisa',24);
                """);
            var select = db.Select<PersonEntity>().Where(i =>
                db.Select<PersonEntity>().Alias("t2").Where(i => i.Id >= 3).ToList(i => i.Age)
                .Contains(i.Age));
            var sql = select.ToSqlList();
            sql.ShouldBe("""
                select t.`Id`,t.`Name`,t.`Age`
                from test t
                where t.`Age` in (select t2.`Age`
                from test t2
                where t2.`Id` >= 3);
                """);
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("[{\"Id\":1,\"Name\":\"jim\",\"Age\":18},{\"Id\":3,\"Name\":\"imi\",\"Age\":18},{\"Id\":4,\"Name\":\"lilei\",\"Age\":18},{\"Id\":5,\"Name\":\"tony\",\"Age\":23},{\"Id\":6,\"Name\":\"lisa\",\"Age\":24}]");
        }
    }
}
