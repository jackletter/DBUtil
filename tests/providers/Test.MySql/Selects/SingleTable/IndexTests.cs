﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects.SingleTable
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Table("t_user")]
        public class TUserEntity
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int Age { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }

            [Column("create_time")]
            public DateTime? CreateTime { get; set; }
        }

        public class TUserDto : TUserEntity
        {
            public bool IsOld { get; set; }
        }
        [Test]
        public void TestToSql()
        {
            string sql = string.Empty;
            //简单场景
            sql = db.Select<TUserEntity>().Distinct().Where(i => i.Name.StartsWith("王")).ToSqlList();
            sql.ShouldBe(@"select distinct t.id `Id`,t.name `Name`,t.age `Age`,t.birth `Birth`,t.create_time `CreateTime`
from t_user t
where t.name like '王%';");

            sql = db.Select<TUserEntity>().Distinct().ToSqlList();
            sql.ShouldBe(@"select distinct t.id `Id`,t.name `Name`,t.age `Age`,t.birth `Birth`,t.create_time `CreateTime`
from t_user t;");

            sql = db.Select<TUserEntity>().Alias("a").WithSql("select t.id,t.name,20,t.birth,t.create_time from t_user t where t.id>10 union all select t.id,t.name,20,t.birth,t.create_time from t_user t where t.id<=10").ToSqlList();
            sql.ShouldBe("""
                select a.id `Id`,a.name `Name`,a.age `Age`,a.birth `Birth`,a.create_time `CreateTime`
                from (select t.id,t.name,20,t.birth,t.create_time from t_user t where t.id>10 union all select t.id,t.name,20,t.birth,t.create_time from t_user t where t.id<=10);
                """);

            sql = db.Select<TUserEntity>().Where(i => i.Id > 10).OrderByDesc(i => i.Age).ToSqlList();
            sql.ShouldBe(@"select t.id `Id`,t.name `Name`,t.age `Age`,t.birth `Birth`,t.create_time `CreateTime`
from t_user t
where t.id > 10
order by t.age desc;");


            sql = db.Select<TUserEntity>().Where(i => i.Id > 10).OrderByDesc(i => i.Age).Limit(10).ToSqlList();
            sql.ShouldBe(@"select t.id `Id`,t.name `Name`,t.age `Age`,t.birth `Birth`,t.create_time `CreateTime`
from t_user t
where t.id > 10
order by t.age desc
limit 10;");

            sql = db.Select<TUserEntity>().Where(i => i.Id > 10).OrderByDesc(i => i.Age).Page(1, 10).ToSqlList();
            sql.ShouldBe(@"select t.id `Id`,t.name `Name`,t.age `Age`,t.birth `Birth`,t.create_time `CreateTime`
from t_user t
where t.id > 10
order by t.age desc
limit 10;");

            sql = db.Select<TUserEntity>().Where(i => i.Id > 10).OrderBy(i => i.Id).OrderByDesc(i => new { i.Name, i.Age }).Order("create_time desc,birth").ToSqlList();
        }

        [Test]
        public void TestExe()
        {
            DropTableIfExist("t_user");
            db.ExecuteSql(@"create table t_user(id int primary key,name varchar(50),age int,birth datetime,create_time datetime)");
            db.ExecuteSql(@"insert into t_user(id,name,age,birth,create_time) values
(1,'小明1',18,'2005-01-01','2023-04-05 07:52:00'),
(2,'小明2',19,'2004-01-01','2023-04-05 07:52:00'),
(3,'小明3',21,'2002-01-01','2023-04-05 07:52:00'),
(4,'小明4',20,'2003-01-01','2023-04-05 07:52:00'),
(5,'小明5',19,'2004-01-01','2023-04-05 07:52:00');");

            var users = db.Select<TUserEntity>().ToList();
            var user = db.Select<TUserEntity>().FirstOrDefault();

            users = db.Select<TUserEntity>().Where(i => i.Name.StartsWith("小")).Distinct().ToList();
            users = db.Select<TUserEntity>().Where(i => i.Name.StartsWith("小")).OrderBy(i => new { i.Age }).ToList();
        }

        [Test]
        public void TestSelectExp()
        {
            string sql = string.Empty;
            //简单场景
            //sql = db.Select<TUserEntity>().Where(i => i.Name.StartsWith("王")).ToSql(i => new { Id = i.Id, Name = i.Name });

            sql = db.Select<TUserEntity>().Where(i => i.Name.StartsWith("王")).ToSqlList(i => new TUserDto { Id = i.Id, Name = i.Name, IsOld = i.Age > 18 });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age > 18 `IsOld`
                from t_user t
                where t.name like '王%';
                """);
        }

        [Test]
        public void TestSelectDtoExe()
        {
            DropTableIfExist("t_user");
            db.ExecuteSql(@"create table t_user(id int primary key,name varchar(50),age int,birth datetime,create_time datetime)");
            db.ExecuteSql(@"insert into t_user(id,name,age,birth,create_time) values
(1,'小明1',18,'2005-01-01','2023-04-05 07:52:00'),
(2,'小明2',19,'2004-01-01','2023-04-05 07:52:00'),
(3,'小明3',21,'2002-01-01','2023-04-05 07:52:00'),
(4,'小明4',20,'2003-01-01','2023-04-05 07:52:00'),
(5,'小明5',19,'2004-01-01','2023-04-05 07:52:00');");

            //var list = db.Select<TUserEntity>().Where(i => i.Name.StartsWith("小")).ToList(i => new TUserDto { Id = i.Id, Name = i.Name, IsOld = i.Age > 18 });

            //var list2 = db.Select<TUserEntity>().Distinct().Where(i => i.Name.StartsWith("小")).ToList(i => i.Name);
            var list2 = db.Select<TUserEntity>().Distinct().Where(i => i.Name.StartsWith("小")).ToList(i => new { i.Id, i.Name });
        }

        [Test]
        public void TestSelectDtoSql()
        {
            var sql = db.Select<TUserEntity>().ToSqlList(i => new { i.Id, i.Name });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`
                from t_user t;
                """);

            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`
                from t_user t;
                """);
        }
    }
}
