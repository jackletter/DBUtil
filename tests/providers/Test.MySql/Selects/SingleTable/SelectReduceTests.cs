﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.MySql.Selects.SingleTable
{
    [TestFixture]
    internal class SelectReduceTests : TestBase
    {
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }

            public DateTime? Birth { get; set; }
        }

        [Test]
        public void Test()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),age int, Birth datetime)");

            var sql = db.Select<Person>().Where(i => i.Id == 1).ToSqlList(i => new { Id = i.Id, Name = i.Name, Age = i.Age > 0 ? i.Age : 0, Birth = i.Birth == null ? DateTime.MinValue : i.Birth });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,if((t.age > 0),t.age,0) `Age`,if((t.`Birth` is null),'0001-01-01',t.`Birth`) `Birth`
                from test t
                where t.id = 1;
                """);
        }
    }
}
