﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.MySql.Selects.SingleTable
{
    [TestFixture]
    internal class AggregateTests : TestBase
    {
        [Table("t_person")]
        public class Person
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("age")]
            public int Age { get; set; }
            [Column("class_id")]
            public int ClassId { get; set; }
            [Column("name")]
            public string Name { get; set; }
        }

        [Test]
        public void Test()
        {
            DropTableIfExist("t_person");
            db.ExecuteSql(@"create table t_person(id int auto_increment primary key,name varchar(50),age int,class_id int)");

            db.Insert<Person>().SetEntity(new[]
            {
                new Person{ClassId = 1,Age=40,Name="小明"},
                new Person{ClassId = 2,Age=50,Name="小明50"},
                new Person{ClassId = 2,Age=40,Name="小明40"},
                new Person{ClassId = 2,Age=30,Name="小明30"},
                new Person{ClassId = 1,Age=50,Name="小明"},

            }).ExecuteAffrows();
            string sql = string.Empty;
            sql = db.Select<Person>().MaxSql(i => i.Id);
            sql.ShouldBe($"select max(t.id){Environment.NewLine}from t_person t;");
            db.Select<Person>().Max(i => i.Id).ShouldBe(5);

            sql = db.Select<Person>().MinSql(i => i.Id);
            sql.ShouldBe($"select min(t.id){Environment.NewLine}from t_person t;");
            db.Select<Person>().Min(i => i.Id).ShouldBe(1);

            sql = db.Select<Person>().SumSql(i => i.Id);
            sql.ShouldBe($"select sum(t.id){Environment.NewLine}from t_person t;");
            db.Select<Person>().Sum(i => i.Id).ShouldBe(15);

            sql = db.Select<Person>().AvgSql(i => i.Id);
            sql.ShouldBe($"select avg(t.id){Environment.NewLine}from t_person t;");
            db.Select<Person>().Avg(i => i.Id).ShouldBe(3);

            sql = db.Select<Person>().GroupBy(i => i.ClassId).Having(i => i.Key != 1 && i.Max(i => i.Age) > 40).ToSqlList(i => new
            {
                ClassId = i.Key,
                MaxAge = i.Max(k => k.Age),
                MinAge = i.Min(k => k.Age),
                AvgAge = i.Avg(k => k.Age),
                SumAge = i.Sum(k => k.Age),
                Name = i.Join(i => i.Name, ",", i => i.Age, true)
            });
            sql.ShouldBe(@"select t.class_id `ClassId`,max(t.age) `MaxAge`,min(t.age) `MinAge`,avg(t.age) `AvgAge`,sum(t.age) `SumAge`,group_concat(t.name order by t.age desc separator ',') `Name`
from t_person t
group by t.class_id
having (t.class_id <> 1) and (max(t.age) > 40);");
            var li = db.Select<Person>().GroupBy(i => i.ClassId).Having(i => i.Key != 1 && i.Max(i => i.Age) > 40).ToList(i => new
            {
                ClassId = i.Key,
                MaxAge = i.Max(k => k.Age),
                MinAge = i.Min(k => k.Age),
                AvgAge = i.Avg(k => k.Age),
                SumAge = i.Sum(k => k.Age),
                Name = i.Join(i => i.Name, ",", i => i.Age, true)
            });
            li[0].ClassId.ShouldBe(2);
            li[0].MaxAge.ShouldBe(50);
            li[0].MinAge.ShouldBe(30);
            li[0].AvgAge.ShouldBe(40);
            li[0].SumAge.ShouldBe(120);
            li[0].Name.ShouldBe("小明50,小明40,小明30");
        }
    }
}
