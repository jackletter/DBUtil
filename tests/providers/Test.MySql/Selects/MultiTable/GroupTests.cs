﻿using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects.MultiTable
{
    [TestFixture]
    internal class GroupTests : TestBase
    {
        #region model
        [Table("t_teacher")]
        public class TeacherEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("age")]
            public int? Age { get; set; }
        }

        [Table("t_student")]
        public class StudentEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("age")]
            public int? Age { get; set; }
            [Column("score")]
            public float? Score { get; set; }
            [Column("teacher_id")]
            public int TeacherId { get; set; }
        }
        #endregion

        [Test]
        public void Test()
        {
            var sql = db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .GroupBy((p, s) => s.Score)
                    .Having(g => g.Length > 2)
                    .ToSqlList(g => new { score = g.Key, count = g.Length });
            sql.ShouldBe("""
                select t2.score,count(1) `count`
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                group by t2.score
                having count(1) > 2;
                """);
        }
    }
}
