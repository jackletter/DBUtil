﻿using System;
using NUnit.Framework;
using Shouldly;

namespace Test.MySql.Selects.MultiTable
{
    [TestFixture]
    public class IMultiTableTests : TestBase
    {
        #region model
        public class Teacher
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public class Student
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int TeacherId { get; set; }
        }
        #endregion

        [Test]
        public void TestSimple()
        {
            var sql = db.Select<Student>().LeftJoin<Teacher>((t, t2) => t.TeacherId == t2.Id)
                .Where(i => i.t2.Name == "tom")
                .ToSqlList((t, t2) => new { t, t2 });
            sql.ShouldBe("""
                select t.`Id` `t.Id`,t.`Name` `t.Name`,t.`TeacherId` `t.TeacherId`,t2.`Id` `t2.Id`,t2.`Name` `t2.Name`
                from `Student` t
                    left join `Teacher` t2 on t.`TeacherId` = t2.`Id`
                where t2.`Name` = 'tom';
                """);
        }

        [Test]
        public void TestSimple2()
        {
            var sql = db.Select<Student>().LeftJoin<Teacher>((t, t2) => t.TeacherId == t2.Id).LeftJoin<Student>((t, t2, tp) => tp.Id == t2.Id)
                .Where(i => i.t2.Name == "tom" && i.t3.Id == 1)
                .ToSqlList((t, t2, tt) => new { t, t2 });
            sql.ShouldBe("""
                select t.`Id` `t.Id`,t.`Name` `t.Name`,t.`TeacherId` `t.TeacherId`,t2.`Id` `t2.Id`,t2.`Name` `t2.Name`
                from `Student` t
                    left join `Teacher` t2 on t.`TeacherId` = t2.`Id`
                    left join `Student` t3 on t3.`Id` = t2.`Id`
                where (t2.`Name` = 'tom') and (t3.`Id` = 1);
                """);
        }

        [Test]
        public void TestComplex()
        {
            var sql = db.Select<Student>().LeftJoin<Teacher>((t, t2) => t.TeacherId == t2.Id)
                .Where(i => i.t2.Name == i.t.Name && i.t.Name == db.Select<Student>().Alias("stu").Where(p => p.Id == 1).FirstOrDefault(p2 => p2.Name))
                .ToSqlList((t, t2) => new { t, t2 });
            sql.ShouldBe("""
                select t.`Id` `t.Id`,t.`Name` `t.Name`,t.`TeacherId` `t.TeacherId`,t2.`Id` `t2.Id`,t2.`Name` `t2.Name`
                from `Student` t
                    left join `Teacher` t2 on t.`TeacherId` = t2.`Id`
                where (t2.`Name` = t.`Name`) and (t.`Name` = (select stu.`Name`
                from `Student` stu
                where stu.`Id` = 1
                limit 1));
                """);
        }

        [Test]
        public void TestSelectClause()
        {
            var sql = db.Select<Student>().LeftJoin<Teacher>((t, t2) => t.TeacherId == t2.Id)
                .ToSqlList(i => new { i.t, i.t2 });
            sql.ShouldBe("""
                select t.`Id` `t.Id`,t.`Name` `t.Name`,t.`TeacherId` `t.TeacherId`,t2.`Id` `t2.Id`,t2.`Name` `t2.Name`
                from `Student` t
                    left join `Teacher` t2 on t.`TeacherId` = t2.`Id`;
                """);

            var sql2 = db.Select<Student>().LeftJoin<Teacher>((t, t2) => t.TeacherId == t2.Id)
                .ToSqlList(i => i.t2.Name);
            sql2.ShouldBe("""
                select t2.`Name`
                from `Student` t
                    left join `Teacher` t2 on t.`TeacherId` = t2.`Id`;
                """);

            var sql3 = db.Select<Student>().LeftJoin<Teacher>((t, t2) => t.TeacherId == t2.Id)
                .ToSqlList(i => new { name = i.t2.Name, id = i.t.Id });
            sql3.ShouldBe("""
                select t2.`Name` `name`,t.`Id` `id`
                from `Student` t
                    left join `Teacher` t2 on t.`TeacherId` = t2.`Id`;
                """);
        }
    }
}
