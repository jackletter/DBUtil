﻿using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects.MultiTable
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        #region model
        [Table("t_teacher")]
        public class TeacherEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("age")]
            public int? Age { get; set; }
        }

        [Table("t_student")]
        public class StudentEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("age")]
            public int? Age { get; set; }
            [Column("score")]
            public float? Score { get; set; }
            [Column("teacher_id")]
            public int TeacherId { get; set; }
        }
        #endregion

        [Test]
        public void LeftJoin()
        {
            var sql = db.Select<StudentEntity>().LeftJoin<TeacherEntity>((s, t) => s.TeacherId == t.Id).ToSqlList((s, t) => new
            {
                Id = t.Id,
                Name = t.Name,
                Age = t.Age,
                StudentName = s.Name
            });
            sql.ShouldBe("""
                select t2.id `Id`,t2.name `Name`,t2.age `Age`,t.name `StudentName`
                from t_student t
                    left join t_teacher t2 on t.teacher_id = t2.id;
                """);
        }

        [Test]
        public void RightJoin()
        {
            var sql = db.Select<StudentEntity>().RightJoin<TeacherEntity>((s, t) => s.TeacherId == t.Id).ToSqlList((s, t) => new
            {
                Id = t.Id,
                Name = t.Name,
                Age = t.Age,
                StudentName = s.Name
            });
            sql.ShouldBe("""
                select t2.id `Id`,t2.name `Name`,t2.age `Age`,t.name `StudentName`
                from t_student t
                    right join t_teacher t2 on t.teacher_id = t2.id;
                """);
        }

        [Test]
        public void InnerJoin()
        {
            var sql = db.Select<StudentEntity>().InnerJoin<TeacherEntity>((s, t) => s.TeacherId == t.Id).ToSqlList((s, t) => new
            {
                Id = t.Id,
                Name = t.Name,
                Age = t.Age,
                StudentName = s.Name
            });
            sql.ShouldBe("""
                select t2.id `Id`,t2.name `Name`,t2.age `Age`,t.name `StudentName`
                from t_student t
                    inner join t_teacher t2 on t.teacher_id = t2.id;
                """);
        }

        [Test]
        public void InnerJoinNoExpr()
        {
            var sql = db.Select<StudentEntity>().InnerJoin<TeacherEntity>(null).ToSqlList((s, t) => new
            {
                Id = t.Id,
                Name = t.Name,
                Age = t.Age,
                StudentName = s.Name
            });
            sql.ShouldBe("""
                select t2.id `Id`,t2.name `Name`,t2.age `Age`,t.name `StudentName`
                from t_student t
                    inner join t_teacher t2;
                """);
        }

        [Test]
        public void CrossJoin()
        {
            var sql = db.Select<StudentEntity>().CrossJoin<TeacherEntity>((s, t) => s.TeacherId == t.Id).ToSqlList((s, t) => new
            {
                Id = t.Id,
                Name = t.Name,
                Age = t.Age,
                StudentName = s.Name
            });
            sql.ShouldBe("""
                select t2.id `Id`,t2.name `Name`,t2.age `Age`,t.name `StudentName`
                from t_student t
                    cross join t_teacher t2 on t.teacher_id = t2.id;
                """);
        }

        [Test]
        public void SimpleAlias()
        {
            var sql = db.Select<StudentEntity>().LeftJoin<TeacherEntity>((s, t) => s.TeacherId == t.Id, "b").ToSqlList((s, t) => new
            {
                Id = t.Id,
                Name = t.Name,
                Age = t.Age,
                StudentName = s.Name
            });
            sql.ShouldBe("""
                select b.id `Id`,b.name `Name`,b.age `Age`,t.name `StudentName`
                from t_student t
                    left join t_teacher b on t.teacher_id = b.id;
                """);
        }

        [Test]
        public void SimpleAlias2()
        {
            var sql = db.Select<StudentEntity>().LeftJoin<TeacherEntity>((s, t) => s.TeacherId == t.Id).Alias("b").ToSqlList((s, t) => new
            {
                Id = t.Id,
                Name = t.Name,
                Age = t.Age,
                StudentName = s.Name
            });
            sql.ShouldBe("""
                select b.id `Id`,b.name `Name`,b.age `Age`,t.name `StudentName`
                from t_student t
                    left join t_teacher b on t.teacher_id = b.id;
                """);
        }
    }
}
