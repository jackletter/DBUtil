﻿using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects.MultiTable
{
    [TestFixture]
    internal class SubSelectTests : TestBase
    {
        #region model
        [Table("t_teacher")]
        public class TeacherEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("age")]
            public int? Age { get; set; }
        }

        [Table("t_student")]
        public class StudentEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("age")]
            public int? Age { get; set; }
            [Column("score")]
            public float? Score { get; set; }
            [Column("teacher_id")]
            public int TeacherId { get; set; }
        }
        #endregion
        [Test]
        public void ToListTest()
        {
            var sql = db.Select<TeacherEntity>()
                .Where(i => db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .Where((p, s) => s.Score > 90)
                    .ToList((p, s) => p.Id)
                    .Contains(i.Id)).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age `Age`
                from t_teacher t
                where t.id in (select t.id
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                where t2.score > 90);
                """);
        }
        [Test]
        public void ToFirstOrDefaultTest()
        {
            var sql = db.Select<TeacherEntity>()
                .Where(i => db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .Where((p, s) => s.Score > 90)
                    .FirstOrDefault((p, s) => p.Id)
                    > i.Id).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age `Age`
                from t_teacher t
                where (select t.id
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                where t2.score > 90
                limit 1) > t.id;
                """);
        }
        [Test]
        public void CountTest()
        {
            var sql = db.Select<TeacherEntity>()
                .Where(i => db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .Where((p, s) => s.Score > 90)
                    .Count()
                    > i.Id).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age `Age`
                from t_teacher t
                where ((select count(1)
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                where t2.score > 90)) > t.id;
                """);
        }
        [Test]
        public void MaxTest()
        {
            var sql = db.Select<TeacherEntity>()
                .Where(i => db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .Where((p, s) => s.Score > 90)
                    .Max((p, s) => s.Score)
                    > i.Id).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age `Age`
                from t_teacher t
                where ((select max(t2.score)
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                where t2.score > 90)) > t.id;
                """);
        }
        [Test]
        public void MinTest()
        {
            var sql = db.Select<TeacherEntity>()
                .Where(i => db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .Where((p, s) => s.Score > 90)
                    .Min((p, s) => s.Score)
                    > i.Id).ToSqlList();

            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age `Age`
                from t_teacher t
                where ((select min(t2.score)
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                where t2.score > 90)) > t.id;
                """);
        }
        [Test]
        public void SumTest()
        {
            var sql = db.Select<TeacherEntity>()
                .Where(i => db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .Where((p, s) => s.Score > 90)
                    .Sum((p, s) => s.Score)
                    > i.Id).ToSqlList();

            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age `Age`
                from t_teacher t
                where ((select sum(t2.score)
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                where t2.score > 90)) > t.id;
                """);
        }
        [Test]
        public void AvgTest()
        {
            var sql = db.Select<TeacherEntity>()
                .Where(i => db.Select<TeacherEntity>()
                    .LeftJoin<StudentEntity>((p, s) => s.TeacherId == p.Id)
                    .Where((p, s) => s.Score > 90)
                    .Avg((p, s) => s.Score) > i.Id).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.age `Age`
                from t_teacher t
                where ((select avg(t2.score)
                from t_teacher t
                    left join t_student t2 on t2.teacher_id = t.id
                where t2.score > 90)) > t.id;
                """);
        }
    }
}
