﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.Selects.Tree
{
    [TestFixture]
    internal class SelectTreeTests : TestBase
    {
        #region model
        [Table("category")]
        public class Category
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("pid")]
            public int ParentId { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("remark")]
            public string Remark { get; set; }
        }

        [Table("category")]
        public class CategoryWithChildren
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("pid")]
            public int ParentId { get; set; }
            [Column("name")]
            public string Name { get; set; }

            public List<CategoryWithChildren> Children { get; set; }
        }

        public class CategoryDto
        {
            public int Id { get; set; }
            public int ParentId { get; set; }
            public string Name { get; set; }

            public List<CategoryDto> Children { get; set; }
        }

        public class CategoryDtoExt
        {
            public int Id { get; set; }
            public int ParentId { get; set; }
            public string Name { get; set; }

            public string Path { get; set; }
            public int Level { get; set; }

            public List<CategoryDtoExt> Children { get; set; }
        }

        [Table("testlimit")]
        public class TestLimit
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("pid")]
            public int ParentId { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("level")]
            public int Level { get; set; }
        }

        public class TestLimitDto
        {
            public int Id { get; set; }
            public int ParentId { get; set; }
            public string Name { get; set; }
            public int Level { get; set; }

            public List<TestLimitDto> Children { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            db.ExecuteSql(db.Manage.DropTableIfExistSql("category"));
            db.ExecuteSql("create table category(id int primary key auto_increment,name varchar(50),pid int,remark varchar(200))");
            db.ExecuteSql("""
                insert into category(name,pid) values
                	('中国',0),
                	('河南',1),
                	('郑州',2),
                	('洛阳',2),
                	('高新区',3),
                	('金水区',3),
                    ('郑东新区',3),
                    ('西工区',4),
                    ('洛龙区',4);
                """);

            db.ExecuteSql(db.Manage.DropTableIfExistSql("testlimit"));
            db.ExecuteSql("create table testlimit(id int primary key auto_increment,name varchar(50),pid int,level int)");
            /*  level    name(id)
             *    1      宇宙(1)
             *    2          |--银河系(2)
             *    3          |    |--太阳系(3)
             *    4          |    |   |--太阳(4)
             *    4          |    |   |--地球(5)
             *    5          |    |   |    |--美国(6)
             *    5          |    |   |    |--中国(7)
             *    6          |    |   |    |    |--河南(8)
             *    7          |    |   |    |    |    |--郑州(9)
             *    8          |    |   |    |    |    |   |--高新区(10)
             */
            db.ExecuteSql("""
                insert into testlimit(name,level,pid) values
                	('宇宙',   1, 0),
                	('银河系', 2, 1),
                	('太阳系', 3, 2),
                	('太阳',   4, 3),
                	('地球',   4, 3),
                    ('美国',   5, 5),
                	('中国',   5, 5),
                    ('河南',   6, 7),
                    ('郑州',   7, 8),
                    ('高新区', 8, 9);
                """);
        }

        [Test]
        public void TestSimpleBoth()
        {
            var select = db.SelectTree<Category, CategoryDto>(i => i.Id, i => i.ParentId, i => i.Children).Where(i => i.Name == "郑州");
            var sql = select.ToSql();
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"ParentId":0,"Name":"中国","Children":[{"Id":2,"ParentId":1,"Name":"河南","Children":[{"Id":3,"ParentId":2,"Name":"郑州","Children":[{"Id":5,"ParentId":3,"Name":"高新区","Children":null},{"Id":6,"ParentId":3,"Name":"金水区","Children":null},{"Id":7,"ParentId":3,"Name":"郑东新区","Children":null}]}]}]}]""");
        }

        [Test]
        public void TestSimpleParentOrSub()
        {
            var select = db.SelectTree<Category, CategoryDto>(i => i.Id, i => i.ParentId, i => i.Children).Where(i => i.Name == "郑州").SetSpreedMode(DBUtil.EnumTreeSpreedMode.Sub);
            var sql = select.ToSql();
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":3,"ParentId":2,"Name":"郑州","Children":[{"Id":5,"ParentId":3,"Name":"高新区","Children":null},{"Id":6,"ParentId":3,"Name":"金水区","Children":null},{"Id":7,"ParentId":3,"Name":"郑东新区","Children":null}]}]""");

            list = select.SetSpreedMode(DBUtil.EnumTreeSpreedMode.Parent).ToList();
            json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"ParentId":0,"Name":"中国","Children":[{"Id":2,"ParentId":1,"Name":"河南","Children":[{"Id":3,"ParentId":2,"Name":"郑州","Children":null}]}]}]""");
        }

        [Test]
        public void TestSimpleSubTwoRoot()
        {
            var select = db.SelectTree<Category, CategoryDto>(i => i.Id, i => i.ParentId, i => i.Children).Where(i => i.Name == "郑州" || i.Name == "洛阳").SetSpreedMode(DBUtil.EnumTreeSpreedMode.Sub);
            var sql = select.ToSql();
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":3,"ParentId":2,"Name":"郑州","Children":[{"Id":5,"ParentId":3,"Name":"高新区","Children":null},{"Id":6,"ParentId":3,"Name":"金水区","Children":null},{"Id":7,"ParentId":3,"Name":"郑东新区","Children":null}]},{"Id":4,"ParentId":2,"Name":"洛阳","Children":[{"Id":8,"ParentId":4,"Name":"西工区","Children":null},{"Id":9,"ParentId":4,"Name":"洛龙区","Children":null}]}]""");
        }

        [Test]
        public void TestSpreedLimit()
        {
            var select = db.SelectTree<TestLimit, TestLimitDto>(i => i.Id, i => i.ParentId, i => i.Children).Where(i => i.Name == "地球").SetSpreedLimit(2, DBUtil.EnumTreeSpreedMode.Sub).SetSpreedMode(DBUtil.EnumTreeSpreedMode.Sub);

            //往下展开限制
            var sql = select.ToSql();
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("""
                [{"Id":5,"ParentId":3,"Name":"地球","Level":4,"Children":[{"Id":6,"ParentId":5,"Name":"美国","Level":5,"Children":null},{"Id":7,"ParentId":5,"Name":"中国","Level":5,"Children":[{"Id":8,"ParentId":7,"Name":"河南","Level":6,"Children":null}]}]}]
                """);

            list = select.SetSpreedLimit(3, DBUtil.EnumTreeSpreedMode.Sub).ToList();
            json = list.ToJson();
            json.ShouldBe("""
                [{"Id":5,"ParentId":3,"Name":"地球","Level":4,"Children":[{"Id":6,"ParentId":5,"Name":"美国","Level":5,"Children":null},{"Id":7,"ParentId":5,"Name":"中国","Level":5,"Children":[{"Id":8,"ParentId":7,"Name":"河南","Level":6,"Children":[{"Id":9,"ParentId":8,"Name":"郑州","Level":7,"Children":null}]}]}]}]
                """);

            //往上展开限制
            list = select.SetSpreedMode(DBUtil.EnumTreeSpreedMode.Parent).SetSpreedLimit(2, DBUtil.EnumTreeSpreedMode.Parent).ToList();
            json = list.ToJson();
            json.ShouldBe("""
                [{"Id":2,"ParentId":1,"Name":"银河系","Level":2,"Children":[{"Id":3,"ParentId":2,"Name":"太阳系","Level":3,"Children":[{"Id":5,"ParentId":3,"Name":"地球","Level":4,"Children":null}]}]}]
                """);
        }

        [Test]
        public void TestVisitInMemory()
        {
            //使用 VisitTreeInMemory 在内存中生成 Path 和 Level, 但要注意: 一般都要展开到跟节点
            var list = db.SelectTree<Category, CategoryDtoExt>(i => i.Id, i => i.ParentId, i => i.Children).Where(i => i.Name == "郑州").VisitTreeInMemory(ctx =>
            {
                ctx.Current.Path = ctx.ParentsWithSelf.Select(i => i.Id).ToStringSeparated("-");
                ctx.Current.Level = ctx.ParentsWithSelf.Count;
            }).ToList();
            var json = list.ToJson();
            json.ShouldBe("""
                [{"Id":1,"ParentId":0,"Name":"中国","Path":"1","Level":1,"Children":[{"Id":2,"ParentId":1,"Name":"河南","Path":"1-2","Level":2,"Children":[{"Id":3,"ParentId":2,"Name":"郑州","Path":"1-2-3","Level":3,"Children":[{"Id":5,"ParentId":3,"Name":"高新区","Path":"1-2-3-5","Level":4,"Children":null},{"Id":6,"ParentId":3,"Name":"金水区","Path":"1-2-3-6","Level":4,"Children":null},{"Id":7,"ParentId":3,"Name":"郑东新区","Path":"1-2-3-7","Level":4,"Children":null}]}]}]}]
                """);
        }

        [Test]
        public void TestWithChildrenSelf()
        {
            var select = db.SelectTree<CategoryWithChildren>(i => i.Id, i => i.ParentId, i => i.Children).Where(i => i.Name == "郑州");
            var sql = select.ToSql();
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"ParentId":0,"Name":"中国","Children":[{"Id":2,"ParentId":1,"Name":"河南","Children":[{"Id":3,"ParentId":2,"Name":"郑州","Children":[{"Id":5,"ParentId":3,"Name":"高新区","Children":null},{"Id":6,"ParentId":3,"Name":"金水区","Children":null},{"Id":7,"ParentId":3,"Name":"郑东新区","Children":null}]}]}]}]""");
        }

        [Test]
        public void TestWithChildrenSelfAndDefaultPropName()
        {
            var select = db.SelectTree<CategoryWithChildren>().Where(i => i.Name == "郑州");
            var sql = select.ToSql();
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"ParentId":0,"Name":"中国","Children":[{"Id":2,"ParentId":1,"Name":"河南","Children":[{"Id":3,"ParentId":2,"Name":"郑州","Children":[{"Id":5,"ParentId":3,"Name":"高新区","Children":null},{"Id":6,"ParentId":3,"Name":"金水区","Children":null},{"Id":7,"ParentId":3,"Name":"郑东新区","Children":null}]}]}]}]""");
        }

        [Test]
        public void TestNoWhere()
        {
            var select = db.SelectTree<CategoryWithChildren>();
            var sql = select.ToSql();
            var list = select.ToList();
            var json = list.ToJson();
            json.ShouldBe("""
                [{"Id":1,"ParentId":0,"Name":"中国","Children":[{"Id":2,"ParentId":1,"Name":"河南","Children":[{"Id":3,"ParentId":2,"Name":"郑州","Children":[{"Id":5,"ParentId":3,"Name":"高新区","Children":null},{"Id":6,"ParentId":3,"Name":"金水区","Children":null},{"Id":7,"ParentId":3,"Name":"郑东新区","Children":null}]},{"Id":4,"ParentId":2,"Name":"洛阳","Children":[{"Id":8,"ParentId":4,"Name":"西工区","Children":null},{"Id":9,"ParentId":4,"Name":"洛龙区","Children":null}]}]}]}]
                """);
        }
    }
}
