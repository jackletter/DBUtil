﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects
{
    [TestFixture]
    internal class SelectApiDesignTests : TestBase
    {
        #region model
        [Table("test")]
        public class PersonEntity
        {
            [Column("a_id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [Column("a_name")]
            public string Name { get; set; }
            [Column("a_addr")]
            public string Addr { get; set; }
            public int Age { get; set; }
            [Column("Birth")]
            public DateTime Birth { get; set; }
        }

        public class PersonDto
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
            public string Desc { get; set; }
        }
        #endregion

        [Test]
        public void TestToSql()
        {
            string sql;
            //ToSqlFirstOrDefault
            //1. entity
            sql = db.Select<PersonEntity>().ToSqlFirstOrDefault();
            sql.ShouldBe(
                """
                select t.a_id `Id`,t.a_name `Name`,t.a_addr `Addr`,t.`Age`,t.Birth
                from test t
                limit 1;
                """);
            //2. dto lambda
            sql = db.Select<PersonEntity>().ToSqlFirstOrDefault(i => new
            {
                Id2 = i.Id,
                Name2 = i.Name,
                Age2 = i.Age,
            });
            sql.ShouldBe(
                """
                select t.a_id `Id2`,t.a_name `Name2`,t.`Age` `Age2`
                from test t
                limit 1;
                """);
            //3. dto direct
            sql = db.Select<PersonEntity>().ToSqlFirstOrDefault<PersonDto>();
            sql.ShouldBe(
                """
                select t.a_id `Id`,t.a_name `Name`,t.`Age`
                from test t
                limit 1;
                """);

            //ToSqlList
            //1. entity
            sql = db.Select<PersonEntity>().ToSqlList();
            sql.ShouldBe(
                """
                select t.a_id `Id`,t.a_name `Name`,t.a_addr `Addr`,t.`Age`,t.Birth
                from test t;
                """);
            //2. dto lambda
            sql = db.Select<PersonEntity>().ToSqlList(i => new
            {
                Id2 = i.Id,
                Name2 = i.Name,
                Age2 = i.Age,
            });
            sql.ShouldBe(
                """
                select t.a_id `Id2`,t.a_name `Name2`,t.`Age` `Age2`
                from test t;
                """);
            //3. dto direct
            sql = db.Select<PersonEntity>().ToSqlList<PersonDto>();
            sql.ShouldBe(
                """
                select t.a_id `Id`,t.a_name `Name`,t.`Age`
                from test t;
                """);

            //ToSqlPage
            //1. entity
            sql = db.Select<PersonEntity>().ToSqlPage(1, 10);
            sql.ShouldBe(
                """
                select t.a_id `Id`,t.a_name `Name`,t.a_addr `Addr`,t.`Age`,t.Birth
                from test t
                limit 10;
                select count(1)
                from test t;
                """);
            //2. dto lambda
            sql = db.Select<PersonEntity>().ToSqlPage(i => new
            {
                Id2 = i.Id,
                Name2 = i.Name,
                Age2 = i.Age,
            }, 1, 10);
            sql.ShouldBe(
                """
                select t.a_id `Id2`,t.a_name `Name2`,t.`Age` `Age2`
                from test t
                limit 10;
                select count(1)
                from test t;
                """);
            //3. dto direct
            sql = db.Select<PersonEntity>().ToSqlPage<PersonDto>(1, 10);
            sql.ShouldBe(
                """
                select t.a_id `Id`,t.a_name `Name`,t.`Age`
                from test t
                limit 10;
                select count(1)
                from test t;
                """);
        }

        [Test]
        public void TestExe()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(a_id int primary key auto_increment, a_name varchar(50),Age int,a_addr varchar(50),Birth datetime)");
            db.ExecuteSql(
                """
                insert into test(a_name,Age,a_addr,birth) values
                    ('tom',18,'tom-addr','2000-01-02'),
                    ('tom-2',19,'tom2-addr','2000-01-02'),
                    ('tom-3',17,'tom3-addr','2000-01-02');
                """);

            string json;
            //FirstOrDefault
            //1. entity
            json = db.Select<PersonEntity>().FirstOrDefault().ToJson();
            json.ShouldBe("{\"Id\":1,\"Name\":\"tom\",\"Addr\":\"tom-addr\",\"Age\":18,\"Birth\":\"2000-01-02T00:00:00.0000000\"}");
            //2. dto lambda
            json = db.Select<PersonEntity>().FirstOrDefault(i => new
            {
                Id2 = i.Id,
                Name2 = i.Name,
                Age2 = i.Age,
            }).ToJson();
            json.ShouldBe("{\"Id2\":1,\"Name2\":\"tom\",\"Age2\":18}");
            //3. dto direct
            json = db.Select<PersonEntity>().FirstOrDefault<PersonDto>().ToJson();
            json.ShouldBe("{\"Id\":1,\"Name\":\"tom\",\"Age\":18,\"Desc\":null}");

            //ToList
            //1. entity
            json = db.Select<PersonEntity>().ToList().ToJson();
            json.ShouldBe("[{\"Id\":1,\"Name\":\"tom\",\"Addr\":\"tom-addr\",\"Age\":18,\"Birth\":\"2000-01-02T00:00:00.0000000\"},{\"Id\":2,\"Name\":\"tom-2\",\"Addr\":\"tom2-addr\",\"Age\":19,\"Birth\":\"2000-01-02T00:00:00.0000000\"},{\"Id\":3,\"Name\":\"tom-3\",\"Addr\":\"tom3-addr\",\"Age\":17,\"Birth\":\"2000-01-02T00:00:00.0000000\"}]");
            //2. dto lambda
            json = db.Select<PersonEntity>().ToList(i => new
            {
                Id2 = i.Id,
                Name2 = i.Name,
                Age2 = i.Age,
            }).ToJson();
            json.ShouldBe("[{\"Id2\":1,\"Name2\":\"tom\",\"Age2\":18},{\"Id2\":2,\"Name2\":\"tom-2\",\"Age2\":19},{\"Id2\":3,\"Name2\":\"tom-3\",\"Age2\":17}]");
            //3. dto direct
            json = db.Select<PersonEntity>().ToList<PersonDto>().ToJson();
            json.ShouldBe("[{\"Id\":1,\"Name\":\"tom\",\"Age\":18,\"Desc\":null},{\"Id\":2,\"Name\":\"tom-2\",\"Age\":19,\"Desc\":null},{\"Id\":3,\"Name\":\"tom-3\",\"Age\":17,\"Desc\":null}]");

            //ToPage
            //1. entity
            json = db.Select<PersonEntity>().ToPage().ToJson();
            json.ShouldBe("{\"TotalCount\":3,\"List\":[{\"Id\":1,\"Name\":\"tom\",\"Addr\":\"tom-addr\",\"Age\":18,\"Birth\":\"2000-01-02T00:00:00.0000000\"},{\"Id\":2,\"Name\":\"tom-2\",\"Addr\":\"tom2-addr\",\"Age\":19,\"Birth\":\"2000-01-02T00:00:00.0000000\"},{\"Id\":3,\"Name\":\"tom-3\",\"Addr\":\"tom3-addr\",\"Age\":17,\"Birth\":\"2000-01-02T00:00:00.0000000\"}]}");
            //2. dto lambda
            json = db.Select<PersonEntity>().ToPage(i => new
            {
                Id2 = i.Id,
                Name2 = i.Name,
                Age2 = i.Age,
            }).ToJson();
            json.ShouldBe("{\"TotalCount\":3,\"List\":[{\"Id2\":1,\"Name2\":\"tom\",\"Age2\":18},{\"Id2\":2,\"Name2\":\"tom-2\",\"Age2\":19},{\"Id2\":3,\"Name2\":\"tom-3\",\"Age2\":17}]}");
            //3. dto direct
            json = db.Select<PersonEntity>().ToPage<PersonDto>().ToJson();
            json.ShouldBe("{\"TotalCount\":3,\"List\":[{\"Id\":1,\"Name\":\"tom\",\"Age\":18,\"Desc\":null},{\"Id\":2,\"Name\":\"tom-2\",\"Age\":19,\"Desc\":null},{\"Id\":3,\"Name\":\"tom-3\",\"Age\":17,\"Desc\":null}]}");
        }

        [Test]
        public void TestGroupToSql()
        {
            string sql;
            //ToSqlFirstOrDefault
            sql = db.Select<PersonEntity>().GroupBy(i => i.Age).Having(i => i.Length > 1).ToSqlFirstOrDefault(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join(i => i.Name, ",")
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(t.a_name separator ',') `Names`
                from test t
                group by t.`Age`
                having count(1) > 1
                limit 1;
                """);

            //ToSqlList
            sql = db.Select<PersonEntity>().GroupBy(i => i.Age).Having(i => i.Length > 1).ToSqlList(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join(i => i.Name, ",")
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(t.a_name separator ',') `Names`
                from test t
                group by t.`Age`
                having count(1) > 1;
                """);

            //ToSqlPage
            sql = db.Select<PersonEntity>().GroupBy(i => i.Age).Having(i => i.Length > 1).ToSqlPage(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join(i => i.Name, ",")
            }, 1, 10);
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(t.a_name separator ',') `Names`
                from test t
                group by t.`Age`
                having count(1) > 1
                limit 10;
                select count(1)
                from test t
                group by t.`Age`
                having count(1) > 1;
                """);
        }

        [Test]
        public void TestGroupJoinSql()
        {
            string sql;
            sql = db.Select<PersonEntity>().GroupBy(i => i.Age).Having(i => i.Length > 1).ToSqlFirstOrDefault(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join(i => i.Name, ",", i => i.Id, true)
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(t.a_name order by t.a_id desc separator ',') `Names`
                from test t
                group by t.`Age`
                having count(1) > 1
                limit 1;
                """);

            sql = db.Select<PersonEntity>().GroupBy(i => i.Age).Having(i => i.Length > 1).ToSqlFirstOrDefault(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join(i => i.Name, ",", i => new { i.Id, i.Addr }, true)
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(t.a_name order by t.a_id,t.a_addr desc separator ',') `Names`
                from test t
                group by t.`Age`
                having count(1) > 1
                limit 1;
                """);

            sql = db.Select<PersonEntity>().GroupBy(i => i.Age).Having(i => i.Length > 1).ToSqlFirstOrDefault(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join(i => i.Name, ",", i => i.Id, false, true)
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(distinct t.a_name order by t.a_id asc separator ',') `Names`
                from test t
                group by t.`Age`
                having count(1) > 1
                limit 1;
                """);
        }
    }
}
