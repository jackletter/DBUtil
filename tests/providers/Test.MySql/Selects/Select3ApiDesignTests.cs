﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects
{
    [TestFixture]
    internal class Select3ApiDesignTests : TestBase
    {
        #region model
        [Table("test")]
        public class AEntity
        {
            [Column("a_id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [Column("a_name")]
            public string Name { get; set; }
            [Column("a_addr")]
            public string Addr { get; set; }
            public int Age { get; set; }
        }
        [Table("test2")]
        public class BEntity
        {
            [Column("b_id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [Column("b_name")]
            public string Name { get; set; }
            [Column("b_addr")]
            public string Addr { get; set; }
            public int Age { get; set; }
        }
        [Table("test3")]
        public class CEntity
        {
            [Column("c_id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [Column("c_name")]
            public string Name { get; set; }
            [Column("c_addr")]
            public string Addr { get; set; }
            public int Age { get; set; }
        }
        #endregion

        [Test]
        public void TestToSql()
        {
            string sql;
            //ToSqlFirstOrDefault
            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).ToSqlFirstOrDefault((i, j, k) => new
            {
                Id1 = i.Id,
                Id2 = j.Id,
                Id3 = k.Id,
                Name1 = i.Name,
                Name2 = j.Name,
                Name3 = k.Name,
            });
            sql.ShouldBe("""
                select t.a_id `Id1`,t2.b_id `Id2`,t3.c_id `Id3`,t.a_name `Name1`,t2.b_name `Name2`,t3.c_name `Name3`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                limit 1;
                """);

            //ToSqlList
            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).ToSqlList((i, j, k) => new
            {
                Id1 = i.Id,
                Id2 = j.Id,
                Id3 = k.Id,
                Name1 = i.Name,
                Name2 = j.Name,
                Name3 = k.Name,
            });
            sql.ShouldBe("""
                select t.a_id `Id1`,t2.b_id `Id2`,t3.c_id `Id3`,t.a_name `Name1`,t2.b_name `Name2`,t3.c_name `Name3`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id;
                """);

            //ToSqlPage
            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).ToSqlPage((i, j, k) => new
            {
                Id1 = i.Id,
                Id2 = j.Id,
                Id3 = k.Id,
                Name1 = i.Name,
                Name2 = j.Name,
                Name3 = k.Name,
            }, 1, 10);
            sql.ShouldBe("""
                select t.a_id `Id1`,t2.b_id `Id2`,t3.c_id `Id3`,t.a_name `Name1`,t2.b_name `Name2`,t3.c_name `Name3`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                limit 10;
                select count(1)
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id;
                """);
        }

        [Test]
        public void TestGroupToSql()
        {
            string sql;
            //ToSqlFirstOrDefault
            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).GroupBy((i, j, k) => i.Age).Having(i => i.Length > 1).ToSqlFirstOrDefault(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join((a, b, c) => a.Name + "_" + b.Name, ",")
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(concat_ws('',concat_ws('',t.a_name,'_'),t2.b_name) separator ',') `Names`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                group by t.`Age`
                having count(1) > 1
                limit 1;
                """);

            //ToSqlList
            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).GroupBy((i, j, k) => i.Age).Having(i => i.Length > 1).ToSqlList(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join((a, b, c) => a.Name + "_" + b.Name, ",")
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(concat_ws('',concat_ws('',t.a_name,'_'),t2.b_name) separator ',') `Names`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                group by t.`Age`
                having count(1) > 1;
                """);

            //ToSqlPage
            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).GroupBy((i, j, k) => i.Age).Having(i => i.Length > 1).ToSqlPage(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join((a, b, c) => a.Name + "_" + b.Name, ",")
            }, 1, 10);
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(concat_ws('',concat_ws('',t.a_name,'_'),t2.b_name) separator ',') `Names`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                group by t.`Age`
                having count(1) > 1
                limit 10;
                select count(1)
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                group by t.`Age`
                having count(1) > 1;
                """);
        }

        [Test]
        public void TestGroupJoinSql()
        {
            string sql;
            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).GroupBy((i, j, k) => i.Age).Having(i => i.Length > 1).ToSqlList(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join((a, b, c) => a.Name + "_" + b.Name, ",", (i, j, k) => i.Age, true)
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(concat_ws('',concat_ws('',t.a_name,'_'),t2.b_name) order by t.`Age` desc separator ',') `Names`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                group by t.`Age`
                having count(1) > 1;
                """);

            sql = db.Select<AEntity>().LeftJoin<BEntity>((a, b) => a.Id == b.Id).LeftJoin<CEntity>((a, b, c) => b.Id == c.Id).GroupBy((i, j, k) => i.Age).Having(i => i.Length > 1).ToSqlList(i => new
            {
                Age = i.Key,
                Count = i.Length,
                Names = i.Join((a, b, c) => a.Name + "_" + b.Name, ",", (i, j, k) => new { i.Age, j.Addr }, false, true)
            });
            sql.ShouldBe("""
                select t.`Age`,count(1) `Count`,group_concat(distinct concat_ws('',concat_ws('',t.a_name,'_'),t2.b_name) order by t.`Age`,t2.b_addr asc separator ',') `Names`
                from test t
                    left join test2 t2 on t.a_id = t2.b_id
                    left join test3 t3 on t2.b_id = t3.c_id
                group by t.`Age`
                having count(1) > 1;
                """);
        }
    }
}
