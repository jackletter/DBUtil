﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects
{
    [TestFixture]
    internal class FirstTests : TestBase
    {

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
        }

        [Test]
        public void SelectScalarTest()
        {
            try
            {
                db.SelectScalar<int>("select id from test");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("无法将 null 或 dbnull 转为");
            }

            var num = db.SelectScalar<int?>("select id from test");
            num.ShouldBe(null);
        }

        [Test]
        public void SelectScalarTest2()
        {
            try
            {
                db.SelectScalar<(int, int)>("select id,id from test");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("无法将 null 或 dbnull 转为");
            }

            var num = db.SelectScalar<(int?, int?)?>("select id,id from test");
            num.ShouldBe(null);
        }

        [Test]
        public void SelectModelTest()
        {
            try
            {
                db.SelectModel<int>("select id from test");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("未查询到(int类型)数据!");
            }

            var num = db.SelectModel<int?>("select id from test");
            num.ShouldBe(null);
        }

        [Test]
        public void SelectModelListTest()
        {
            var list = db.SelectModelList<int>("select id from test");
            list.Count.ShouldBe(0);

            var list2 = db.SelectModelList<int?>("select id from test");
            list2.Count.ShouldBe(0);

            db.ExecuteSql("insert into test(id,name) values(1,null)");
            var list3 = db.SelectModelList<string>("select name from test");
            list3.Count.ShouldBe(1);
            list3[0].ShouldBe(null);
        }

        #region model
        [Table("test")]
        public class Person
        {
            [Column("id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Custom)]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }
        }
        public struct PersonStruct
        {
            [Column("id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Custom)]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }
        }
        #endregion

        [Test]
        public void SelectBuilderTest()
        {
            var model = db.Select<Person>().FirstOrDefault();
            model.ShouldBe(null);

            try
            {
                db.Select<Person>().FirstOrDefault<PersonStruct>();
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("未查询到(Test.MySql.Selects.FirstTests.PersonStruct类型)数据!");
            }
        }
    }
}
