﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects.SubSelects
{
    [TestFixture]
    internal class SelectBuilder : TestBase
    {
        #region model
        [Table("test")]
        public class PersonEntity
        {
            [Column("a_id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [Column("a_name")]
            public string Name { get; set; }
            [Column("a_addr")]
            public string Addr { get; set; }
            public int Age { get; set; }
            [Column("Birth")]
            public DateTime Birth { get; set; }
        }

        public class PersonDto
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
            public string Desc { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(a_id int primary key auto_increment,a_name varchar(50),a_addr varchar(500),age int,birth datetime)");
            db.ExecuteSql("""
                insert into test(a_name,a_addr,age,birth) values
                    ('jack','road1',20,'2001-01-02'),
                    ('jack2','road2',21,'2001-01-02'),
                    ('jack3','road3',22,'2001-01-02');
                """);
        }

        [Test]
        public void Test()
        {
            var sql = db.Select<PersonEntity>().Where(i => i.Id == db.Select<PersonEntity>().Where(i => i.Age == 20).FirstOrDefault(i => i.Id)).ToSqlList();
            sql.ShouldBe("""
                select t.a_id `Id`,t.a_name `Name`,t.a_addr `Addr`,t.`Age`,t.Birth
                from test t
                where t.a_id = (select t.a_id
                from test t
                where t.`Age` = 20
                limit 1);
                """);
        }
    }
}
