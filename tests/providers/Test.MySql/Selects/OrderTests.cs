﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.Selects
{
    [TestFixture]
    internal class OrderTests : TestBase
    {
        #region model
        [Table("test")]
        public class Student
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("age")]
            public int Age { get; set; }
            public string Name { get; set; }
            [JsonStore(Bucket = "ext")]
            public JsonObject Ext { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {

        }

        [Test]
        public void TestSimple1()
        {
            var sql = db.Select<Student>().OrderBy(i => i.Id).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                order by t.id;
                """);

            var sql2 = db.Select<Student>().OrderByDesc(i => i.Id).ToSqlList();
            sql2.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                order by t.id desc;
                """);

            var sql3 = db.Select<Student>().OrderByIf(true, i => i.Id).ToSqlList();
            sql3.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                order by t.id;
                """);

            var sql4 = db.Select<Student>().OrderByIf(false, i => i.Id).ToSqlList();
            sql4.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t;
                """);

            var sql5 = db.Select<Student>().OrderByDescIf(true, i => i.Id).ToSqlList();
            sql5.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                order by t.id desc;
                """);

            var sql6 = db.Select<Student>().OrderByDescIf(false, i => i.Id).ToSqlList();
            sql6.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t;
                """);

            var sql7 = db.Select<Student>().Order("t.id desc,t.age").ToSqlList();
            sql7.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                order by t.id desc,t.age;
                """);
        }

        [Test]
        public void TestWithJson1()
        {
            var sql = db.Select<Student>().OrderBy(i => i.Ext["Score"]).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                order by json_value(json_value(t.ext,'$'),'$."Score"');
                """);
        }

        [Test]
        public void TestCombine1()
        {
            var sql = db.Select<Student>().OrderBy(i => i.Id).OrderByDesc(i => i.Age).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                order by t.id,t.age desc;
                """);
        }

        [Test]
        public void TestNewInit1()
        {
            var sql = db.Select<Student>().Where(i => i.Age > 18).OrderByDesc(i => i.Age > 19 ? 1 : 0).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                where t.age > 18
                order by if((t.age > 19),1,0) desc;
                """);

            var sql2 = db.Select<Student>().Where(i => i.Age > 18).OrderBy(i => new { Age = i.Age > 19 ? 1 : 0, Rank = i.Id }).OrderByDesc(i => i.Id).ToSqlList();
            sql2.ShouldBe("""
                select t.id `Id`,t.age `Age`,t.`Name`,json_value(t.ext,'$') `Ext`
                from test t
                where t.age > 18
                order by if((t.age > 19),1,0),t.id,t.id desc;
                """);
        }

        #region select2
        [Test]
        public void Test2()
        {
            var sql = db.Select<Student>().LeftJoin<Student>((i, j) => i.Id == j.Id).OrderBy(i => i.t.Id).ToSqlList((i, j) => new
            {
                s1 = i,
                s2 = j
            });
            sql.ShouldBe("""
                select t.id `s1.Id`,t.age `s1.Age`,t.`Name` `s1.Name`,json_value(t.ext,'$') `s1.Ext`,t2.id `s2.Id`,t2.age `s2.Age`,t2.`Name` `s2.Name`,json_value(t2.ext,'$') `s2.Ext`
                from test t
                    left join test t2 on t.id = t2.id
                order by t.id;
                """);
            var sql2 = db.Select<Student>().LeftJoin<Student>((i, j) => i.Id == j.Id)
                .OrderByIf(true, i => i.t2.Id)
                .OrderByIf(false, i => i.t2.Age)
                .OrderByDesc((i, j) => i.Age)
                .ToSqlList((i, j) => new
                {
                    id = i.Id,
                    age = j.Age,
                });
            sql2.ShouldBe("""
                select t.id,t2.age
                from test t
                    left join test t2 on t.id = t2.id
                order by t2.id,t.age desc;
                """);
        }
        #endregion

        #region select3
        [Test]
        public void Test3()
        {
            var sql = db.Select<Student>()
                .LeftJoin<Student>((i, j) => i.Id == j.Id)
                .LeftJoin<Student>((i, j, k) => i.Id == k.Id)
                .OrderBy(i => i.t.Id)
                .OrderBy((i, j, k) => k.Id)
                .OrderByDesc((i, j, k) => new { i.Age, j.Id })
                .ToSqlList((i, j, k) => new
                {
                    id = i.Id,
                    age = j.Age,
                });
            sql.ShouldBe("""
                select t.id,t2.age
                from test t
                    left join test t2 on t.id = t2.id
                    left join test t3 on t.id = t3.id
                order by t.id,t3.id,t.age desc,t2.id desc;
                """);
        }
        #endregion
    }
}
