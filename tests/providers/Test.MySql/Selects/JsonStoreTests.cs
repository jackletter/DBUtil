﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.Selects
{
    [TestFixture]
    internal class JsonStoreTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            public int Id { get; set; }
            public int Age { get; set; }
            public string Name { get; set; }

            [JsonStore(Bucket = "ext", Key = "CardIds")]
            public List<int> CardIds { get; set; }
        }

        [Table("test_selectkey")]
        public class PersonJsonSelectKey
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "ext", Key = "Score", SelectKey = "score")]
            public float Score { get; set; }

            [JsonStore(Bucket = "ext", Key = "Addrs")]
            public List<string> Addrs { get; set; }
        }
        #endregion

        [SetUp]
        public void Setup()
        {
            DropTableIfExist("test_selectkey");
            db.ExecuteSql(
                """
                create table test_selectkey(
                    id int auto_increment primary key,
                    name varchar(50),
                    ext json,
                    score float generated always as (ext->>'$.Score')
                )
                """);
            db.ExecuteSql(
                """
                insert into test_selectkey(name,ext) values
                    ('jack','{"Score":95.5,"Addrs":["jack road","road2"]}'),
                    ('tom','{"Score":100,"Addrs":["tom road","road3"]}');
                """);
        }

        [Test]
        public void ContainsTest()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,age int,name varchar(50),ext json)");

            db.ExecuteSql("""
                insert into test(name,age,ext) values
                ('jack',18,'{"CardIds":[1,2,3]}'),
                ('tom',20,'{"CardIds":[1,3,5]}'),
                ('lisa',22,'{"CardIds":[2,3]}');
                """);

            var select = db.Select<Person>().Where(i => i.CardIds.Contains(2));
            var sql = select.ToSqlList();
            sql.ShouldBe("""
                select t.`Id`,t.`Age`,t.`Name`,json_value(t.ext,'$."CardIds"') `CardIds`
                from test t
                where json_contains(json_value(t.ext,'$."CardIds"'),cast(2 as json));
                """);

            var json = select.ToList().ToJson();
            json.ShouldBe("[{\"Id\":1,\"Age\":18,\"Name\":\"jack\",\"CardIds\":[1,2,3]},{\"Id\":3,\"Age\":22,\"Name\":\"lisa\",\"CardIds\":[2,3]}]");
        }

        [Test]
        public void ContainsAnyTest()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,age int,name varchar(50),ext json)");

            db.ExecuteSql("""
                insert into test(name,age,ext) values
                ('jack',18,'{"CardIds":[1,2,3]}'),
                ('tom',20,'{"CardIds":[1,3,5]}'),
                ('lisa',22,'{"CardIds":[2,3]}');
                """);

            var cardIds = new[] { 1, 4 };
            var select = db.Select<Person>().Where(i => cardIds.ContainsAny(i.CardIds, null));
            var sql = select.ToSqlList();
            sql.ShouldBe("""
                select t.`Id`,t.`Age`,t.`Name`,json_value(t.ext,'$."CardIds"') `CardIds`
                from test t
                where json_overlaps('[1,4]',json_value(t.ext,'$."CardIds"'));
                """);

            var json = select.ToList().ToJson();
            json.ShouldBe("[{\"Id\":1,\"Age\":18,\"Name\":\"jack\",\"CardIds\":[1,2,3]},{\"Id\":2,\"Age\":20,\"Name\":\"tom\",\"CardIds\":[1,3,5]}]");
        }

        [Test]
        public void ContainsObjectTest()
        {
            //DropTableIfExist("test");
            //db.ExecuteSql("create table test(id int auto_increment primary key,age int,name varchar(50),ext json)");

            //db.ExecuteSql("""
            //    insert into test(name,age,ext) values
            //    ('jack',18,'{"desc":"test"}'),
            //    ('tom',20,'{"desc":[1,3,5]}'),
            //    ('lisa',22,'{"desc":[2,3]}');
            //    """);

            //var cardIds = new[] { 1, 4 };
            //var select = db.Select<Person>().Where(i => cardIds.ContainsAny(i.CardIds, null));
            //var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.`Id`,t.`Age`,t.`Name`,json_value(t.ext,'$."CardIds"') `CardIds`
            //    from test t
            //    where json_overlaps('[1,4]',json_value(t.ext,'$."CardIds"'));
            //    """);

            //var json = select.ToList().ToJson();
            //json.ShouldBe("[{\"Id\":1,\"Age\":18,\"Name\":\"jack\",\"CardIds\":[1,2,3]},{\"Id\":2,\"Age\":20,\"Name\":\"tom\",\"CardIds\":[1,3,5]}]");

            var sql = db.WhereSeg<JsonArray>(objs => objs.ContainsObject(new { name = "tom" }.ToJson().ToObject<JsonObject>(), null), "t");

        }

        [Test]
        public void TestSelectKey()
        {
            //select
            var sql = db.Select<PersonJsonSelectKey>().Where(i => i.Score > 60 && i.Addrs.Contains("road2")).ToSqlList();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.score `Score`,json_value(t.ext,'$."Addrs"') `Addrs`
                from test_selectkey t
                where (t.score > 60) and json_contains(json_value(t.ext,'$."Addrs"'),'"road2"');
                """);

            //update
            var sql2 = db.Update<PersonJsonSelectKey>().SetExpr(i => new PersonJsonSelectKey
            {
                Name = i.Name + "2",
                Score = i.Score + 10
            }).Where(i => i.Score < 99 && i.Id == 1).ToSql();
            sql2.ShouldBe("""
                update test_selectkey set
                    name = concat_ws('',name,'2'),
                    ext = json_set(ifnull(ext,json_object()),
                        '$."Score"',score + 10)
                where (score < 99) and (id = 1);
                """);
            //insert
            var sql3 = db.Insert<PersonJsonSelectKey>().SetEntity(new PersonJsonSelectKey
            {
                Name = "jack",
                Addrs = new List<string> { "ji", "lp" },
                Score = 99
            }).ToSql();
            sql3.ShouldBe("""
                insert into test_selectkey(name,ext) values('jack',json_object('Score',99,'Addrs',cast('["ji","lp"]' as json)));
                """);
            var dt = db.Insert<PersonJsonSelectKey>().SetEntity(new PersonJsonSelectKey
            {
                Name = "jack",
                Addrs = new List<string> { "ji", "lp" },
                Score = 99
            }).ToDataTable();
            dt.Columns.Count.ShouldBe(2);
            dt.Rows[0]["ext"].ToString().ShouldBe("{\"Score\":99,\"Addrs\":[\"ji\",\"lp\"]}");
        }
    }
}
