﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Selects
{
    [TestFixture]
    internal class NestedModelTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            public int Age { get; set; }

            [Column("addr")]
            public string Addr { get; set; }
        }
        public class PersonDto
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public PersonDtoExt Ext { get; set; }
            public class PersonDtoExt
            {
                public int Age { get; set; }
                public PersonDtoExt2 Ext2 { get; set; }
            }
            public class PersonDtoExt2
            {
                public string Addr { get; set; }
            }
        }
        [Table("test2")]
        public class Student
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            public int Age { get; set; }

            [Column("addr")]
            public string Addr { get; set; }
        }

        public class PersonStudentInfo
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Person Person { get; set; }
            public Student Student { get; set; }
        }
        #endregion
        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""
                create table test(id int primary key auto_increment, name varchar(50),age int,addr varchar(50));
                insert into test(name,age,addr) values
                    ('tom',18,'天命'),
                    ('tom2',19,'天命2'),
                    ('tom',20,'天命3');
                """);
            DropTableIfExist("test2");
            db.ExecuteSql("""
                create table test2(id int primary key auto_increment, name varchar(50),age int,addr varchar(50));
                insert into test2(name,age,addr) values
                    ('jack',180,'天明'),
                    ('jack2',190,'天明2'),
                    ('jack',200,'天明3');
                """);
        }
        [Test]
        public void TestSimpleNested()
        {
            string sql;
            sql = db.Select<Person>().ToSqlList(i => new
            {
                Id = i.Id,
                Name = i.Name,
                Ext = new
                {
                    Age = i.Age,
                    Ext2 = new
                    {
                        Addr = i.Addr
                    }
                }
            });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.`Age` `Ext.Age`,t.addr `Ext.Ext2.Addr`
                from test t;
                """);
            var list = db.Select<Person>().ToList(i => new
            {
                Id = i.Id,
                Name = i.Name,
                Ext = new
                {
                    Age = i.Age,
                    Ext2 = new
                    {
                        Addr = i.Addr
                    }
                }
            });
            list.ToJson().ShouldBe("""
                [{"Id":1,"Name":"tom","Ext":{"Age":18,"Ext2":{"Addr":"天命"}}},{"Id":2,"Name":"tom2","Ext":{"Age":19,"Ext2":{"Addr":"天命2"}}},{"Id":3,"Name":"tom","Ext":{"Age":20,"Ext2":{"Addr":"天命3"}}}]
                """);
        }

        [Test]
        public void TestSimpleNested2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""
                create table test(id int primary key auto_increment, name varchar(50),age int,addr varchar(50));
                insert into test(name,age,addr) values
                    ('tom',18,'天命'),
                    ('tom2',19,'天命2'),
                    ('tom',20,'天命3');
                """);

            string sql;
            sql = db.Select<Person>().ToSqlList(i => new PersonDto
            {
                Id = i.Id,
                Name = i.Name,
                Ext = new PersonDto.PersonDtoExt
                {
                    Age = i.Age,
                    Ext2 = new PersonDto.PersonDtoExt2
                    {
                        Addr = i.Addr
                    }
                }
            });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.`Age` `Ext.Age`,t.addr `Ext.Ext2.Addr`
                from test t;
                """);

            var list = db.Select<Person>().ToList(i => new PersonDto
            {
                Id = i.Id,
                Name = i.Name,
                Ext = new PersonDto.PersonDtoExt
                {
                    Age = i.Age,
                    Ext2 = new PersonDto.PersonDtoExt2
                    {
                        Addr = i.Addr
                    }
                }
            });
            list.ToJson().ShouldBe("""
                [{"Id":1,"Name":"tom","Ext":{"Age":18,"Ext2":{"Addr":"天命"}}},{"Id":2,"Name":"tom2","Ext":{"Age":19,"Ext2":{"Addr":"天命2"}}},{"Id":3,"Name":"tom","Ext":{"Age":20,"Ext2":{"Addr":"天命3"}}}]
                """);
        }

        [Test]
        public void TestMultiNested()
        {
            string sql;
            sql = db.Select<Person>().LeftJoin<Student>((i, j) => i.Id == j.Id).ToSqlList((i, j) => new
            {
                Id = i.Id,
                Name = i.Name,
                Person = i,
                Student = j
            });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.id `Person.Id`,t.name `Person.Name`,t.`Age` `Person.Age`,t.addr `Person.Addr`,t2.id `Student.Id`,t2.name `Student.Name`,t2.`Age` `Student.Age`,t2.addr `Student.Addr`
                from test t
                    left join test2 t2 on t.id = t2.id;
                """);
            var list = db.Select<Person>().LeftJoin<Student>((i, j) => i.Id == j.Id).ToList((i, j) => new
            {
                Id = i.Id,
                Name = i.Name,
                Person = i,
                Student = j
            });
            list.ToJson().ShouldBe("""
                [{"Id":1,"Name":"tom","Person":{"Id":1,"Name":"tom","Age":18,"Addr":"天命"},"Student":{"Id":1,"Name":"jack","Age":180,"Addr":"天明"}},{"Id":2,"Name":"tom2","Person":{"Id":2,"Name":"tom2","Age":19,"Addr":"天命2"},"Student":{"Id":2,"Name":"jack2","Age":190,"Addr":"天明2"}},{"Id":3,"Name":"tom","Person":{"Id":3,"Name":"tom","Age":20,"Addr":"天命3"},"Student":{"Id":3,"Name":"jack","Age":200,"Addr":"天明3"}}]
                """);
        }

        [Test]
        public void TestMultiNested2()
        {
            string sql;
            sql = db.Select<Person>().LeftJoin<Student>((i, j) => i.Id == j.Id).ToSqlList((i, j) => new PersonStudentInfo
            {
                Id = i.Id,
                Name = i.Name,
                Person = i,
                Student = j
            });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.id `Person.Id`,t.name `Person.Name`,t.`Age` `Person.Age`,t.addr `Person.Addr`,t2.id `Student.Id`,t2.name `Student.Name`,t2.`Age` `Student.Age`,t2.addr `Student.Addr`
                from test t
                    left join test2 t2 on t.id = t2.id;
                """);
            var list = db.Select<Person>().LeftJoin<Student>((i, j) => i.Id == j.Id).ToList((i, j) => new PersonStudentInfo
            {
                Id = i.Id,
                Name = i.Name,
                Person = i,
                Student = j
            });
            list.ToJson().ShouldBe("""
                [{"Id":1,"Name":"tom","Person":{"Id":1,"Name":"tom","Age":18,"Addr":"天命"},"Student":{"Id":1,"Name":"jack","Age":180,"Addr":"天明"}},{"Id":2,"Name":"tom2","Person":{"Id":2,"Name":"tom2","Age":19,"Addr":"天命2"},"Student":{"Id":2,"Name":"jack2","Age":190,"Addr":"天明2"}},{"Id":3,"Name":"tom","Person":{"Id":3,"Name":"tom","Age":20,"Addr":"天命3"},"Student":{"Id":3,"Name":"jack","Age":200,"Addr":"天明3"}}]
                """);
        }

        [Test]
        public void TestMultiNested3()
        {
            string sql;
            sql = db.Select<Person>().LeftJoin<Student>((i, j) => i.Id == j.Id).ToSqlList((i, j) => new PersonStudentInfo
            {
                Id = i.Id,
                Name = i.Name,
                Person = new Person
                {
                    Id = i.Id,
                    Name = i.Name,
                },
                Student = j
            });
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,t.id `Person.Id`,t.name `Person.Name`,t2.id `Student.Id`,t2.name `Student.Name`,t2.`Age` `Student.Age`,t2.addr `Student.Addr`
                from test t
                    left join test2 t2 on t.id = t2.id;
                """);
            var list = db.Select<Person>().LeftJoin<Student>((i, j) => i.Id == j.Id).ToList((i, j) => new PersonStudentInfo
            {
                Id = i.Id,
                Name = i.Name,
                Person = new Person
                {
                    Id = i.Id,
                    Name = i.Name,
                },
                Student = j
            });
            list.ToJson().ShouldBe("""
                [{"Id":1,"Name":"tom","Person":{"Id":1,"Name":"tom","Age":0,"Addr":null},"Student":{"Id":1,"Name":"jack","Age":180,"Addr":"天明"}},{"Id":2,"Name":"tom2","Person":{"Id":2,"Name":"tom2","Age":0,"Addr":null},"Student":{"Id":2,"Name":"jack2","Age":190,"Addr":"天明2"}},{"Id":3,"Name":"tom","Person":{"Id":3,"Name":"tom","Age":0,"Addr":null},"Student":{"Id":3,"Name":"jack","Age":200,"Addr":"天明3"}}]
                """);
        }
    }
}
