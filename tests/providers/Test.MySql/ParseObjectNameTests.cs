﻿using DBUtil;
using NUnit.Framework;
using Shouldly;
using System;

namespace Test.MySql
{
    [TestFixture]
    internal sealed class ParseObjectNameTests : TestBase
    {
        #region 测试表名解析
        [Test]
        public void TestNormal()
        {
            var refer = new ObjectName
            {
                FullName = "table1",
                DataBaseName = "",
                SchemaName = "",
                Name = "table1",
                NormalName = "table1",
                SegCount = 1,
                FullNameQuoted = "`table1`",
                DataBaseNameQuoted = "",
                SchemaNameQuoted = "",
                NameQuoted = "`table1`",
                NormalNameQuoted = "`table1`",
                OriginalName = "",//自动设置
            };
            tt(" table1  ", refer);
            tt(" `table1`\t", refer);
            tt("`table1`", refer);
            tt("table1", refer);
            tt(" \"table1\" \t", refer);
            tt(" table1 \t", refer);
            tt(" \"table1\" ", refer);
        }

        [Test]
        public void TestNormal2Seg()
        {
            var refer = new ObjectName
            {
                FullName = "testdb.table1",
                DataBaseName = "testdb",
                SchemaName = "testdb",
                Name = "table1",
                NormalName = "table1",
                SegCount = 2,
                FullNameQuoted = "`testdb`.`table1`",
                DataBaseNameQuoted = "`testdb`",
                SchemaNameQuoted = "`testdb`",
                NameQuoted = "`table1`",
                NormalNameQuoted = "`table1`",
                OriginalName = "",//自动设置
            };
            tt(" testdb.table1  ", refer);
            tt("`testdb`.`table1`", refer);
            tt("testdb.`table1`", refer);
            tt("`testdb`.table1", refer);
            tt(" \"testdb\".\"table1\" \t", refer);
            tt(" \"testdb\".table1 \t", refer);
            tt(" testdb.\"table1\" ", refer);
        }
        private void tt(string name, ObjectName refer)
        {
            refer.OriginalName = name;

            var objectName = db.ParseObjectName(name);
            objectName.ShouldNotBeNull();
            objectName.FullName.ShouldBe(refer.FullName);
            objectName.DataBaseName.ShouldBe(refer.DataBaseName);
            objectName.SchemaName.ShouldBe(refer.SchemaName);
            objectName.Name.ShouldBe(refer.Name);
            objectName.NormalName.ShouldBe(refer.NormalName);
            objectName.OriginalName.ShouldBe(refer.OriginalName);
            objectName.SegCount.ShouldBe(refer.SegCount);

            objectName.FullNameQuoted.ShouldBe(refer.FullNameQuoted);
            objectName.DataBaseNameQuoted.ShouldBe(refer.DataBaseNameQuoted);
            objectName.SchemaNameQuoted.ShouldBe(refer.SchemaNameQuoted);
            objectName.NameQuoted.ShouldBe(refer.NameQuoted);
            objectName.NormalNameQuoted.ShouldBe(refer.NormalNameQuoted);
        }

        [Test]
        public void TestError()
        {
            Exception ex = null;
            ex = Should.Throw<Exception>(() => db.ParseObjectName("testdb.sche.table1"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`table1"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("table1`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("table1\""));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("\"table1"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`table1\""));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`testdb``table1`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`testdb  ``table1`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`testdb  `\r`table1`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`testdb  `\n`table1`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`testdb  `\t`table1`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("`testdb`..`table1`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName(".`testdb`"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("test'db"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("testdb..table1"));
            ex = Should.Throw<Exception>(() => db.ParseObjectName("table1."));
        }
        #endregion
    }
}
