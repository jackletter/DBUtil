﻿using DBUtil;
using DBUtil.Provider.MySql;
using DotNetCommon;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;

namespace Test.MySql
{
    /// <summary>
    /// 测试Id和流水号生成,基于数据库缓存
    /// </summary>
    [TestFixture]
    public sealed partial class GeneratorTests : TestBase
    {
        private void ClearDBCache()
        {
            var generator = db.Setting.Generator as MySqlIdSNOGenerator;
            if (generator != null) generator.ClearCache(db);
        }

        #region Id生成中的各种情况测试
        /// <summary>
        /// 当表不存在报错时
        /// </summary>
        [Test]
        public void TestNoTableError()
        {
            ClearDBCache();
            db.ExecuteSql(db.Manage.DropTableIfExistSql("test5"));
            try
            {
                var id = db.NewId("test5", "id");
                throw new Exception("竟然没报错!");
            }
            catch { }
        }

        /// <summary>
        /// 当表中还没有数据时
        /// </summary>
        [Test]
        public void TestTableHasNoData()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            var id = db.NewId("test", "id");
            Assert.IsTrue(id == 1);
            id = db.NewId("test", "id");
            Assert.IsTrue(id == 2);
        }

        /// <summary>
        /// 当表中已有数据时
        /// </summary>
        [Test]
        public void TestTableHasData()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            db.Insert("test", new { id = 1, name = "小明" }.ToDictionary()).ExecuteAffrows();
            var id = db.NewId("test", "id");
            Assert.IsTrue(id == 2);
            id = db.NewId("test", "id");
            Assert.IsTrue(id == 3);
        }

        /// <summary>
        /// 当表中已有缓存时
        /// </summary>
        [Test]
        public void TestHasDataCache()
        {
            ClearDBCache();
            db.ExecuteSql(@"
CREATE TABLE `__generator_id` (
  `tablename` varchar(50) NOT NULL,
  `colname` varchar(50) NOT NULL,
  `currentid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`tablename`,`colname`)
) ENGINE=InnoDB;");
            db.Insert("__generator_id", new { tablename = "test", colname = "id", currentid = 5 }.ToDictionary()).ExecuteAffrows();
            var id = db.NewId("test", "id");
            Assert.IsTrue(id == 6);
            id = db.NewId("test", "id");
            Assert.IsTrue(id == 7);
        }

        /// <summary>
        /// 测试未获取到锁超时
        /// </summary>
        [Test]
        public async Task TestErrorTimeout()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            var id = db.NewId("test", "id");
            Assert.IsTrue(id == 1);

            //制造一个锁 30秒
            db.RunInSession(() =>
            {
                var res = db.SelectScalar<int?>($"select GET_LOCK({$"'{db.DBName}_test_id',3"})");
            });

            var task1 = Task.Run(() =>
            {
                db.RunInSession(() =>
                {
                    var res = db.SelectScalar<int?>($"select GET_LOCK({$"'{db.DBName}_test_id',3"})");
                    if (res != 1) throw new Exception($"获取锁失败!");
                    Thread.Sleep(25 * 1000);
                    db.SelectScalar<int?>($"select RELEASE_LOCK({$"'{db.DBName}_test_id'"})");
                });
            });
            var task2 = Task.Run(() =>
             {
                 try
                 {
                     Thread.Sleep(1000);
                     var id = db.NewId("test", "id");
                     throw new Exception("应该报超时的错!");
                 }
                 catch (Exception ex)
                 {
                     //锁超时信息
                     Assert.IsTrue(ex.Message.Contains("20秒内"));
                 }
             });
            Task.WaitAll(task1, task2);
        }
        #endregion

        #region Id生成
        [Test]
        public void NewId_DBCacheTest()
        {
            ClearDBCache();
            //准备测试表
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(
id int primary key,
name varchar(50)
)");
            //单个id生成
            long id = db.NewId("test", "id");
            Assert.IsTrue(id == 1);
            id = db.NewId("test", "id");
            Assert.IsTrue(id == 2);
            //根据表中已有的id,继续生成
            var dic = new Dictionary<string, object>();
            dic.Add("id", id);
            dic.Add("name", "xiaoming" + id);
            db.Insert("test", dic).ExecuteAffrows();
            ClearDBCache();
            id = db.NewId("test", "id");
            Assert.IsTrue(id == 3);
            //根据缓存批量生成id
            var ids = db.NewIds("test", "id", 5);
            var expect = new List<long>() { 4, 5, 6, 7, 8 };
            var expectStr = string.Join(",", expect);
            var actStr = string.Join(",", ids);
            Assert.IsTrue(expectStr == actStr);
            //根据表中已有的id,继续批量生成
            ClearDBCache();
            ids = db.NewIds("test", "id", 5);
            expect = new List<long>() { 3, 4, 5, 6, 7 };
            expectStr = string.Join(",", expect);
            actStr = string.Join(",", ids);
            Assert.IsTrue(expectStr == actStr);
            //直接从缓存中生成
            TruncateTable("test");
            ClearDBCache();
            ids = db.NewIds("test", "id", 5);
            expect = new List<long>() { 1, 2, 3, 4, 5 };
            expectStr = string.Join(",", expect);
            actStr = string.Join(",", ids);
            Assert.IsTrue(expectStr == actStr);
        }


        [Test]
        public void NewIdConcurrency_DBCacheTest()
        {
            ClearDBCache();
            //准备测试表
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(
                id int primary key,
                name varchar(50)
            )");
            var tasks = new List<Task>();
            var idres = new System.Collections.Concurrent.ConcurrentQueue<(long id, int threadid)>();
            for (int i = 0; i < 100; i++)
            {
                tasks.Add(Task.Run(() =>
                {
                    try
                    {
                        for (int j = 0; j < 1; j++)
                        {
                            var idg = db.NewId("test", "id");
                            idres.Enqueue((idg, Thread.CurrentThread.ManagedThreadId));
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex?.Message + ex?.StackTrace);
                        Debug.Fail(ex?.Message, ex?.StackTrace);
                    }
                }));
            }
            Task.WaitAll(tasks.ToArray());
            var list = idres.ToList();
            list = list.OrderByDescending(t => t.id).ToList();
            var expactCount = 100 * 1;
            Assert.IsTrue(list.LastOrDefault().id == 1);
            Assert.IsTrue(list.FirstOrDefault().id == expactCount);
            Assert.IsTrue(list.Count == expactCount);
        }
        #endregion

        #region SNO生成中的各种情况测试
        /// <summary>
        /// 表不存在,报错
        /// </summary>
        [Test]
        public void TestErrorSql()
        {
            ClearDBCache();
            DropTableIfExist("test");
            try
            {
                var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
                throw new Exception("竟然没报错!");
            }
            catch (Exception ex)
            {
                int i = 0;
            }
        }

        /// <summary>
        /// 无缓存 表中没有匹配数据
        /// </summary>
        [Test]
        public void TestNoCacheNoData()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql("create table test(sno varchar(50))");
            var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000001");
            sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000002");
        }

        /// <summary>
        /// 无缓存 表中有匹配数据
        /// </summary>
        [Test]
        public void TestNoCacheHasData()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql("create table test(sno varchar(50))");
            db.Insert("test", new { sno = $"SNO{DateTime.Now.ToString("yyyyMMdd")}000005" }.ToDictionary()).ExecuteAffrows();
            var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000006");
            sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000007");
        }

        /// <summary>
        /// 有缓存,时间戳对不上
        /// </summary>
        [Test]
        public void TestHasCacheWrongTimeStamp()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql(@"
create table __generator_sno
(
	tablename varchar(50),
	colname varchar(50),
	statictext varchar(200),
	machineidstr varchar(50),
	nowstr varchar(100),
	currentno bigint,
	primary key(tablename,colname,statictext)
)");
            db.Insert("__generator_sno", new { tablename = "test", colname = "sno", statictext = $"SNO", nowstr = DateTime.Now.AddDays(-1).ToString("yyyyMMdd"), currentno = "5" }.ToDictionary()).ExecuteAffrows();
            var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));

            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000001");
            sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000002");
        }

        /// <summary>
        /// 有缓存,时间戳对得上
        /// </summary>
        [Test]
        public void TestHasCacheRightTimeStamp()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql(@"
create table __generator_sno
(
	tablename varchar(50),
	colname varchar(50),
	statictext varchar(200),
	machineidstr varchar(50),
	nowstr varchar(100),
	currentno bigint,
	primary key(tablename,colname,statictext)
)");
            db.Insert("__generator_sno", new { tablename = "test", colname = "sno", statictext = $"SNO", nowstr = DateTime.Now.ToString("yyyyMMdd"), currentno = "2" }.ToDictionary()).ExecuteAffrows();
            var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));

            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000003");
            sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000004");
        }

        [Test]
        public void TestLockTimeout()
        {
            ClearDBCache();
            DropTableIfExist("test");
            db.ExecuteSql("create table test(sno varchar(50))");
            var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}000001");

            //制造一个锁 30秒
            var task1 = Task.Run(() =>
            {
                try
                {
                    db.RunInSession(() =>
                    {
                        var res = db.SelectScalar<int?>($"select GET_LOCK({$"'{db.DBName}_test_sno_SNO',3"})");
                        if (res != 1) throw new Exception($"获取锁失败!");
                        Thread.Sleep(25 * 1000);
                        db.SelectScalar<int?>($"select RELEASE_LOCK({$"'{db.DBName}_test_sno_SNO'"})");
                    });
                }
                catch (Exception ex)
                {
                    //不应该报错
                    throw;
                }
            });
            var task2 = Task.Factory.StartNew(() =>
            {
                try
                {
                    Thread.Sleep(1000);
                    var id = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
                    throw new Exception("应该报超时的错!");
                }
                catch (Exception ex)
                {
                    //锁超时信息
                    Assert.IsTrue(ex.Message.Contains("20秒内"));
                }
            });
            Task.WaitAll(task1, task2);
        }
        #endregion

        #region SNO生成
        [Test]
        public void NewSNO_DBCacheTest()
        {
            ClearDBCache();
            //准备测试表
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(
id int primary key,
sno varchar(50),
name varchar(50)
)");
            //单个生成
            var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == "SNO" + DateTime.Now.ToString("yyyyMMdd") + "000001");
            sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == "SNO" + DateTime.Now.ToString("yyyyMMdd") + "000002");
            //根据表中已有的sno,继续生成
            var dic = new Dictionary<string, object>();
            var id = db.NewId("test", "id");
            dic.Add("id", id);
            dic.Add("name", "xiaoming" + id);
            dic.Add("sno", sno);
            db.Insert("test", dic).ExecuteAffrows();
            ClearDBCache();
            sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == "SNO" + DateTime.Now.ToString("yyyyMMdd") + "000003");
            var dt3 = db.SelectDataTable($"select * from test");
            //根据缓存批量生成sno
            var snos = db.NewSNOs("test", "sno", SerialFormat.CreateFast("SNO"), 5);
            var expect = new List<string>() { $"SNO{DateTime.Now.ToString("yyyyMMdd")}000004", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000005", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000006", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000007", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000008" };
            var expectStr = string.Join(",", expect);
            var actStr = string.Join(",", snos);
            Assert.IsTrue(expectStr == actStr);
            //根据表中已有的id,继续批量生成
            ClearDBCache();
            snos = db.NewSNOs("test", "sno", SerialFormat.CreateFast("SNO"), 5);
            expect = new List<string>() { $"SNO{DateTime.Now.ToString("yyyyMMdd")}000003", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000004", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000005", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000006", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000007" };
            expectStr = string.Join(",", expect);
            actStr = string.Join(",", snos);
            Assert.IsTrue(expectStr == actStr);
            //直接从缓存中生成
            TruncateTable("test");
            ClearDBCache();
            sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
            Assert.IsTrue(sno == "SNO" + DateTime.Now.ToString("yyyyMMdd") + "000001");
            snos = db.NewSNOs("test", "sno", SerialFormat.CreateFast("SNO"), 5);
            expect = new List<string>() { $"SNO{DateTime.Now.ToString("yyyyMMdd")}000002", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000003", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000004", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000005", $"SNO{DateTime.Now.ToString("yyyyMMdd")}000006" };
            expectStr = string.Join(",", expect);
            actStr = string.Join(",", snos);
            Assert.IsTrue(expectStr == actStr);
        }


        [Test]
        public void NewSNO_DBCacheConcurrencyTest()
        {
            ClearDBCache();
            //准备测试表
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(
            id int primary key,
            sno varchar(50),
            name varchar(50) 
            )");
            TruncateTable("test");
            var tasks = new List<Task>();
            var snores = new System.Collections.Concurrent.ConcurrentQueue<(string sno, int threadid)>();
            var exceptions = new ConcurrentBag<Exception>();
            for (int i = 0; i < 100; i++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    try
                    {
                        for (int j = 0; j < 1; j++)
                        {
                            var sno = db.NewSNO("test", "sno", SerialFormat.CreateFast("SNO"));
                            snores.Enqueue((sno, Thread.CurrentThread.ManagedThreadId));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex?.Message + ex?.StackTrace);
                        exceptions.Add(ex);
                    }
                }, TaskCreationOptions.LongRunning));
            }
            Task.WaitAll(tasks.ToArray());
            var list = snores.ToList();
            list = list.OrderByDescending(t => t.sno).ToList();
            var expactCount = 100 * 1;
            Assert.IsTrue(list.FirstOrDefault().sno == $"SNO{DateTime.Now.ToString("yyyyMMdd")}{expactCount.ToString().PadLeft(6, '0')}");
            Assert.IsTrue(list.Count == expactCount);
            Assert.IsEmpty(exceptions);
        }
        #endregion

        #region 自定义id和sno生成器

        public class MemoryIdSNOGenerator : IIdSNOGenerator
        {
            internal ConcurrentDictionary<string, long> memocache = new ConcurrentDictionary<string, long>();
            public long NewId(DBAccess db, string tableName, string colName)
            {
                var list = NewIds(db, tableName, colName, 1);
                return list.First();
            }

            public async Task<long> NewIdAsync(DBAccess db, string tableName, string colName, CancellationToken cancellationToken = default)
            {
                var list = await NewIdsAsync(db, tableName, colName, 1);
                return list.First();
            }

            public long[] NewIds(DBAccess db, string tableName, string colName, int count)
            {
                return NewIdsAsync(db, tableName, colName, count).Result;
            }

            public Task<long[]> NewIdsAsync(DBAccess db, string tableName, string colName, int count, CancellationToken cancellationToken = default)
            {
                var res = memocache.AddOrUpdate($"{tableName}_{colName}", 1, (key, old) => old + count);
                var list = new List<long>();
                for (long i = res - count + 1; i <= res; i++) list.Add(i);
                return Task.FromResult(list.ToArray());
            }

            public string NewSNO(DBAccess db, string tableName, string colName, SerialFormat serialFormat)
            {
                return NewSNOAsync(db, tableName, colName, serialFormat).Result;
            }

            public async Task<string> NewSNOAsync(DBAccess db, string tableName, string colName, SerialFormat serialFormat, CancellationToken cancellationToken = default)
            {
                var res = await NewSNOsAsync(db, tableName, colName, serialFormat, 1, cancellationToken);
                return res.First();
            }

            public string[] NewSNOs(DBAccess db, string tableName, string colName, SerialFormat serialFormat, int count)
            {
                return NewSNOsAsync(db, tableName, colName, serialFormat, count).Result;
            }

            public Task<string[]> NewSNOsAsync(DBAccess db, string tableName, string colName, SerialFormat serialFormat, int count, CancellationToken cancellationToken = default)
            {
                (string likeStr, DateTime snoNow, int startIndex) = SerialFormat.Parse(serialFormat, DateTime.Now, DotNetCommon.Machine.MachineIdString);
                var key = $"{tableName}_{colName}_{likeStr}";
                var res = memocache.AddOrUpdate(key, 1, (key, old) => old + count);
                var list = new List<string>();
                var sernum = serialFormat.Chunks.FirstOrDefault(i => i.Type == SerialFormatChunkType.SerialNo);
                for (long i = res - count + 1; i <= res; i++)
                {
                    var seg = i.ToString().PadLeft(sernum.Length, '0');
                    list.Add($"{likeStr.TrimEnd('%')}{seg}");
                }
                return Task.FromResult(list.ToArray());
            }
        }

        [Test]
        public void TestCustomeId()
        {
            var memocache = new ConcurrentDictionary<string, long>();
            db = DBFactory.CreateDB(db.DBType, db.DBConn, DBSetting.NewInstance()
                .SetIdSNOGenerator(new MemoryIdSNOGenerator()));

            var id = db.NewId("test", "id");
            id.ShouldBe(1);
            id = db.NewId("test", "id");
            id.ShouldBe(2);
            var generator = db.Setting.Generator as MemoryIdSNOGenerator;
            generator.memocache.Clear();
            id = db.NewId("test", "id");
            id.ShouldBe(1);
        }
        [Test]
        public void TestCustomeSNO()
        {
            db = DBFactory.CreateDB(db.DBType, db.DBConn, DBSetting.NewInstance()
                .SetIdSNOGenerator(new MemoryIdSNOGenerator()));

            var id = db.NewSNO("test", "id", SerialFormat.CreateDistributeFast("tttt"));
            id.ShouldBe($"tttt{DateTime.Now.ToString("yyyyMMdd")}{DotNetCommon.Machine.MachineId.ToString().PadLeft(4, '0')}000001");
            id = db.NewSNO("test", "id", SerialFormat.CreateDistributeFast("tttt"));
            id.ShouldBe($"tttt{DateTime.Now.ToString("yyyyMMdd")}{DotNetCommon.Machine.MachineId.ToString().PadLeft(4, '0')}000002");
            var generator = db.Setting.Generator as MemoryIdSNOGenerator;
            generator.memocache.Clear();
            id = db.NewSNO("test", "id", SerialFormat.CreateDistributeFast("tttt"));
            id.ShouldBe($"tttt{DateTime.Now.ToString("yyyyMMdd")}{DotNetCommon.Machine.MachineId.ToString().PadLeft(4, '0')}000001");
        }
        #endregion
    }
}
