﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.Updates.UpdateByExpression
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Table("t_user")]
        public class TUser
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int Age { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }

            [Column("score")]
            public float? Score { get; set; }

            [Column("addr")]
            public string Addr { get; set; }
        }

        [Test]
        public void Test()
        {
            var dt = DateTime.Parse("2023-09-01 01:01:01.123456");
            var sql = "";
            sql = db.Update<TUser>().SetExpr(() => new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = '小明',
                    addr = '天命路'
                where id = 1;
                """);

            sql = db.Update<TUser>().SetExpr(() => new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = '小明',
                    addr = '天命路'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(() => new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = '小明',
                    addr = '天命路',
                    CreateTime = '2023-09-01 01:01:01.123456'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(() => new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    addr = '天命路',
                    CreateTime = '2023-09-01 01:01:01.123456'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(() => new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .IgnoreColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    CreateTime = '2023-09-01 01:01:01.123456',
                    addr = '天明路2'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(() => new TUser
            {
                Id = 1,
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .OnlyColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    score = 98.5
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);
        }

        [Test]
        public void TestUseOldData()
        {
            var dt = DateTime.Parse("2023-09-01 01:01:01.123456");
            var sql = "";
            sql = db.Update<TUser>().SetExpr(i => new TUser
            {
                Id = 1,
                Name = i.Name + "小明",
                Addr = "天命路",
            }).ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = concat_ws('',name,'小明'),
                    addr = '天命路'
                where id = 1;
                """);

            sql = db.Update<TUser>().SetExpr(i => new TUser
            {
                Id = 1,
                Name = i.Name + "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = concat_ws('',name,'小明'),
                    addr = '天命路'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(i => new TUser
            {
                Id = 1,
                Name = i.Name + "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = concat_ws('',name,'小明'),
                    addr = '天命路',
                    CreateTime = '2023-09-01 01:01:01.123456'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(i => new TUser
            {
                Id = 1,
                Name = i.Name + "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Addr)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = concat_ws('',name,'小明'),
                    CreateTime = '2023-09-01 01:01:01.123456'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(i => new TUser
            {
                Id = 1,
                Name = i.Name + "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Addr)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .IgnoreColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    name = concat_ws('',name,'小明'),
                    CreateTime = '2023-09-01 01:01:01.123456',
                    addr = '天明路2'
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(i => new TUser
            {
                Id = 1,
                Age = 20 + i.Age,
                Birth = dt,
                Score = 0.5f + i.Score
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .OnlyColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe("""
                update t_user set
                    score = 0.5 + score
                where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '2023-09-01 01:01:01.123456'));
                """);

            sql = db.Update<TUser>().SetExpr(old => new TUser { Name = old.Name + "_test" }).AsTableIf(true, old => old + "_user")
                .WhereSeg<int>(id => id == 1)
                .ToSql();
            sql.ShouldBe("""
                update t_user_user set
                    name = concat_ws('',name,'_test')
                where id = 1;
                """);
        }
    }
}
