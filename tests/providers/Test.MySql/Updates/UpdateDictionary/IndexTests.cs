﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Linq;

namespace Test.MySql.Updates.UpdateDictionary
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Test]
        public void TestSql()
        {
            var dt = DateTime.Parse("2023-02-02 01:02:03.123456");
            string sql = "";
            sql = db.Update("test", new { Name = "小明", Age = 20, Addr = "天明路", Birth = dt }.ToDictionary())
                .Where("id=1")
                .ToSql();
            sql.ShouldBe(@"update test set 
    Name = '小明',
    Age = 20,
    Addr = '天明路',
    Birth = '2023-02-02 01:02:03.123456'
where id=1;");

            sql = db.Update("test", new { Name = "小明", Age = 20, Addr = "天明路", Birth = dt }.ToDictionary())
                .Where("id=1").Where("Age>10")
                .ToSql();
            sql.ShouldBe(@"update test set 
    Name = '小明',
    Age = 20,
    Addr = '天明路',
    Birth = '2023-02-02 01:02:03.123456'
where (id=1) and (Age>10);");

            sql = db.Update("test", new { Name = "小明", Age = 20, Addr = "天明路", Birth = dt }.ToDictionary())
                .SetColumn("CreateTime", dt)
                .Where("id=1").Where("Age>10")
                .ToSql();
            sql.ShouldBe(@"update test set 
    Name = '小明',
    Age = 20,
    Addr = '天明路',
    Birth = '2023-02-02 01:02:03.123456',
    CreateTime = '2023-02-02 01:02:03.123456'
where (id=1) and (Age>10);");

            sql = db.Update("test", new { Name = "小明", Age = 20, Addr = "天明路", Birth = dt }.ToDictionary())
                .SetColumn("CreateTime", dt)
                .IgnoreColumns("Name")
                .Where("id=1").Where("Age>10")
                .ToSql();
            sql.ShouldBe(@"update test set 
    Age = 20,
    Addr = '天明路',
    Birth = '2023-02-02 01:02:03.123456',
    CreateTime = '2023-02-02 01:02:03.123456'
where (id=1) and (Age>10);");

            sql = db.Update("test", new { Name = "小明", Age = 20, Addr = "天明路", Birth = dt }.ToDictionary())
                .SetColumn("CreateTime", dt)
                .IgnoreColumns("Name")
                .OnlyColumns("Age")
                .Where("id=1").Where("Age>10")
                .ToSql();
            sql.ShouldBe(@"update test set 
    Age = 20
where (id=1) and (Age>10);");
        }

        [Test]
        public void Test()
        {
            DropTable("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50),age int,addr varchar(200),birth datetime(6))");
            db.Insert("test", new
            {
                Id = 1,
                Name = "小红",
                Age = 18,
                Addr = "胡同",
                Birth = DateTime.Now,
            }.ToDictionary()).ExecuteAffrows();

            var update = db.Update("test")
                .SetColumn("Name", "小明")
                .SetColumn("Age", 20)
                .SetColumn("Addr", "天明路")
                .SetColumn("Birth", DateTime.Parse("2023-02-02 20:00:10.708404"))
                .WhereSeg<int>(id => id == 1);
            var sql = update.ToSql();
            sql.ShouldBe(@"update test set 
    Name = '小明',
    Age = 20,
    Addr = '天明路',
    Birth = '2023-02-02 20:00:10.708404'
where id = 1;");
            update.ExecuteAffrows().ShouldBe(1);
            var dic = db.SelectDictionary("select * from test");
            dic["id"].ShouldBe(1);
            dic["name"].ShouldBe("小明");
            dic["age"].ShouldBe(20);
            dic["addr"].ShouldBe("天明路");
            dic["birth"].ShouldBe(DateTime.Parse("2023-02-02 20:00:10.708404"));
        }
    }
}
