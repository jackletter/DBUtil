﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.MySql.Updates
{
    [TestFixture]
    internal class WithSelectTests : TestBase
    {
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }
        }

        [Table("student")]
        class Student
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }
        }

        [Test]
        public void Test()
        {
            var sql = string.Empty;
            var p = new Person();

            sql = db.Update<Person>()
                .SetExpr(i => new Person
                {
                    Name = db.Select<Student>().FirstOrDefault(i => i.Name),
                    Age = 10
                })
                .Where(i => i.Id == 1)
                .ToSql();
            sql.ShouldBe("""
                update test set
                    name = (select t.name
                from student t
                limit 1),
                    age = 10
                where id = 1;
                """);

            var ids = new List<int>() { 1, 2, 3 };
            sql = db.Update<Person>()
                  .SetExpr(i => new Person
                  {
                      Name = db.Select<Student>().Where(i => ids.Select(i => i + 1).Contains(i.Id)).FirstOrDefault(i => i.Name),
                      Age = 10
                  })
                  .Where(i => i.Id == 1)
                  .ToSql();
            sql.ShouldBe("""
                update test set
                    name = (select t.name
                from student t
                where t.id in (2,3,4)
                limit 1),
                    age = 10
                where id = 1;
                """);

            sql = db.Update<Person>()
                .SetExpr(i => new Person
                {
                    Name = db.Select<Student>().Where(j => j.Id == i.Id).FirstOrDefault(i => i.Name),
                    Age = 10
                })
                .Where(i => i.Id == 1)
                .ToSql();
            sql.ShouldBe("""
                update test set
                    name = (select t.name
                from student t
                where t.id = id
                limit 1),
                    age = 10
                where id = 1;
                """);

            Console.WriteLine(sql);
        }
    }
}
