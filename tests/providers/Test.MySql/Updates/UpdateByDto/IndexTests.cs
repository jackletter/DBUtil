﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Updates.UpdateByDto
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Table("test")]
        public class TUser
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int Age { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }

            [Column("score")]
            public float? Score { get; set; }

            [Column("addr")]
            public string Addr { get; set; }
        }

        [Test]
        public void Test()
        {
            var dt = DateTime.Parse("1990-01-02 01:02:03");
            var sql = "";
            var update = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            });
            sql = update.ToSql();
            sql.ShouldBe(@"update test set
    name = '小明',
    addr = '天命路'
where id = 1;");

            sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .ToSql();
            sql.ShouldBe(@"update test set
    name = '小明',
    addr = '天命路'
where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '1990-01-02 01:02:03'));");

            sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).UpdatePrimary().Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .ToSql();
            sql.ShouldBe(@"update test set
    id = 1,
    name = '小明',
    addr = '天命路'
where (Age>18) and (Age<60) and (name like '王%') and (birth > '1990-01-02 01:02:03');");

            sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .ToSql();
            sql.ShouldBe(@"update test set
    name = '小明',
    addr = '天命路',
    CreateTime = '1990-01-02 01:02:03'
where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '1990-01-02 01:02:03'));");

            sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .ToSql();
            sql.ShouldBe(@"update test set
    addr = '天命路',
    CreateTime = '1990-01-02 01:02:03'
where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '1990-01-02 01:02:03'));");

            sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .IgnoreColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe(@"update test set
    CreateTime = '1990-01-02 01:02:03',
    addr = '天明路2'
where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '1990-01-02 01:02:03'));");

            sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .OnlyColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe(@"update test set
    score = 98.5
where id = 1 and ((Age>18) and (Age<60) and (name like '王%') and (birth > '1990-01-02 01:02:03'));");
        }

        [Test]
        public void Test2()
        {
            //Entity中未声明的列不会参与生成sql,写了也没用
            var sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
                NoProp = 2
            }).ToSql();
            sql.ShouldBe(@"update test set
    name = '小明'
where id = 1;");
        }

        [Test]
        public void TestValidError()
        {
            try
            {
                db.Update<TUser>().SetDto(new
                {
                    Id = 1,
                    Name = "小明"
                }).UpdatePrimary().ToSql();
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("UpdateByDto<Test.MySql.Updates.UpdateByDto.IndexTests.TUser>:必须指定过滤条件");
            }
        }

        /// <summary>
        /// 测试实体中未定义的列
        /// </summary>
        [Test]
        public void TestOtherColumns()
        {
            var sql = db.Update<TUser>().SetDto(new
            {
                Id = 1,
                Name = "小明",
            }).SetColumn("haha", 12).ToSql();
            sql.ShouldBe("""
                update test set
                    name = '小明',
                    haha = 12
                where id = 1;
                """);
        }
    }
}
