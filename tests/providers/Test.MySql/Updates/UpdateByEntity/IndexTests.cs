﻿using NUnit.Framework;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using DBUtil.Attributes;
using Shouldly;

namespace Test.MySql.Updates.UpdateByEntity
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Table("test")]
        public class TUser
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int Age { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }

            [Column("score")]
            public float? Score { get; set; }

            [Column("addr")]
            public string Addr { get; set; }
        }

        [Test]
        public void Test()
        {
            var dt = DateTime.Parse("1990-01-02 01:02:03");
            var sql = "";
            sql = db.Update<TUser>().SetEntity(new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).ToSql();
            sql.ShouldBe(@"update test set
    name = '小明',
    age = 20,
    birth = '1990-01-02 01:02:03',
    score = 98.5,
    addr = '天命路'
where id = 1;");

            sql = db.Update<TUser>().SetEntity(new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).UpdatePrimary()
            .Where("Age>18").Where("Age<60")
            .Where(i => i.Name.StartsWith("王")).Where(i => i.Birth > dt)
            .ToSql();
            sql.ShouldBe(@"update test set
    id = 1,
    name = '小明',
    age = 20,
    birth = '1990-01-02 01:02:03',
    score = 98.5,
    addr = '天命路'
where (Age>18) and (Age<60) and (name like '王%') and (birth > '1990-01-02 01:02:03');");

            sql = db.Update<TUser>().SetEntity(new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).SetColumn("CreateTime", dt)
            .ToSql();
            sql.ShouldBe(@"update test set
    name = '小明',
    age = 20,
    birth = '1990-01-02 01:02:03',
    score = 98.5,
    addr = '天命路',
    CreateTime = '1990-01-02 01:02:03'
where id = 1;");

            sql = db.Update<TUser>().SetEntity(new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .ToSql();
            sql.ShouldBe(@"update test set
    age = 20,
    birth = '1990-01-02 01:02:03',
    score = 98.5,
    addr = '天命路',
    CreateTime = '1990-01-02 01:02:03'
where id = 1;");

            sql = db.Update<TUser>().SetEntity(new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .IgnoreColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe(@"update test set
    age = 20,
    birth = '1990-01-02 01:02:03',
    CreateTime = '1990-01-02 01:02:03',
    addr = '天明路2'
where id = 1;");

            sql = db.Update<TUser>().SetEntity(new TUser
            {
                Id = 1,
                Name = "小明",
                Addr = "天命路",
                Age = 20,
                Birth = dt,
                Score = 98.5f
            }).SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .OnlyColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe(@"update test set
    score = 98.5
where id = 1;");
        }

        [Test]
        public void TestMulti()
        {
            var dt = DateTime.Parse("1990-01-02 01:02:03");
            var sql = "";
            sql = db.Update<TUser>().SetEntity(new[] {
                new TUser{Id = 1,Name = "小明",Addr = "天命路",Age = 20,Birth = dt,Score = 98.5f},
                new TUser{Id = 2,Name = "小红",Addr = "天命路",Age = 18,Birth = dt,Score = 80.6f}
            }).ToSql();
            sql.ShouldBe(@"update test set
    name = case id
      when 1 then '小明'
      when 2 then '小红' end,
    age = case id
      when 1 then 20
      when 2 then 18 end,
    birth = case id
      when 1 then '1990-01-02 01:02:03'
      when 2 then '1990-01-02 01:02:03' end,
    score = case id
      when 1 then 98.5
      when 2 then 80.6 end,
    addr = case id
      when 1 then '天命路'
      when 2 then '天命路' end
where id in (1,2);");

            sql = db.Update<TUser>().SetEntity(new[] {
                new TUser{Id = 1,Name = "小明",Addr = "天命路",Age = 20,Birth = dt,Score = 98.5f},
                new TUser{Id = 2,Name = "小红",Addr = "天命路",Age = 18,Birth = dt,Score = 80.6f}
            }).SetColumn("CreateTime", dt)
            .ToSql();
            sql.ShouldBe(@"update test set
    name = case id
      when 1 then '小明'
      when 2 then '小红' end,
    age = case id
      when 1 then 20
      when 2 then 18 end,
    birth = case id
      when 1 then '1990-01-02 01:02:03'
      when 2 then '1990-01-02 01:02:03' end,
    score = case id
      when 1 then 98.5
      when 2 then 80.6 end,
    addr = case id
      when 1 then '天命路'
      when 2 then '天命路' end,
    CreateTime = '1990-01-02 01:02:03'
where id in (1,2);");

            sql = db.Update<TUser>().SetEntity(new[] {
                new TUser{Id = 1,Name = "小明",Addr = "天命路",Age = 20,Birth = dt,Score = 98.5f},
                new TUser{Id = 2,Name = "小红",Addr = "天命路",Age = 18,Birth = dt,Score = 80.6f}
            }).SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .ToSql();
            sql.ShouldBe(@"update test set
    age = case id
      when 1 then 20
      when 2 then 18 end,
    birth = case id
      when 1 then '1990-01-02 01:02:03'
      when 2 then '1990-01-02 01:02:03' end,
    score = case id
      when 1 then 98.5
      when 2 then 80.6 end,
    addr = case id
      when 1 then '天命路'
      when 2 then '天命路' end,
    CreateTime = '1990-01-02 01:02:03'
where id in (1,2);");

            sql = db.Update<TUser>().SetEntity(new[] {
                new TUser{Id = 1,Name = "小明",Addr = "天命路",Age = 20,Birth = dt,Score = 98.5f},
                new TUser{Id = 2,Name = "小红",Addr = "天命路",Age = 18,Birth = dt,Score = 80.6f}
            }).SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .IgnoreColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe(@"update test set
    age = case id
      when 1 then 20
      when 2 then 18 end,
    birth = case id
      when 1 then '1990-01-02 01:02:03'
      when 2 then '1990-01-02 01:02:03' end,
    CreateTime = '1990-01-02 01:02:03',
    addr = '天明路2'
where id in (1,2);");

            sql = db.Update<TUser>().SetEntity(new[] {
                new TUser{Id = 1,Name = "小明",Addr = "天命路",Age = 20,Birth = dt,Score = 98.5f},
                new TUser{Id = 2,Name = "小红",Addr = "天命路",Age = 18,Birth = dt,Score = 80.6f}
            })
            .SetColumn("CreateTime", dt)
            .IgnoreColumnsExpr(i => i.Name)
            .SetColumnExpr(i => i.Addr, "天明路2")
            .OnlyColumnsExpr(i => i.Score)
            .ToSql();
            sql.ShouldBe(@"update test set
    score = case id
      when 1 then 98.5
      when 2 then 80.6 end
where id in (1,2);");
        }

        [Test]
        public void TestValidError()
        {
            try
            {
                var sql = db.Update<TUser>().SetEntity(new[] {
                    new TUser{Id = 1,Name = "小明",Addr = "天命路",Age = 20},
                }).UpdatePrimary().ToSql();
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("UpdateByEntity<Test.MySql.Updates.UpdateByEntity.IndexTests.TUser>:必须指定过滤条件");
            }

            try
            {
                var sql = db.Update<TUser>().SetEntity(new[] {
                    new TUser{Id = 1,Name = "小明",Addr = "天命路",Age = 20},
                    new TUser{Id = 2,Name = "小明",Addr = "天命路",Age = 20},
                }).ToSql();
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("当更新多行数据时,无法更新主键值");
            }
        }
    }
}
