﻿using DBUtil.Attributes;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Updates.UpdateByEntity
{
    [TestFixture]
    internal class UpdateMultiPrimaryTests : TestBase
    {
        [Table("teacher_student")]
        public class TeacherStudentEntity
        {
            [PrimaryKey]
            [Column("teacherId")]
            public int TeacherId { get; set; }
            [PrimaryKey]
            [Column("studentId")]
            public int StudentId { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("createTime")]
            public DateTime CreateTime { get; set; }
        }

        [Test]
        public void Test()
        {
            var now = DateTime.Parse("2023-02-08 01:02:03.123");
            //单行
            var sql = db.Update<TeacherStudentEntity>().SetEntity(new TeacherStudentEntity
            {
                TeacherId = 1,
                StudentId = 1,
                Name = "Test",
                CreateTime = now,
            }).ToSql();
            sql.ShouldBe(@"update teacher_student set
    name = 'Test',
    createTime = '2023-02-08 01:02:03.123'
where teacherId = 1 and studentId = 1;");

            //多行
            sql = db.Update<TeacherStudentEntity>().SetEntity(new[]{new  TeacherStudentEntity
            {
                TeacherId = 1,
                StudentId = 1,
                Name = "Test",
                CreateTime = now,
            },new TeacherStudentEntity
            {
                TeacherId = 1,
                StudentId = 2,
                Name = "Test",
                CreateTime = now,
            } }).OnlyColumnsExpr(i => i.Name).ToSql();
            sql.ShouldBe(@"update teacher_student set
    name = case 
      when teacherId = 1 and studentId = 1 then 'Test'
      when teacherId = 1 and studentId = 2 then 'Test' end
where (teacherId,studentId) in ((1,1),(1,2));");
        }
    }
}
