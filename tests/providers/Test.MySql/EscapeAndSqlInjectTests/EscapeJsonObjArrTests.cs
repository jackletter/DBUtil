﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.EscapeAndSqlInjectTests
{
    internal class EscapeJsonObjArrTests : TestBase
    {
        #region nested model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "ext")]
            public JsonObject Ext { get; set; }
            [JsonStore(Bucket = "ext2", Key = "123 arr")]
            public JsonArray Arr { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int auto_increment primary key,ext json,ext2 json)""");
            db.ExecuteSql("""
                insert into test(ext,ext2) values('{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}',json_object('123 arr',cast('[{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}]' as json)));
                """);
        }

        private string str = """
                52'" k%_p\r\n t
                2
                """;

        #region insert
        [Test]
        public void TestInsertEntity()
        {
            var insert = db.Insert<Person>().SetEntity(new Person
            {
                Ext = new { Name = str }.ToJsonObject(),
                Arr = new[] { new { Name = str } }.ToJsonArray()
            });
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(ext,ext2) values('{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}',json_object('123 arr',cast('[{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}]' as json)));
                select id `Id`,json_value(ext,'$') `Ext`,json_value(ext2,'$."123 arr"') `Arr` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Ext["Name"].GetValue<string>().ShouldBe(str);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str);
        }

        [Test]
        public void TestInsertEntity_DataTable()
        {
            var insert = db.Insert<Person>().SetEntity(new Person
            {
                Ext = new { Name = str }.ToJsonObject(),
                Arr = new[] { new { Name = str } }.ToJsonArray()
            });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<Person>().Max(i => i.Id);
            var p = db.Select<Person>().Where(i => i.Id == id).FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str);
        }

        [Test]
        public void TestInsertSetColumn()
        {
            var insert = db.Insert<Person>().SetColumnExpr(i => i.Ext, new { Name = str }.ToJsonObject()).SetColumnExpr(i => i.Arr, new[] { new { Name = str } }.ToJsonArray());
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(ext,ext2) values('{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}',json_object('123 arr',cast('[{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}]' as json)));
                select id `Id`,json_value(ext,'$') `Ext`,json_value(ext2,'$."123 arr"') `Arr` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Ext["Name"].GetValue<string>().ShouldBe(str);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str);
        }

        [Test]
        public void TestInsertSetColumn_DataTable()
        {
            var dt = db.Insert<Person>().SetColumnExpr(i => i.Ext, new { Name = str }.ToJsonObject()).SetColumnExpr(i => i.Arr, new[] { new { Name = str } }.ToJsonArray()).ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<Person>().Max(i => i.Id);
            var p = db.Select<Person>().Where(i => i.Id == id).FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str);
        }
        #endregion

        #region update
        [Test]
        public void TestUpdateEntity()
        {
            var str2 = str + "_2";
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext["Name"] = str2;
            p.Arr[0]["Name"] = str2;
            var update = db.Update<Person>().SetEntity(p);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = '{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}',
                    ext2 = json_set(ifnull(ext2,json_object()),
                        '$."123 arr"',cast('[{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}]' as json))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str2);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str2);
        }
        [Test]
        public void TestUpdateSetColumn()
        {
            var str2 = str + "_2";
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Ext, i => i.Ext.SetFluent("Name", str2))
                .SetColumnExpr(i => i.Arr, i => i.Arr.SetFluent(0, i => i.AsObject().SetFluent("Name", str2)))
                .Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(json_value(ext,'$'),'{}'),'$."Name"','52\'\" k%_p\\r\\n t\r\n2_2'),
                    ext2 = json_set(ifnull(ext2,json_object()),
                        '$."123 arr"',json_set(ifnull(json_value(ext2,'$."123 arr"'),'[]'),'$[0]',json_set(ifnull(json_value(ifnull(json_value(ext2,'$."123 arr"'),'[]'),'$[0]'),'{}'),'$."Name"','52\'\" k%_p\\r\\n t\r\n2_2')))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str2);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetDto()
        {
            var str2 = str + "_2";
            var update = db.Update<Person>().SetDto(new
            {
                Ext = new { Name = str2 }.ToJsonObject(),
                Arr = new[] { new { Name = str2 } }.ToJsonArray(),
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = '{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}',
                    ext2 = json_set(ifnull(ext2,json_object()),
                        '$."123 arr"',cast('[{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}]' as json))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str2);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str2);
        }
        [Test]
        public void TestUpdateSetExpr()
        {
            var str2 = str + "_2";
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = i.Ext.SetFluent("Name", str2),
                Arr = i.Arr.SetFluent(0, i => i.AsObject().SetFluent("Name", str2)),
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(json_value(ext,'$'),'{}'),'$."Name"','52\'\" k%_p\\r\\n t\r\n2_2'),
                    ext2 = json_set(ifnull(ext2,json_object()),
                        '$."123 arr"',json_set(ifnull(json_value(ext2,'$."123 arr"'),'[]'),'$[0]',json_set(ifnull(json_value(ifnull(json_value(ext2,'$."123 arr"'),'[]'),'$[0]'),'{}'),'$."Name"','52\'\" k%_p\\r\\n t\r\n2_2')))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str2);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str2);
        }
        [Test]
        public void TestUpdateIncr()
        {
            var str2 = str + "_2";
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = i.Ext.SetFluent("Name", i => i + "_2"),
                Arr = i.Arr.SetFluent(0, i => i.AsObject().SetFluent("Name", i => i + "_2"))
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(json_value(ext,'$'),'{}'),'$."Name"',concat_ws('',json_value(ifnull(json_value(ext,'$'),'{}'),'$."Name"'),'_2')),
                    ext2 = json_set(ifnull(ext2,json_object()),
                        '$."123 arr"',json_set(ifnull(json_value(ext2,'$."123 arr"'),'[]'),'$[0]',json_set(ifnull(json_value(ifnull(json_value(ext2,'$."123 arr"'),'[]'),'$[0]'),'{}'),'$."Name"',concat_ws('',json_value(ifnull(json_value(ifnull(json_value(ext2,'$."123 arr"'),'[]'),'$[0]'),'{}'),'$."Name"'),'_2'))))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str2);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str2);
        }
        #endregion

        #region select
        [Test]
        public void TestSelect1()
        {
            var select = db.Select<Person>().Where(i => i.Ext["Name"].GetValue<string>().Contains("%"));
            var sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,json_value(t.ext,'$') `Ext`,json_value(t.ext2,'$."123 arr"') `Arr`
                from test t
                where (json_value(json_value(t.ext,'$'),'$."Name"' returning char)) like '%\%%';
                """);
            var p = select.FirstOrDefault();
            p.Ext["Name"].GetValue<string>().ShouldBe(str);
            p.Arr[0]["Name"].GetValue<string>().ShouldBe(str);

            //这个查不到数据 检查sql就行
            select = db.Select<Person>().Where(i => i.Ext["Name"].GetValue<string>().Contains("\"") && i.Ext["Name"].GetValue<string>().Contains("'\r\n"));
            sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,json_value(t.ext,'$') `Ext`,json_value(t.ext2,'$."123 arr"') `Arr`
                from test t
                where ((json_value(json_value(t.ext,'$'),'$."Name"' returning char)) like '%"%') and ((json_value(json_value(t.ext,'$'),'$."Name"' returning char)) like '%\'\r\n%');
                """);
        }

        class PersonTmp
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public JsonObject Ext { get; set; }
            public JsonObject Obj { get; set; }
        }

        [Test]
        public void TestWhereSeg()
        {
            var sql =
                $"""
                select {db.ColumnSeg<Person>(t => new { t.Id, t.Ext, Name = t.Ext["Name"].GetValue<string>(), Obj = t.Arr[0] })}
                from {db.TableSeg<Person>("t")}
                where {db.WhereSeg<Person>(t => t.Ext["Name"].GetValue<string>().Contains("%"))}
                """;
            sql.ShouldBe(
                """
                select t.id `Id`,json_value(t.ext,'$') `Ext`,json_value(json_value(t.ext,'$'),'$."Name"' returning char) `Name`,json_value(json_value(t.ext2,'$."123 arr"'),'$[0]') `Obj`
                from test t
                where (json_value(json_value(t.ext,'$'),'$."Name"' returning char)) like '%\%%'
                """);
            var p = db.SelectModel<PersonTmp>(sql);
            p.Name.ShouldBe(str);
            p.Ext["Name"].GetValue<string>().ShouldBe(str);
            p.Obj["Name"].GetValue<string>().ShouldBe(str);
        }
        class PersonTmp2
        {
            public string CaseInfo { get; set; }
            public Ext Ext { get; set; }
        }
        class Ext
        {
            public string Name { get; set; }
        }
        [Test]
        public void TestCaseSeg()
        {
            var sql =
                $"""
                select 
                    {db.CaseSeg().WhenSeg<Person>(i => i.Ext["Name"].GetValue<string>().Contains("%")).Then("danger").WhenSeg<Person>(i => i.Ext["Name"].GetValue<string>().Contains("\"")).Then("da").WhenSeg<Person>(i => i.Ext["Name"].GetValue<string>().EndsWith("'\r\n")).Then("d").EndAs("caseinfo")},
                    {db.ColumnSeg<Person>(i => new { Ext = new { Name = i.Ext["Name"].GetValue<string>() } })}
                from {db.TableSeg<Person>("i")}
                """;
            sql.ShouldBe("""
                select 
                    case 
                  when (json_value(json_value(i.ext,'$'),'$."Name"' returning char)) like '%\%%' then 'danger'
                  when (json_value(json_value(i.ext,'$'),'$."Name"' returning char)) like '%"%' then 'da'
                  when (json_value(json_value(i.ext,'$'),'$."Name"' returning char)) like '%\'\r\n' then 'd' end caseinfo,
                    json_value(json_value(i.ext,'$'),'$."Name"' returning char) `Ext.Name`
                from test i
                """);
            var p = db.SelectModel<PersonTmp2>(sql);
            p.CaseInfo.ShouldBe("danger");
            p.Ext.Name.ShouldBe(str);
        }
        #endregion
    }
}
