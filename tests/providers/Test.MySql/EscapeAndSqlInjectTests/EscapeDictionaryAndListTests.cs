﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Test.MySql.EscapeAndSqlInjectTests.EscapeStringTests;

namespace Test.MySql.EscapeAndSqlInjectTests
{
    [TestFixture]
    internal class EscapeDictionaryAndListTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "ext", Key = "123 %\"_")]
            public List<string> Names { get; set; }
            [JsonStore(Bucket = "ext2")]
            public Dictionary<string, string> Ext { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int auto_increment primary key,ext json,ext2 json)""");
            db.ExecuteSql("""
                insert into test(ext,ext2) values(json_object('123 %"_',cast('["52\'\\" k%_p\\\\r\\\\n t\\r\\n2","52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"]' as json)),'{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}');
                """);
        }
        private string str = """
                52'" k%_p\r\n t
                2
                """;
        private string str2 = """
                52'" k%_p\r\n t
                2_2
                """;
        #region insert
        [Test]
        public void TestInsertEntity()
        {
            var insert = db.Insert<Person>().SetEntity(new Person
            {
                Names = new List<string> { str, str2 },
                Ext = new Dictionary<string, string> { { "Name", str } }
            });
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(ext,ext2) values(json_object('123 %"_',cast('["52\'\\" k%_p\\\\r\\\\n t\\r\\n2","52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"]' as json)),'{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}');
                select id `Id`,json_value(ext,'$."123 %\\"_"') `Names`,json_value(ext2,'$') `Ext` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Names[0].ShouldBe(str);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str);
        }

        [Test]
        public void TestInsertEntity_DataTable()
        {
            var insert = db.Insert<Person>().SetEntity(new Person
            {
                Names = new List<string> { str, str2 },
                Ext = new Dictionary<string, string> { { "Name", str } }
            });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<Person>().Max(i => i.Id);
            var p = db.Select<Person>().Where(i => i.Id == id).FirstOrDefault();
            p.Names[0].ShouldBe(str);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str);
        }
        [Test]
        public void TestInsertSetColumn()
        {
            var insert = db.Insert<Person>().SetColumnExpr(i => i.Names, new List<string> { str, str2 }).SetColumnExpr(i => i.Ext, new { Name = str }.ToDictionary());
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(ext,ext2) values(json_object('123 %"_',cast('["52\'\\" k%_p\\\\r\\\\n t\\r\\n2","52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"]' as json)),'{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}');
                select id `Id`,json_value(ext,'$."123 %\\"_"') `Names`,json_value(ext2,'$') `Ext` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Names[0].ShouldBe(str);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str);
        }
        [Test]
        public void TestInsertSetColumn_DataTable()
        {
            var dt = db.Insert<Person>().SetColumnExpr(i => i.Names, new List<string> { str, str2 }).SetColumnExpr(i => i.Ext, new { Name = str }.ToDictionary()).ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<Person>().Max(i => i.Id);
            var p = db.Select<Person>().Where(i => i.Id == id).FirstOrDefault();
            p.Names[0].ShouldBe(str);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str);
        }
        #endregion

        #region update
        [Test]
        public void TestUpdateEntity()
        {
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Names[0] = str2;
            p.Ext["Name"] = str2;
            var update = db.Update<Person>().SetEntity(p);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 %\\"_"',cast('["52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2","52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"]' as json)),
                    ext2 = '{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}'
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Names[0].ShouldBe(str2);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str2);
        }
        [Test]
        public void TestUpdateSetColumn()
        {
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Names, i => i.Names.SetFluent(0, str2))
                .SetColumnExpr(i => i.Ext, i => i.Ext.SetFluent("Name", str2))
                .Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 %\\"_"',json_set(ifnull(json_value(ext,'$."123 %\\"_"'),'[]'),'$[0]','52\'\" k%_p\\r\\n t\r\n2_2')),
                    ext2 = json_set(ifnull(json_value(ext2,'$'),'{}'),'$."Name"','52\'\" k%_p\\r\\n t\r\n2_2')
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Names[0].ShouldBe(str2);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str2);
        }
        [Test]
        public void TestUpdateSetDto()
        {
            var update = db.Update<Person>().SetDto(new
            {
                Names = new List<string> { str2, str2 },
                Ext = new { Name = str2 }.ToDictionary(),
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 %\\"_"',cast('["52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2","52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"]' as json)),
                    ext2 = '{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}'
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Names[0].ShouldBe(str2);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str2);
        }
        [Test]
        public void TestUpdateSetExpr()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = i.Ext.SetFluent("Name", str2),
                Names = i.Names.SetFluent(i => true, i => str2),
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 %\\"_"',(select json_arrayagg(case when 1 then '52\'\" k%_p\\r\\n t\r\n2_2' else t.`i` end) from json_table(json_value(ext,'$."123 %\\"_"'),'$[*]' columns(`i` json path '$')) t)),
                    ext2 = json_set(ifnull(json_value(ext2,'$'),'{}'),'$."Name"','52\'\" k%_p\\r\\n t\r\n2_2')
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Names[0].ShouldBe(str2);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str2);
        }
        [Test]
        public void TestUpdateIncr()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = i.Ext.SetFluent("Name", i => i + "_2"),
                Names = i.Names.SetFluent(0, i => i + "_2")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 %\\"_"',json_set(ifnull(json_value(ext,'$."123 %\\"_"'),'[]'),'$[0]',concat_ws('',json_value(ifnull(json_value(ext,'$."123 %\\"_"'),'[]'),'$[0]' returning char),'_2'))),
                    ext2 = json_set(ifnull(json_value(ext2,'$'),'{}'),'$."Name"',concat_ws('',json_value(ifnull(json_value(ext2,'$'),'{}'),'$."Name"' returning char),'_2'))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Names[0].ShouldBe(str2);
            p.Names[1].ShouldBe(str2);
            p.Ext["Name"].ShouldBe(str2);
        }
        #endregion

        #region select
        [Test]
        public void TestSelect1()
        {
            var select = db.Select<Person>().Where(i => i.Ext["Name"].Contains("%"));
            var sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,json_value(t.ext,'$."123 %\\"_"') `Names`,json_value(t.ext2,'$') `Ext`
                from test t
                where (json_value(json_value(t.ext2,'$'),'$."Name"' returning char)) like '%\%%';
                """);
            var p = select.FirstOrDefault();
            p.Ext["Name"].ShouldBe(str);
            p.Names[0].ShouldBe(str);
            p.Names[1].ShouldBe(str2);

            //这个查不到数据 检查sql就行
            select = db.Select<Person>().Where(i => i.Ext["Name"].Contains("\"") && i.Ext["Name"].Contains("'\r\n"));
            sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,json_value(t.ext,'$."123 %\\"_"') `Names`,json_value(t.ext2,'$') `Ext`
                from test t
                where ((json_value(json_value(t.ext2,'$'),'$."Name"' returning char)) like '%"%') and ((json_value(json_value(t.ext2,'$'),'$."Name"' returning char)) like '%\'\r\n%');
                """);
        }
        [Test]
        public void TestWhereSeg()
        {
            var sql =
                $"""
                select {db.ColumnSeg<Person>(t => new { t.Id, t.Names, t.Ext })}
                from {db.TableSeg<Person>("t")}
                where {db.WhereSeg<Person>(t => t.Names.Any(i => i.Contains("%")))}
                """;
            sql.ShouldBe(
                """
                select t.id `Id`,json_value(t.ext,'$."123 %\\"_"') `Names`,json_value(t.ext2,'$') `Ext`
                from test t
                where 0<(select 1 from json_table(json_value(t.ext,'$."123 %\\"_"'),'$[*]' columns(`i` text path '$')) t where t.`i` like '%\%%' limit 1)
                """);
            var p = db.SelectModel<Person>(sql);
            p.Ext["Name"].ShouldBe(str);
            p.Names[0].ShouldBe(str);
            p.Names[1].ShouldBe(str2);
        }
        #endregion
    }
}
