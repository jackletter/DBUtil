﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.EscapeAndSqlInjectTests
{
    [TestFixture]
    internal class EscapeNestedTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person1
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "ext", Key = "123 addr")]
            public Person2 Person2 { get; set; }
        }
        public class Person2 { public Person3 Person3 { get; set; } }
        public class Person3 { public string Name { get; set; } }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int auto_increment primary key,ext json)""");
            db.ExecuteSql("""
                insert into test(ext) values(json_object('123 addr',cast('{"Person3":{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}}' as json)));
                """);
        }

        private string str = """
                52'" k%_p\r\n t
                2
                """;

        #region insert
        [Test]
        public void TestInsertEntity()
        {
            var insert = db.Insert<Person1>().SetEntity(new Person1
            {
                Person2 = new Person2
                {
                    Person3 = new Person3
                    {
                        Name = str
                    }
                }
            });
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(ext) values(json_object('123 addr',cast('{"Person3":{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}}' as json)));
                select id `Id`,json_value(ext,'$."123 addr"') `Person2` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Person2.Person3.Name.ShouldBe(str);
        }

        [Test]
        public void TestInsertEntity_DataTable()
        {
            var insert = db.Insert<Person1>().SetEntity(new Person1
            {
                Person2 = new Person2
                {
                    Person3 = new Person3
                    {
                        Name = str
                    }
                }
            });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<Person1>().Max(x => x.Id);
            var p = db.Select<Person1>().Where(x => x.Id == id).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str);
        }

        [Test]
        public void TestInsertSetColumn()
        {
            var insert = db.Insert<Person1>().SetColumnExpr(i => i.Person2, () => new Person2
            {
                Person3 = new Person3
                {
                    Name = str
                }
            });
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(ext) values(json_object('123 addr',cast('{"Person3":{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}}' as json)));
                select id `Id`,json_value(ext,'$."123 addr"') `Person2` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Person2.Person3.Name.ShouldBe(str);
        }
        [Test]
        public void TestInsertSetColumn_DataTable()
        {
            var insert = db.Insert<Person1>().SetColumnExpr(i => i.Person2, () => new Person2
            {
                Person3 = new Person3
                {
                    Name = str
                }
            });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<Person1>().Max(x => x.Id);
            var p = db.Select<Person1>().Where(x => x.Id == id).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str);
        }

        [Test]
        public void TestInsertDic()
        {
            var dic = new Dictionary<string, object>();
            dic.Add("ext", new Dictionary<string, object> { { "123 addr", new Dictionary<string, object> { { "Person3", new Dictionary<string, object> { { "Name", str } } } } } }.ToJson());
            var insert = db.Insert("test", dic);
            var sql = insert.ToSql();
            sql.ShouldBe("""
                insert into test(ext) values('{"123 addr":{"Person3":{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}}}');
                """);
            var id = insert.ExecuteIdentity();
            var p = db.Select<Person1>().Where(i => i.Id == id).FirstOrDefault();
            p.Id.ShouldBeGreaterThan(0);
            p.Person2.Person3.Name.ShouldBe(str);
        }

        [Test]
        public void TestInsertDic_DataTable()
        {
            var dic = new Dictionary<string, object>();
            dic.Add("ext", new Dictionary<string, object> { { "123 addr", new Dictionary<string, object> { { "Person3", new Dictionary<string, object> { { "Name", str } } } } } }.ToJson());
            var insert = db.Insert("test", dic);

            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<Person1>().Max(x => x.Id);
            var p = db.Select<Person1>().Where(x => x.Id == id).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str);
        }
        #endregion

        #region update
        [Test]
        public void TestUpdateEntity()
        {
            var str2 = str + "_2";
            var p = db.Select<Person1>().Where(i => i.Id == 1).FirstOrDefault();
            p.Person2.Person3.Name = str2;
            var update = db.Update<Person1>().SetEntity(p);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 addr"',cast('{"Person3":{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}}' as json))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            p = db.Select<Person1>().Where(i => i.Id == 1).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetColumn()
        {
            var str2 = str + "_2";
            var update = db.Update<Person1>().SetColumnExpr(i => i.Person2, new Person2
            {
                Person3 = new Person3
                {
                    Name = str2
                }
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 addr"',cast(ifnull(json_value(ext,'$."123 addr"'),'{}') as json),
                        '$."123 addr"',cast('{"Person3":{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}}' as json))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person1>().Where(i => i.Id == 1).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetColumnExpr()
        {
            var str2 = str + "_2";
            var update = db.Update<Person1>().SetColumnExpr(i => i.Person2, i => i.Person2.ModifyByDto(new
            {
                Person3 = new
                {
                    Name = str2
                }
            })).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 addr"',cast(ifnull(json_value(ext,'$."123 addr"'),'{}') as json),
                        '$."123 addr"',json_merge_patch(json_value(ext,'$."123 addr"'),json_object('Person3',json_object('Name','52\'\" k%_p\\r\\n t\r\n2_2'))))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person1>().Where(i => i.Id == 1).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetDto()
        {
            var str2 = str + "_2";
            var update = db.Update<Person1>().SetDto(new
            {
                Person2 = new
                {
                    Person3 = new
                    {
                        Name = str2
                    }
                }
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 addr"',cast(ifnull(json_value(ext,'$."123 addr"'),'{}') as json),
                        '$."123 addr"',cast('{"Person3":{"Name":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"}}' as json))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person1>().Where(i => i.Id == 1).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetExpr()
        {
            var str2 = str + "_2";
            var update = db.Update<Person1>().SetExpr(i => new Person1
            {
                Person2 = new Person2
                {
                    Person3 = new Person3
                    {
                        Name = str2
                    }
                }
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 addr"',cast(ifnull(json_value(ext,'$."123 addr"'),'{}') as json),
                        '$."123 addr"."Person3"',cast(ifnull(json_value(ext,'$."123 addr"."Person3"'),'{}') as json),
                        '$."123 addr"."Person3"."Name"','52\'\" k%_p\\r\\n t\r\n2_2')
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person1>().Where(i => i.Id == 1).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateIncr()
        {
            var str2 = str + "_2";
            var update = db.Update<Person1>().SetExpr(i => new Person1
            {
                Person2 = new Person2
                {
                    Person3 = new Person3
                    {
                        Name = i.Person2.Person3.Name + "_2"
                    }
                }
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123 addr"',cast(ifnull(json_value(ext,'$."123 addr"'),'{}') as json),
                        '$."123 addr"."Person3"',cast(ifnull(json_value(ext,'$."123 addr"."Person3"'),'{}') as json),
                        '$."123 addr"."Person3"."Name"',concat_ws('',json_value(json_value(json_value(ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char),'_2'))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person1>().Where(i => i.Id == 1).FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str2);
        }
        #endregion

        #region select
        [Test]
        public void TestSelect1()
        {
            var str2 = str + "_2";
            var select = db.Select<Person1>().Where(i => i.Person2.Person3.Name.Contains("%"));
            var sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,json_value(t.ext,'$."123 addr"') `Person2`
                from test t
                where (json_value(json_value(json_value(t.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char)) like '%\%%';
                """);
            var p = select.FirstOrDefault();
            p.Person2.Person3.Name.ShouldBe(str);

            //这个查不到数据 检查sql就行
            select = db.Select<Person1>().Where(i => i.Person2.Person3.Name.Contains("\"") && i.Person2.Person3.Name.Contains("'\r\n"));
            sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,json_value(t.ext,'$."123 addr"') `Person2`
                from test t
                where ((json_value(json_value(json_value(t.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char)) like '%"%') and ((json_value(json_value(json_value(t.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char)) like '%\'\r\n%');
                """);
        }

        class PersonTmp
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Person2 Person2 { get; set; }
            public Person3 Person3 { get; set; }
        }
        [Test]
        public void TestWhereSeg()
        {
            var sql =
                $"""
                select {db.ColumnSeg<Person1>(t => new { t.Id, t.Person2, t.Person2.Person3, t.Person2.Person3.Name })}
                from {db.TableSeg<Person1>("t")}
                where {db.WhereSeg<Person1>(t => t.Person2.Person3.Name.Contains("%"))}
                """;
            sql.ShouldBe(
                """
                select t.id `Id`,json_value(t.ext,'$."123 addr"') `Person2`,json_value(json_value(t.ext,'$."123 addr"'),'$."Person3"') `Person3`,json_value(json_value(json_value(t.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char) `Name`
                from test t
                where (json_value(json_value(json_value(t.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char)) like '%\%%'
                """);
            var p = db.SelectModel<PersonTmp>(sql);
            p.Name.ShouldBe(str);
            p.Person2.Person3.Name.ShouldBe(str);
            p.Person3.Name.ShouldBe(str);
        }

        [Test]
        public void TestCaseSeg()
        {
            var sql =
                $"""
                select 
                    {db.CaseSeg().WhenSeg<Person1>(i => i.Person2.Person3.Name.Contains("%")).Then("danger").WhenSeg<Person1>(i => i.Person2.Person3.Name.Contains("\"")).Then("da").WhenSeg<Person1>(i => i.Person2.Person3.Name.EndsWith("'\r\n")).Then("d").EndAs("caseinfo")},
                    {db.ColumnSeg<Person1>(i => new { i.Person2.Person3.Name })}
                from {db.TableSeg<Person1>("i")}
                """;
            sql.ShouldBe("""
                select 
                    case 
                  when (json_value(json_value(json_value(i.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char)) like '%\%%' then 'danger'
                  when (json_value(json_value(json_value(i.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char)) like '%"%' then 'da'
                  when (json_value(json_value(json_value(i.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char)) like '%\'\r\n' then 'd' end caseinfo,
                    json_value(json_value(json_value(i.ext,'$."123 addr"'),'$."Person3"'),'$."Name"' returning char) `Name`
                from test i
                """);
            var p = db.SelectDictionary(sql);
            p["Name"].ShouldBe(str);
            p["caseinfo"].ShouldBe("danger");
        }
        #endregion
    }
}
