﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.EscapeAndSqlInjectTests
{
    [TestFixture]
    internal class EscapeStringTests : TestBase
    {
        #region model
        [Table("test")]
        public class PersonString
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "ext2")]
            public string Info { get; set; }

            [JsonStore(Bucket = "ext", Key = "123\r\n %addr")]
            public string Addr { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int auto_increment primary key,name varchar(50),ext json,ext2 json)""");
            db.ExecuteSql("""
                insert into test(name,ext2,ext) values('52\'" k%_p\\r\\n t\r\n2','"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"',json_object('123\r\n %addr','52\'" k%_p\\r\\n t\r\n2'));
                """);
        }

        private string str = """
                52'" k%_p\r\n t
                2
                """;

        [Test]
        public void TestBase()
        {
            var str2 = db.GetJsonPathWrapByPropertyName(str);
            str2.ShouldBe(
                """
                '$."52\'\\" k%_p\\\\r\\\\n t\\r\\n2"'
                """);

            var str3 = db.GetJsonPathWrapByPropertyName("""
                a
                @pp"'w
                """);
            str3.ShouldBe("""
                '$."a\\r\\n@pp\\"\'w"'
                """);
        }
        #region insert
        [Test]
        public void TestInsertEntity()
        {
            var insert = db.Insert<PersonString>(new PersonString
            {
                Name = str,
                Addr = str,
                Info = str,
            });
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(name,ext2,ext) values('52\'" k%_p\\r\\n t\r\n2','"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"',json_object('123\r\n %addr','52\'\" k%_p\\r\\n t\r\n2'));
                select id `Id`,name `Name`,json_value(ext2,'$' returning char) `Info`,json_value(ext,'$."123\\r\\n %addr"' returning char) `Addr` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Id.ShouldBeGreaterThan(0);
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);
        }
        [Test]
        public void TestInsertEntity_DataTable()
        {
            var dt = db.Insert<PersonString>(new PersonString
            {
                Name = str,
                Info = str,
                Addr = str,
            }).ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<PersonString>().Max(i => i.Id);
            var p = db.Select<PersonString>().Where(i => i.Id == id).FirstOrDefault();
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);
        }

        [Test]
        public void TestInsertSetColumn()
        {
            var insert = db.Insert<PersonString>().SetColumnExpr(i => i.Name, str).SetColumnExpr(i => i.Addr, str).SetColumnExpr(i => i.Info, str);
            var sql = insert.ToSql(DBUtil.EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(name,ext,ext2) values('52\'" k%_p\\r\\n t\r\n2',json_object('123\r\n %addr','52\'\" k%_p\\r\\n t\r\n2'),'"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"');
                select id `Id`,name `Name`,json_value(ext2,'$' returning char) `Info`,json_value(ext,'$."123\\r\\n %addr"' returning char) `Addr` from test where id=last_insert_id();
                """);
            var p = insert.ExecuteInserted();
            p.Id.ShouldBeGreaterThan(0);
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);
        }
        [Test]
        public void TestInsertSetColumn_DataTable()
        {
            var dt = db.Insert<PersonString>().SetColumnExpr(i => i.Name, str).SetColumnExpr(i => i.Addr, str).SetColumnExpr(i => i.Info, str).ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<PersonString>().Max(i => i.Id);
            var p = db.Select<PersonString>().Where(i => i.Id == id).FirstOrDefault();
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);
        }

        [Test]
        public void TestInsertDic()
        {
            var dic = new Dictionary<string, object>();
            dic.Add("name", str);
            dic.Add("ext", new Dictionary<string, object> { { "123\r\n %addr", str } }.ToJson());
            dic.Add("ext2", str.ToJson());
            var insert = db.Insert("test", dic);
            var sql = insert.ToSql();
            sql.ShouldBe("""
                insert into test(name,ext,ext2) values('52\'" k%_p\\r\\n t\r\n2','{"123\\r\\n %addr":"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"}','"52\'\\" k%_p\\\\r\\\\n t\\r\\n2"');
                """);
            var id = insert.ExecuteIdentity();
            var p = db.Select<PersonString>().Where(i => i.Id == id).FirstOrDefault();
            p.Id.ShouldBeGreaterThan(0);
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);
        }

        [Test]
        public void TestInsertDic_DataTable()
        {
            var dic = new Dictionary<string, object>();
            dic.Add("name", str);
            dic.Add("ext", new Dictionary<string, object> { { "123\r\n %addr", str } }.ToJson());
            dic.Add("ext2", str.ToJson());
            var insert = db.Insert("test", dic);

            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            var id = db.Select<PersonString>().Max(i => i.Id);
            var p = db.Select<PersonString>().Where(i => i.Id == id).FirstOrDefault();
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);
        }
        #endregion

        #region update
        [Test]
        public void TestUpdateEntity()
        {
            var str2 = str + "_2";
            var p = db.Select<PersonString>().Where(i => i.Id == 1).FirstOrDefault();
            p.Name = str2;
            p.Info = str2;
            p.Addr = str2;
            var update = db.Update<PersonString>().SetEntity(p);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    name = '52\'" k%_p\\r\\n t\r\n2_2',
                    ext2 = '"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"',
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123\\r\\n %addr"','52\'\" k%_p\\r\\n t\r\n2_2')
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            p = db.Select<PersonString>().Where(i => i.Id == 1).FirstOrDefault();
            p.Name.ShouldBe(str2);
            p.Addr.ShouldBe(str2);
            p.Info.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetColumn()
        {
            var str2 = str + "_2";
            var update = db.Update<PersonString>().SetColumnExpr(i => i.Name, str2).SetColumnExpr(i => i.Addr, str2).SetColumnExpr(i => i.Info, str2).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    name = '52\'" k%_p\\r\\n t\r\n2_2',
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123\\r\\n %addr"','52\'\" k%_p\\r\\n t\r\n2_2'),
                    ext2 = '"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"'
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<PersonString>().Where(i => i.Id == 1).FirstOrDefault();
            p.Name.ShouldBe(str2);
            p.Addr.ShouldBe(str2);
            p.Info.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetDto()
        {
            var str2 = str + "_2";
            var update = db.Update<PersonString>().SetDto(new
            {
                Name = str2,
                Addr = str2,
                Info = str2,
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    name = '52\'" k%_p\\r\\n t\r\n2_2',
                    ext2 = '"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"',
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123\\r\\n %addr"','52\'\" k%_p\\r\\n t\r\n2_2')
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<PersonString>().Where(i => i.Id == 1).FirstOrDefault();
            p.Name.ShouldBe(str2);
            p.Addr.ShouldBe(str2);
            p.Info.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateSetExpr()
        {
            var str2 = str + "_2";
            var update = db.Update<PersonString>().SetExpr(i => new PersonString
            {
                Name = str2,
                Addr = str2,
                Info = str2,
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    name = '52\'" k%_p\\r\\n t\r\n2_2',
                    ext2 = '"52\'\\" k%_p\\\\r\\\\n t\\r\\n2_2"',
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123\\r\\n %addr"','52\'\" k%_p\\r\\n t\r\n2_2')
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<PersonString>().Where(i => i.Id == 1).FirstOrDefault();
            p.Name.ShouldBe(str2);
            p.Addr.ShouldBe(str2);
            p.Info.ShouldBe(str2);
        }

        [Test]
        public void TestUpdateIncr()
        {
            var str2 = str + "_2";
            var update = db.Update<PersonString>().SetExpr(i => new PersonString
            {
                Name = i.Name + "_2",
                Addr = i.Addr + "_2",
                Info = i.Info + "_2",
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    name = concat_ws('',name,'_2'),
                    ext2 = json_quote(concat_ws('',json_value(ext2,'$' returning char),'_2')),
                    ext = json_set(ifnull(ext,json_object()),
                        '$."123\\r\\n %addr"',concat_ws('',json_value(ext,'$."123\\r\\n %addr"' returning char),'_2'))
                where id = 1;
                """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<PersonString>().Where(i => i.Id == 1).FirstOrDefault();
            p.Name.ShouldBe(str2);
            p.Addr.ShouldBe(str2);
            p.Info.ShouldBe(str2);
        }
        #endregion

        #region select
        [Test]
        public void TestSelect1()
        {
            var select = db.Select<PersonString>().Where(i => i.Name.Contains("%"));
            var sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,json_value(t.ext2,'$' returning char) `Info`,json_value(t.ext,'$."123\\r\\n %addr"' returning char) `Addr`
                from test t
                where t.name like '%\%%';
                """);
            var p = select.FirstOrDefault();
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);

            //这个查不到数据 检查sql就行
            select = db.Select<PersonString>().Where(i => i.Name.Contains("\"") && i.Name.Contains("'\r\n"));
            sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,json_value(t.ext2,'$' returning char) `Info`,json_value(t.ext,'$."123\\r\\n %addr"' returning char) `Addr`
                from test t
                where (t.name like '%"%') and (t.name like '%\'\r\n%');
                """);

            select = db.Select<PersonString>().Where(i => i.Addr.StartsWith("\'%_\"") && i.Info.Contains("'\r\n"));
            sql = select.ToSql();
            sql.ShouldBe("""
                select t.id `Id`,t.name `Name`,json_value(t.ext2,'$' returning char) `Info`,json_value(t.ext,'$."123\\r\\n %addr"' returning char) `Addr`
                from test t
                where ((json_value(t.ext,'$."123\\r\\n %addr"' returning char)) like '\'\%\_"%') and ((json_value(t.ext2,'$' returning char)) like '%\'\r\n%');
                """);
        }

        [Test]
        public void TestWhereSeg()
        {
            var sql =
                $"""
                select {db.ColumnSeg<PersonString>(t => new { t.Id, t.Name, t.Addr, t.Info })}
                from {db.TableSeg<PersonString>("t")}
                where {db.WhereSeg<PersonString>(t => t.Name.Contains("%"))}
                """;
            sql.ShouldBe(
                """
                select t.id `Id`,t.name `Name`,json_value(t.ext,'$."123\\r\\n %addr"' returning char) `Addr`,json_value(t.ext2,'$' returning char) `Info`
                from test t
                where t.name like '%\%%'
                """);
            var p = db.SelectModel<PersonString>(sql);
            p.Name.ShouldBe(str);
            p.Addr.ShouldBe(str);
            p.Info.ShouldBe(str);
        }

        [Test]
        public void TestCaseSeg()
        {
            var sql =
                $"""
                select 
                    {db.CaseSeg().WhenSeg<PersonString>(i => i.Addr.Contains("%")).Then("danger").WhenSeg<PersonString>(i => i.Name.Contains("\"")).Then("da").WhenSeg<PersonString>(i => i.Info.EndsWith("'\r\n")).Then("d").EndAs("caseinfo")},
                    {db.ColumnSeg<PersonString>(i => new { i.Addr, i.Name, i.Info })}
                from {db.TableSeg<PersonString>("i")}
                """;
            var p = db.SelectDictionary(sql);
            p["Name"].ShouldBe(str);
            p["Addr"].ShouldBe(str);
            p["Info"].ShouldBe(str);
            p["caseinfo"].ShouldBe("danger");
        }
        #endregion
    }
}
