﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class WithIfNullUseTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "ext")]
            public JsonObject Ext { get; set; }
            [JsonStore(Bucket = "arr")]
            public JsonArray Arr { get; set; }

            [JsonStore(Bucket = "ext2")]
            public Detail Ext2 { get; set; }

            [JsonStore(Bucket = "ext3")]
            public List<string> Ext3 { get; set; }

            [JsonStore(Bucket = "ext4")]
            public Dictionary<string, List<string>> Ext4 { get; set; }

        }
        public class Detail
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int auto_increment primary key,ext json,ext2 json,ext3 json,ext4 json,arr json)""");
            db.Insert<Person>(new[]{new Person
            {
                Ext = new { Name = "jim", Age = 18 }.ToJsonObject(),
                Arr = new[] { new { Name = "jim", Age = 18 }, new { Name = "lisa", Age = 20 } }.ToJsonArray(),
                Ext2 = new Detail { Id = 1, Name = "jim" },
                Ext3 = new List<string> { "tom", "jack" },
                Ext4 = new Dictionary<string, List<string>> { { "a", new List<string> { "b", "c" } } }
            },new Person() }).ExecuteAffrows();
        }

        [Test]
        public void Test()
        {
            var old = db.Select<Person>().ToList();
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Ext, i => i.Ext.IfNullUseNew().ModifyByDto(new
                {
                    Name = "jack"
                }))
                .Where(i => i.Id == 2);
            var sql = update.ToSql();
            Console.WriteLine(sql);
        }

        [Table("t_normal")]
        public class PersonNormal
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? Age { get; set; }
        }
        [Test]
        public void Test2()
        {
            DropTableIfExist("t_normal");
            db.ExecuteSql("create table t_normal(id int primary key,name varchar(50),age int)");
            db.ExecuteSql("""
                insert into t_normal(id,name,age) values(1,null,null);
                """);

            var update = db.Update<PersonNormal>()
                .SetExpr(i => new PersonNormal
                {
                    Name = i.Name.IfNullUse("tom") + "2",
                    Age = i.Age.IfNullUse(20) + 1
                })
                .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql
            //"""
            //update t_normal set
            //    `Name` = concat_ws('',ifnull(`Name`,'tom'),'2'),
            //    `Age` = ifnull(`Age`,20) + 1
            //where `Id` = 1;
            //"""
        }
    }
}
