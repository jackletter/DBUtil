﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests.AddIfNotExistRange
{
    [TestFixture]
    internal class AddIfNotExistTests : TestBase
    {
        #region model
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "addrs")]
            public List<string> Addrs { get; set; }
            [JsonStore(Bucket = "ext", Key = "Scores")]
            public List<double> Scores { get; set; }
            [JsonStore(Bucket = "ext", Key = "IListScores")]
            public IList<double> IListScores { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,addrs json,ext json)");
            db.ExecuteSql("""
                insert into test(addrs,ext) values
                ('["a-road","b-road","c-road"]','{"Scores":[80,60,92],"IListScores":[80,60,92]}'),
                ('["t-road","i-road","k-road"]','{}'),
                ('["t-road","i-road","k-road"]',null),
                ('["a-road","b-road","c-road"]','{"Scores":[80,80,60,92],"IListScores":[80,80,60,92]}'),
                ('["a-road","b-road","c-road"]','{"Scores":[80.0,92]}'),
                (null,'{"Scores":[80.0,92]}');
                """);
        }

        [Test]
        public void TestNoKey()
        {
            //因为已经存在 b-road 所以实际执行不会有变化
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Addrs, i => i.Addrs.AddIfNotExistFluent("b-road"))
                .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        addrs = case when json_contains(json_value(addrs,'$'),'"b-road"') then json_value(addrs,'$') else json_merge_preserve(ifnull(json_value(addrs,'$'),'[]'),json_array('b-road')) end
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).ToList();
            var json = p.ToJson();
            json.ShouldBe("""[{"Id":1,"Addrs":["a-road","b-road","c-road"],"Scores":[80,60,92],"IListScores":[80,60,92]}]""");

            //原来没有 d-road 实际执行将 d-road 加了进去
            var update2 = db.Update<Person>()
                .SetColumnExpr(i => i.Addrs, i => i.Addrs.AddIfNotExistFluent("d-road"))
                .Where(i => i.Id == 1);
            var sql2 = update2.ToSql();
            //sql2.ShouldBe("""
            //    update test set
            //        addrs = case when json_contains(json_value(addrs,'$'),'"d-road"') then json_value(addrs,'$') else json_merge_preserve(ifnull(json_value(addrs,'$'),'[]'),json_array('d-road')) end
            //    where id = 1;
            //    """);
            var r2 = update2.ExecuteAffrows();
            r2.ShouldBe(1);
            var p2 = db.Select<Person>().Where(i => i.Id == 1).ToList();
            var json2 = p2.ToJson();
            json2.ShouldBe("""[{"Id":1,"Addrs":["a-road","b-road","c-road","d-road"],"Scores":[80,60,92],"IListScores":[80,60,92]}]""");

            //原来没有是 null, 执行后应该是 ["d-road"]
            var update3 = db.Update<Person>()
                .SetColumnExpr(i => i.Addrs, i => i.Addrs.AddIfNotExistFluent("d-road"))
                .Where(i => i.Id == 6);
            var sql3 = update3.ToSql();
            //sql3.ShouldBe("""
            //    update test set
            //        addrs = case when json_contains(json_value(addrs,'$'),'"d-road"') then json_value(addrs,'$') else json_merge_preserve(ifnull(json_value(addrs,'$'),'[]'),json_array('d-road')) end
            //    where id = 6;
            //    """);
            var r3 = update3.ExecuteAffrows();
            r3.ShouldBe(1);
            var p3 = db.Select<Person>().Where(i => i.Id == 6).ToList();
            var json3 = p3.ToJson();
            json3.ShouldBe("""[{"Id":6,"Addrs":["d-road"],"Scores":[80,92],"IListScores":null}]""");
        }

        [Test]
        public void TestWithKey()
        {
            //Scores原来就有 80 所以实际执行不会添加进去
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Scores, i => i.Scores.AddIfNotExistFluent(80))
                .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',case when json_contains(json_value(ext,'$."Scores"'),cast(80 as json)) then json_value(ext,'$."Scores"') else json_merge_preserve(ifnull(json_value(ext,'$."Scores"'),'[]'),json_array(80)) end)
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Addrs":["a-road","b-road","c-road"],"Scores":[80,60,92],"IListScores":[80,60,92]}""");

            //Scores原来没有 35 实际执行会添加进去
            var update4 = db.Update<Person>()
                .SetExpr(i => new Person
                {
                    Scores = i.Scores.AddIfNotExistFluent(35)
                })
                .Where(i => i.Id == 4);
            var sql4 = update4.ToSql();
            //sql4.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',case when json_contains(json_value(ext,'$."Scores"'),cast(35 as json)) then json_value(ext,'$."Scores"') else json_merge_preserve(ifnull(json_value(ext,'$."Scores"'),'[]'),json_array(35)) end)
            //    where id = 4;
            //    """);
            var r4 = update4.ExecuteAffrows();
            r4.ShouldBe(1);
            var p4 = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            var json4 = p4.ToJson();
            json4.ShouldBe("""{"Id":4,"Addrs":["a-road","b-road","c-road"],"Scores":[80,80,60,92,35],"IListScores":[80,80,60,92]}""");


            //当原json中没有 $.Scores 属性的时候
            //原来是: {}, 执行后应该是: {"Scores":[80]}
            var update2 = db.Update<Person>()
                .SetColumnExpr(i => i.Scores, i => i.Scores.AddIfNotExistFluent(80))
                .Where(i => i.Id == 2);
            var sql2 = update2.ToSql();
            //sql2.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',case when json_contains(json_value(ext,'$."Scores"'),cast(80 as json)) then json_value(ext,'$."Scores"') else json_merge_preserve(ifnull(json_value(ext,'$."Scores"'),'[]'),json_array(80)) end)
            //    where id = 2;
            //    """);
            var r2 = update2.ExecuteAffrows();
            r2.ShouldBe(1);
            var p2 = db.Select<Person>().Where(i => i.Id == 2).FirstOrDefault();
            var json2 = p2.ToJson();
            json2.ShouldBe("""{"Id":2,"Addrs":["t-road","i-road","k-road"],"Scores":[80],"IListScores":null}""");

            //当原json中没有 $.Scores 属性的时候
            //原来是: null, 执行后应该是: {"Scores":[80]}
            var update3 = db.Update<Person>()
                .SetColumnExpr(i => i.Scores, i => i.Scores.AddIfNotExistFluent(80))
                .Where(i => i.Id == 3);
            var sql3 = update3.ToSql();
            //sql3.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',case when json_contains(json_value(ext,'$."Scores"'),cast(80 as json)) then json_value(ext,'$."Scores"') else json_merge_preserve(ifnull(json_value(ext,'$."Scores"'),'[]'),json_array(80)) end)
            //    where id = 3;
            //    """);
            var r3 = update3.ExecuteAffrows();
            r3.ShouldBe(1);
            var p3 = db.Select<Person>().Where(i => i.Id == 3).FirstOrDefault();
            var json3 = p3.ToJson();
            json3.ShouldBe("""{"Id":3,"Addrs":["t-road","i-road","k-road"],"Scores":[80],"IListScores":null}""");

            //有小数点也不影响
            var update5 = db.Update<Person>()
                .SetColumnExpr(i => i.Scores, i => i.Scores.AddIfNotExistFluent(80.000))
                .Where(i => i.Id == 5);
            var sql5 = update5.ToSql();
            //sql5.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',case when json_contains(json_value(ext,'$."Scores"'),cast(80 as json)) then json_value(ext,'$."Scores"') else json_merge_preserve(ifnull(json_value(ext,'$."Scores"'),'[]'),json_array(80)) end)
            //    where id = 5;
            //    """);
            var r5 = update5.ExecuteAffrows();
            r5.ShouldBe(1);
            var p5 = db.Select<Person>().Where(i => i.Id == 5).FirstOrDefault();
            var json5 = p5.ToJson();
            json5.ShouldBe("""{"Id":5,"Addrs":["a-road","b-road","c-road"],"Scores":[80,92],"IListScores":null}""");

            //顺带测下 IList
            var update6 = db.Update<Person>()
                .SetExpr(i => new Person
                {
                    IListScores = i.IListScores.AddIfNotExistFluent(35)
                })
                .Where(i => i.Id == 1);
            var sql6 = update6.ToSql();
            //sql6.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."IListScores"',case when json_contains(json_value(ext,'$."IListScores"'),cast(35 as json)) then json_value(ext,'$."IListScores"') else json_merge_preserve(ifnull(json_value(ext,'$."IListScores"'),'[]'),json_array(35)) end)
            //    where id = 1;
            //    """);
            var r6 = update6.ExecuteAffrows();
            r6.ShouldBe(1);
            var p6 = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json6 = p6.ToJson();
            json6.ShouldBe("""{"Id":1,"Addrs":["a-road","b-road","c-road"],"Scores":[80,60,92],"IListScores":[80,60,92,35]}""");
        }

        [Test]
        public void TestAddRangeTest()
        {
            //有 distinct 参数且为false
            // 原来是 [80,80,60,92], 执行后应该是 [80,80,60,92,2,3]
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Scores, i => i.Scores.AddRangeIfNotExistFluent(new double[] { 2, 3.0, 3 }, false))
                .Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',(select json_arrayagg(i) from(  
            //        select i from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(i json path '$')) t
            //        union all
            //        select distinct i from json_table('[2,3,3]','$[*]' columns(i json path '$')) t where t.i not in (select i from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(i json path '$')) t)
            //    ) t))
            //    where id = 4;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":4,"Addrs":["a-road","b-road","c-road"],"Scores":[80,80,60,92,2,3],"IListScores":[80,80,60,92]}""");

            //有 distinct 参数且为true
            //原来是 [80,80,60,92], 执行后应该是 [80,60,92,2,3]
            var update2 = db.Update<Person>()
                .SetColumnExpr(i => i.IListScores, i => i.IListScores.AddRangeIfNotExistFluent(new double[] { 2, 3.0, 3 }, true))
                .Where(i => i.Id == 4);
            var sql2 = update2.ToSql();
            //sql2.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."IListScores"',(select json_arrayagg(i) from(  
            //        select i from json_table(json_value(ext,'$."IListScores"'),'$[*]' columns(i json path '$')) t
            //        union
            //        select i from json_table('[2,3,3]','$[*]' columns(i json path '$')) t
            //    ) t))
            //    where id = 4;
            //    """);
            var r2 = update2.ExecuteAffrows();
            r2.ShouldBe(1);
            var p2 = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            var json2 = p2.ToJson();
            json2.ShouldBe("""{"Id":4,"Addrs":["a-road","b-road","c-road"],"Scores":[80,80,60,92,2,3],"IListScores":[80,60,92,2,3]}""");

            //无 distinct 参数
            var update3 = db.Update<Person>()
                .SetColumnExpr(i => i.Scores, i => i.Scores.AddRangeIfNotExistFluent(new double[] { 2, 3.0, 3 }))
                .Where(i => i.Id == 4);
            var sql3 = update3.ToSql();
            //sql3.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',(select json_arrayagg(i) from(  
            //        select i from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(i json path '$')) t
            //        union all
            //        select distinct i from json_table('[2,3,3]','$[*]' columns(i json path '$')) t where t.i not in (select i from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(i json path '$')) t)
            //    ) t))
            //    where id = 4;
            //    """);

            //无key
            var sql4 = db.Update<Person>()
                .SetExpr(i => new Person
                {
                    Addrs = i.Addrs.AddRangeIfNotExistFluent(new[] { "d-road", "a-road" })
                })
                .Where(i => i.Id == 1)
                .ToSql();
            //sql4.ShouldBe("""
            //    update test set
            //        addrs = (select json_arrayagg(i) from(  
            //        select i from json_table(json_value(addrs,'$'),'$[*]' columns(i json path '$')) t
            //        union all
            //        select distinct i from json_table('["d-road","a-road"]','$[*]' columns(i json path '$')) t where t.i not in (select i from json_table(json_value(addrs,'$'),'$[*]' columns(i json path '$')) t)
            //    ) t)
            //    where id = 1;
            //    """);
        }
    }
}
