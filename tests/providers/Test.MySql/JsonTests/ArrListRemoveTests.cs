﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class ArrListRemoveTests : TestBase
    {
        #region model
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "addrs")]
            public List<string> Addrs { get; set; }
            [JsonStore(Bucket = "ext", Key = "Scores")]
            public List<double> Scores { get; set; }
        }

        [Table("test2")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "Students")]
            public List<Student> Students { get; set; }
        }
        class Student
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public double Score { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,addrs json,ext json)");
            db.ExecuteSql("""
                insert into test(addrs,ext) values
                ('["a-road","b-road","c-road"]','{"Scores":[80,60,92]}'),
                ('["t-road","i-road","k-road"]','{}'),
                ('["t-road","i-road","k-road"]',null),
                ('["a-road","b-road","c-road"]','{"Scores":[80,80,60,92]}');
                """);

            DropTableIfExist("test2");
            db.ExecuteSql("create table test2(id int primary key auto_increment,Students json)");
            db.ExecuteSql("""
                insert into test2(Students) values
                ('[{"Id":1,"Name":"tom","Age":18},{"Id":2,"Name":"lisa","Age":20},{"Id":3,"Name":"小明","Age":23}]'),
                ('[{"Id":4,"Name":"tom2","Age":18},{"Id":5,"Name":"lisa2","Age":20},{"Id":6,"Name":"小明2","Age":23}]');
                """);
        }

        [Test]
        public void TestRemoveNumberFluent()
        {
            //移除数字
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Scores = i.Scores.RemoveFluent(80)
            }).Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',(select json_arrayagg(t.i) from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(i json path '$')) t where t.i<>80))
            //    where id = 4;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            //Addrs没有变
            p.Addrs.ShouldBe(["a-road", "b-road", "c-road"]);
            //已经没有80了
            p.Scores.ShouldBe([60, 92]);

            //再移除小数的 60.123
            //先补充插入60.123
            db.Update<Person>().SetExpr(i => new Person { Scores = i.Scores.AddFluent(60.123) }).Where(i => i.Id == 4).ExecuteAffrows();
            //验证一下 是否插入成功
            p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            p.Scores.ToJson().ShouldBe("[60,92,60.123]");
            var update2 = db.Update<Person>().SetExpr(i => new Person { Scores = i.Scores.RemoveFluent(60.1230) }).Where(i => i.Id == 4);
            var sql2 = update2.ToSql();
            //sql2.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',(select json_arrayagg(t.i) from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(i json path '$')) t where t.i<>60.123))
            //    where id = 4;
            //    """);
            r = update2.ExecuteAffrows();
            r.ShouldBe(1);
            p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            p.Scores.ShouldBe([60, 92]);
        }

        [Test]
        public void TestRemoveStringFluent()
        {
            //移除字符串
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Addrs = i.Addrs.RemoveFluent("b-road")
            }).Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        addrs = (select json_arrayagg(t.i) from json_table(json_value(addrs,'$'),'$[*]' columns(i json path '$')) t where t.i<>'b-road')
            //    where id = 4;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            //Addrs已经没有 'b-road' 了
            p.Addrs.ShouldBe(["a-road", "c-road"]);
            //Scores没有变
            p.Scores.ShouldBe([80, 80, 60, 92]);
        }

        [Test]
        public void TestRemoveAtFluent()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Scores = i.Scores.RemoveAtFluent(2)
            }).Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',json_remove(ifnull(json_value(ext,'$."Scores"'),'[]'),'$[2]'))
            //    where id = 4;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            //原来是 80 80 60 92, 移除第二个即:60
            p.Scores.ShouldBe([80, 80, 92]);
        }

        [Test]
        public void TestRemoveAtFluentIndexOf()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Addrs = i.Addrs.RemoveAtFluent(i.Addrs.IndexOf("b-road"))
            }).Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        addrs = json_remove(ifnull(json_value(addrs,'$'),'[]'),ifnull(concat('$[',(select t.idx-1 from(select row_number() over() as idx,t.i from json_table(json_value(addrs,'$'), '$[*]' columns (i json path '$')) t) t where t.i='b-road' limit 1),']'),'$.null'))
            //    where id = 4;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            p.Addrs.ShouldBe(["a-road", "c-road"]);
        }

        [Test]
        public void TestRemoveFluentCondi()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Scores = i.Scores.RemoveFluent(j => j > 60)
            }).Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',(select json_arrayagg(t.`aj`) from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(`aj` json path '$',`j` decimal(30,15) path '$'))t where not (t.`j` > 60)))
            //    where id = 4;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            p.Scores.ShouldBe([60]);
        }

        [Test]
        public void TestRemoveFluentCondi2()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Addrs = i.Addrs.RemoveFluent(i => i.StartsWith("a"))
            }).Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        addrs = (select json_arrayagg(t.`ai`) from json_table(json_value(addrs,'$'),'$[*]' columns(`ai` json path '$',`i` text path '$'))t where not (t.`i` like 'a%'))
            //    where id = 4;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            p.Addrs.ShouldBe(["b-road", "c-road"]);
        }

        [Test]
        public void TestRemoveFindIndex()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Scores = i.Scores.RemoveAtFluent(i.Scores.FindIndex(i => i > 60))
            }).Where(i => i.Id == 4);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Scores"',json_remove(ifnull(json_value(ext,'$."Scores"'),'[]'),ifnull(concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$."Scores"'),'$[*]' columns(`i` decimal(30,15) path '$')) t) t where t.`i` > 60 limit 1),']'),'$.null')))
            //    where id = 4;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 4).FirstOrDefault();
            p.Scores.ShouldBe([80, 60, 92]);
        }
        [Test]
        public void TestRemoveFindIndex2()
        {
            var update = db.Update<Person2>().SetExpr(i => new Person2
            {
                Students = i.Students.RemoveAtFluent(i.Students.FindIndex(i => i.Name == "小明"))
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test2 set
            //        Students = json_remove(ifnull(json_value(Students,'$'),'[]'),ifnull(concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(Students,'$'),'$[*]' columns(`i.Name` text path '$.Name')) t) t where t.`i.Name` = '小明' limit 1),']'),'$.null'))
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.Students.ToJson();
            json.ShouldBe("""[{"Id":1,"Name":"tom","Score":0},{"Id":2,"Name":"lisa","Score":0}]""");
        }

        [Test]
        public void RemovePerson()
        {
            var update = db.Update<Person2>().SetExpr(i => new Person2
            {
                Students = i.Students.RemoveFluent(i => i.Name != "小明")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test2 set
            //        Students = (select json_arrayagg(t.`ai`) from json_table(json_value(Students,'$'),'$[*]' columns(`ai` json path '$',`i.Name` text path '$.Name'))t where not (t.`i.Name` <> '小明'))
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.Students.ToJson();
            json.ShouldBe("""[{"Id":3,"Name":"小明","Score":0}]""");
        }
    }
}
