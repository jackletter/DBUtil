﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests.CustomClass
{
    [TestFixture]
    internal class JsonStoreTests : TestBase
    {
        #region model
        [Table("t_person")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties", Key = "name")]
            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "age")]
            [Column("age")]
            public int Age { get; set; }
        }

        [Table("t_person")]
        public class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties", Key = "addrs")]
            [Column("addrs")]
            public List<string> Addrs { get; set; }

            [JsonStore(Bucket = "properties", Key = "meta")]
            [Column("meta")]
            public MetaData Meta { get; set; }

            public class MetaData
            {
                public int Id { get; set; }
                public string Name { get; set; }
            }
        }

        [Table("t_person")]
        public class Person3
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties", Key = "meta")]
            [Column("meta")]
            public MetaData Meta { get; set; }

            public class MetaData
            {
                public int Id { get; set; }
                public string Name { get; set; }
                public Ext Ext { get; set; }

            }

            public class Ext
            {
                public int Age { get; set; }
            }
        }
        #endregion

        [Test]
        public void Test()
        {
            DropTableIfExist("t_person");
            db.ExecuteSql("create table t_person(id int auto_increment primary key,properties json)");

            var persons = db.Insert<Person>().SetEntity(new Person[]
            {
                new Person { Name="小明",Age=18},
                new Person { Name="小红",Age=20},
            }).ExecuteInsertedList();
            persons.Count.ShouldBe(2);
            persons[0].Id.ShouldBe(1);
            persons[0].Name.ShouldBe("小明");
            persons[0].Age.ShouldBe(18);
            persons[1].Id.ShouldBe(2);
            persons[1].Name.ShouldBe("小红");
            persons[1].Age.ShouldBe(20);
            var sql = $@"select {db.ColumnSeg<Person>(t => t)} from {db.TableSeg<Person>("t")} where {db.WhereSeg<Person>(t => t.Age == 18)}";
            sql.ShouldBe(@"select t.id `Id`,json_value(t.properties,'$.""name""' returning char) `Name`,json_value(t.properties,'$.""age""' returning signed) `Age` from t_person t where (json_value(t.properties,'$.""age""' returning signed)) = 18");
        }

        [Test]
        public void Test2()
        {
            DropTableIfExist("t_person");
            db.ExecuteSql("create table t_person(id int auto_increment primary key,properties json)");

            var persons = db.Insert<Person2>().SetEntity(new Person2[]
            {
                new Person2 {Addrs =new List<string>{"北京","上海" },Meta=new Person2.MetaData{ Id=1,Name="meta1"} },
                new Person2 {Addrs =new List<string>{"南京","天津" },Meta=new Person2.MetaData{ Id=2,Name="meta2"} },
            }).ExecuteInsertedList();

            persons.Count.ShouldBe(2);
            persons[0].Id.ShouldBe(1);
            persons[0].Addrs.ShouldBe(new List<string> { "北京", "上海" });
            persons[0].Meta.Id.ShouldBe(1);
            persons[0].Meta.Name.ShouldBe("meta1");
            persons[1].Id.ShouldBe(2);
            persons[1].Addrs.ShouldBe(new List<string> { "南京", "天津" });
            persons[1].Meta.Id.ShouldBe(2);
            persons[1].Meta.Name.ShouldBe("meta2");
        }

        [Test]
        public void Test3()
        {
            DropTableIfExist("t_person");
            db.ExecuteSql("create table t_person(id int auto_increment primary key,properties json)");

            db.Insert<Person2>().SetEntity(new Person2
            {
                Addrs = new List<string> { "南京", "武汉" },
                Meta = new Person2.MetaData
                {
                    Id = 1,
                    Name = "元数据"
                }
            }).ExecuteAffrows();

            var sql = db.Select<Person2>().Where(i => i.Meta.Id > 0).ToSqlList();
            var model = db.Select<Person2>().Where(i => i.Meta.Id > 0).FirstOrDefault();
            model.ShouldNotBeNull();
            model.Id.ShouldBe(1);
            model.Addrs.ShouldBe(new List<string> { "南京", "武汉" });
            model.Meta.Id.ShouldBe(1);
            model.Meta.Name.ShouldBe("元数据");

            model = db.Select<Person2>().Where(i => i.Meta.Id > 1).FirstOrDefault();
            model.ShouldBe(null);
        }

        [Test]
        public void Test4()
        {
            DropTableIfExist("t_person");
            db.ExecuteSql("create table t_person(id int auto_increment primary key,properties json)");

            db.Insert<Person3>().SetEntity(new Person3
            {
                Meta = new Person3.MetaData
                {
                    Id = 1,
                    Name = "元数据",
                    Ext = new Person3.Ext
                    {
                        Age = 20
                    }
                }
            }).ExecuteAffrows();

            var sql = db.Select<Person3>().Where(i => i.Meta.Ext.Age > 18).ToSqlList();
            //sql.ShouldBe("""
            //    select t.id `Id`,json_value(t.properties,'$."meta"') `Meta`
            //    from t_person t
            //    where (json_value(json_value(json_value(t.properties,'$."meta"'),'$."Ext"'),'$."Age"' returning signed)) > 18;
            //    """);
            var model = db.Select<Person3>().Where(i => i.Meta.Ext.Age > 18).FirstOrDefault();
            model.ShouldNotBeNull();
            model.Id.ShouldBe(1);
            model.Meta.Id.ShouldBe(1);
            model.Meta.Name.ShouldBe("元数据");
            model.Meta.Ext.Age.ShouldBe(20);

            model = db.Select<Person3>().Where(i => i.Meta.Ext.Age > 30).FirstOrDefault();
            model.ShouldBe(null);
        }

        [Table("t_test")]
        public class DictionaryJsonEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "attrs")]
            public Dictionary<string, string> Attrs { get; set; }
        }

        [Test]
        public void TestDictionary()
        {
            DropTableIfExist("t_test");
            db.ExecuteSql("create table t_test(id int auto_increment primary key,properties json)");

            db.Insert<DictionaryJsonEntity>().SetColumnExpr(i => i.Attrs, new Dictionary<string, string>() { { "name", "小明" }, { "age", "18" } }).ExecuteAffrows();

            var sql = db.Select<DictionaryJsonEntity>().Where(i => i.Attrs["name"] == "小明").ToSqlList();
            //            sql.ShouldBe(@"select t.id `Id`,json_value(t.properties,'$.""attrs""') `Attrs`
            //from t_test t
            //where (json_value(json_value(t.properties,'$.""attrs""'),'$.""name""' returning char)) = '小明';");
            var list = db.Select<DictionaryJsonEntity>().Where(i => i.Attrs["name"] == "小明").ToList();

            list.Count.ShouldBe(1);
            list[0].Id.ShouldBe(1);
            list[0].Attrs.Count.ShouldBe(2);
            list[0].Attrs["name"].ShouldBe("小明");
            list[0].Attrs["age"].ShouldBe("18");
        }

        [Table("t_test")]
        public class DictionaryJsonEntity2
        {
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "attrs")]
            public Dictionary<string, object> Attrs { get; set; }
        }

        [Test]
        public void TestDictionary2()
        {
            DropTableIfExist("t_test");
            db.ExecuteSql("create table t_test(id int auto_increment primary key,properties json)");

            db.Insert<DictionaryJsonEntity2>().SetColumnExpr(i => i.Attrs, new Dictionary<string, object>() { { "base", new Dictionary<string, object> { { "name", "小明" }, { "age", 18 } } }, { "ext", new Dictionary<string, object> { { "addr", "天明路" } } } }).ExecuteAffrows();

            var sql = db.Select<DictionaryJsonEntity2>().Where(i => (i.Attrs["base"] as Dictionary<string, object>)["name"].ToString() == "小明").ToSqlList();
            var sql2 = db.Select<DictionaryJsonEntity2>().Where(i => ((Dictionary<string, object>)i.Attrs["base"])["name"].ToString() == "小明").ToSqlList();
            sql.ShouldBe(sql2);
            //sql.ShouldBe("""
            //    select t.id `Id`,json_value(t.properties,'$."attrs"') `Attrs`
            //    from t_test t
            //    where convert(json_value(json_value(json_value(t.properties,'$."attrs"'),'$."base"'),'$."name"'),char) = '小明';
            //    """);
            var list = db.Select<DictionaryJsonEntity2>().Where(i => (i.Attrs["base"] as Dictionary<string, object>)["name"].ToString() == "小明").ToList();
            list.Count.ShouldBe(1);
            list[0].Attrs.Count.ShouldBe(2);

            list[0].Attrs["base"].ToJson().ShouldBe("{\"age\":18,\"name\":\"小明\"}");
        }

        [Table("test")]
        class Person4
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonObject ExtInfo { get; set; }
        }

        [Test]
        public void TestJsonObject()
        {
            DropTable("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");
            db.Insert<Person4>().SetEntity(new Person4
            {
                ExtInfo = new
                {
                    score = new { math = 95 },
                    addrs = new[] { "天明路", "幸福路" }
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteIdentity();
            string sql = string.Empty;
            sql = db.Select<Person4>().Where(i => i.ExtInfo["score"]["math"].GetValue<float>() > 90).ToSqlList();
            //sql.ShouldBe(
            //    """
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where (json_value(json_value(json_value(t.properties,'$."ext"'),'$."score"'),'$."math"' returning float)) > 90;
            //    """);
            var person = db.Select<Person4>().Where(i => i.ExtInfo["score"]["math"].GetValue<float>() > 90).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["score"]["math"].GetValue<float>().ShouldBe(95);

            //array
            sql = db.Select<Person4>().Where(i => i.ExtInfo["addrs"][1].GetValue<string>() == "幸福路").ToSqlList();
            //sql.ShouldBe(
            //    """
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where (json_value(json_value(json_value(t.properties,'$."ext"'),'$."addrs"'),'$[1]' returning char)) = '幸福路';
            //    """);

            person = db.Select<Person4>().Where(i => i.ExtInfo["addrs"][1].GetValue<string>() == "幸福路").FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");
            Console.WriteLine(sql);
        }

        [Test]
        public void TestJsonObject2()
        {
            DropTable("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");
            db.Insert<Person4>().SetEntity(new Person4
            {
                ExtInfo = new
                {
                    score = new { math = 95 },
                    addrs = new[] { "天明路", "幸福路" },
                    types = new[] { 1, 2, 4, 8, 16 },
                    others = new object[] { true, 5, false },
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteIdentity();
            string sql = string.Empty;

            //JsonObject.ContainsKey
            sql = db.Select<Person4>().Where(i => i.ExtInfo.ContainsKey("score")).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where json_contains_path(json_value(t.properties,'$."ext"'),'one','$."score"');
            //    """);
            var person = db.Select<Person4>().Where(i => i.ExtInfo.ContainsKey("score")).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");

            //JsonArray.Contains string
            sql = db.Select<Person4>().Where(i => i.ExtInfo["addrs"].AsArray().Contains("天明路")).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."addrs"'),'"天明路"');
            //    """);
            person = db.Select<Person4>().Where(i => i.ExtInfo["addrs"].AsArray().Contains("天明路")).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");

            //JsonArray.Contains number
            sql = db.Select<Person4>().Where(i => i.ExtInfo["types"].AsArray().Contains(8)).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."types"'),cast(8 as json));
            //    """);
            person = db.Select<Person4>().Where(i => i.ExtInfo["types"].AsArray().Contains(8)).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");

            //JsonArray.Contains bool
            sql = db.Select<Person4>().Where(i => i.ExtInfo["others"].AsArray().Contains(true)).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."others"'),'true');
            //    """);
            person = db.Select<Person4>().Where(i => i.ExtInfo["others"].AsArray().Contains(true)).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");
        }

        [Test]
        public void TestJsonObject3()
        {
            DropTable("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");
            db.Insert<Person4>().SetEntity(new Person4
            {
                ExtInfo = new
                {
                    score = new { math = 95, english = 82 },
                    addrs = new[] { "天明路", "幸福路" },
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteIdentity();
            string sql = string.Empty;

            //JsonArray.Count
            sql = db.Select<Person4>().Where(i => i.ExtInfo["addrs"].AsArray().Count > 1).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where json_length(json_value(json_value(t.properties,'$."ext"'),'$."addrs"')) > 1;
            //    """);
            var person = db.Select<Person4>().Where(i => i.ExtInfo["addrs"].AsArray().Count > 1).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");

            //JsonObject.Count
            sql = db.Select<Person4>().Where(i => i.ExtInfo["score"].AsObject().Count > 1).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `ExtInfo`
            //    from test t
            //    where json_length(json_value(json_value(t.properties,'$."ext"'),'$."score"')) > 1;
            //    """);
            person = db.Select<Person4>().Where(i => i.ExtInfo["score"].AsObject().Count > 1).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");
        }

        [Test]
        public void TestWhereSeg()
        {
            DropTable("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");
            db.Insert<Person4>().SetEntity(new Person4
            {
                ExtInfo = new
                {
                    score = new { math = 95, english = 82 },
                    addrs = new[] { "天明路", "幸福路" },
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteIdentity();
            string sql = string.Empty;

            //WhereSeg<Entity>
            sql = db.WhereSeg<Person4>(i => i.ExtInfo["score"].AsObject().Count > 1);
            sql.ShouldBe(@"json_length(json_value(json_value(i.properties,'$.""ext""'),'$.""score""')) > 1");

            //WhereSeg<JsonObject>
            sql = db.Select<Person4>().WhereSeg<JsonObject>(properties => properties["ext"]["score"].AsObject().Count > 1).ToSqlList();
            var person = db.Select<Person4>().WhereSeg<JsonObject>(properties => properties["ext"]["score"].AsObject().Count > 1).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");

            //WhereSeg<JsonArray>
            sql = db.Select<Person4>().WhereSeg<JsonArray>(properties => properties["ext"]["addrs"].AsArray().Contains("天明路")).ToSqlList();
            person = db.Select<Person4>().WhereSeg<JsonArray>(properties => properties["ext"]["addrs"].AsArray().Contains("天明路")).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.ExtInfo["addrs"][1].GetValue<string>().ShouldBe("幸福路");

            //WhereSeg<JsonObject> WhereSeg<JsonArray>
            sql = db.WhereSeg<JsonObject>(props => props["score"].AsObject().Count > 1, "t");
            sql.ShouldBe(@"json_length(json_value(t.props,'$.""score""')) > 1");
            sql = db.WhereSeg<JsonArray>(props => props[1]["name"].GetValue<string>() == "小明");
            sql.ShouldBe(@"(json_value(json_value(props,'$[1]'),'$.""name""' returning char)) = '小明'");
            sql = db.WhereSeg<JsonArray>(props => props.Count > 0, "t");
            sql.ShouldBe("json_length(t.props) > 0");
        }

        [Flags]
        public enum EnumFlag
        {
            Open = 1,
            Close = 2,
            None = 4
        }
        [Table("test")]
        class Person5
        {
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonObject Ext { get; set; }
        }

        [Test]
        public void TestEnumFlag()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person5>().SetEntity(new Person5
            {
                Ext = new
                {
                    Type = EnumFlag.Open | EnumFlag.Close,
                    Addrs = new[] { "天明路", "幸福路" }
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteAffrows();

            string sql = string.Empty;
            sql = db.Select<Person5>().Where(i => i.Ext["Type"].GetValue<EnumFlag>().Contains(EnumFlag.Close)).ToSqlList();
            //sql.ShouldBe(
            //    """
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where (json_value(json_value(t.properties,'$."ext"'),'$."Type"' returning signed)) & 2 = 2;
            //    """);
            var person = db.Select<Person5>().Where(i => i.Ext["Type"].GetValue<EnumFlag>().Contains(EnumFlag.Close)).FirstOrDefault();
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.Ext["Type"].GetValue<int>().To<EnumFlag>().ShouldBe(EnumFlag.Open | EnumFlag.Close);
            person.Ext["Addrs"][0].GetValue<string>().ShouldBe("天明路");
            person.Ext["Addrs"][1].GetValue<string>().ShouldBe("幸福路");

        }

        [Test]
        public void TestArrayContains()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person5>().SetEntity(new Person5
            {
                Ext = new
                {
                    Types = new[] { EnumFlag.Open, EnumFlag.Close, EnumFlag.Open },
                    Addrs = new[] { "天明路", "幸福路" },
                    Scores = new[] { 95, 85 },
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteAffrows();

            //Contains
            string sql = string.Empty;
            sql = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<EnumFlag[]>().Contains(EnumFlag.Open)).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."Types"'),cast(1 as json));
            //    """);
            var person = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<EnumFlag[]>().Contains(EnumFlag.Open)).FirstOrDefault();
            validate(person);

            //ContainsAny
            sql = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<EnumFlag[]>().ContainsAny(new[] { EnumFlag.Open, EnumFlag.Close })).ToSqlList();
            //            sql.ShouldBe(@"select t.`Id`,json_value(t.properties,'$.""ext""') `Ext`
            //from test t
            //where json_overlaps(json_value(json_value(t.properties,'$.""ext""'),'$.""Types""'),'[1,2]');");
            person = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<EnumFlag[]>().ContainsAny(new[] { EnumFlag.Open, EnumFlag.Close })).FirstOrDefault();
            validate(person);

            //ContainsAll
            sql = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<EnumFlag[]>().ContainsAll(new[] { EnumFlag.Open, EnumFlag.Close })).ToSqlList();
            //            sql.ShouldBe(@"select t.`Id`,json_value(t.properties,'$.""ext""') `Ext`
            //from test t
            //where json_contains(json_value(json_value(t.properties,'$.""ext""'),'$.""Types""'),'[1,2]');");
            person = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<EnumFlag[]>().ContainsAll(new[] { EnumFlag.Open, EnumFlag.Close })).FirstOrDefault();
            validate(person);
        }

        [Test]
        public void TestListContains()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person5>().SetEntity(new Person5
            {
                Ext = new
                {
                    Types = new[] { EnumFlag.Open, EnumFlag.Close, EnumFlag.Open },
                    Addrs = new[] { "天明路", "幸福路" },
                    Scores = new[] { 95, 85 },
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteAffrows();

            //Contains
            string sql = string.Empty;
            sql = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<List<EnumFlag>>().Contains(EnumFlag.Open)).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."Types"'),cast(1 as json));
            //    """);
            var person = db.Select<Person5>().Where(i => i.Ext["Types"].GetValue<List<EnumFlag>>().Contains(EnumFlag.Open)).FirstOrDefault();
            validate(person);
        }

        [Test]
        public void TestJsonArrayContains()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person5>().SetEntity(new Person5
            {
                Ext = new
                {
                    Types = new[] { EnumFlag.Open, EnumFlag.Close, EnumFlag.Open },
                    Addrs = new[] { "天明路", "幸福路" },
                    Scores = new[] { 95, 85 },
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteAffrows();

            var sql = string.Empty;
            Person5 person = null;

            //JsonArray.ContainsObject(scalar)
            sql = db.Select<Person5>().Where(i => i.Ext["Addrs"].AsArray().ContainsObject("天明路")).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."Addrs"'),'"天明路"');
            //    """);
            person = db.Select<Person5>().Where(i => i.Ext["Addrs"].AsArray().ContainsObject("天明路")).FirstOrDefault();
            validate(person);

            //JsonArray.ContainsObjectAny(scalar[])
            sql = db.Select<Person5>().Where(i => i.Ext["Types"].AsArray().ContainsObjectAny(new[] { EnumFlag.Open, EnumFlag.Close })).ToSqlList();
            //sql.ShouldBe(
            //    """
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_overlaps(json_value(json_value(t.properties,'$."ext"'),'$."Types"'),'[1,2]');
            //    """);
            person = db.Select<Person5>().Where(i => i.Ext["Types"].AsArray().ContainsObjectAny(new[] { EnumFlag.Open, EnumFlag.Close })).FirstOrDefault();
            validate(person);

            //JsonArray.ContainsObjectAll(scalar[])
            sql = db.Select<Person5>().Where(i => i.Ext["Types"].AsArray().ContainsObjectAll(new[] { EnumFlag.Open, EnumFlag.Close })).ToSqlList();
            //sql.ShouldBe(
            //    """
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."Types"'),'[1,2]');
            //    """);
            person = db.Select<Person5>().Where(i => i.Ext["Types"].AsArray().ContainsObjectAll(new[] { EnumFlag.Open, EnumFlag.Close })).FirstOrDefault();
            validate(person);
        }

        [Test]
        public void TestJsonDictionary()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person5>().SetEntity(new Person5
            {
                Ext = new
                {
                    Types = new[] { EnumFlag.Open, EnumFlag.Close, EnumFlag.Open },
                    Addrs = new[] { "天明路", "幸福路" },
                    Scores = new[] { 95, 85 },
                    Infos = new { Name = "小明", Age = 18 }
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteAffrows();

            //Dictionary.ContainsKey
            var sql = string.Empty;
            sql = db.Select<Person5>().Where(i => i.Ext["Infos"].ToJson().ToObject<Dictionary<string, object>>().ContainsKey("Name")).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_contains_path(cast(convert(json_value(json_value(t.properties,'$."ext"'),'$."Infos"'),char) as json),'one','$."Name"');
            //    """);
            sql = db.Select<Person5>().Where(i => i.Ext["Infos"].ToJsonString(null).ToObject<Dictionary<string, object>>().ContainsKey("Name")).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_contains_path(cast(convert(json_value(json_value(t.properties,'$."ext"'),'$."Infos"'),char) as json),'one','$."Name"');
            //    """);
            var person = db.Select<Person5>().Where(i => i.Ext["Infos"].ToJson().ToObject<Dictionary<string, object>>().ContainsKey("Name")).FirstOrDefault();
            validate(person);
        }

        private void validate(Person5 person)
        {
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.Ext["Types"].ToJson().ShouldBe("[1,2,1]");
            person.Ext["Addrs"].ToJson().ShouldBe("[\"天明路\",\"幸福路\"]");
            person.Ext["Scores"].ToJson().ShouldBe("[95,85]");
        }

        [Test]
        public void TestArrayAny()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person5>().SetEntity(new Person5
            {
                Ext = new
                {
                    Types = new[] { EnumFlag.Open, EnumFlag.Close, EnumFlag.Open },
                    Addrs = new[] { "天明路", "幸福路" },
                    Scores = new[] { 95, 85 },
                    Infos = new[] { new { Name = "小明", Age = 18 }, new { Name = "小花", Age = 20 } }
                }.ToJson().ToObject<JsonObject>()
            }).ExecuteAffrows();

            var sql = string.Empty;
            Person5 person = null;

            //JsonArray.ContainsObject
            sql = db.Select<Person5>().Where(i => i.Ext["Infos"].AsArray().ContainsObject(new { Name = "小明", Age = 18 })).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`
            //    from test t
            //    where json_contains(json_value(json_value(t.properties,'$."ext"'),'$."Infos"'),'{"Name":"小明","Age":18}');
            //    """);
            person = db.Select<Person5>().Where(i => i.Ext["Infos"].AsArray().ContainsObject(new { Name = "小明", Age = 18 })).FirstOrDefault();
            validate(person);

            //
            sql = db.Select<Person5>().Where(i => i.Ext["Infos"].AsArray().ContainsObjectAny(new[] { new { Name = "小明", Age = 18 }, new { Name = "小明", Age = 20 } })).ToSqlList();
            Console.WriteLine(sql);
        }

        [Table("test")]
        public class Person6
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "Infos")]
            public List<Infos> Infos { get; set; }
            [JsonStore(Bucket = "properties", Key = "Infos2")]
            public Infos[] InfoArray { get; set; }
        }

        public class Infos
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public EnumFlag Flag { get; set; }
        }
        [Test]
        public void TestJsonStoreModel()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person6>().SetEntity(new Person6
            {
                Infos = new List<Infos> {
                    new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None },
                    new Infos { Id = 2, Name = "小刚", Flag = EnumFlag.None|EnumFlag.Open }
                },
                InfoArray = new[] {
                    new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None },
                    new Infos { Id = 2, Name = "小刚", Flag = EnumFlag.None|EnumFlag.Open }
                }
            }).ExecuteAffrows();

            var sql = string.Empty;
            Person6 person = null;

            //List.Count
            sql = db.Select<Person6>().Where(i => i.Infos.Count() > 0).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."Infos"') `Infos`,json_value(t.properties,'$."Infos2"') `InfoArray`
            //    from test t
            //    where json_length(json_value(t.properties,'$."Infos"')) > 0;
            //    """);
            person = db.Select<Person6>().Where(i => i.Infos.Count() > 0).FirstOrDefault();
            validate(person);

            //Array.Length
            sql = db.Select<Person6>().Where(i => i.InfoArray.Length > 0).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."Infos"') `Infos`,json_value(t.properties,'$."Infos2"') `InfoArray`
            //    from test t
            //    where json_length(json_value(t.properties,'$."Infos2"')) > 0;
            //    """);
            validate(person);

            //ToJson.ToObject.ContainsObject
            sql = db.Select<Person6>().Where(i => i.InfoArray.ToJson().ToObject<JsonArray>().ContainsObject(new { Id = 1, Name = "小明" })).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."Infos"') `Infos`,json_value(t.properties,'$."Infos2"') `InfoArray`
            //    from test t
            //    where json_contains(cast(convert(json_value(t.properties,'$."Infos2"'),char) as json),'{"Id":1,"Name":"小明"}');
            //    """);
            person = db.Select<Person6>().Where(i => i.InfoArray.ToJson().ToObject<JsonArray>().ContainsObject(new { Id = 1, Name = "小明" })).FirstOrDefault();
            validate(person);

            //IEnumerable.Contains
            sql = db.Select<Person6>().Where(i => i.InfoArray.Contains(new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None })).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."Infos"') `Infos`,json_value(t.properties,'$."Infos2"') `InfoArray`
            //    from test t
            //    where json_contains(json_value(t.properties,'$."Infos2"'),'{"Id":1,"Name":"小明","Flag":4}');
            //    """);
            person = db.Select<Person6>().Where(i => i.InfoArray.Contains(new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None })).FirstOrDefault();
            validate(person);

            //IEnumerable.ContainsAny
            sql = db.Select<Person6>().Where(i => i.InfoArray.ContainsAny(new[] { new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None } })).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."Infos"') `Infos`,json_value(t.properties,'$."Infos2"') `InfoArray`
            //    from test t
            //    where json_overlaps(json_value(t.properties,'$."Infos2"'),'[{"Id":1,"Name":"小明","Flag":4}]');
            //    """);
            person = db.Select<Person6>().Where(i => i.InfoArray.ContainsAny(new[] { new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None } })).FirstOrDefault();
            validate(person);

            //IEnumerable.ContainsAll
            sql = db.Select<Person6>().Where(i => i.InfoArray.ContainsAll(new[] { new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None } })).ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."Infos"') `Infos`,json_value(t.properties,'$."Infos2"') `InfoArray`
            //    from test t
            //    where json_contains(json_value(t.properties,'$."Infos2"'),'[{"Id":1,"Name":"小明","Flag":4}]');
            //    """);
            person = db.Select<Person6>().Where(i => i.InfoArray.ContainsAll(new[] { new Infos { Id = 1, Name = "小明", Flag = EnumFlag.None } })).FirstOrDefault();
            validate(person);
        }

        private void validate(Person6 person)
        {
            person.ShouldNotBe(null);
            person.Id.ShouldBe(1);
            person.Infos.Count().ShouldBe(2);
            person.Infos[0].Id.ShouldBe(1);
            person.Infos[0].Name.ShouldBe("小明");
            person.Infos[1].Name.ShouldBe("小刚");
            person.Infos[1].Flag.ShouldBe(EnumFlag.None | EnumFlag.Open);

            person.InfoArray.Length.ShouldBe(2);
            person.InfoArray[0].Id.ShouldBe(1);
            person.InfoArray[0].Name.ShouldBe("小明");
            person.InfoArray[1].Name.ShouldBe("小刚");
            person.InfoArray[1].Flag.ShouldBe(EnumFlag.None | EnumFlag.Open);
        }

        [Table("test")]
        public class Person7
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext2")]
            public JsonArray Ext2 { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext3")]
            public Dictionary<string, string> Props { get; set; }
        }

        [Test]
        public void TestProtectPropertyName()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json)");

            db.Insert<Person7>().SetEntity(new Person7
            {
                Id = 1,
                Ext = new Dictionary<string, string> { { "\"Name", "小明" }, { "Age", "18" } }.ToJson().ToObject<JsonObject>(),
                Ext2 = new[] { new Dictionary<string, object> { { "to m", 20 } } }.ToJson().ToObject<JsonArray>(),
                Props = new Dictionary<string, string> { { "1Name", "lisa" } }
            }).ExecuteAffrows();
            string sql = string.Empty;
            sql = db.Select<Person7>()
                .Where(i => i.Ext["\"Name"].GetValue<string>() == "小明")
                .Where(i => i.Ext2.AsArray().Contains(new Dictionary<string, object> { { "to m", 20 } }.ToJson().ToObject<JsonObject>()))
                .Where(i => i.Ext2.AsArray()[0]["to m"].GetValue<int>() == 20)
                .Where(i => i.Props["1Name"] == "lisa")
                .ToSqlList();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.properties,'$."ext"') `Ext`,json_value(t.properties,'$."ext2"') `Ext2`,json_value(t.properties,'$."ext3"') `Props`
            //    from test t
            //    where ((json_value(json_value(t.properties,'$."ext"'),'$."\\"Name"' returning char)) = '小明') and (json_contains(json_value(t.properties,'$."ext2"'),'{"to m":20}')) and ((json_value(json_value(json_value(t.properties,'$."ext2"'),'$[0]'),'$."to m"' returning signed)) = 20) and ((json_value(json_value(t.properties,'$."ext3"'),'$."1Name"' returning char)) = 'lisa');
            //    """);
            var person = db.Select<Person7>()
                .Where(i => i.Ext["\"Name"].GetValue<string>() == "小明")
                .Where(i => i.Ext2.AsArray().Contains(new Dictionary<string, object> { { "to m", 20 } }.ToJson().ToObject<JsonObject>()))
                .Where(i => i.Ext2.AsArray()[0]["to m"].GetValue<int>() == 20)
                .Where(i => i.Props["1Name"] == "lisa").FirstOrDefault();
            person.Id.ShouldBe(1);
            person.Ext.ToJson().ShouldBe(@"{""Age"":""18"",""\""Name"":""小明""}");
            person.Ext2.ToJson().ShouldBe(@"[{""to m"":20}]");
            person.Props.ToJson().ShouldBe(@"{""1Name"":""lisa""}");
        }

        [Table("t_test8")]
        public class Person8
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext", Key = "StuIds")]
            public List<int> StuIds { get; set; }
        }

        [Test]
        public void Test8()
        {
            var sql = db.Select<Person8>().Where(i => i.StuIds[1] == 5).ToSqlList();
            Console.WriteLine(sql);
        }
    }
}
