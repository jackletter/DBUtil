﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonPrimetiveTests : TestBase
    {
        #region nokey
        [Table("test")]
        internal class JsonTestPerson
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "p_byte")]
            public byte p_byte { get; set; }

            [JsonStore(Bucket = "p_sbyte")]
            public sbyte p_sbyte { get; set; }

            [JsonStore(Bucket = "p_short")]
            public short p_short { get; set; }

            [JsonStore(Bucket = "p_ushort")]
            public ushort p_ushort { get; set; }

            [JsonStore(Bucket = "p_int")]
            public int p_int { get; set; }

            [JsonStore(Bucket = "p_uint")]
            public uint p_uint { get; set; }

            [JsonStore(Bucket = "p_long")]
            public long p_long { get; set; }

            [JsonStore(Bucket = "p_ulong")]
            public ulong p_ulong { get; set; }

            [JsonStore(Bucket = "p_float")]
            public float p_float { get; set; }

            [JsonStore(Bucket = "p_double")]
            public double p_double { get; set; }

            [JsonStore(Bucket = "p_decimal")]
            public decimal p_decimal { get; set; }

            [JsonStore(Bucket = "p_long_null")]
            public int? p_long_null { get; set; }

            [JsonStore(Bucket = "p_string")]
            public string p_string { get; set; }

            [JsonStore(Bucket = "p_datetime")]
            public DateTime p_datetime { get; set; }

            [JsonStore(Bucket = "p_dateonly")]
            public DateOnly p_dateonly { get; set; }

            [JsonStore(Bucket = "p_timeonly")]
            public TimeOnly p_timeonly { get; set; }

            [JsonStore(Bucket = "p_timespan")]
            public TimeSpan p_timespan { get; set; }

            [JsonStore(Bucket = "p_datetime_null")]
            public DateTime? p_datetime_null { get; set; }

            [JsonStore(Bucket = "p_guid")]
            public Guid p_guid { get; set; }

            [JsonStore(Bucket = "p_guid_null")]
            public Guid? p_guid_null { get; set; }

            [JsonStore(Bucket = "p_bool")]
            public bool p_bool { get; set; }

            [JsonStore(Bucket = "p_bool_null")]
            public bool? p_bool_null { get; set; }

            [JsonStore(Bucket = "p_object")]
            public object p_object { get; set; }


            [JsonStore(Bucket = "p_object_null")]
            public object p_object_null { get; set; }

            [JsonStore(Bucket = "p_list")]
            public List<string> p_list { get; set; }

            [JsonStore(Bucket = "p_arr")]
            public string[] p_arr { get; set; }

            [JsonStore(Bucket = "p_dic")]
            public Dictionary<string, object> p_dic { get; set; }

            [JsonStore(Bucket = "p_jsonobj")]
            public JsonObject p_jsonobj { get; set; }

            [JsonStore(Bucket = "p_jsonarr")]
            public JsonArray p_jsonarr { get; set; }
        }

        private (Expression<Func<JsonTestPerson>> insert, Action<JsonTestPerson> validate) Prepare()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""
                create table test(
                    id int primary key auto_increment,
                    p_byte json,p_sbyte json,
                    p_short json,p_ushort json,
                    p_int json,p_uint json,
                    p_long json,p_ulong json,
                    p_float json,p_double json,p_decimal json,
                    p_long_null json,
                    p_string json,
                    p_datetime json,p_dateonly json,p_timeonly json,p_timespan json,p_datetime_null json,
                    p_guid json,p_guid_null json,
                    p_bool json,p_bool_null json,
                    p_object json,p_object_null json,
                    p_list json,p_arr json,
                    p_dic json,
                    p_jsonobj json,
                    p_jsonarr json
                )
                """);
            return (() => new JsonTestPerson
            {
                p_byte = 1,
                p_sbyte = 2,
                p_short = 3,
                p_ushort = 4,
                p_int = 5,
                p_uint = 6,
                p_long = 7,
                p_ulong = 8,
                p_float = 9,
                p_double = 10,
                p_decimal = 11,
                p_string = "tom",
                p_datetime = DateTime.Parse("2023-07-06 17:32:00"),
                p_dateonly = DateOnly.Parse("2023-07-06"),//dateonly 在 bulkcopy 的时候不受支持,todo 改成 datetime?
                p_timeonly = TimeOnly.Parse("17:32:00"),//timeonly 在 bulkcopy 的时候不受支持,todo 改成 timespan
                p_timespan = TimeSpan.FromSeconds(3600),
                p_datetime_null = DateTime.Parse("2023-07-06 17:32:00"),
                p_guid = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_guid_null = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_bool = true,
                p_bool_null = true,
                p_object = new { Age = 18, Name = "jim" },
                p_object_null = new { Age = 18, Name = "jim", Other = "oth" },
                p_list = new List<string> { "tom", "lisa" },
                p_arr = new[] { "lisa", "jim" },
                p_dic = new Dictionary<string, object> { { "name", "tom" }, { "age", 18 } },
                p_jsonobj = new { name = "tom", age = 18 }.ToJson().ToObject<JsonObject>(),
                p_jsonarr = new[] { new { name = "tom", age = 18 } }.ToJson().ToObject<JsonArray>(),
            },
            (ent) =>
            {
                ent.ShouldNotBeNull();
                ent.Id.ShouldBe(1);
                ent.p_byte.ShouldBe((byte)1);
                ent.p_sbyte.ShouldBe((sbyte)2);
                ent.p_short.ShouldBe((short)3);
                ent.p_ushort.ShouldBe((ushort)4);
                ent.p_int.ShouldBe(5);
                ent.p_uint.ShouldBe((uint)6);
                ent.p_long.ShouldBe(7);
                ent.p_ulong.ShouldBe((ulong)8);
                ent.p_float.ShouldBe(9);
                ent.p_double.ShouldBe(10);
                ent.p_decimal.ShouldBe(11);
                ent.p_long_null.ShouldBe(null);
                //
                ent.p_string.ShouldBe("tom");
                ent.p_datetime.ShouldBe(DateTime.Parse("2023-07-06 17:32:00"));
                ent.p_dateonly.ShouldBe(DateOnly.Parse("2023-07-06"));
                ent.p_timeonly.ShouldBe(TimeOnly.Parse("17:32:00"));
                ent.p_timespan.ShouldBe(TimeSpan.FromSeconds(3600));
                ent.p_datetime_null.ShouldBe(DateTime.Parse("2023-07-06 17:32:00"));
                ent.p_guid.ShouldBe(Guid.Parse("96d80686975340dda55492ecda9c92f2"));
                ent.p_guid_null.ShouldBe(Guid.Parse("96d80686975340dda55492ecda9c92f2"));
                ent.p_bool.ShouldBe(true);
                ent.p_bool_null.ShouldBe(true);
                ent.p_object.ToJson().ShouldBe(new { Age = 18, Name = "jim" }.ToJson());
                ent.p_object_null.ToJson().ShouldBe(new { Age = 18, Name = "jim", Other = "oth" }.ToJson());
                ent.p_list.ToJson().ShouldBe(new List<string> { "tom", "lisa" }.ToJson());
                ent.p_arr.ToJson().ShouldBe(new[] { "lisa", "jim" }.ToJson());
                ent.p_dic.ToJson().ShouldBe(new Dictionary<string, object> { { "age", 18 }, { "name", "tom" } }.ToJson());
                ent.p_jsonarr.ToJson().ShouldBe(new[] { new { age = 18, name = "tom" } }.ToJson().ToObject<JsonArray>().ToJson());
                ent.p_jsonobj.ToJson().ShouldBe(new { age = 18, name = "tom" }.ToJson().ToObject<JsonObject>().ToJson());
            }
            );
        }

        [Test]
        public void UpdateByExpression_NoKey()
        {
            var dt = DateTime.Parse("2023-09-18 08:15:00");
            var guid = Guid.Parse("4814a295-6075-4cd7-bc99-938312152ef0").ToString("N");
            var (exp, validate) = Prepare();

            var sql = db.Update<JsonTestPerson>().SetExpr(() => new JsonTestPerson
            {
                p_byte = 1,
                p_sbyte = 2,
                p_short = 3,
                p_ushort = 4,
                p_int = 5,
                p_uint = 6,
                p_long = 7,
                p_ulong = 8,
                p_float = 9,
                p_double = 10,
                p_decimal = 11,
                p_long_null = null,
                p_string = "abc",
                p_datetime = dt,
                p_dateonly = DateOnly.Parse("2023-09-18"),
                p_timeonly = TimeOnly.Parse("08:15:00"),
                p_timespan = TimeSpan.FromDays(1),
                p_datetime_null = null,
                p_guid = Guid.Parse(guid),
                p_guid_null = null,
                p_bool = true,
                p_bool_null = null,
                p_object = new { name = "tom", age = 18 },
                p_object_null = null,
                p_list = new List<string> { "tom", "lisa" },
                p_arr = new string[] { "tom", "lisa" },
                p_dic = new Dictionary<string, object> { { "name", "lisa" }, { "age", 18 } },
                p_jsonobj = new { name = "lisa", age = 18 }.ToJsonObject(),
                p_jsonarr = new[] { 1, 2, 3 }.ToJsonArray(),
            }).Where(i => i.Id == 1).ToSql();
            sql.ShouldBe("""
                update test set
                    p_byte = cast(1 as json),
                    p_sbyte = cast(2 as json),
                    p_short = cast(3 as json),
                    p_ushort = cast(4 as json),
                    p_int = cast(5 as json),
                    p_uint = cast(6 as json),
                    p_long = cast(7 as json),
                    p_ulong = cast(8 as json),
                    p_float = cast(9 as json),
                    p_double = cast(10 as json),
                    p_decimal = cast(11 as json),
                    p_long_null = null,
                    p_string = '"abc"',
                    p_datetime = '"2023-09-18T08:15:00.0000000"',
                    p_dateonly = '"2023-09-18"',
                    p_timeonly = '"08:15:00"',
                    p_timespan = '"1.00:00:00"',
                    p_datetime_null = null,
                    p_guid = '"4814a295-6075-4cd7-bc99-938312152ef0"',
                    p_guid_null = null,
                    p_bool = 'true',
                    p_bool_null = null,
                    p_object = json_set(ifnull(p_object,json_object()),
                        '$."name"','tom',
                        '$."age"',18),
                    p_object_null = null,
                    p_list = '["tom","lisa"]',
                    p_arr = '["tom","lisa"]',
                    p_dic = '{"name":"lisa","age":18}',
                    p_jsonobj = json_object('name','lisa','age',18),
                    p_jsonarr = '[1,2,3]'
                where `Id` = 1;
                """);
        }

        [Test]
        public void UpdateByExpression_NoKey_setremove()
        {
            var dt = DateTime.Parse("2023-09-18 08:15:00");
            var guid = Guid.Parse("4814a295-6075-4cd7-bc99-938312152ef0").ToString("N");
            var (exp, validate) = Prepare();

            var update = db.Update<JsonTestPerson>().SetExpr(exp)
                .SetColumnExpr(i => i.p_long, 456)
                .SetColumnExpr(i => i.p_string, "modi")
                .SetColumnExpr(i => i.p_uint, uint.MaxValue)
                .SetColumn("p_ulong", 789)
                .SetColumnExpr(i => i.p_object, () => new
                {
                    K1 = "k1",
                    K2 = new
                    {
                        K21 = 1,
                        K22 = new[] { 1, 2 },
                    },
                    K3 = DateTime.Parse("2023-07-15 17:35:00"),
                    K4 = new List<int> { 33, 44 },
                    K5 = new Dictionary<string, string> { { "age", "18" }, { "name", "tom" } }
                }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        p_byte = cast(1 as json),
            //        p_sbyte = cast(2 as json),
            //        p_short = cast(3 as json),
            //        p_ushort = cast(4 as json),
            //        p_int = cast(5 as json),
            //        p_float = cast(9 as json),
            //        p_double = cast(10 as json),
            //        p_decimal = cast(11 as json),
            //        p_datetime = '"2023-07-06T17:32:00.0000000"',
            //        p_dateonly = '"2023-07-06"',
            //        p_timeonly = '"17:32:00"',
            //        p_timespan = '"01:00:00"',
            //        p_datetime_null = '"2023-07-06T17:32:00.0000000"',
            //        p_guid = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_guid_null = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_bool = 'true',
            //        p_bool_null = 'true',
            //        p_object_null = json_set(ifnull(p_object_null,json_object()),
            //            '$."Age"',18,
            //            '$."Name"','jim',
            //            '$."Other"','oth'),
            //        p_list = '["tom","lisa"]',
            //        p_arr = '["lisa","jim"]',
            //        p_dic = '{"name":"tom","age":18}',
            //        p_jsonobj = cast(convert(json_object('name','tom','age',18),char) as json),
            //        p_jsonarr = cast(convert(json_array(json_object('name','tom','age',18)),char) as json),
            //        p_long = cast(456 as json),
            //        p_string = '"modi"',
            //        p_uint = cast(4294967295 as json),
            //        p_ulong = cast(789 as json),
            //        p_object = json_set(ifnull(p_object,json_object()),
            //            '$."K1"','k1',
            //            '$."K2"',cast(ifnull(json_value(p_object,'$."K2"'),'{}') as json),
            //            '$."K2"."K21"',1,
            //            '$."K2"."K22"',cast(ifnull(json_value(p_object,'$."K2"."K22"'),'[]') as json),
            //            '$."K2"."K22"',cast('[1,2]' as json),
            //            '$."K3"','2023-07-15T17:35:00.0000000',
            //            '$."K4"',cast(ifnull(json_value(p_object,'$."K4"'),'[]') as json),
            //            '$."K4"',cast('[33,44]' as json),
            //            '$."K5"',cast(ifnull(json_value(p_object,'$."K5"'),'{}') as json),
            //            '$."K5"',cast('{"age":"18","name":"tom"}' as json))
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByEntity_NoKey()
        {
            var (exp, validate) = Prepare();

            var ent = exp.Compile()();
            ent.Id = 1;
            var sql = db.Update<JsonTestPerson>().SetEntity(ent).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        p_byte = cast(1 as json),
            //        p_sbyte = cast(2 as json),
            //        p_short = cast(3 as json),
            //        p_ushort = cast(4 as json),
            //        p_int = cast(5 as json),
            //        p_uint = cast(6 as json),
            //        p_long = cast(7 as json),
            //        p_ulong = cast(8 as json),
            //        p_float = cast(9 as json),
            //        p_double = cast(10 as json),
            //        p_decimal = cast(11 as json),
            //        p_long_null = null,
            //        p_string = '"tom"',
            //        p_datetime = '"2023-07-06T17:32:00.0000000"',
            //        p_dateonly = '"2023-07-06"',
            //        p_timeonly = '"17:32:00"',
            //        p_timespan = '"01:00:00"',
            //        p_datetime_null = '"2023-07-06T17:32:00.0000000"',
            //        p_guid = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_guid_null = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_bool = 'true',
            //        p_bool_null = 'true',
            //        p_object = '{"Age":18,"Name":"jim"}',
            //        p_object_null = '{"Age":18,"Name":"jim","Other":"oth"}',
            //        p_list = '["tom","lisa"]',
            //        p_arr = '["lisa","jim"]',
            //        p_dic = '{"name":"tom","age":18}',
            //        p_jsonobj = '{"name":"tom","age":18}',
            //        p_jsonarr = '[{"name":"tom","age":18}]'
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByEntity_NoKey_MultiValues()
        {
            var (exp, validate) = Prepare();

            var ent = exp.Compile()();
            ent.Id = 1;
            var ent2 = exp.Compile()();
            ent2.Id = 2;
            var sql = db.Update<JsonTestPerson>().SetEntity([ent, ent2]).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        p_byte = case `Id`
            //          when 1 then cast(1 as json)
            //          when 2 then cast(1 as json) end,
            //        p_sbyte = case `Id`
            //          when 1 then cast(2 as json)
            //          when 2 then cast(2 as json) end,
            //        p_short = case `Id`
            //          when 1 then cast(3 as json)
            //          when 2 then cast(3 as json) end,
            //        p_ushort = case `Id`
            //          when 1 then cast(4 as json)
            //          when 2 then cast(4 as json) end,
            //        p_int = case `Id`
            //          when 1 then cast(5 as json)
            //          when 2 then cast(5 as json) end,
            //        p_uint = case `Id`
            //          when 1 then cast(6 as json)
            //          when 2 then cast(6 as json) end,
            //        p_long = case `Id`
            //          when 1 then cast(7 as json)
            //          when 2 then cast(7 as json) end,
            //        p_ulong = case `Id`
            //          when 1 then cast(8 as json)
            //          when 2 then cast(8 as json) end,
            //        p_float = case `Id`
            //          when 1 then cast(9 as json)
            //          when 2 then cast(9 as json) end,
            //        p_double = case `Id`
            //          when 1 then cast(10 as json)
            //          when 2 then cast(10 as json) end,
            //        p_decimal = case `Id`
            //          when 1 then cast(11 as json)
            //          when 2 then cast(11 as json) end,
            //        p_long_null = case `Id`
            //          when 1 then null
            //          when 2 then null end,
            //        p_string = case `Id`
            //          when 1 then '"tom"'
            //          when 2 then '"tom"' end,
            //        p_datetime = case `Id`
            //          when 1 then '"2023-07-06T17:32:00.0000000"'
            //          when 2 then '"2023-07-06T17:32:00.0000000"' end,
            //        p_dateonly = case `Id`
            //          when 1 then '"2023-07-06"'
            //          when 2 then '"2023-07-06"' end,
            //        p_timeonly = case `Id`
            //          when 1 then '"17:32:00"'
            //          when 2 then '"17:32:00"' end,
            //        p_timespan = case `Id`
            //          when 1 then '"01:00:00"'
            //          when 2 then '"01:00:00"' end,
            //        p_datetime_null = case `Id`
            //          when 1 then '"2023-07-06T17:32:00.0000000"'
            //          when 2 then '"2023-07-06T17:32:00.0000000"' end,
            //        p_guid = case `Id`
            //          when 1 then '"96d80686-9753-40dd-a554-92ecda9c92f2"'
            //          when 2 then '"96d80686-9753-40dd-a554-92ecda9c92f2"' end,
            //        p_guid_null = case `Id`
            //          when 1 then '"96d80686-9753-40dd-a554-92ecda9c92f2"'
            //          when 2 then '"96d80686-9753-40dd-a554-92ecda9c92f2"' end,
            //        p_bool = case `Id`
            //          when 1 then 'true'
            //          when 2 then 'true' end,
            //        p_bool_null = case `Id`
            //          when 1 then 'true'
            //          when 2 then 'true' end,
            //        p_object = case `Id`
            //          when 1 then '{"Age":18,"Name":"jim"}'
            //          when 2 then '{"Age":18,"Name":"jim"}' end,
            //        p_object_null = case `Id`
            //          when 1 then '{"Age":18,"Name":"jim","Other":"oth"}'
            //          when 2 then '{"Age":18,"Name":"jim","Other":"oth"}' end,
            //        p_list = case `Id`
            //          when 1 then '["tom","lisa"]'
            //          when 2 then '["tom","lisa"]' end,
            //        p_arr = case `Id`
            //          when 1 then '["lisa","jim"]'
            //          when 2 then '["lisa","jim"]' end,
            //        p_dic = case `Id`
            //          when 1 then '{"name":"tom","age":18}'
            //          when 2 then '{"name":"tom","age":18}' end,
            //        p_jsonobj = case `Id`
            //          when 1 then '{"name":"tom","age":18}'
            //          when 2 then '{"name":"tom","age":18}' end,
            //        p_jsonarr = case `Id`
            //          when 1 then '[{"name":"tom","age":18}]'
            //          when 2 then '[{"name":"tom","age":18}]' end
            //    where `Id` in (1,2);
            //    """);
        }

        [Test]
        public void UpdateByEntity_NoKey_setremove()
        {
            var (exp, validate) = Prepare();

            var ent = exp.Compile()();
            ent.Id = 1;
            var update = db.Update<JsonTestPerson>().SetEntity(ent)
                .SetColumnExpr(i => i.p_long, 456)
                .SetColumnExpr(i => i.p_string, "modi")
                .SetColumnExpr(i => i.p_uint, uint.MaxValue)
                .SetColumn("p_ulong", 789)
                .SetColumnExpr(i => i.p_object, () => new
                {
                    K1 = "k1",
                    K2 = new
                    {
                        K21 = 1,
                        K22 = new[] { 1, 2 },
                    },
                    K3 = DateTime.Parse("2023-07-15 17:35:00"),
                    K4 = new List<int> { 33, 44 },
                    K5 = new Dictionary<string, string> { { "age", "18" }, { "name", "tom" } }
                });
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        p_byte = cast(1 as json),
            //        p_sbyte = cast(2 as json),
            //        p_short = cast(3 as json),
            //        p_ushort = cast(4 as json),
            //        p_int = cast(5 as json),
            //        p_float = cast(9 as json),
            //        p_double = cast(10 as json),
            //        p_decimal = cast(11 as json),
            //        p_long_null = null,
            //        p_datetime = '"2023-07-06T17:32:00.0000000"',
            //        p_dateonly = '"2023-07-06"',
            //        p_timeonly = '"17:32:00"',
            //        p_timespan = '"01:00:00"',
            //        p_datetime_null = '"2023-07-06T17:32:00.0000000"',
            //        p_guid = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_guid_null = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_bool = 'true',
            //        p_bool_null = 'true',
            //        p_object_null = '{"Age":18,"Name":"jim","Other":"oth"}',
            //        p_list = '["tom","lisa"]',
            //        p_arr = '["lisa","jim"]',
            //        p_dic = '{"name":"tom","age":18}',
            //        p_jsonobj = '{"name":"tom","age":18}',
            //        p_jsonarr = '[{"name":"tom","age":18}]',
            //        p_long = cast(456 as json),
            //        p_string = '"modi"',
            //        p_uint = cast(4294967295 as json),
            //        p_ulong = cast(789 as json),
            //        p_object = json_set(ifnull(p_object,json_object()),
            //            '$."K1"','k1',
            //            '$."K2"',cast(ifnull(json_value(p_object,'$."K2"'),'{}') as json),
            //            '$."K2"."K21"',1,
            //            '$."K2"."K22"',cast(ifnull(json_value(p_object,'$."K2"."K22"'),'[]') as json),
            //            '$."K2"."K22"',cast('[1,2]' as json),
            //            '$."K3"','2023-07-15T17:35:00.0000000',
            //            '$."K4"',cast(ifnull(json_value(p_object,'$."K4"'),'[]') as json),
            //            '$."K4"',cast('[33,44]' as json),
            //            '$."K5"',cast(ifnull(json_value(p_object,'$."K5"'),'{}') as json),
            //            '$."K5"',cast('{"age":"18","name":"tom"}' as json))
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByDto_NoKey()
        {
            var update = db.Update<JsonTestPerson>().SetDto(new
            {
                p_byte = 1,
                p_sbyte = 2,
                p_short = 3,
                p_ushort = 4,
                p_int = 5,
                p_uint = 6,
                p_long = 7,
                p_ulong = 8,
                p_float = 9,
                p_double = 10,
                p_decimal = 11,
                p_string = "tom",
                p_datetime = DateTime.Parse("2023-07-06 17:32:00"),
                p_dateonly = DateOnly.Parse("2023-07-06"),//dateonly 在 bulkcopy 的时候不受支持,todo 改成 datetime?
                p_timeonly = TimeOnly.Parse("17:32:00"),//timeonly 在 bulkcopy 的时候不受支持,todo 改成 timespan
                p_timespan = TimeSpan.FromSeconds(3600),
                p_datetime_null = DateTime.Parse("2023-07-06 17:32:00"),
                p_guid = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_guid_null = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_bool = true,
                p_bool_null = true,
                p_object = new { Age = 18, Name = "jim" },
                p_object_null = new { Age = 18, Name = "jim", Other = "oth" },
                p_list = new List<string> { "tom", "lisa" },
                p_arr = new[] { "lisa", "jim" },
                p_dic = new Dictionary<string, object> { { "name", "tom" }, { "age", 18 } },
                p_jsonobj = new { name = "tom", age = 18 }.ToJson().ToObject<JsonObject>(),
                p_jsonarr = new[] { new { name = "tom", age = 18 } }.ToJson().ToObject<JsonArray>(),
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        p_byte = cast(1 as json),
            //        p_sbyte = cast(2 as json),
            //        p_short = cast(3 as json),
            //        p_ushort = cast(4 as json),
            //        p_int = cast(5 as json),
            //        p_uint = cast(6 as json),
            //        p_long = cast(7 as json),
            //        p_ulong = cast(8 as json),
            //        p_float = cast(9 as json),
            //        p_double = cast(10 as json),
            //        p_decimal = cast(11 as json),
            //        p_string = '"tom"',
            //        p_datetime = '"2023-07-06T17:32:00.0000000"',
            //        p_dateonly = '"2023-07-06"',
            //        p_timeonly = '"17:32:00"',
            //        p_timespan = '"01:00:00"',
            //        p_datetime_null = '"2023-07-06T17:32:00.0000000"',
            //        p_guid = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_guid_null = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_bool = 'true',
            //        p_bool_null = 'true',
            //        p_object = '{"Age":18,"Name":"jim"}',
            //        p_object_null = '{"Age":18,"Name":"jim","Other":"oth"}',
            //        p_list = '["tom","lisa"]',
            //        p_arr = '["lisa","jim"]',
            //        p_dic = '{"name":"tom","age":18}',
            //        p_jsonobj = '{"name":"tom","age":18}',
            //        p_jsonarr = '[{"name":"tom","age":18}]'
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByDto_NoKey_setremove()
        {
            var update = db.Update<JsonTestPerson>().SetDto(new
            {
                p_byte = 1,
                p_sbyte = 2,
                p_short = 3,
                p_ushort = 4,
                p_int = 5,
                p_uint = 6,
                p_long = 7,
                p_ulong = 8,
                p_float = 9,
                p_double = 10,
                p_decimal = 11,
                p_string = "tom",
                p_datetime = DateTime.Parse("2023-07-06 17:32:00"),
                p_dateonly = DateOnly.Parse("2023-07-06"),//dateonly 在 bulkcopy 的时候不受支持,todo 改成 datetime?
                p_timeonly = TimeOnly.Parse("17:32:00"),//timeonly 在 bulkcopy 的时候不受支持,todo 改成 timespan
                p_timespan = TimeSpan.FromSeconds(3600),
                p_datetime_null = DateTime.Parse("2023-07-06 17:32:00"),
                p_guid = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_guid_null = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_bool = true,
                p_bool_null = true,
                p_object = new { Age = 18, Name = "jim" },
                p_object_null = new { Age = 18, Name = "jim", Other = "oth" },
                p_list = new List<string> { "tom", "lisa" },
                p_arr = new[] { "lisa", "jim" },
                p_dic = new Dictionary<string, object> { { "name", "tom" }, { "age", 18 } },
                p_jsonobj = new { name = "tom", age = 18 }.ToJson().ToObject<JsonObject>(),
                p_jsonarr = new[] { new { name = "tom", age = 18 } }.ToJson().ToObject<JsonArray>(),
            }).SetColumnExpr(i => i.p_long, 456)
                .SetColumnExpr(i => i.p_string, "modi")
                .SetColumnExpr(i => i.p_uint, uint.MaxValue)
                .SetColumn("p_ulong", 789)
                .SetColumnExpr(i => i.p_object, () => new
                {
                    K1 = "k1",
                    K2 = new
                    {
                        K21 = 1,
                        K22 = new[] { 1, 2 },
                    },
                    K3 = DateTime.Parse("2023-07-15 17:35:00"),
                    K4 = new List<int> { 33, 44 },
                    K5 = new Dictionary<string, string> { { "age", "18" }, { "name", "tom" } }
                }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        p_byte = cast(1 as json),
            //        p_sbyte = cast(2 as json),
            //        p_short = cast(3 as json),
            //        p_ushort = cast(4 as json),
            //        p_int = cast(5 as json),
            //        p_float = cast(9 as json),
            //        p_double = cast(10 as json),
            //        p_decimal = cast(11 as json),
            //        p_datetime = '"2023-07-06T17:32:00.0000000"',
            //        p_dateonly = '"2023-07-06"',
            //        p_timeonly = '"17:32:00"',
            //        p_timespan = '"01:00:00"',
            //        p_datetime_null = '"2023-07-06T17:32:00.0000000"',
            //        p_guid = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_guid_null = '"96d80686-9753-40dd-a554-92ecda9c92f2"',
            //        p_bool = 'true',
            //        p_bool_null = 'true',
            //        p_object_null = '{"Age":18,"Name":"jim","Other":"oth"}',
            //        p_list = '["tom","lisa"]',
            //        p_arr = '["lisa","jim"]',
            //        p_dic = '{"name":"tom","age":18}',
            //        p_jsonobj = '{"name":"tom","age":18}',
            //        p_jsonarr = '[{"name":"tom","age":18}]',
            //        p_long = cast(456 as json),
            //        p_string = '"modi"',
            //        p_uint = cast(4294967295 as json),
            //        p_ulong = cast(789 as json),
            //        p_object = json_set(ifnull(p_object,json_object()),
            //            '$."K1"','k1',
            //            '$."K2"',cast(ifnull(json_value(p_object,'$."K2"'),'{}') as json),
            //            '$."K2"."K21"',1,
            //            '$."K2"."K22"',cast(ifnull(json_value(p_object,'$."K2"."K22"'),'[]') as json),
            //            '$."K2"."K22"',cast('[1,2]' as json),
            //            '$."K3"','2023-07-15T17:35:00.0000000',
            //            '$."K4"',cast(ifnull(json_value(p_object,'$."K4"'),'[]') as json),
            //            '$."K4"',cast('[33,44]' as json),
            //            '$."K5"',cast(ifnull(json_value(p_object,'$."K5"'),'{}') as json),
            //            '$."K5"',cast('{"age":"18","name":"tom"}' as json))
            //    where `Id` = 1;
            //    """);
        }
        #endregion

        #region usekey
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "age")]
            public int Age { get; set; }
            [JsonStore(Bucket = "properties", Key = "score")]
            public float Score { get; set; }

            [JsonStore(Bucket = "properties2", Key = "types")]
            public int[] Types { get; set; }
            [JsonStore(Bucket = "properties2", Key = "dic")]
            public Dictionary<string, string> Dic { get; set; }
        }

        [Test]
        public void UpdateByExpression_UseKey_pure()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var update = db.Update<Person>().SetExpr(() => new Person
            {
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."age"',18,
            //            '$."score"',99),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."types"',cast('[1,2]' as json),
            //            '$."dic"',cast(ifnull(json_value(properties2,'$."dic"'),'{}') as json),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json))
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByExpression_UseKey_setremove()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var update = db.Update<Person>().SetExpr(() => new Person
            {
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            }).SetColumnExpr(i => i.Age, 60).SetColumnExpr(i => i.Types, new[] { 8, 9 }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."score"',99,
            //            '$."age"',60),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."dic"',cast(ifnull(json_value(properties2,'$."dic"'),'{}') as json),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json),
            //            '$."types"',cast('[8,9]' as json))
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByEntity_UseKey_pure()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var update = db.Update<Person>().SetEntity(new Person
            {
                Id = 1,
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            });
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."age"',18,
            //            '$."score"',99),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."types"',cast('[1,2]' as json),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json))
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByEntity_UseKey_pure_MultiValues()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var ent = new Person
            {
                Id = 1,
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            };
            var ent2 = new Person
            {
                Id = 2,
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            };
            var update = db.Update<Person>().SetEntity(new[] { ent, ent2 });
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = case `Id`
            //          when 1 then json_set(ifnull(properties,json_object()),
            //            '$."age"',18,
            //            '$."score"',99)
            //          when 2 then json_set(ifnull(properties,json_object()),
            //            '$."age"',18,
            //            '$."score"',99) end,
            //        properties2 = case `Id`
            //          when 1 then json_set(ifnull(properties2,json_object()),
            //            '$."types"',cast('[1,2]' as json),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json))
            //          when 2 then json_set(ifnull(properties2,json_object()),
            //            '$."types"',cast('[1,2]' as json),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json)) end
            //    where `Id` in (1,2);
            //    """);
        }

        [Test]
        public void UpdateByEntity_UseKey_setremove()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var update = db.Update<Person>().SetEntity(new Person
            {
                Id = 1,
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            }).SetColumnExpr(i => i.Age, 60).SetColumnExpr(i => i.Types, new[] { 8, 9 });

            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."score"',99,
            //            '$."age"',60),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json),
            //            '$."types"',cast('[8,9]' as json))
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void UpdateByDto_UseKey_pure()
        {
            var update = db.Update<Person>().SetDto(new
            {
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 },
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."age"',18,
            //            '$."score"',99),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."types"',cast('[1,2]' as json),
            //            '$."dic"',cast(ifnull(json_value(properties2,'$."dic"'),'{}') as json),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json))
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void InsertByDto_UseKey_setremove()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var update = db.Update<Person>().SetDto(new
            {
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            }).SetColumnExpr(i => i.Age, 60).SetColumnExpr(i => i.Types, new[] { 8, 9 }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."score"',99,
            //            '$."age"',60),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."dic"',cast(ifnull(json_value(properties2,'$."dic"'),'{}') as json),
            //            '$."dic"',cast('{"age":"20","name":"tom"}' as json),
            //            '$."types"',cast('[8,9]' as json))
            //    where `Id` = 1;
            //    """);

        }
        #endregion

        #region 枚举
        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            public string Name { get; set; }

            [JsonStore(Bucket = "properties")]
            public EnumTest State { get; set; }
        }

        [Table("test")]
        class Person4
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "e1")]
            public EnumTest State { get; set; }
            [JsonStore(Bucket = "properties", Key = "e2")]
            public EnumFlagTest StateFlag { get; set; }
        }

        enum EnumTest
        {
            A = 1, B = 2, C = 3,
        }
        enum EnumFlagTest
        {
            A = 1, B = 2, C = 4,
        }

        [Test]
        public void Test_Enum_NoKey()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),state json)");

            var sql = db.Update<Person2>().SetExpr(() => new Person2
            {
                State = EnumTest.C,
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = cast(3 as json)
            //    where `Id` = 1;
            //    """);
        }

        [Test]
        public void Test_FlagEnum_WithKey()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json)");

            var sql = db.Update<Person4>().SetExpr(() => new Person4
            {
                State = EnumTest.C,
                StateFlag = EnumFlagTest.A | EnumFlagTest.B | EnumFlagTest.C,
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."e1"',3,
            //            '$."e2"',7)
            //    where `Id` = 1;
            //    """);
        }
        #endregion
    }
}
