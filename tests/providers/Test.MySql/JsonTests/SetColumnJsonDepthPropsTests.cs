﻿using DBUtil;
using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class SetColumnJsonDepthPropsTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public Ext Ext { get; set; }

            [JsonStore(Bucket = "ext2", Key = "CreateTime")]
            public DateTime CreateTime { get; set; }

            [JsonStore(Bucket = "ext2", Key = "UpdateTime")]
            public DateTime UpdateTime { get; set; }
        }

        public class Ext
        {
            public Detail Detail { get; set; }
        }

        public class Detail
        {
            public int? Age { get; set; }
            public DateTime UpdateTime { get; set; }
        }

        [Table("test2")]
        public class PersonJsonObject
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "ext2")]
            public JsonNode Ext2 { get; set; }
        }

        [Table("test3")]
        public class PersonDictionary
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public Dictionary<string, string> Ext { get; set; }

            [JsonStore(Bucket = "ext2")]
            public Dictionary<string, Dictionary<string, object>> Ext2 { get; set; }
        }
        [Table("test4")]
        public class PersonJsonArray
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public JsonArray Ext { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,ext json,ext2 json)");

            DropTableIfExist("test2");
            db.ExecuteSql("create table test2(id int primary key auto_increment,ext json,ext2 json)");

            DropTableIfExist("test3");
            db.ExecuteSql("create table test3(id int primary key auto_increment,ext json,ext2 json)");

            DropTableIfExist("test4");
            db.ExecuteSql("create table test4(id int primary key auto_increment,ext json)");
        }

        [Test]
        public void Test()
        {
            //var insert = db.Insert<Person>().SetColumn(i => i.Ext.Detail.Age, 10);
            //var insert = db.Insert<Person>().SetColumn(i => i.Ext.Detail.Age.Value, 10);
            //var insert = db.Insert<Person>().SetColumn(i => new { i.Ext.Detail.UpdateTime, i.CreateTime }, DateTime.Now);
            //var insert = db.Insert<Person>().SetColumn(i => new Person { CreateTime = i.Ext.Detail.UpdateTime, Id = i.Ext.Detail.Age.Value }, DateTime.Now);

            //var insert = db.Insert<Person>().SetColumn(i => new Person { Id = i.Id }, 20);
            //var insert = db.Insert<Person>().SetColumn(i => i.Id, 20);

            var insert = db.Insert<Person>()
                .SetEntity(new Person { CreateTime = DateTime.Now, Ext = new Ext { Detail = new Detail { Age = 10, UpdateTime = DateTime.Now } } })
                .SetColumnExpr(i => i.Ext.Detail.Age, 20);
            var sql = insert.ToSql();

        }

        [Test]
        public void Test2()
        {
            var sql = db.Insert<Person>()
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .SetColumnExpr(i => i.CreateTime, DateTime.Now)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, DateTime.Now)
                .ToSql();
            //
            //insert into test(ext,ext2) values('{"Detail":{"Age":20,"UpdateTime":"2024-08-19T17:21:02.9141017+08:00"}}','{"CreateTime":"2024-08-19T17:21:02.9140831+08:00"}');
            var list = db.Select<Person>().ToList();
        }

        [Test]
        public void Test3()
        {
            var sql = db.Insert<Person>()
                .SetEntity(new Person
                {
                    Ext = new Ext { Detail = new Detail { Age = 10, UpdateTime = DateTime.Now } },
                    CreateTime = DateTime.Now,
                })
                .SetColumnExpr(i => i.Ext.Detail.Age, 100)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, DateTime.Parse("2000-01-01"))
                .SetColumnExpr(i => i.CreateTime, DateTime.Parse("2000-01-01"))
                .ToSql();
            //
            //insert into test(ext,ext2) values('{"Detail":{"Age":100,"UpdateTime":"2000-01-01T00:00:00.0000000"}}','{"CreateTime":"2000-01-01T00:00:00.0000000"}');
            var list = db.Select<Person>().ToList();
        }

        [Test]
        public void Test4()
        {
            var sql = db.Insert<Person>()
                .SetEntity([new Person
                    {
                        Ext = new Ext { Detail = new Detail { Age = 10, UpdateTime = DateTime.Now } },
                        CreateTime = DateTime.Now,
                    },
                    new Person
                    {
                        Ext = new Ext { Detail = new Detail { Age = 10, UpdateTime = DateTime.Now } },
                        CreateTime = DateTime.Now,
                    },
                ])
                .SetColumnExpr(i => i.Ext.Detail.Age, 100)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, DateTime.Parse("2000-01-01"))
                .SetColumnExpr(i => i.CreateTime, DateTime.Parse("2000-01-01"))
                .ToSql();
            /*
            insert into test(ext,ext2) values
                ('{"Detail":{"Age":100,"UpdateTime":"2000-01-01T00:00:00.0000000"}}','{"CreateTime":"2000-01-01T00:00:00.0000000"}'),
                ('{"Detail":{"Age":100,"UpdateTime":"2000-01-01T00:00:00.0000000"}}','{"CreateTime":"2000-01-01T00:00:00.0000000"}');
            */
            var list = db.Select<Person>().ToList();
        }

        [Test]
        public void Test5()
        {
            var sql = db.Insert<Person>()
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .SetColumnExpr(i => i.CreateTime, DateTime.Now)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, new RawString("now()"))
                .ToSql();
            //
            //insert into test(ext,ext2) values('{"Detail":{"Age":20,"UpdateTime":"2024-08-19T17:21:02.9141017+08:00"}}','{"CreateTime":"2024-08-19T17:21:02.9140831+08:00"}');
            var list = db.Select<Person>().ToList();
        }
        [Test]
        public void Test6()
        {
            var dt = db.Insert<Person>()
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .SetColumnExpr(i => i.CreateTime, DateTime.Now)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, new RawString("select now()"))
                .ToDataTable();
            Console.WriteLine(dt);
        }
        [Test]
        public void Test7()
        {
            var dt = db.Insert<Person>()
                .SetEntity([
                    new Person
                    {
                        Ext = new Ext{Detail= new Detail{Age=10} },
                        CreateTime=DateTime.Now
                    },
                    new Person
                    {
                        Ext = new Ext{Detail= new Detail{Age=50} },
                        CreateTime=DateTime.Now
                    }])
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .SetColumnExpr(i => i.CreateTime, DateTime.Now)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, new RawString("select now()"))
                .ToDataTable();
            Console.WriteLine(dt);
        }
        [Test]
        public void Test8()
        {
            var sql = db.Insert<Person>()
                .SetEntity([
                    new Person
                    {
                        Ext = new Ext{Detail= new Detail{Age=10} },
                        CreateTime=DateTime.Now
                    },
                    new Person
                    {
                        Ext = new Ext{Detail= new Detail{Age=50} },
                        CreateTime=DateTime.Now
                    }])
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .SetColumnExpr(i => i.CreateTime, DateTime.Now)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, new RawString("select now()"))
                .ToSql();
            Console.WriteLine(sql);
        }
        [Test]
        public void Test9()
        {
            var sql = db.Insert<Person>()
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .SetColumnExpr(i => i.CreateTime, DateTime.Parse("2000-01-01"))
                .SetColumnExpr(i => i.UpdateTime, DateTime.Parse("2000-01-01"))
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, DateTime.Parse("2000-01-01"))
                .ToSql();
            Console.WriteLine(sql);
        }

        [Test]
        public void Test10()
        {
            var sql = db.Insert<PersonJsonObject>()
                .SetColumnExpr(i => i.Ext["Detail"]["Age"], 20)
                .SetColumnExpr(i => i.Ext["Detail"]["Birth"], DateTime.Parse("2000-01-01"))
                .SetColumnExpr(i => i.Ext2["Detail"]["Age2"], 20)
                .ToSql();
            Console.WriteLine(sql);
        }

        [Test]
        public void Test11()
        {
            var sql = db.Insert<PersonDictionary>()
                .SetColumnExpr(i => i.Ext["Name"], "jack")
                .SetColumnExpr(i => i.Ext["Age"], "20")
                .SetColumnExpr(i => i.Ext2["Detail"]["Age2"], 20)
                .ToSql();
            Console.WriteLine(sql);
        }

        [Test]
        public void Test12()
        {
            var sql = db.Update<Person>()
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .SetColumnExpr(i => i.Ext.Detail.UpdateTime, DateTime.Now)
                .Where(i => i.Id == 1)
                .ToSql();
            //update test set
            //    ext = json_set(ifnull(ext, json_object()),
            //        '$."Detail"', ifnull(json_value(ext, '$."Detail"'), json_object()),
            //        '$."Detail"."Age"', 20,
            //        '$."Detail"."UpdateTime"', '2024-09-20T10:22:11.4877004+08:00')
            //where `Id` = 1;
            Console.WriteLine(sql);
        }

        [Test]
        public void Test13()
        {
            var sql = db.Update<PersonJsonObject>()
                .SetColumnExpr(i => i.Ext["Detail"]["Age"], 20)
                .SetColumnExpr(i => i.Ext["Detail"]["UpdateTime"], DateTime.Now)
                .Where(i => i.Id == 1)
                .ToSql();
            //update test2 set
            //    ext = json_set(ifnull(ext, json_object()),
            //        '$."Detail"', ifnull(json_value(ext, '$."Detail"'), json_object()),
            //        '$."Detail"."Age"', 20,
            //        '$."Detail"."UpdateTime"', '2024-09-20T10:22:11.4877004+08:00')
            //where `Id` = 1;
            Console.WriteLine(sql);
        }

        [Test]
        public void Test14()
        {
            var sql = db.Update<PersonJsonArray>()
                .SetExpr(i => new PersonJsonArray
                {
                    Ext = new[] { new { Name = "tom" } }.ToJsonArray()
                })
                .SetColumnExpr(i => i.Ext, new[] { new { Name = "jack" } }.ToJsonArray())
                .SetColumnExpr(i => i.Ext, i => i.Ext.IfNullUseNew().AddFluent(new[] { new { Name = "lisa" } }.ToJsonArray()))
                .Where(i => i.Id == 1)
                .ToSql();
            //update test4 set
            //    ext = json_array_append(ifnull(json_value(ext, '$'), json_array()), '$', json_array(json_object('Name', 'lisa')))
            //where `Id` = 1;
            Console.WriteLine(sql);
        }

        [Test]
        public void Test15()
        {
            var sql = db.Update<Person>()
                .SetEntity(new Person
                {
                    Ext = new Ext
                    {
                        Detail = new Detail
                        {
                            Age = 18,
                            UpdateTime = DateTime.Now
                        }
                    }
                })
                .SetColumnExpr(i => i.Ext.Detail.Age, 20)
                .Where(i => i.Id == 1)
                .ToSql();
            //update test4 set
            //    ext = json_array_append(ifnull(json_value(ext, '$'), json_array()), '$', json_array(json_object('Name', 'lisa')))
            //where `Id` = 1;
            Console.WriteLine(sql);
        }
    }
}
