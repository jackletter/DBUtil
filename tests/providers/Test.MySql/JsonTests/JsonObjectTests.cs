﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonObjectTests : TestBase
    {
        #region Along 独占json
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "properties2")]
            public JsonObject Ext2 { get; set; }

            [JsonStore(Bucket = "properties3")]
            public JsonObject Ext3 { get; set; }
        }

        [Test]
        public void TestJsonObject_Along_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person>()
                 .SetExpr(() => new Person
                 {
                     Ext = new JsonObject(new[] { new KeyValuePair<string, JsonNode>("name", "tom") }, null),
                 })
                 .SetColumnExpr(i => i.Ext2, i => new JsonObject(new[] { new KeyValuePair<string, JsonNode>("name", "tom") }, null))
                 .SetColumnExpr(i => i.Ext3, i => new { name = "tom", age = 18 }.ToJson().ToObject<JsonObject>())
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = '{"name":"tom"}',
            //        properties2 = '{"name":"tom"}',
            //        properties3 = cast(convert(json_object('name','tom','age',18),char) as json)
            //    where id = 1;
            //    """);
        }

        [Test]
        public void TestJsonObject_Along_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim" }.ToJson().ToObject<JsonArray>()))
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc'),
            //        properties2 = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc'),
            //        properties3 = json_set(ifnull(json_value(properties3,'$'),'{}'),'$."name"',cast('["tom","jim"]' as json))
            //    where id = 1;
            //    """);
        }

        [Test]
        public void TestJsonObject_Along_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .SetColumnExpr(i => i.Ext2, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .ExecuteIdentity();

            var sql = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"].ToString() }.ToJson().ToObject<JsonObject>()))
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$'),'$."name"'),'2')),'{}'),'$."addr"','abc'),
            //        properties2 = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$'),'$."name"'),'2')),'{}'),'$."addr"','abc'),
            //        properties3 = json_set(ifnull(json_value(properties3,'$'),'{}'),'$."name"',cast(convert(json_array('tom','jim',convert(json_value(json_value(properties,'$'),'$."name"'),char)),char) as json))
            //    where id = 1;
            //    """);
        }

        [Test]
        public void TestJsonObject_Along_PartialUpdate3()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .SetColumnExpr(i => i.Ext2, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .ExecuteIdentity();

            var sql = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.ClearFluent().SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.RemoveFluent("name").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"].ToString() }.ToJson().ToObject<JsonObject>()))
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(json_object(),'$."addr"','abc'),
            //        properties2 = json_set(ifnull(json_remove(ifnull(json_value(properties,'$'),'{}'),'$."name"'),'{}'),'$."addr"','abc'),
            //        properties3 = json_set(ifnull(json_value(properties3,'$'),'{}'),'$."name"',cast(convert(json_array('tom','jim',convert(json_value(json_value(properties,'$'),'$."name"'),char)),char) as json))
            //    where id = 1;
            //    """);
        }
        #endregion

        #region NotAlong 非独占json
        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "properties2", Key = "ext")]
            public JsonObject Ext2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public JsonObject Ext3 { get; set; }
        }

        [Test]
        public void TestJsonObject_NotAlong_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person2>()
                 .SetExpr(() => new Person2
                 {
                     Ext = new JsonObject(new[] { new KeyValuePair<string, JsonNode>("name", "tom") }, null),
                 })
                 .SetColumnExpr(i => i.Ext2, i => new JsonObject(new[] { new KeyValuePair<string, JsonNode>("name", "tom") }, null))
                 .SetColumnExpr(i => i.Ext3, i => new { name = "tom", age = 18 }.ToJson().ToObject<JsonObject>())
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"name":"tom"}' as json)),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"name":"tom"}' as json)),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',cast(convert(json_object('name','tom','age',18),char) as json))
            //    where id = 1;
            //    """);
        }

        [Test]
        public void TestJsonObject_NotAlong_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim" }.ToJson().ToObject<JsonArray>()))
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_value(properties3,'$."ext"'),'{}'),'$."name"',cast('["tom","jim"]' as json)))
            //    where id = 1;
            //    """);
        }

        [Test]
        public void TestJsonObject_NotAlong_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .SetColumnExpr(i => i.Ext2, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .ExecuteIdentity();

            var sql = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"].ToString() }.ToJson().ToObject<JsonObject>()))
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$."ext"'),'$."name"'),'2')),'{}'),'$."addr"','abc')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$."ext"'),'$."name"'),'2')),'{}'),'$."addr"','abc')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_value(properties3,'$."ext"'),'{}'),'$."name"',cast(convert(json_array('tom','jim',convert(json_value(json_value(properties,'$."ext"'),'$."name"'),char)),char) as json)))
            //    where id = 1;
            //    """);
        }

        [Test]
        public void TestJsonObject_NotAlong_PartialUpdate3()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .SetColumnExpr(i => i.Ext2, new { name = "tom" }.ToJson().ToObject<JsonObject>())
                .ExecuteIdentity();

            var sql = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.ClearFluent().SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.RemoveFluent("name").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"].ToString() }.ToJson().ToObject<JsonObject>()))
                 .Where(i => i.Id == 1)
                 .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(json_object(),'$."addr"','abc')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_remove(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"'),'{}'),'$."addr"','abc')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_value(properties3,'$."ext"'),'{}'),'$."name"',cast(convert(json_array('tom','jim',convert(json_value(json_value(properties,'$."ext"'),'$."name"'),char)),char) as json)))
            //    where id = 1;
            //    """);
        }
        #endregion
    }
}
