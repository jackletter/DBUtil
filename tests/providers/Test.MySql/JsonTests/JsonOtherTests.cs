﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonOtherTests : TestBase
    {
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "properties2", Key = "ext")]
            public JsonObject Ext2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public JsonArray Ext3 { get; set; }
        }

        [Test]
        public void TestToJson()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person>()
                .SetExpr(i => new Person
                {
                    Ext = new { name = "tom", age = 18 }.ToJson().ToObject<JsonObject>(),
                    Ext2 = new { name = "tom", age = 18 }.ToJsonObject(),
                    Ext3 = new[] { new { name = "tom", age = 18 } }.ToJsonArray(),
                })
                .Where(i => i.Id == 1)
                .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',cast(convert(json_object('name','tom','age',18),char) as json)),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_object('name','tom','age',18)),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',json_array(json_object('name','tom','age',18)))
            //    where id = 1;
            //    """);
        }


        [Table("test")]
        class Person2
        {
            [Column("id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public List<int> Scores { get; set; }

            [JsonStore(Bucket = "properties2")]
            public List<int> Scores2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public JsonArray Arr { get; set; }

            [JsonStore(Bucket = "properties4")]
            public JsonArray Arr2 { get; set; }
        }
        [Test]
        public void TestListSet()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json,properties4 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person2>().SetExpr(i => new Person2
            {
                Id = 1,
                Scores = i.Scores.AddFluent(2).AddFluent(3).SetFluent(0, 6),
                Scores2 = i.Scores.AddFluent(2).AddFluent(3).SetFluent(0, 6),
                Arr = i.Arr.AddFluent(2).AddFluent(3).SetFluent(0, 6),
                Arr2 = i.Arr.AddFluent(2).AddFluent(3).SetFluent(0, 6),
            }).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',json_set(ifnull(json_array_append(json_array_append(ifnull(json_value(properties,'$."ext"'),'[]'),'$',2),'$',3),'[]'),'$[0]',6)),
            //        properties2 = json_set(ifnull(json_array_append(json_array_append(ifnull(json_value(properties,'$."ext"'),'[]'),'$',2),'$',3),'[]'),'$[0]',6),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',json_set(ifnull(json_array_append(json_array_append(ifnull(json_value(properties3,'$."ext"'),'[]'),'$',2),'$',3),'[]'),'$[0]',6)),
            //        properties4 = json_set(ifnull(json_array_append(json_array_append(ifnull(json_value(properties3,'$."ext"'),'[]'),'$',2),'$',3),'[]'),'$[0]',6)
            //    where id = 1;
            //    """);
        }
    }
}
