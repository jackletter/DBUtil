﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class EvalTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "ext")]
            public Ext Ext { get; set; }
        }
        public class Ext
        {
            public int Age { get; set; }
            public Detail Detail { get; set; }
        }
        public class Detail
        {
            public int Id { get; set; }
            public string Name { get; set; }

        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int auto_increment primary key,ext json)""");
            db.Insert<Person>().SetEntity(new Person
            {
                Ext = new Ext
                {
                    Age = 18,
                    Detail = new Detail
                    {
                        Id = 1,
                        Name = "jack"
                    }
                }
            }).ExecuteAffrows();
        }

        [Test]
        public void Test()
        {
            var p = db.Select<Person>().FirstOrDefault();
            p.ToJson().ShouldBe("""{"Id":1,"Ext":{"Age":18,"Detail":{"Id":1,"Name":"jack"}}}""");
            //使用了 Eval(), 所以 Name 会被覆盖为null,而Age仍然是18
            db.Update<Person>().SetColumnExpr(i => i.Ext, i => new Ext
            {
                //Age
                Detail = new Detail
                {
                    Id = 2,
                    //Name
                }.Eval()
            }).Where(i => i.Id == 1).ExecuteAffrows();
            var p2 = db.Select<Person>().FirstOrDefault();
            p2.ToJson().ShouldBe("""{"Id":1,"Ext":{"Age":18,"Detail":{"Id":2,"Name":null}}}""");
        }
    }
}
