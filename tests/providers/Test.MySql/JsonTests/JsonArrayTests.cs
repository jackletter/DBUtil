﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    class JsonArrayTests : TestBase
    {
        #region NoKey
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties")]
            public JsonArray Ext { get; set; }

            [JsonStore(Bucket = "properties2")]
            public JsonArray Ext2 { get; set; }

            [JsonStore(Bucket = "properties3")]
            public JsonArray Ext3 { get; set; }
        }

        [Test]
        public void TestJsonObject_Along_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person>()
                 .SetExpr(() => new Person
                 {
                     Ext = new JsonArray(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>(), new { name = "jim", age = 20 }.ToJson().ToObject<JsonNode>()),
                 })
                 .SetColumnExpr(i => i.Ext2, i => new JsonArray(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>(), new { name = "jim", age = 20 }.ToJsonArray()))
                 .SetColumnExpr(i => i.Ext3, i => new[] { "tom", "tim" }.ToJsonArray())
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_array(cast(convert(json_object('name','tom','age',18),char) as json),cast(convert(json_object('name','jim','age',20),char) as json)),
            //        properties2 = json_array(cast(convert(json_object('name','tom','age',18),char) as json),cast(convert(json_object('name','jim','age',20),char) as json)),
            //        properties3 = '["tom","tim"]'
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);

            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""[{"age":18,"name":"tom"},{"age":20,"name":"jim"}]""");
            p.Ext2.ToJson().ShouldBe("""[{"age":18,"name":"tom"},{"age":20,"name":"jim"}]""");
            p.Ext3.ToJson().ShouldBe("""["tom","tim"]""");
        }

        [Test]
        public void TestJsonObject_Along_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.AddFluent(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>()),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.AddFluent(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>()))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.AddRangeFluent(new[] { new { name = "tom", age = 18 }, new { name = "jim", age = 20 } }.ToJson().ToObject<JsonNode[]>()))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_array_append(ifnull(json_value(properties,'$'),'[]'),'$',cast(convert(json_object('name','tom','age',18),char) as json)),
            //        properties2 = json_array_append(ifnull(json_value(properties,'$'),'[]'),'$',cast(convert(json_object('name','tom','age',18),char) as json)),
            //        properties3 = json_merge_preserve(ifnull(json_value(properties3,'$'),'[]'),cast(convert(json_array(json_object('name','tom','age',18),json_object('name','jim','age',20)),char) as json))
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);

            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""[{"age":18,"name":"tom"}]""");
            p.Ext2.ToJson().ShouldBe("""[{"age":18,"name":"tom"},{"age":18,"name":"tom"}]""");
            p.Ext3.ToJson().ShouldBe("""[{"age":18,"name":"tom"},{"age":20,"name":"jim"}]""");
        }

        [Test]
        public void TestJsonObject_Along_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .SetColumnExpr(i => i.Ext2, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .ExecuteIdentity();
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"name":"tom"}],"Ext2":[{"name":"tom"}],"Ext3":null}""");

            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.RemoveAtFluent(0).AddFluent(new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>()),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.RemoveAtFluent(0).AddFluent(new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>()))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext.AddFluent(new { name = i.Ext[0]["name"].ToString(), age = i.Ext[0]["age"].To<int>() + 2 }.ToJson().ToObject<JsonNode>()))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_array_append(json_remove(ifnull(json_value(properties,'$'),'[]'),'$[0]'),'$',cast(convert(json_object('name','jim','age',18),char) as json)),
            //        properties2 = json_array_append(json_remove(ifnull(json_value(properties,'$'),'[]'),'$[0]'),'$',cast(convert(json_object('name','jim','age',18),char) as json)),
            //        properties3 = json_array_append(ifnull(json_value(properties,'$'),'[]'),'$',cast(convert(json_object('name',convert(json_value(json_value(json_value(properties,'$'),'$[0]'),'$."name"'),char),'age',convert(json_value(json_value(json_value(properties,'$'),'$[0]'),'$."age"'),decimal) + 2),char) as json))
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"age":18,"name":"jim"}],"Ext2":[{"age":18,"name":"jim"}],"Ext3":[{"age":18,"name":"jim"},{"age":20,"name":"jim"}]}""");
        }

        [Test]
        public void TestJsonObject_Along_PartialUpdate3()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .SetColumnExpr(i => i.Ext2, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .SetColumnExpr(i => i.Ext3, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .ExecuteIdentity();
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"name":"tom"}],"Ext2":[{"name":"tom"}],"Ext3":[{"name":"tom"}]}""");

            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.InsertAtFluent(0, new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>())
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.InsertAtFluent(0, new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>()))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3
                    .ClearFluent()
                    .AddFluent(new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>())
                    .AddFluent(new { name = "tom", age = 20 }.ToJson().ToObject<JsonNode>())
                    .RemoveAtFluent(0))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_array_insert(ifnull(json_value(properties,'$'),'[]'),'$[0]',cast(convert(json_object('name','jim','age',18),char) as json)),
            //        properties2 = json_array_insert(ifnull(json_value(properties,'$'),'[]'),'$[0]',cast(convert(json_object('name','jim','age',18),char) as json)),
            //        properties3 = json_remove(json_array_append(json_array_append(json_array(),'$',cast(convert(json_object('name','jim','age',18),char) as json)),'$',cast(convert(json_object('name','tom','age',20),char) as json)),'$[0]')
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"age":18,"name":"jim"},{"name":"tom"}],"Ext2":[{"age":18,"name":"jim"},{"age":18,"name":"jim"},{"name":"tom"}],"Ext3":[{"age":20,"name":"tom"}]}""");
        }
        #endregion

        #region Key
        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonArray Ext { get; set; }

            [JsonStore(Bucket = "properties2", Key = "ext")]
            public JsonArray Ext2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public JsonArray Ext3 { get; set; }
        }

        [Test]
        public void TestJsonObject_NotAlong_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(() => new Person2
                 {
                     Ext = new JsonArray(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>(), new { name = "jim", age = 20 }.ToJson().ToObject<JsonNode>()),
                 })
                 .SetColumnExpr(i => i.Ext2, i => new JsonArray(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>(), new { name = "jim", age = 20 }.ToJsonArray()))
                 .SetColumnExpr(i => i.Ext3, i => new[] { "tom", "tim" }.ToJsonArray())
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',json_array(cast(convert(json_object('name','tom','age',18),char) as json),cast(convert(json_object('name','jim','age',20),char) as json))),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',json_array(cast(convert(json_object('name','tom','age',18),char) as json),cast(convert(json_object('name','jim','age',20),char) as json))),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast('["tom","tim"]' as json))
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"age":18,"name":"tom"},{"age":20,"name":"jim"}],"Ext2":[{"age":18,"name":"tom"},{"age":20,"name":"jim"}],"Ext3":["tom","tim"]}""");
        }

        [Test]
        public void TestJsonObject_NotAlong_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.AddFluent(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>()),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.AddFluent(new { name = "tom", age = 18 }.ToJson().ToObject<JsonNode>()))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.AddRangeFluent(new[] { new { name = "tom", age = 18 }, new { name = "jim", age = 20 } }.ToJson().ToObject<JsonNode[]>()))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',json_array_append(ifnull(json_value(properties,'$."ext"'),'[]'),'$',cast(convert(json_object('name','tom','age',18),char) as json))),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',json_array_append(ifnull(json_value(properties,'$."ext"'),'[]'),'$',cast(convert(json_object('name','tom','age',18),char) as json))),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',json_merge_preserve(ifnull(json_value(properties3,'$."ext"'),'[]'),cast(convert(json_array(json_object('name','tom','age',18),json_object('name','jim','age',20)),char) as json)))
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"age":18,"name":"tom"}],"Ext2":[{"age":18,"name":"tom"},{"age":18,"name":"tom"}],"Ext3":[{"age":18,"name":"tom"},{"age":20,"name":"jim"}]}""");
        }

        [Test]
        public void TestJsonObject_NotAlong_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .SetColumnExpr(i => i.Ext2, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.RemoveAtFluent(0).AddFluent(new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>()),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.RemoveAtFluent(0).AddFluent(new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>()))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext.AddFluent(new { name = i.Ext[0]["name"].ToString(), age = i.Ext[0]["age"].To<int>() + 2 }.ToJson().ToObject<JsonNode>()))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',json_array_append(json_remove(ifnull(json_value(properties,'$."ext"'),'[]'),'$[0]'),'$',cast(convert(json_object('name','jim','age',18),char) as json))),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',json_array_append(json_remove(ifnull(json_value(properties,'$."ext"'),'[]'),'$[0]'),'$',cast(convert(json_object('name','jim','age',18),char) as json))),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',json_array_append(ifnull(json_value(properties,'$."ext"'),'[]'),'$',cast(convert(json_object('name',convert(json_value(json_value(json_value(properties,'$."ext"'),'$[0]'),'$."name"'),char),'age',convert(json_value(json_value(json_value(properties,'$."ext"'),'$[0]'),'$."age"'),decimal) + 2),char) as json)))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);

            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"age":18,"name":"jim"}],"Ext2":[{"age":18,"name":"jim"}],"Ext3":[{"age":18,"name":"jim"},{"age":20,"name":"jim"}]}""");
        }

        [Test]
        public void TestJsonObject_NotAlong_PartialUpdate3()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .SetColumnExpr(i => i.Ext2, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .SetColumnExpr(i => i.Ext3, new[] { new { name = "tom" } }.ToJson().ToObject<JsonArray>())
                .ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.InsertAtFluent(0, new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>())
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.InsertAtFluent(0, new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>()))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3
                    .ClearFluent()
                    .AddFluent(new { name = "jim", age = 18 }.ToJson().ToObject<JsonNode>())
                    .AddFluent(new { name = "tom", age = 20 }.ToJson().ToObject<JsonNode>())
                    .RemoveAtFluent(0))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',json_array_insert(ifnull(json_value(properties,'$."ext"'),'[]'),'$[0]',cast(convert(json_object('name','jim','age',18),char) as json))),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',json_array_insert(ifnull(json_value(properties,'$."ext"'),'[]'),'$[0]',cast(convert(json_object('name','jim','age',18),char) as json))),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',json_remove(json_array_append(json_array_append(json_array(),'$',cast(convert(json_object('name','jim','age',18),char) as json)),'$',cast(convert(json_object('name','tom','age',20),char) as json)),'$[0]'))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);

            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Name":"lisa","Ext":[{"age":18,"name":"jim"},{"name":"tom"}],"Ext2":[{"age":18,"name":"jim"},{"age":18,"name":"jim"},{"name":"tom"}],"Ext3":[{"age":20,"name":"tom"}]}""");
        }
        #endregion

        #region SetFluent
        [Table("test")]
        public class Person3
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public JsonArray Arr { get; set; }
        }

        [Test]
        public void TestSetFluent()
        {
            //先准备数据
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,ext json)");
            db.Insert<Person3>().SetEntity([new Person3() { Arr = new[] { new { Name = "tom", Age = 20 }, new { Name = "lisa", Age = 18 } }.ToJsonArray() }]).ExecuteAffrows();

            var update = db.Update<Person3>().SetColumnExpr(i => i.Arr, i => i.Arr.SetFluent(0, new { Name = "koko", Age = 23 }.ToJsonObject())).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),'$[0]',json_object('Name','koko','Age',23))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Arr":[{"Age":23,"Name":"koko"},{"Age":18,"Name":"lisa"}]}""");
        }

        [Test]
        public void TestSetFluent2()
        {
            //先准备数据
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,ext json)");
            db.Insert<Person3>().SetEntity([new Person3() { Arr = new[] { new { Name = "tom", Age = 20 }, new { Name = "lisa", Age = 18 } }.ToJsonArray() }]).ExecuteAffrows();

            var update = db.Update<Person3>().SetColumnExpr(i => i.Arr, i => i.Arr.SetFluent(i.Arr.FindIndex(i => i["Name"].GetValue<string>() == "tom"), i => i.ModifyByDto(new { Name = "tom2" }))).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'tom' limit 1),']'),json_merge_patch(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'tom' limit 1),']'))),json_object('Name','tom2')))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Arr":[{"Age":20,"Name":"tom2"},{"Age":18,"Name":"lisa"}]}""");
        }

        [Test]
        public void TestSetFluent3()
        {
            //先准备数据
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,ext json)");
            db.Insert<Person3>().SetEntity([new Person3() { Arr = new[] { new { Name = "tom", Age = 20 }, new { Name = "lisa", Age = 18 } }.ToJsonArray() }]).ExecuteAffrows();

            var update = db.Update<Person3>().SetColumnExpr(i => i.Arr, i => i.Arr.SetFluent(i => i["Age"].GetValue<int>() > 16, i => i.ModifyByDto(new { Name = i["Name"].GetValue<string>() + ">16" }))).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = (select json_arrayagg(case when (json_value(t.`i`,'$."Age"' returning signed)) > 16 then json_merge_patch(t.`i`,json_object('Name',concat_ws('',json_value(t.`i`,'$."Name"' returning char),'>16'))) else t.`i` end) from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t)
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Arr":[{"Age":20,"Name":"tom>16"},{"Age":18,"Name":"lisa>16"}]}""");
        }
        #endregion
    }
}
