﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonDicTests : TestBase
    {
        #region Along 独占json
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties")]
            public Dictionary<string, string> Ext { get; set; }

            [JsonStore(Bucket = "properties2")]
            public Dictionary<string, string> Ext2 { get; set; }

            [JsonStore(Bucket = "properties3")]
            public Dictionary<string, List<string>> Ext3 { get; set; }
        }

        [Test]
        public void TestDic_Along_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person>()
                 .SetExpr(() => new Person
                 {
                     Ext = new Dictionary<string, string> { { "name", "tom" }, { "addr", "abc" } },
                 })
                 .SetColumnExpr(i => i.Ext2, i => new Dictionary<string, string> { { "name", "tom" }, { "addr", "abc" } })
                 .SetColumnExpr(i => i.Ext3, i => new Dictionary<string, List<string>>
                 {
                    { "a",new List<string> { "b", "c" } }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = '{"name":"tom","addr":"abc"}',
            //        properties2 = '{"name":"tom","addr":"abc"}',
            //        properties3 = '{"a":["b","c"]}'
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext3.ToJson().ShouldBe("""{"a":["b","c"]}""");
        }

        [Test]
        public void TestDic_Along_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim" }))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc'),
            //        properties2 = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc'),
            //        properties3 = json_set(ifnull(json_value(properties3,'$'),'{}'),'$."name"',cast('["tom","jim"]' as json))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext3.ToJson().ShouldBe("""{"name":["tom","jim"]}""");
        }

        [Test]
        public void TestDic_Along_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new Dictionary<string, string> { { "name", "tom" } })
                .SetColumnExpr(i => i.Ext2, new Dictionary<string, string> { { "name", "tom" } })
                .ExecuteIdentity();

            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"] }))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$'),'$."name"' returning char),'2')),'{}'),'$."addr"','abc'),
            //        properties2 = json_set(ifnull(json_set(ifnull(json_value(properties,'$'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$'),'$."name"' returning char),'2')),'{}'),'$."addr"','abc'),
            //        properties3 = json_set(ifnull(json_value(properties3,'$'),'{}'),'$."name"',json_array('tom','jim',json_value(json_value(properties,'$'),'$."name"' returning char)))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc","name":"tom2"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc","name":"tom22"}""");
            p.Ext3.ToJson().ShouldBe("""{"name":["tom","jim","tom2"]}""");
        }

        [Test]
        public void TestDic_Along_PartialUpdate3()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new Dictionary<string, string> { { "name", "tom" } })
                .SetColumnExpr(i => i.Ext2, new Dictionary<string, string> { { "name", "tom" } })
                .ExecuteIdentity();

            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = i.Ext.ClearFluent().SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.RemoveFluent("name").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"] }))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(json_object(),'$."addr"','abc'),
            //        properties2 = json_set(ifnull(json_remove(ifnull(json_value(properties,'$'),'{}'),'$."name"'),'{}'),'$."addr"','abc'),
            //        properties3 = json_set(ifnull(json_value(properties3,'$'),'{}'),'$."name"',json_array('tom','jim',json_value(json_value(properties,'$'),'$."name"' returning char)))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc"}""");
            p.Ext3.ToJson().ShouldBe("""{"name":["tom","jim",null]}""");
        }
        #endregion

        #region 非独占json
        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public Dictionary<string, string> Ext { get; set; }

            [JsonStore(Bucket = "properties2", Key = "ext")]
            public Dictionary<string, string> Ext2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public Dictionary<string, List<string>> Ext3 { get; set; }
        }

        [Test]
        public void TestDic_NotAlong_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(() => new Person2
                 {
                     Ext = new Dictionary<string, string> { { "name", "tom" }, { "addr", "abc" } },
                 })
                 .SetColumnExpr(i => i.Ext2, i => new Dictionary<string, string> { { "name", "tom" }, { "addr", "abc" } })
                 .SetColumnExpr(i => i.Ext3, i => new Dictionary<string, List<string>>
                 {
                    { "a",new List<string> { "b", "c" } }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"name":"tom","addr":"abc"}' as json)),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"name":"tom","addr":"abc"}' as json)),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"a":["b","c"]}' as json))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext3.ToJson().ShouldBe("""{"a":["b","c"]}""");
        }

        [Test]
        public void TestDic_NotAlong_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", "tom").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim" }))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"','tom'),'{}'),'$."addr"','abc')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_value(properties3,'$."ext"'),'{}'),'$."name"',cast('["tom","jim"]' as json)))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc","name":"tom"}""");
            p.Ext3.ToJson().ShouldBe("""{"name":["tom","jim"]}""");
        }

        [Test]
        public void TestDic_NotAlong_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new Dictionary<string, string> { { "name", "tom" } })
                .SetColumnExpr(i => i.Ext2, new Dictionary<string, string> { { "name", "tom" } })
                .ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.SetFluent("name", i.Ext["name"] + "2").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"] }))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$."ext"'),'$."name"' returning char),'2')),'{}'),'$."addr"','abc')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"',concat_ws('',json_value(json_value(properties,'$."ext"'),'$."name"' returning char),'2')),'{}'),'$."addr"','abc')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_value(properties3,'$."ext"'),'{}'),'$."name"',json_array('tom','jim',json_value(json_value(properties,'$."ext"'),'$."name"' returning char))))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc","name":"tom2"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc","name":"tom22"}""");
            p.Ext3.ToJson().ShouldBe("""{"name":["tom","jim","tom2"]}""");
        }

        [Test]
        public void TestDic_NotAlong_PartialUpdate3()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Ext, new Dictionary<string, string> { { "name", "tom" } })
                .SetColumnExpr(i => i.Ext2, new Dictionary<string, string> { { "name", "tom" } })
                .ExecuteIdentity();

            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = i.Ext.ClearFluent().SetFluent("addr", "abc"),
                 })
                 .SetColumnExpr(i => i.Ext2, i => i.Ext.RemoveFluent("name").SetFluent("addr", "abc"))
                 .SetColumnExpr(i => i.Ext3, i => i.Ext3.SetFluent("name", new List<string> { "tom", "jim", i.Ext["name"] }))
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(json_object(),'$."addr"','abc')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_remove(ifnull(json_value(properties,'$."ext"'),'{}'),'$."name"'),'{}'),'$."addr"','abc')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_value(properties3,'$."ext"'),'{}'),'$."name"',json_array('tom','jim',json_value(json_value(properties,'$."ext"'),'$."name"' returning char))))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"addr":"abc"}""");
            p.Ext2.ToJson().ShouldBe("""{"addr":"abc"}""");
            p.Ext3.ToJson().ShouldBe("""{"name":["tom","jim",null]}""");
        }
        #endregion
    }
}
