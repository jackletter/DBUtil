﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonStoreTests : TestBase
    {
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties")]
            public string Name { get; set; }

            [JsonStore(Bucket = "addrs")]
            public List<string> Addrs { get; set; }

            [JsonStore(Bucket = "properties2", Key = "score")]
            public int Score { get; set; }

            [JsonStore(Bucket = "properties2", Key = "scores")]
            public List<int> Scores { get; set; }
        }

        [Test]
        public void Test()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json,addrs json,properties2 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, null).ExecuteAffrows();

            var sql = "";
            var tmpNames = new[] { "刘备", "关羽" };

            //简单更改
            sql = db.Update<Person>().SetExpr(i => new Person
            {
                Name = "小花",
                Addrs = new List<string> { "a", "b" },
                Score = 100,
                Scores = new List<int> { 95, 100 }
            }).Where(i => i.Id == 1)
             .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = '"小花"',
            //        addrs = '["a","b"]',
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."score"',100,
            //            '$."scores"',cast('[95,100]' as json))
            //    where `Id` = 1;
            //    """);
            db.ExecuteSql(sql);
            var ent = db.SelectModel<(int id, string properties, string addrs, string properties2)>("select id,properties,addrs,properties2 from test where id = 1");
            ent.id.ShouldBe(1);
            ent.properties.ShouldBe("\"小花\"");
            ent.addrs.ShouldBe("[\"a\", \"b\"]");
            ent.properties2.ShouldBe("{\"score\": 100, \"scores\": [95, 100]}");

            //带参数/局部变量更改
            sql = db.Update<Person>().SetExpr(i => new Person
            {
                Name = i.Name + tmpNames.FirstOrDefault(),
                Addrs = new List<string> { "a", i.Name + tmpNames.FirstOrDefault() },
                Score = i.Score - 10,
                Scores = new List<int> { 95, i.Scores[0] + tmpNames.Length }
            }).Where(i => i.Id == 1)
             .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_quote(concat_ws('',json_value(properties,'$' returning char),'刘备')),
            //        addrs = json_array('a',concat_ws('',json_value(properties,'$' returning char),'刘备')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."score"',(json_value(properties2,'$."score"' returning signed)) - 10,
            //            '$."scores"',json_array(95,(json_value(json_value(properties2,'$."scores"'),'$[0]' returning signed)) + 2))
            //    where `Id` = 1;
            //    """);
            db.ExecuteSql(sql);
            ent = db.SelectModel<(int id, string properties, string addrs, string properties2)>("select id,properties,addrs,properties2 from test where id = 1");
            ent.id.ShouldBe(1);
            ent.properties.ShouldBe("\"小花刘备\"");
            ent.addrs.ShouldBe("[\"a\", \"小花刘备刘备\"]");
            ent.properties2.ShouldBe("{\"score\": 90, \"scores\": [95, 97]}");

            //局部变量直接赋值
            var li = tmpNames.ToList();
            var linum = new List<int> { 1, 2, 3 };
            sql = db.Update<Person>().SetExpr(i => new Person
            {
                Addrs = li,
                Scores = linum
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe(
            //  """
            //    update test set
            //        addrs = '["刘备","关羽"]',
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."scores"',cast('[1,2,3]' as json))
            //    where `Id` = 1;
            //    """);

            //AddFluent
            sql = db.Update<Person>().SetExpr(i => new Person
            {
                Addrs = li,
                Scores = linum.AddFluent(2).AddFluent(3)
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        addrs = '["刘备","关羽"]',
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."scores"',cast('[1,2,3,2,3]' as json))
            //    where `Id` = 1;
            //    """);

            //RemoveAtFluent AddRangeFluent AddFluent
            sql = db.Update<Person>().SetExpr(i => new Person
            {
                Addrs = i.Addrs.AddFluent("xiaomign"),
                Scores = i.Scores.RemoveAtFluent(0).AddRangeFluent(new List<int> { 7, 8 })
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        addrs = json_array_append(ifnull(json_value(addrs,'$'),'[]'),'$','xiaomign'),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."scores"',json_merge_preserve(json_remove(ifnull(json_value(properties2,'$."scores"'),'[]'),'$[0]'),'[7,8]'))
            //    where `Id` = 1;
            //    """);

            //ClearFluent
            sql = db.Update<Person>().SetExpr(i => new Person
            {
                Addrs = i.Addrs.AddFluent("xiaomign").ClearFluent().AddFluent("小明")
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        addrs = json_array_append(json_array(),'$','小明')
            //    where `Id` = 1;
            //    """);
        }

        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties")]
            public Dictionary<string, object> Props { get; set; }
            [JsonStore(Bucket = "properties2", Key = "ext")]
            public Dictionary<string, object> Props2 { get; set; }
        }

        [Test]
        public void TestDic()
        {
            var sql = db.Update<Person2>().SetExpr(i => new Person2
            {
                Props = new Dictionary<string, object> { { "name", "lisa" }, { "age", 30 } },
                Props2 = new Dictionary<string, object> { { "name", "lisa" }, { "age", 30 } },
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = '{"name":"lisa","age":30}',
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"name":"lisa","age":30}' as json))
            //    where `Id` = 1;
            //    """);
        }


        [Table("test")]
        class Person3
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties2", Key = "name")]
            public string Name2 { get; set; }
        }

        [Test]
        public void TestStr()
        {
            string sql = "";
            sql = db.Update<Person3>().SetExpr(i => new Person3
            {
                Name = "小明",
                Name2 = "tom"
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = '"小明"',
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."name"','tom')
            //    where `Id` = 1;
            //    """);
        }
    }
}
