﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonToJsonObjectArrayTests : TestBase
    {
        #region Along
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "properties2")]
            public JsonObject Ext2 { get; set; }

            [JsonStore(Bucket = "properties3")]
            public JsonArray Ext3 { get; set; }
        }

        [Test]
        public void TestToObject_long()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person>()
                  .SetExpr(() => new Person
                  {
                      Ext = new { name = "tom", age = 20 }.ToJsonObject(),
                  })
                  .SetColumnExpr(i => i.Ext2, i => new { name = "tom", age = 20 }.ToJsonObject())
                  .SetColumnExpr(i => i.Ext3, i => new[] { "name", "age" }.ToJsonArray())
                  .Where(i => i.Id == 1)
                  .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_object('name','tom','age',20),
            //        properties2 = json_object('name','tom','age',20),
            //        properties3 = '["name","age"]'
            //    where id = 1;
            //    """);
        }
        #endregion

        #region NotAlong
        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "properties2", Key = "ext")]
            public JsonObject Ext2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public JsonArray Ext3 { get; set; }
        }

        [Test]
        public void TestToObject_NotAlong()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var sql = db.Update<Person2>()
                  .SetExpr(() => new Person2
                  {
                      Ext = new { name = "tom", age = 20 }.ToJsonObject(),
                  })
                  .SetColumnExpr(i => i.Ext2, i => new { name = "tom", age = 20 }.ToJsonObject())
                  .SetColumnExpr(i => i.Ext3, i => new[] { "name", "age" }.ToJsonArray())
                  .Where(i => i.Id == 1)
                  .ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',json_object('name','tom','age',20)),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',json_object('name','tom','age',20)),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast('["name","age"]' as json))
            //    where id = 1;
            //    """);
        }
        #endregion
    }
}
