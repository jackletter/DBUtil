﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonPropertyNameTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public Ext Ext { get; set; }

            [JsonStore(Bucket = "ext2", Key = "key")]
            public Ext ExtKey { get; set; }

            [JsonStore(Bucket = "ext3")]
            [JsonPropertyName("kext")]
            public Ext Ext2 { get; set; }
        }

        public class Ext
        {
            [JsonPropertyName("kname")]
            public string Name { get; set; }
            [JsonPropertyName("kdetail")]
            public Detail Detail { get; set; }
        }
        public class Detail
        {
            public int Age { get; set; }
        }
        #endregion

        [Test]
        public void Test()
        {
            //JsonPropertyName 需要不起作用
        }
    }
}
