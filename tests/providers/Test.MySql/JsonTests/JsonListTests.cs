﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonListTests : TestBase
    {
        #region Along 独占json列
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            public string Name { get; set; }

            [JsonStore(Bucket = "properties")]
            public List<string> Addrs { get; set; }

            [JsonStore(Bucket = "properties2")]
            public string[] Addrs2 { get; set; }

            [JsonStore(Bucket = "properties3")]
            public List<string> Addrs3 { get; set; }
        }

        [Test]
        public void TestList_Along_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person>()
                  .SetExpr(() => new Person
                  {
                      Addrs = new List<string> { "a", "b" },
                      Addrs2 = new List<string> { "a", "b" }.ToArray()
                  })
                  .SetColumnExpr(i => i.Addrs3, i => new List<string> { "a", "b" })
                  .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = '["a","b"]',
            //        properties2 = '["a","b"]',
            //        properties3 = '["a","b"]'
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Addrs.ToJson().ShouldBe("""["a","b"]""");
            p.Addrs2.ToJson().ShouldBe("""["a","b"]""");
            p.Addrs3.ToJson().ShouldBe("""["a","b"]""");
        }

        [Test]
        public void TestList_Along_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person>()
                  .SetExpr(i => new Person
                  {
                      Addrs = i.Addrs.AddFluent("c"),
                      Addrs2 = i.Addrs2.AddFluent("c").ToArray()
                  })
                  .SetColumnExpr(i => i.Addrs3, i => i.Addrs3.AddFluent("c").AddFluent("d").AddFluent("e").RemoveAtFluent(0))
                  .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_array_append(ifnull(json_value(properties,'$'),'[]'),'$','c'),
            //        properties2 = json_array_append(ifnull(json_value(properties2,'$'),'[]'),'$','c'),
            //        properties3 = json_remove(json_array_append(json_array_append(json_array_append(ifnull(json_value(properties3,'$'),'[]'),'$','c'),'$','d'),'$','e'),'$[0]')
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Addrs.ToJson().ShouldBe("""["c"]""");
            p.Addrs2.ToJson().ShouldBe("""["c"]""");
            p.Addrs3.ToJson().ShouldBe("""["d","e"]""");
        }

        [Test]
        public void TestList_Along_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Addrs, new List<string> { "a", "b" })
                .SetColumnExpr(i => i.Addrs3, new List<string> { "a", "b", "c" })
                .ExecuteIdentity();

            var update = db.Update<Person>()
                  .SetExpr(i => new Person
                  {
                      Addrs = i.Addrs.AddFluent("g").ClearFluent().AddFluent("f").InsertAtFluent(1, "g"),
                  })
                  .SetColumnExpr(i => i.Addrs3, i => new List<string> { i.Addrs3[0], "f", i.Addrs3[2] })
                  .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_array_insert(ifnull(json_array_append(json_array(),'$','f'),'[]'),'$[1]','g'),
            //        properties3 = json_array(json_value(json_value(properties3,'$'),'$[0]' returning char),'f',json_value(json_value(properties3,'$'),'$[2]' returning char))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Addrs.ToJson().ShouldBe("""["f","g"]""");
            p.Addrs3.ToJson().ShouldBe("""["a","f","c"]""");
        }
        #endregion

        #region 非独占json列

        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public List<string> Addrs { get; set; }

            [JsonStore(Bucket = "properties2", Key = "ext")]
            public string[] Addrs2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public List<string> Addrs3 { get; set; }
        }

        [Test]
        public void TestList_NotAlong_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person2>()
                  .SetExpr(() => new Person2
                  {
                      Addrs = new List<string> { "a", "b" },
                      Addrs2 = new List<string> { "a", "b" }.ToArray()
                  })
                  .SetColumnExpr(i => i.Addrs3, i => new List<string> { "a", "b" })
                  .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast('["a","b"]' as json)),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast('["a","b"]' as json)),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast('["a","b"]' as json))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Addrs.ToJson().ShouldBe("""["a","b"]""");
            p.Addrs2.ToJson().ShouldBe("""["a","b"]""");
            p.Addrs3.ToJson().ShouldBe("""["a","b"]""");
        }

        [Test]
        public void TestList_NotAlong_PartialUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var update = db.Update<Person2>()
                  .SetExpr(i => new Person2
                  {
                      Addrs = i.Addrs.AddFluent("c"),
                      Addrs2 = i.Addrs2.AddFluent("c").ToArray()
                  })
                  .SetColumnExpr(i => i.Addrs3, i => i.Addrs3.AddFluent("c").AddFluent("d").AddFluent("e").RemoveAtFluent(0))
                  .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',json_array_append(ifnull(json_value(properties,'$."ext"'),'[]'),'$','c')),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',json_array_append(ifnull(json_value(properties2,'$."ext"'),'[]'),'$','c')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',json_remove(json_array_append(json_array_append(json_array_append(ifnull(json_value(properties3,'$."ext"'),'[]'),'$','c'),'$','d'),'$','e'),'$[0]'))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Addrs.ToJson().ShouldBe("""["c"]""");
            p.Addrs2.ToJson().ShouldBe("""["c"]""");
            p.Addrs3.ToJson().ShouldBe("""["d","e"]""");
        }

        [Test]
        public void TestList_NotAlong_PartialUpdate2()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>()
                .SetColumnExpr(i => i.Name, "lisa")
                .SetColumnExpr(i => i.Addrs, new List<string> { "a", "b" })
                .SetColumnExpr(i => i.Addrs3, new List<string> { "a", "b", "c" })
                .ExecuteIdentity();

            var update = db.Update<Person2>()
                  .SetExpr(i => new Person2
                  {
                      Addrs = i.Addrs.AddFluent("g").ClearFluent().AddFluent("f").InsertAtFluent(1, "g"),
                  })
                  .SetColumnExpr(i => i.Addrs3, i => new List<string> { i.Addrs3[0], "f", i.Addrs3[2] })
                  .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',json_array_insert(ifnull(json_array_append(json_array(),'$','f'),'[]'),'$[1]','g')),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',json_array(json_value(json_value(properties3,'$."ext"'),'$[0]' returning char),'f',json_value(json_value(properties3,'$."ext"'),'$[2]' returning char)))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Addrs.ToJson().ShouldBe("""["f","g"]""");
            p.Addrs3.ToJson().ShouldBe("""["a","f","c"]""");
        }

        #endregion
    }
}
