﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests.SetFluentTests
{
    [TestFixture]
    internal class JsonArraySetFluentTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public JsonArray Ext { get; set; }
        }
        #endregion
        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,ext json)");
            db.ExecuteSql("""
                insert into test(ext) values
                ('[{"Name":"jack","Age":18,"Detail":{"Score":98}},{"Name":"tom","Age":20},{"Name":"lisa","Age":28,"Detail":{"Score":60}}]'),
                ('[{"Name":"jack2","Age":18,"Detail":{"Score":95}},{"Name":"tom2","Age":20},{"Name":"lisa2","Age":28,"Detail":{"Score":65}}]');
                """);
        }

        [Test]
        public void SetFluentTest()
        {
            //var sql = db.Update<Person>().SetExpr(i => new Person
            //{
            //    Ext = i.Ext.SetFluent(
            //        i.Ext.FindIndex(i => i["Name"].GetValue<string>() == "jack"),
            //        i => i.AsObject().SetFluent("Name", i["Name"].GetValue<string>() + "-222"))
            //}).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'),json_set(ifnull(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'))),'{}'),'$."Name"',concat_ws('',json_value(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'))),'$."Name"' returning char),'-222')))
            //    where id = 1;
            //    """);

            //var sql2 = db.Update<Person>().SetExpr(i => new Person
            //{
            //    Ext = i.Ext.SetFluent(0, i => i.AsObject().SetFluent("Name", i["Name"].GetValue<string>() + "-222"))
            //}).Where(i => i.Id == 1).ToSql();
            //sql2.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),'$[0]',json_set(ifnull(json_value(ifnull(json_value(ext,'$'),'[]'),'$[0]'),'{}'),'$."Name"',concat_ws('',json_value(json_value(ifnull(json_value(ext,'$'),'[]'),'$[0]'),'$."Name"' returning char),'-222')))
            //    where id = 1;
            //    """);

            //var sql3 = db.Update<Person>().SetExpr(i => new Person
            //{
            //    Ext = i.Ext.SetFluent(i.Ext.FindIndex(i => i["Name"].GetValue<string>() == "jack"), i => i.AsObject().SetFluent("Name", i => i.GetValue<string>() + "-222"))
            //}).Where(i => i.Id == 1).ToSql();
            //sql3.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'),json_set(ifnull(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'))),'{}'),'$."Name"',concat_ws('',json_value(ifnull(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'))),'{}'),'$."Name"' returning char),'-222')))
            //    where id = 1;
            //    """);

            //var sql4 = db.Update<Person>().SetExpr(i => new Person
            //{
            //    Ext = i.Ext.SetFluent(i.Ext.FindIndex(i => i["Name"].GetValue<string>() == "jack"), i => i.AsObject().SetFluent("Detail", i => i.AsObject().SetFluent("Score", 66)))
            //}).Where(i => i.Id == 1).ToSql();
            //sql4.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'),json_set(ifnull(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'))),'{}'),'$."Detail"',json_set(ifnull(json_value(ifnull(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'))),'{}'),'$."Detail"'),'{}'),'$."Score"',cast(66 as json))))
            //    where id = 1;
            //    """);

            //var sql5 = db.Update<Person>().SetExpr(i => new Person
            //{
            //    Ext = i.Ext.SetFluent(i.Ext.FindIndex(i => i["Name"].GetValue<string>() == "jack"), i => i.ModifyByDto(new { Detail = new { Score = 66 } }))
            //}).Where(i => i.Id == 1).ToSql();
            //sql5.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'),json_merge_patch(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Name"' returning char)) = 'jack' limit 1),']'))),json_object('Detail',json_object('Score',66))))
            //    where id = 1;
            //    """);

            //var sql6 = db.Update<Person>().SetExpr(i => new Person
            //{
            //    Ext = i.Ext.SetFluent(i => i["Age"].GetValue<int>() > 16, i => i.ModifyByDto(new { Detail = new { Score = i["Detail"]["Score"].GetValue<int>() + 10 } }))
            //}).Where(i => i.Id == 1).ToSql();
            //sql6.ShouldBe("""
            //    update test set
            //        ext = (select json_arrayagg(case when (json_value(t.`i`,'$."Age"' returning signed)) > 16 then json_merge_patch(t.`i`,json_object('Detail',json_object('Score',(json_value(json_value(t.`i`,'$."Detail"'),'$."Score"' returning signed)) + 10))) else t.`i` end) from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t)
            //    where id = 1;
            //    """);

            var sql7 = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = i.Ext.SetFluent(i.Ext.FindLastIndex(i => i["Age"].GetValue<int>() > 16), i => i.ModifyByDto(new { Detail = new { Score = i["Detail"]["Score"].GetValue<int>() + 10 } }))
            }).Where(i => i.Id == 1).ToSql();
            //sql7.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 as idx from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Age"' returning signed)) > 16 order by t.idx desc limit 1),']'),json_merge_patch(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 as idx from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Age"' returning signed)) > 16 order by t.idx desc limit 1),']'))),json_object('Detail',json_object('Score',(json_value(json_value(json_unquote(json_extract(ifnull(json_value(ext,'$'),'[]'),concat('$[',(select idx-1 as idx from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$'),'$[*]' columns(`i` json path '$')) t) t where (json_value(t.`i`,'$."Age"' returning signed)) > 16 order by t.idx desc limit 1),']'))),'$."Detail"'),'$."Score"' returning signed)) + 10))))
            //    where id = 1;
            //    """);
        }
    }
}
