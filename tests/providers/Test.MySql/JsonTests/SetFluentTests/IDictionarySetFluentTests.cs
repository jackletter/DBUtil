﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests.SetFluentTests
{
    [TestFixture]
    internal class IDictionarySetFluentTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext")]
            public IDictionary<string, string> Ext { get; set; }
        }
        [Table("test2")]
        public class PersonKey
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "prop", Key = "ext")]
            public IDictionary<string, string> Ext { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int primary key auto_increment,ext json)""");
            db.ExecuteSql("""
                insert into test(ext) values
                    ('{"name":"jack","addr":"road1"}'),
                    ('{"name":"tom","addr":"road2"}');
                """);

            DropTableIfExist("test2");
            db.ExecuteSql("""create table test2(id int primary key auto_increment,prop json)""");
            db.ExecuteSql("""
                insert into test2(prop) values
                    ('{"ext":{"name":"jack","addr":"road1"}}'),
                    ('{"ext":{"name":"tom","addr":"road2"}}');
                """);
        }

        #region NoKey
        [Test]
        public void UpdateTestSimple()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = i.Ext.SetFluent("addr", "road1+1").SetFluent("name", "jack2")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_set(ifnull(json_value(ext,'$'),'{}'),'$."addr"','road1+1'),'{}'),'$."name"','jack2')
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var list = db.Select<Person>().Where(i => i.Id == 1).ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"Ext":{"addr":"road1+1","name":"jack2"}}]""");
        }

        [Test]
        public void UpdateTest2()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = i.Ext.SetFluent("addr", i => i + "2").SetFluent("name", i => i + "22")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(json_set(ifnull(json_value(ext,'$'),'{}'),'$."addr"',concat_ws('',json_value(ifnull(json_value(ext,'$'),'{}'),'$."addr"' returning char),'2')),'{}'),'$."name"',concat_ws('',json_value(ifnull(json_set(ifnull(json_value(ext,'$'),'{}'),'$."addr"',concat_ws('',json_value(ifnull(json_value(ext,'$'),'{}'),'$."addr"' returning char),'2')),'{}'),'$."name"' returning char),'22'))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var list = db.Select<Person>().Where(i => i.Id == 1).ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"Ext":{"addr":"road12","name":"jack22"}}]""");
        }
        #endregion

        #region Key
        [Test]
        public void UpdateTestSimpleKey()
        {
            var update = db.Update<PersonKey>().SetExpr(i => new PersonKey
            {
                Ext = i.Ext.SetFluent("addr", "road1+1").SetFluent("name", "jack2")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test2 set
            //        prop = json_set(ifnull(prop,json_object()),
            //            '$."ext"',cast(ifnull(json_value(prop,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(prop,'$."ext"'),'{}'),'$."addr"','road1+1'),'{}'),'$."name"','jack2'))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var list = db.Select<PersonKey>().Where(i => i.Id == 1).ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"Ext":{"addr":"road1+1","name":"jack2"}}]""");
        }

        [Test]
        public void UpdateTest2Key()
        {
            var update = db.Update<PersonKey>().SetExpr(i => new PersonKey
            {
                Ext = i.Ext.SetFluent("addr", i => i + "2").SetFluent("name", i => i + "22")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test2 set
            //        prop = json_set(ifnull(prop,json_object()),
            //            '$."ext"',cast(ifnull(json_value(prop,'$."ext"'),'{}') as json),
            //            '$."ext"',json_set(ifnull(json_set(ifnull(json_value(prop,'$."ext"'),'{}'),'$."addr"',concat_ws('',json_value(ifnull(json_value(prop,'$."ext"'),'{}'),'$."addr"' returning char),'2')),'{}'),'$."name"',concat_ws('',json_value(ifnull(json_set(ifnull(json_value(prop,'$."ext"'),'{}'),'$."addr"',concat_ws('',json_value(ifnull(json_value(prop,'$."ext"'),'{}'),'$."addr"' returning char),'2')),'{}'),'$."name"' returning char),'22')))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var list = db.Select<PersonKey>().Where(i => i.Id == 1).ToList();
            var json = list.ToJson();
            json.ShouldBe("""[{"Id":1,"Ext":{"addr":"road12","name":"jack22"}}]""");
        }
        #endregion
    }
}
