﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests.SetFluentTests
{
    [TestFixture]
    internal class ArraySetFluentTests : TestBase
    {
        #region model
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "ext", Key = "Names")]
            public string[] Names { get; set; }
            [JsonStore(Bucket = "ext", Key = "Students")]
            public Student[] Students { get; set; }
        }
        class Student
        {
            public string Name { get; set; }
            public int Age { get; set; }
        }
        [Table("test2")]
        class PersonNoKey
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [JsonStore(Bucket = "Names")]
            public List<string> Names { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int auto_increment primary key,ext json)""");
            db.ExecuteSql(
                """
                insert into test(ext) values
                ('{"Names":["jack","tom","jack2"],"Students":[{"Name":"xiaowang","Age":18},{"Name":"xiaowang2","Age":28}]}'),
                ('{"Names":["lisa","tom","lilei"],"Students":[{"Name":"laozhang","Age":18},{"Name":"xiaowang2","Age":28}]}');
                """);

            DropTableIfExist("test2");
            db.ExecuteSql("""create table test2(id int auto_increment primary key,Names json)""");
            db.ExecuteSql(
                """
                insert into test2(Names) values
                ('["jack","tom","jack2"]'),
                ('["lisa","tom","lilei"]');
                """);
        }

        [Test]
        public void TestSetSimple()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Names = i.Names.SetFluent(1, "ok")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Names"',json_set(ifnull(json_value(ext,'$."Names"'),'[]'),'$[1]','ok'))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","ok","jack2"],"Students":[{"Name":"xiaowang","Age":18},{"Name":"xiaowang2","Age":28}]}""");
        }

        [Test]
        public void TestSetSimple2()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Names = i.Names.SetFluent(1, i => i + "ok")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Names"',json_set(ifnull(json_value(ext,'$."Names"'),'[]'),'$[1]',concat_ws('',json_value(ifnull(json_value(ext,'$."Names"'),'[]'),'$[1]' returning char),'ok')))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","tomok","jack2"],"Students":[{"Name":"xiaowang","Age":18},{"Name":"xiaowang2","Age":28}]}""");
        }

        [Test]
        public void TestSetSimpleNoKey()
        {
            var update = db.Update<PersonNoKey>().SetExpr(i => new PersonNoKey
            {
                Names = i.Names.SetFluent(1, "ok")
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test2 set
            //        Names = json_set(ifnull(json_value(Names,'$'),'[]'),'$[1]','ok')
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<PersonNoKey>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","ok","jack2"]}""");
        }

        [Test]
        public void TestSetComplexModify()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Students = i.Students.SetFluent(1, i.Students[1].ModifyByDto(new { Name = "ok" }))
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Students"',json_set(ifnull(json_value(ext,'$."Students"'),'[]'),'$[1]',json_merge_patch(json_value(json_value(ext,'$."Students"'),'$[1]'),json_object('Name','ok'))))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","tom","jack2"],"Students":[{"Name":"xiaowang","Age":18},{"Name":"ok","Age":28}]}""");
        }

        [Test]
        public void TestSetComplexNewObj()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Students = i.Students.SetFluent(1, new Student { Age = 10, Name = "little boy" })
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Students"',json_set(ifnull(json_value(ext,'$."Students"'),'[]'),'$[1]',json_object('Age',10,'Name','little boy')))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","tom","jack2"],"Students":[{"Name":"xiaowang","Age":18},{"Name":"little boy","Age":10}]}""");
        }

        [Test]
        public void TestSetFluentFindIndex()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Students = i.Students.SetFluent(i.Students.FindIndex(i => i.Name == "xiaowang"), i.Students[i.Students.FindIndex(i => i.Name == "xiaowang")].ModifyByDto(new { Name = "ok" }))
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Students"',json_set(ifnull(json_value(ext,'$."Students"'),'[]'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$."Students"'),'$[*]' columns(`i.Name` text path '$.Name')) t) t where t.`i.Name` = 'xiaowang' limit 1),']'),json_merge_patch(json_unquote(json_extract(json_value(ext,'$."Students"'),concat('$[',(select idx-1 from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$."Students"'),'$[*]' columns(`i.Name` text path '$.Name')) t) t where t.`i.Name` = 'xiaowang' limit 1),']'))),json_object('Name','ok'))))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","tom","jack2"],"Students":[{"Name":"ok","Age":18},{"Name":"xiaowang2","Age":28}]}""");
        }

        [Test]
        public void TestSetBatch()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Students = i.Students.SetFluent(i => i.Age > 14, j => j.ModifyByDto(new { Name = j.Name + ">14" }))
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Students"',(select json_arrayagg(case when t.`i.Age` > 14 then json_merge_patch(t.`i`,json_object('Name',concat_ws('',t.`i.Name`,'>14'))) else t.`i` end) from json_table(json_value(ext,'$."Students"'),'$[*]' columns(`i.Age` int path '$.Age',`i` json path '$',`i.Name` text path '$.Name')) t))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","tom","jack2"],"Students":[{"Name":"xiaowang>14","Age":18},{"Name":"xiaowang2>14","Age":28}]}""");
        }

        [Test]
        public void TestSetFindLastIndex()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Students = i.Students.SetFluent(i.Students.FindLastIndex(i => i.Age > 14), j => j.ModifyByDto(new { Name = j.Name + ">14" }))
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        ext = json_set(ifnull(ext,json_object()),
            //            '$."Students"',json_set(ifnull(json_value(ext,'$."Students"'),'[]'),concat('$[',(select idx-1 as idx from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$."Students"'),'$[*]' columns(`i.Age` int path '$.Age')) t) t where t.`i.Age` > 14 order by t.idx desc limit 1),']'),json_merge_patch(json_unquote(json_extract(ifnull(json_value(ext,'$."Students"'),'[]'),concat('$[',(select idx-1 as idx from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$."Students"'),'$[*]' columns(`i.Age` int path '$.Age')) t) t where t.`i.Age` > 14 order by t.idx desc limit 1),']'))),json_object('Name',concat_ws('',json_value(json_unquote(json_extract(ifnull(json_value(ext,'$."Students"'),'[]'),concat('$[',(select idx-1 as idx from (select row_number() over() as idx,t.* from json_table(json_value(ext,'$."Students"'),'$[*]' columns(`i.Age` int path '$.Age')) t) t where t.`i.Age` > 14 order by t.idx desc limit 1),']'))),'$."Name"' returning char),'>14')))))
            //    where id = 1;
            //    """);
            var r = update.ExecuteAffrows();
            r.ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = p.ToJson();
            json.ShouldBe("""{"Id":1,"Names":["jack","tom","jack2"],"Students":[{"Name":"xiaowang","Age":18},{"Name":"xiaowang2>14","Age":28}]}""");
        }
    }
}
