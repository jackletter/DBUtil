﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests.SetFluentTests
{
    internal class SetMemberFluentTests : TestBase
    {
        #region model
        [Table("t_test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "ext")]
            public Ext Ext { get; set; }
        }
        public class Ext
        {
            public int Age { get; set; }
            public string Name { get; set; }
            public Detail Detail { get; set; }
        }
        public class Detail
        {
            public string Addr { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("t_test");
            db.ExecuteSql("create table t_test(id int auto_increment primary key,ext json)");

            db.ExecuteSql("""
                insert into t_test(ext) values
                (null);
                """);
        }

        //[Test]
        //public void TestCaseMemberSet1()
        //{
        //    var update = db.Update<Person>().SetExpr(i => new Person
        //    {
        //        Ext = i.Ext
        //            .IfNullUseNew()
        //            .SetMemberFluent(i => i.Name, "tom")
        //            .SetMemberFluent("Age", 20)
        //            .SetMemberFluent(i => i.Detail, i => i.IfNullUseNew().SetMemberFluent(i => i.Addr, "roada"))
        //    }).Where(i => i.Id == 1);
        //    var sql = update.ToSql();
        //    //todo?因为级联调用导致冗余的sql
        //    //"""
        //    //update t_test set
        //    //    ext = json_set(json_set(json_set(ifnull(json_value(ext,'$'),json_object()),'$."Name"','tom'),'$."Age"',20),'$."Detail"',json_set(ifnull(json_value(json_set(json_set(ifnull(json_value(ext,'$'),json_object()),'$."Name"','tom'),'$."Age"',20),'$."Detail"'),json_object()),'$."Addr"','roada'))
        //    //where `Id` = 1;
        //    //""";
        //    update.ExecuteAffrows().ShouldBe(1);

        //    var person = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
        //    var json = person.ToJson();
        //    json.ShouldBe("""{"Id":1,"Ext":{"Age":20,"Name":"tom","Detail":{"Addr":"roada"}}}""");
        //}

        //[Test]
        //public void TestCaseMemberSet2()
        //{
        //    var update = db.Update<Person>().SetExpr(i => new Person
        //    {
        //        Ext = i.Ext
        //            .IfNullUseNew()
        //            .SetMemberFluent(i => i.Name, "tom")
        //            .SetMemberFluent("Age", 20)
        //            .SetMemberFluent(i => i.Detail, i.Ext.Detail.IfNullUseNew().SetMemberFluent(i => i.Addr, "roada"))
        //    }).Where(i => i.Id == 1);
        //    var sql = update.ToSql();
        //    //相对于上面就要好很多,没有冗余的sql
        //    //"""
        //    //update t_test set
        //    //    ext = json_set(json_set(json_set(ifnull(json_value(ext,'$'),json_object()),'$."Name"','tom'),'$."Age"',20),'$."Detail"',json_set(ifnull(json_value(json_value(ext,'$'),'$."Detail"'),json_object()),'$."Addr"','roada'))
        //    //where `Id` = 1;
        //    //""";
        //    update.ExecuteAffrows().ShouldBe(1);

        //    var person = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
        //    var json = person.ToJson();
        //    json.ShouldBe("""{"Id":1,"Ext":{"Age":20,"Name":"tom","Detail":{"Addr":"roada"}}}""");
        //}

        [Test]
        public void TestCaseMemberSet3()
        {
            var update = db.Update<Person>().SetExpr(i => new Person
            {
                Ext = new Ext
                {
                    Name = "tom",
                    Age = 20,
                    Detail = new Detail
                    {
                        Addr = "roada"
                    }
                }
            }).Where(i => i.Id == 1);
            var sql = update.ToSql();
            //最简洁的写法, 丝毫没有冗余数据
            //"""
            //update t_test set
            //    ext = json_set(ifnull(ext,json_object()),
            //        '$."Name"','tom',
            //        '$."Age"',20,
            //        '$."Detail"',cast(ifnull(json_value(ext,'$."Detail"'),json_object()) as json),
            //        '$."Detail"."Addr"','roada')
            //where `Id` = 1;
            //""";
            update.ExecuteAffrows().ShouldBe(1);

            var person = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = person.ToJson();
            json.ShouldBe("""{"Id":1,"Ext":{"Age":20,"Name":"tom","Detail":{"Addr":"roada"}}}""");
        }

        [Test]
        public void TestCaseMemberSet4()
        {
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Ext.Detail, new Detail { Addr = "roada" })
                //.SetColumnExpr(i => i.Ext.Name, "tom")
                //.SetColumnExpr(i => i.Ext.Age, 20)
                //.SetColumnExpr(i => i.Ext.Detail.Addr, "roada")
                .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //虽然简洁, 但是没有自动创建对象 todo? 在指定深度属性的时候,增加自动创建对象?
            //"""
            //update t_test set
            //    ext = json_set(ifnull(ext,json_object()),
            //        '$."Name"','tom',
            //        '$."Age"',20,
            //        '$."Detail"."Addr"','roada')
            //where `Id` = 1;
            //""";
            update.ExecuteAffrows().ShouldBe(1);

            var person = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = person.ToJson();
            json.ShouldBe("""{"Id":1,"Ext":{"Age":20,"Name":"tom","Detail":{"Addr":"roada"}}}""");
        }

        [Test]
        public void TestCaseMemberSet5()
        {
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Ext, new Ext
                {
                    Name = "tom",
                    Age = 20,
                    Detail = new Detail
                    {
                        Addr = "roada"
                    }
                })
                .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //虽然简洁, 但是没有自动创建对象 todo? 要和 SetExpr 中行为一致
            //"""
            //update t_test set
            //    ext = '{"Age":20,"Name":"tom","Detail":{"Addr":"roada"}}'
            //where `Id` = 1;
            //""";
            update.ExecuteAffrows().ShouldBe(1);

            var person = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            var json = person.ToJson();
            json.ShouldBe("""{"Id":1,"Ext":{"Age":20,"Name":"tom","Detail":{"Addr":"roada"}}}""");
        }
    }
}
