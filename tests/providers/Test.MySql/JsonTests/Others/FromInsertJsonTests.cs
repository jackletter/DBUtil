﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests.Others
{
    [TestFixture]
    internal class FromInsertJsonTests : TestBase
    {
        #region nokey
        [Table("test")]
        internal class JsonTestPerson
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }

            [JsonStore(Bucket = "p_byte")]
            public byte p_byte { get; set; }

            [JsonStore(Bucket = "p_sbyte")]
            public sbyte p_sbyte { get; set; }

            [JsonStore(Bucket = "p_short")]
            public short p_short { get; set; }

            [JsonStore(Bucket = "p_ushort")]
            public ushort p_ushort { get; set; }

            [JsonStore(Bucket = "p_int")]
            public int p_int { get; set; }

            [JsonStore(Bucket = "p_uint")]
            public uint p_uint { get; set; }

            [JsonStore(Bucket = "p_long")]
            public long p_long { get; set; }

            [JsonStore(Bucket = "p_ulong")]
            public ulong p_ulong { get; set; }

            [JsonStore(Bucket = "p_float")]
            public float p_float { get; set; }

            [JsonStore(Bucket = "p_double")]
            public double p_double { get; set; }

            [JsonStore(Bucket = "p_decimal")]
            public decimal p_decimal { get; set; }

            [JsonStore(Bucket = "p_long_null")]
            public int? p_long_null { get; set; }

            [JsonStore(Bucket = "p_string")]
            public string p_string { get; set; }

            [JsonStore(Bucket = "p_datetime")]
            public DateTime p_datetime { get; set; }

            [JsonStore(Bucket = "p_dateonly")]
            public DateOnly p_dateonly { get; set; }

            [JsonStore(Bucket = "p_timeonly")]
            public TimeOnly p_timeonly { get; set; }

            [JsonStore(Bucket = "p_timespan")]
            public TimeSpan p_timespan { get; set; }

            [JsonStore(Bucket = "p_datetime_null")]
            public DateTime? p_datetime_null { get; set; }

            [JsonStore(Bucket = "p_guid")]
            public Guid p_guid { get; set; }

            [JsonStore(Bucket = "p_guid_null")]
            public Guid? p_guid_null { get; set; }

            [JsonStore(Bucket = "p_bool")]
            public bool p_bool { get; set; }

            [JsonStore(Bucket = "p_bool_null")]
            public bool? p_bool_null { get; set; }

            [JsonStore(Bucket = "p_object")]
            public object p_object { get; set; }


            [JsonStore(Bucket = "p_object_null")]
            public object p_object_null { get; set; }

            [JsonStore(Bucket = "p_list")]
            public List<string> p_list { get; set; }

            [JsonStore(Bucket = "p_arr")]
            public string[] p_arr { get; set; }

            [JsonStore(Bucket = "p_dic")]
            public Dictionary<string, object> p_dic { get; set; }

            [JsonStore(Bucket = "p_jsonobj")]
            public JsonObject p_jsonobj { get; set; }

            [JsonStore(Bucket = "p_jsonarr")]
            public JsonArray p_jsonarr { get; set; }
        }

        private (JsonTestPerson insert, Action<JsonTestPerson> validate) Prepare()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""
                create table test(
                    id int primary key auto_increment,
                    p_byte json,p_sbyte json,
                    p_short json,p_ushort json,
                    p_int json,p_uint json,
                    p_long json,p_ulong json,
                    p_float json,p_double json,p_decimal json,
                    p_long_null json,
                    p_string json,
                    p_datetime json,p_dateonly json,p_timeonly json,p_timespan json,p_datetime_null json,
                    p_guid json,p_guid_null json,
                    p_bool json,p_bool_null json,
                    p_object json,p_object_null json,
                    p_list json,p_arr json,
                    p_dic json,
                    p_jsonobj json,
                    p_jsonarr json
                )
                """);
            return (new JsonTestPerson
            {
                p_byte = 1,
                p_sbyte = 2,
                p_short = 3,
                p_ushort = 4,
                p_int = 5,
                p_uint = 6,
                p_long = 7,
                p_ulong = 8,
                p_float = 9,
                p_double = 10,
                p_decimal = 11,
                p_string = "tom",
                p_datetime = DateTime.Parse("2023-07-06 17:32:00"),
                p_dateonly = DateOnly.Parse("2023-07-06"),//dateonly 在 bulkcopy 的时候不受支持,todo 改成 datetime?
                p_timeonly = TimeOnly.Parse("17:32:00"),//timeonly 在 bulkcopy 的时候不受支持,todo 改成 timespan
                p_timespan = TimeSpan.FromSeconds(3600),
                p_datetime_null = DateTime.Parse("2023-07-06 17:32:00"),
                p_guid = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_guid_null = Guid.Parse("96d80686975340dda55492ecda9c92f2"),
                p_bool = true,
                p_bool_null = true,
                p_object = new { Age = 18, Name = "jim" },
                p_object_null = new { Age = 18, Name = "jim", Other = "oth" },
                p_list = new List<string> { "tom", "lisa" },
                p_arr = new[] { "lisa", "jim" },
                p_dic = new Dictionary<string, object> { { "name", "tom" }, { "age", 18 } },
                p_jsonobj = new { name = "tom", age = 18 }.ToJson().ToObject<JsonObject>(),
                p_jsonarr = new[] { new { name = "tom", age = 18 } }.ToJson().ToObject<JsonArray>(),
            },
            (ent) =>
            {
                ent.ShouldNotBeNull();
                ent.Id.ShouldBe(1);
                ent.p_byte.ShouldBe((byte)1);
                ent.p_sbyte.ShouldBe((sbyte)2);
                ent.p_short.ShouldBe((short)3);
                ent.p_ushort.ShouldBe((ushort)4);
                ent.p_int.ShouldBe(5);
                ent.p_uint.ShouldBe((uint)6);
                ent.p_long.ShouldBe(7);
                ent.p_ulong.ShouldBe((ulong)8);
                ent.p_float.ShouldBe(9);
                ent.p_double.ShouldBe(10);
                ent.p_decimal.ShouldBe(11);
                ent.p_long_null.ShouldBe(null);
                //
                ent.p_string.ShouldBe("tom");
                ent.p_datetime.ShouldBe(DateTime.Parse("2023-07-06 17:32:00"));
                ent.p_dateonly.ShouldBe(DateOnly.Parse("2023-07-06"));
                ent.p_timeonly.ShouldBe(TimeOnly.Parse("17:32:00"));
                ent.p_timespan.ShouldBe(TimeSpan.FromSeconds(3600));
                ent.p_datetime_null.ShouldBe(DateTime.Parse("2023-07-06 17:32:00"));
                ent.p_guid.ShouldBe(Guid.Parse("96d80686975340dda55492ecda9c92f2"));
                ent.p_guid_null.ShouldBe(Guid.Parse("96d80686975340dda55492ecda9c92f2"));
                ent.p_bool.ShouldBe(true);
                ent.p_bool_null.ShouldBe(true);
                ent.p_object.ToJson().ShouldBe(new { Age = 18, Name = "jim" }.ToJson());
                ent.p_object_null.ToJson().ShouldBe(new { Age = 18, Name = "jim", Other = "oth" }.ToJson());
                ent.p_list.ToJson().ShouldBe(new List<string> { "tom", "lisa" }.ToJson());
                ent.p_arr.ToJson().ShouldBe(new[] { "lisa", "jim" }.ToJson());
                ent.p_dic.ToJson().ShouldBe(new Dictionary<string, object> { { "age", 18 }, { "name", "tom" } }.ToJson());
                ent.p_jsonarr.ToJson().ShouldBe(new[] { new { age = 18, name = "tom" } }.ToJson().ToObject<JsonArray>().ToJson());
                ent.p_jsonobj.ToJson().ShouldBe(new { age = 18, name = "tom" }.ToJson().ToObject<JsonObject>().ToJson());
            }
            );
        }

        [Test]
        public void InsertByExpression_NoKey()
        {
            var (exp, validate) = Prepare();
            var insert = db.Insert(exp);
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            myValidate();
            Prepare();
            insert.ExecuteAffrows();
            myValidate();
            void myValidate()
            {
                var ent = db.Select<JsonTestPerson>().Where(i => i.Id == 1).FirstOrDefault();
                validate(ent);
            }
        }

        [Test]
        public void InsertByExpression_NoKey_setremove()
        {
            var (exp, validate) = Prepare();
            var insert = db.Insert(exp)
                .SetColumnExpr(i => i.p_long, 456)
                .SetColumnExpr(i => i.p_string, "modi")
                .SetColumnExpr(i => i.p_uint, uint.MaxValue)
                .SetColumn("p_ulong", 789)
                .SetColumnExpr(i => i.p_object, () => new
                {
                    K1 = "k1",
                    K2 = new
                    {
                        K21 = 1,
                        K22 = new[] { 1, 2 },
                    },
                    K3 = DateTime.Parse("2023-07-15 17:35:00"),
                    K4 = new List<int> { 33, 44 },
                    K5 = new Dictionary<string, string> { { "age", "18" }, { "name", "tom" } }
                });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            myValidate();
            Prepare();
            insert.ExecuteAffrows();
            myValidate();
            void myValidate()
            {
                var ent = db.Select<JsonTestPerson>().Where(i => i.Id == 1).FirstOrDefault();
                ent.ShouldNotBeNull();
                ent.Id.ShouldBe(1);
                ent.p_uint.ShouldBe(uint.MaxValue);
                ent.p_long.ShouldBe(456);
                ent.p_ulong.ShouldBe((ulong)789);
                //
                ent.p_string.ShouldBe("modi");
                ent.p_object.ToJson().Replace("2023-07-15T17:35:00", "2023-07-15 17:35:00").ShouldBe(new
                {
                    K1 = "k1",
                    K2 = new
                    {
                        K21 = 1,
                        K22 = new[] { 1, 2 },
                    },
                    K3 = DateTime.Parse("2023-07-15 17:35:00"),
                    K4 = new List<int> { 33, 44 },
                    K5 = new Dictionary<string, string> { { "age", "18" }, { "name", "tom" } }
                }.ToJson().Replace("2023-07-15T17:35:00", "2023-07-15 17:35:00"));
            }
        }

        [Test]
        public void InsertByEntity_NoKey()
        {
            var (exp, validate) = Prepare();
            var insert = db.Insert(exp);
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            myValidate();
            Prepare();
            insert.ExecuteAffrows();
            myValidate();
            void myValidate()
            {
                var ent = db.Select<JsonTestPerson>().Where(i => i.Id == 1).FirstOrDefault();
                validate(ent);
            }
        }

        [Test]
        public void InsertByEntity_NoKey_setremove()
        {
            var (exp, validate) = Prepare();
            var insert = db.Insert(exp)
                .SetColumnExpr(i => i.p_long, 456)
                .SetColumnExpr(i => i.p_string, "modi")
                .SetColumnExpr(i => i.p_uint, uint.MaxValue)
                .SetColumn("p_ulong", 789)
                .SetColumnExpr(i => i.p_object, () => new
                {
                    K1 = "k1",
                    K2 = new
                    {
                        K21 = 1,
                        K22 = new[] { 1, 2 },
                    },
                    K3 = DateTime.Parse("2023-07-15 17:35:00"),
                    K4 = new List<int> { 33, 44 },
                    K5 = new Dictionary<string, string> { { "age", "18" }, { "name", "tom" } }
                });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            myValidate();
            Prepare();
            insert.ExecuteAffrows();
            myValidate();
            void myValidate()
            {
                var ent = db.Select<JsonTestPerson>().Where(i => i.Id == 1).FirstOrDefault();
                ent.ShouldNotBeNull();
                ent.Id.ShouldBe(1);
                ent.p_uint.ShouldBe(uint.MaxValue);
                ent.p_long.ShouldBe(456);
                ent.p_ulong.ShouldBe((ulong)789);
                //
                ent.p_string.ShouldBe("modi");
                ent.p_object.ToJson().Replace("2023-07-15T17:35:00", "2023-07-15 17:35:00").ShouldBe(new
                {
                    K1 = "k1",
                    K2 = new
                    {
                        K21 = 1,
                        K22 = new[] { 1, 2 },
                    },
                    K3 = DateTime.Parse("2023-07-15 17:35:00"),
                    K4 = new List<int> { 33, 44 },
                    K5 = new Dictionary<string, string> { { "age", "18" }, { "name", "tom" } }
                }.ToJson().Replace("2023-07-15T17:35:00", "2023-07-15 17:35:00"));
            }
        }
        #endregion

        #region usekey
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "age")]
            public int Age { get; set; }
            [JsonStore(Bucket = "properties", Key = "score")]
            public float Score { get; set; }

            [JsonStore(Bucket = "properties2", Key = "types")]
            public int[] Types { get; set; }
            [JsonStore(Bucket = "properties2", Key = "dic")]
            public Dictionary<string, string> Dic { get; set; }
        }

        [Test]
        public void InsertByEntity_UseKey_pure()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var insert = db.Insert<Person>().SetEntity(new Person
            {
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = [1, 2]
            });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var ent = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
                ent.Id.ShouldBe(1);
                ent.Age.ShouldBe(18);
                ent.Score.ShouldBe(99);
                ent.Types.ShouldBe(new int[] { 1, 2 });
                ent.Dic.ShouldBe(new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } });
            }
        }

        [Test]
        public void InsertByEntity_UseKey_setremove()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");

            var insert = db.Insert<Person>().SetEntity(new Person
            {
                Age = 18,
                Dic = new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } },
                Score = 99,
                Types = new int[] { 1, 2 }
            }).SetColumnExpr(i => i.Age, 60).SetColumnExpr(i => i.Types, new[] { 8, 9 });

            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json,properties2 json)");
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var ent = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
                ent.Id.ShouldBe(1);
                ent.Age.ShouldBe(60);
                ent.Score.ShouldBe(99);
                ent.Types.ShouldBe(new int[] { 8, 9 });
                ent.Dic.ShouldBe(new Dictionary<string, string> { { "age", "20" }, { "name", "tom" } });
            }
        }
        #endregion

        #region nestedclass
        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "ext")]
            public Info Ext { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext2")]
            public Info Ext2 { get; set; }

        }

        class Info
        {
            public string[] Addrs { get; set; }
            public float Score { get; set; }
            public EnumSex Sex { get; set; }
        }

        enum EnumSex
        {
            None, Male, FeMale
        }

        [Test]
        public void TestNested()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");

            var insert = db.Insert<Person2>()
                .SetColumnExpr(i => i.Ext, new Info
                {
                    Addrs = ["天命", "幸福"]
                })
                .SetColumnExpr(i => i.Ext2, new Info
                {
                    Score = 95.56f,
                    Sex = EnumSex.FeMale
                });
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var person = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
                person.ShouldNotBeNull();
                person.Id.ShouldBe(1);
                person.Ext.Addrs.ShouldBe(new string[] { "天命", "幸福" });
                person.Ext2.Score.ShouldBe(95.56f);
                person.Ext2.Sex.ShouldBe(EnumSex.FeMale);
            }
        }

        [Test]
        public void TestNested_setremove()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");

            var insert = db.Insert<Person2>()
                .SetColumnExpr(i => i.Ext, new Info
                {
                    Addrs = new string[] { "天命", "幸福" }
                })
                .SetColumnExpr(i => i.Ext2, new Info
                {
                    Score = 95.56f,
                    Sex = EnumSex.FeMale
                })
                .SetColumnExpr(i => i.Ext, () => new Info
                {
                    Sex = EnumSex.Male
                }).IgnoreColumnsExpr(i => i.Ext2);
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var person = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
                person.ShouldNotBeNull();
                person.Id.ShouldBe(1);
                person.Ext.Sex.ShouldBe(EnumSex.Male);
                person.Ext.Score.ShouldBe(0);
                person.Ext.Addrs.ShouldBeNull();
                person.Ext2.ShouldBeNull();
            }
        }
        #endregion

        #region jsonobject&jsonarray

        [Table("test")]
        class Person3
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "ext")]
            public JsonObject Ext { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext2")]
            public JsonArray Ext2 { get; set; }

        }
        [Test]
        public void TestJsonObject()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");

            var insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext, new JsonObject(null).SetFluent("name", "tom").SetFluent("age", 18));
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var person = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
                person.ShouldNotBeNull();
                person.Id.ShouldBe(1);
                person.Ext.ToJson().ShouldBe(new JsonObject(null).SetFluent("age", 18).SetFluent("name", "tom").ToJson());
            }
        }

        [Test]
        public void TestJsonObject_SetProp()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");

            var insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext, () => new JsonObject(new KeyValuePair<string, JsonNode>[] { }, null).SetFluent("name", "小明"));
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var person = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
                person.ShouldNotBeNull();
                person.Id.ShouldBe(1);
                person.Ext.ToJson().ShouldBe(new JsonObject(null).SetFluent("name", "小明").ToJson());
            }
        }

        [Test]
        public void TestJsonObject_Remove()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");

            var insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext, () => new JsonObject(new KeyValuePair<string, JsonNode>[] { new KeyValuePair<string, JsonNode>("age", 18) }, null)
                .SetFluent("name", "小明")
                .RemoveFluent("age"));
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var person = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
                person.ShouldNotBeNull();
                person.Id.ShouldBe(1);
                person.Ext.ToJson().ShouldBe(new JsonObject(null).SetFluent("name", "小明").ToJson());
            }
        }

        [Test]
        public void TestJsonObjectList()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            var name = "小明";
            var insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext, new JsonObject(new KeyValuePair<string, JsonNode>[] {
                        new KeyValuePair<string, JsonNode>("name",name),
                        new KeyValuePair<string, JsonNode>("age",20)
                    }, null).SetFluent("addr", "天明路"));
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate(name);
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate(name);

            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            name = "小红";
            insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext, new JsonObject(new KeyValuePair<string, JsonNode>[] {
                        new KeyValuePair<string, JsonNode>("name",name),
                        new KeyValuePair<string, JsonNode>("age",20)
                    }, null).SetFluent("addr", "天明路"));
            dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate(name);
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate(name);

            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext, new JsonObject(new KeyValuePair<string, JsonNode>[] {
                        new KeyValuePair<string, JsonNode>("name","小刚"),
                        new KeyValuePair<string, JsonNode>("age",20)
                    }, null).SetFluent("addr", "天明路"));
            dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate("小刚");
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate("小刚");

            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext, new JsonObject(new KeyValuePair<string, JsonNode>[] {
                        new KeyValuePair<string, JsonNode>("name","小王"),
                        new KeyValuePair<string, JsonNode>("age",20)
                    }, null).SetFluent("addr", "天明路"));
            dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate("小王");
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate("小王");

            void validate(string name)
            {
                var person = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
                person.ShouldNotBeNull();
                person.Id.ShouldBe(1);
                person.Ext.ToJson().ShouldBe(new JsonObject(null).SetFluent("age", 20).SetFluent("addr", "天明路").SetFluent("name", name).ToJson());
            }
        }

        [Test]
        public void TestJsonArray()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");

            var insert = db.Insert<Person3>().SetColumnExpr(i => i.Ext2, new[] { new { Name = "tom", Age = 18 }, new { Name = "jim", Age = 20 } }.ToJson().ToObject<JsonArray>());
            var dt = insert.ToDataTable();
            db.BulkCopy(dt);
            validate();
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key auto_increment,properties json)");
            insert.ExecuteAffrows();
            validate();

            void validate()
            {
                var person = db.Select<Person3>().Where(i => i.Id == 1).FirstOrDefault();
                person.ShouldNotBeNull();
                person.Id.ShouldBe(1);
                person.Ext2.ToJson().ShouldBe(new[] { new { Age = 18, Name = "tom" }, new { Age = 20, Name = "jim" } }.ToJson());
            }
        }
        #endregion

        #region dic
        [Table("test")]
        class Test3
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public Dictionary<string, string> Ext { get; set; }

            [JsonStore(Bucket = "properties2")]
            public Dictionary<string, string[]> Ext2 { get; set; }
        }

        [Test]
        public void TestDic()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,properties json,properties2 json)");

            var keys = new List<string> { "name", "age", "addr" };
            var insert = db.Insert<Test3>().SetColumnExpr(i => i.Ext, new Dictionary<string, string>() { { "name", "小明" }, { keys.LastOrDefault(), "hha" } });
            var sql = insert.ToSql();
            //sql.ShouldBe("""
            //    insert into test(properties) values(json_object('ext',cast('{"name":"小明","addr":"hha"}' as json)));
            //    """);
            insert.ExecuteAffrows();
            var ent = db.Select<Test3>().Where(i => i.Id == 1).FirstOrDefault();
            ent.Ext["name"].ShouldBe("小明");
            ent.Ext["addr"].ShouldBe("hha");

            insert = db.Insert<Test3>().SetColumnExpr(i => i.Ext2, new Dictionary<string, string[]>() { { "name", new[] { "天命" } }, { keys.LastOrDefault(), new string[0] } });
            sql = insert.ToSql();
            //sql.ShouldBe(@"insert into test(properties2) values('{""name"":[""天命""],""addr"":[]}');");
            insert.ExecuteAffrows();
            ent = db.Select<Test3>().Where(i => i.Id == 2).FirstOrDefault();
            ent.Ext2["name"].ShouldBe(new string[] { "天命" });
            ent.Ext2["addr"].ShouldBe(new string[0]);

        }

        [Table("test")]
        class Test4
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public Dictionary<string, Dictionary<string, object>> Ext { get; set; }

            [JsonStore(Bucket = "properties2")]
            public Dictionary<string, Dictionary<string, object>> Ext2 { get; set; }
        }
        [Test]
        public void TestNestedDic()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,properties json,properties2 json)");

            var keys = new List<string> { "name", "age" };
            var insert = db.Insert<Test4>()
                .SetColumnExpr(i => i.Ext, new Dictionary<string, Dictionary<string, object>> { { "info", new Dictionary<string, object> { { "name", "小明" }, { keys.LastOrDefault(), 18 } } } })
                .SetColumnExpr(i => i.Ext2, new Dictionary<string, Dictionary<string, object>> { { "info", new Dictionary<string, object> { { "name", "小明" }, { keys.LastOrDefault(), 18 }, { "detail", new { id = 1, score = 99.5 } } } } });
            var sql = insert.ToSql();
            //sql.ShouldBe("""
            //    insert into test(properties,properties2) values(json_object('ext',cast('{"info":{"name":"小明","age":18}}' as json)),'{"info":{"name":"小明","age":18,"detail":{"id":1,"score":99.5}}}');
            //    """);
            insert.ExecuteAffrows();
            var ent = db.Select<Test4>().Where(i => i.Id == 1).FirstOrDefault();
            ent.Ext.ToJson().ShouldBe(@"{""info"":{""age"":18,""name"":""小明""}}");
            ent.Ext2.ToJson().ShouldBe(@"{""info"":{""age"":18,""name"":""小明"",""detail"":{""id"":1,""score"":99.5}}}");
        }

        [Test]
        public void TestDicSet()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,properties json,properties2 json)");

            var keys = new List<string> { "name", "age" };
            var insert = db.Insert<Test4>().SetEntity(new Test4())
                 .SetColumnExpr(i => i.Ext, () => new Dictionary<string, Dictionary<string, object>> { { "info", new Dictionary<string, object> { { "name", "小明" }, { keys.LastOrDefault(), 18 } } } })
                 .SetColumnExpr(i => i.Ext2, () => new Dictionary<string, Dictionary<string, object>> { { "info", new Dictionary<string, object> { { "name", "小明" }, { keys.LastOrDefault(), 18 }, { "detail", new { id = 1, score = 99.5 } } } } });
            var sql = insert.ToSql();
            //sql.ShouldBe("""
            //    insert into test(properties,properties2) values(json_object('ext',cast('{"info":{"name":"小明","age":18}}' as json)),'{"info":{"name":"小明","age":18,"detail":{"id":1,"score":99.5}}}');
            //    """);
            insert.ExecuteAffrows();
            var ent = db.Select<Test4>().Where(i => i.Id == 1).FirstOrDefault();
            ent.Ext.ToJson().ShouldBe(@"{""info"":{""age"":18,""name"":""小明""}}");
            ent.Ext2.ToJson().ShouldBe(@"{""info"":{""age"":18,""name"":""小明"",""detail"":{""id"":1,""score"":99.5}}}");
        }
        #endregion

        #region TestNoSet
        [Table("test")]
        class Test8
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public Detail Ext { get; set; }
        }
        class Detail
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }
        [Test]
        public void TestNoSet()
        {
            var sql = "";
            //插入一条
            var names = new List<string>() { "刘备", "关羽" };
            var insert = db.Insert<Test8>().SetColumnExpr(i => i.Ext, () => new Detail
            {
                Id = 1,
                Name = names.LastOrDefault()
            });
            insert.Type.ToString().ShouldBe("InsertByDto");
            sql = insert.ToSql();
            //sql.ShouldBe("""
            //    insert into test(properties) values(json_object('ext',cast('{"Id":1,"Name":"关羽","Age":0}' as json)));
            //    """);
        }
        #endregion
    }
}