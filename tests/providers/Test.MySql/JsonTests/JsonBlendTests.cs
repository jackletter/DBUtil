﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DotNetCommon.Extensions;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonBlendTests : TestBase
    {
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "ext")]
            public ExtInfo Ext { get; set; }

            [JsonStore(Bucket = "properties", Key = "desc")]
            public string Desc { get; set; }
            [JsonStore(Bucket = "properties", Key = "desc2")]
            public string Desc2 { get; set; }
            [JsonStore(Bucket = "properties", Key = "addrs")]
            public string[] Addrs { get; set; }
            [JsonStore(Bucket = "properties", Key = "addrs2")]
            public List<string> Addrs2 { get; set; }

            [JsonStore(Bucket = "properties2")]
            public List<string> Addrs3 { get; set; }
            [JsonStore(Bucket = "properties3")]
            public string Addrs4 { get; set; }
        }
        public class ExtInfo
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Name2 { get; set; }
            public List<string> Addrs { get; set; }
            public List<ExtInfo> Addrs2 { get; set; }
            public List<string> Addrs3 { get; set; }
            public string[] Addrs4 { get; set; }
            public ExtInfo Child { get; set; }
            public Simple Simple { get; set; }
        }

        public class Simple
        {
            public string Name { get; set; }
        }
        [Test]
        public void TestJson()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,properties json,properties2 json,properties3 json)");

            db.Insert<Person>().SetEntity(new Person
            {
                Ext = new ExtInfo
                {
                    Id = 1,
                    Name = "小明",
                    Addrs = new List<string> { "tom", "jim" }
                }
            }).ExecuteAffrows();

            var sql = string.Empty;
            var list = new List<string>() { "-的" };
            sql = db.Update<Person>().SetExpr(i => new Person
            {
                Desc = "描述" + list.FirstOrDefault(),
                Desc2 = i.Desc2 + "好",
                Addrs = new[] { "天明路", "幸福路", list.FirstOrDefault() + "陆" },
                Addrs2 = i.Addrs2.AddFluent("明天"),
                Ext = new ExtInfo
                {
                    Name = "小红" + list.FirstOrDefault(),
                    Child = new ExtInfo
                    {
                        Id = 1,
                        Name = "消化"
                    },
                    Name2 = i.Ext.Name2 + "2",
                    Addrs = i.Ext.Addrs.AddFluent("lisa"),
                    Addrs2 = new List<ExtInfo> { new ExtInfo { Id = 2, Name = "小明" }, new ExtInfo { Name = "3", Name2 = i.Ext.Addrs2[0].Name2 + "5" } },
                    Addrs3 = new List<string> { "a", "b", list.FirstOrDefault() + "c" },
                    Addrs4 = new[] { "tom", i.Ext.Addrs4[2] },
                    Simple = new Simple { Name = "ko" }
                },
                Addrs3 = new List<string> { "ab" + list.FirstOrDefault(), "cd" },
                Addrs4 = list.FirstOrDefault() + "12" + i.Addrs4
            }).Where(i => i.Id == 1).ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"','小红-的',
            //            '$."ext"."Child"',cast(ifnull(json_value(properties,'$."ext"."Child"'),'{}') as json),
            //            '$."ext"."Child"."Id"',1,
            //            '$."ext"."Child"."Name"','消化',
            //            '$."ext"."Name2"',concat_ws('',json_value(json_value(properties,'$."ext"'),'$."Name2"' returning char),'2'),
            //            '$."ext"."Addrs"',cast(ifnull(json_value(properties,'$."ext"."Addrs"'),'[]') as json),
            //            '$."ext"."Addrs"',json_array_append(ifnull(json_value(json_value(properties,'$."ext"'),'$."Addrs"'),'[]'),'$','lisa'),
            //            '$."ext"."Addrs2"',cast(ifnull(json_value(properties,'$."ext"."Addrs2"'),'[]') as json),
            //            '$."ext"."Addrs2"',json_array(json_object('Id',2,'Name','小明'),json_object('Name','3','Name2',concat_ws('',json_value(json_value(json_value(json_value(properties,'$."ext"'),'$."Addrs2"'),'$[0]'),'$."Name2"' returning char),'5'))),
            //            '$."ext"."Addrs3"',cast(ifnull(json_value(properties,'$."ext"."Addrs3"'),'[]') as json),
            //            '$."ext"."Addrs3"',cast('["a","b","-的c"]' as json),
            //            '$."ext"."Addrs4"',cast(ifnull(json_value(properties,'$."ext"."Addrs4"'),'[]') as json),
            //            '$."ext"."Addrs4"',json_array('tom',json_value(json_value(json_value(properties,'$."ext"'),'$."Addrs4"'),'$[2]' returning char)),
            //            '$."ext"."Simple"',cast(ifnull(json_value(properties,'$."ext"."Simple"'),'{}') as json),
            //            '$."ext"."Simple"."Name"','ko',
            //            '$."desc"','描述-的',
            //            '$."desc2"',concat_ws('',json_value(properties,'$."desc2"' returning char),'好'),
            //            '$."addrs"',cast('["天明路","幸福路","-的陆"]' as json),
            //            '$."addrs2"',json_array_append(ifnull(json_value(properties,'$."addrs2"'),'[]'),'$','明天')),
            //        properties2 = '["ab-的","cd"]',
            //        properties3 = json_quote(concat_ws('','-的12',json_value(properties3,'$' returning char)))
            //    where `Id` = 1;
            //    """);
            db.ExecuteSql(sql);
            var ent = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            ent.Desc.ShouldBe("描述" + list.FirstOrDefault());
            ent.Desc2.ShouldBe("好");
            ent.Addrs.ShouldBe(["天明路", "幸福路", list.FirstOrDefault() + "陆"]);
            ent.Addrs2.ShouldBe(new[] { "明天" });
            ent.Ext.Name.ShouldBe("小红" + list.FirstOrDefault());
            ent.Ext.Child.Id.ShouldBe(1);
            ent.Ext.Name.ShouldBe("小红-的");
            ent.Ext.Name2.ShouldBe("2");
            ent.Ext.Addrs.ShouldBe(new List<string> { "tom", "jim", "lisa" });
            ent.Ext.Addrs2.Count.ShouldBe(2);
            ent.Ext.Addrs2[0].Id.ShouldBe(2);
            ent.Ext.Addrs2[0].Name.ShouldBe("小明");
            ent.Ext.Addrs2[1].Name.ShouldBe("3");
            ent.Ext.Addrs2[1].Name2.ShouldBe("5");
            ent.Ext.Addrs3.ShouldBe(new[] { "a", "b", "-的c" });
            ent.Ext.Addrs4.ShouldBe(new List<string> { "tom", null });
            ent.Ext.Simple.Name.ShouldBe("ko");
            ent.Id.ShouldBe(1);

            var (id, properties) = db.SelectModel<(int id, string properties)>("select * from test where id=1");
            id.ShouldBe(1);
            properties.ShouldBe("{\"ext\": {\"Id\": 1, \"Name\": \"小红-的\", \"Addrs\": [\"tom\", \"jim\", \"lisa\"], \"Child\": {\"Id\": 1, \"Name\": \"消化\"}, \"Name2\": \"2\", \"Addrs2\": [{\"Id\": 2, \"Name\": \"小明\"}, {\"Name\": \"3\", \"Name2\": \"5\"}], \"Addrs3\": [\"a\", \"b\", \"-的c\"], \"Addrs4\": [\"tom\", null], \"Simple\": {\"Name\": \"ko\"}}, \"desc\": \"描述-的\", \"addrs\": [\"天明路\", \"幸福路\", \"-的陆\"], \"desc2\": \"好\", \"addrs2\": [\"明天\"]}");
        }
    }
}
