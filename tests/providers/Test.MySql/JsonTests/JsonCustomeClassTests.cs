﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.JsonTests
{
    [TestFixture]
    internal class JsonCustomeClassTests : TestBase
    {
        #region 独占json
        [Table("test")]
        class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties")]
            public Ext Ext { get; set; }

            [JsonStore(Bucket = "properties2")]
            public Ext Ext2 { get; set; }

            [JsonStore(Bucket = "properties3")]
            public Ext Ext3 { get; set; }
        }

        class Ext
        {
            public int? Id { get; set; }
            public string Name { get; set; }
            public Detail Detail { get; set; }
            public Detail Detail2 { get; set; }
        }

        class Detail
        {
            public int? Id { get; set; }
            public string Addr { get; set; }
            public int? Age { get; set; }
        }

        [Test]
        public void Test_Json_Along_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var ext = new Ext
            {
                Name = "jim",
                Detail = new Detail
                {
                    Id = 1,
                    Addr = "addr"
                },
                Detail2 = new Detail
                {
                    Addr = "addr"
                }
            };
            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Ext = ext
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = '{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}'
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
        }

        [Test]
        public void Test_Json_Along_CaseCade_Custome_Part_Update()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();
            //独占json列
            //自定义类
            //局部更新
            var update = db.Update<Person>()
                 .SetExpr(() => new Person
                 {
                     Name = "tom",
                     Ext = new Ext
                     {
                         Name = "jim",
                         Detail = new Detail
                         {
                             Id = 1,
                             Addr = "addr"
                         },
                         Detail2 = new Detail
                         {
                             Addr = "addr"
                         }
                     }
                 })
                 .SetColumnExpr(i => i.Ext2, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                         Addr = "addr"
                     },
                     Detail2 = new Detail
                     {
                         Addr = "addr"
                     }
                 })
                 .SetColumnExpr(i => i.Ext3, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                     }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        name = 'tom',
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."Name"','jim',
            //            '$."Detail"',cast(ifnull(json_value(properties,'$."Detail"'),'{}') as json),
            //            '$."Detail"."Id"',1,
            //            '$."Detail"."Addr"','addr',
            //            '$."Detail2"',cast(ifnull(json_value(properties,'$."Detail2"'),'{}') as json),
            //            '$."Detail2"."Addr"','addr'),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."Name"','jim',
            //            '$."Detail"',cast(ifnull(json_value(properties2,'$."Detail"'),'{}') as json),
            //            '$."Detail"."Id"',1,
            //            '$."Detail"."Addr"','addr',
            //            '$."Detail2"',cast(ifnull(json_value(properties2,'$."Detail2"'),'{}') as json),
            //            '$."Detail2"."Addr"','addr'),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."Name"','jim',
            //            '$."Detail"',cast(ifnull(json_value(properties3,'$."Detail"'),'{}') as json),
            //            '$."Detail"."Id"',1)
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext2.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext3.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":null,"Age":null},"Detail2":null}""");
        }

        [Test]
        public void Test_Json_Along_CaseCade_Custome_Part_Update_WithParam()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();
            //独占json列
            //自定义类
            //局部更新
            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Name = "@" + i.Name + "tom",
                     Ext = new Ext
                     {
                         Name = "jim" + i.Ext.Name + i.Name.Length.ToString(),
                         Detail = new Detail
                         {
                             Id = 1 + i.Ext.Detail.Id.IfNullUse(0),
                             Addr = "addr"
                         },
                         Detail2 = new Detail
                         {
                             Addr = "addr"
                         }
                     }
                 })
                 .SetColumnExpr(i => i.Ext2, i => new Ext
                 {
                     Name = "jim" + i.Ext.Name + i.Name.Length.ToString(),
                     Detail = new Detail
                     {
                         Id = 1 + i.Ext.Detail.Id.IfNullUse(0),
                         Addr = "addr"
                     },
                     Detail2 = new Detail
                     {
                         Addr = "addr"
                     }
                 })
                 .SetColumnExpr(i => i.Ext3, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                     }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        name = concat_ws('',concat_ws('','@',name),'tom'),
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."Name"',concat_ws('',concat_ws('','jim',json_value(json_value(properties,'$'),'$."Name"' returning char)),convert(length(name),char)),
            //            '$."Detail"',cast(ifnull(json_value(properties,'$."Detail"'),'{}') as json),
            //            '$."Detail"."Id"',1 + (ifnull(json_value(json_value(json_value(properties,'$'),'$."Detail"'),'$."Id"' returning signed),0)),
            //            '$."Detail"."Addr"','addr',
            //            '$."Detail2"',cast(ifnull(json_value(properties,'$."Detail2"'),'{}') as json),
            //            '$."Detail2"."Addr"','addr'),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."Name"',concat_ws('',concat_ws('','jim',json_value(json_value(properties,'$'),'$."Name"' returning char)),convert(length(name),char)),
            //            '$."Detail"',cast(ifnull(json_value(properties2,'$."Detail"'),'{}') as json),
            //            '$."Detail"."Id"',1 + (ifnull(json_value(json_value(json_value(properties,'$'),'$."Detail"'),'$."Id"' returning signed),0)),
            //            '$."Detail"."Addr"','addr',
            //            '$."Detail2"',cast(ifnull(json_value(properties2,'$."Detail2"'),'{}') as json),
            //            '$."Detail2"."Addr"','addr'),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."Name"','jim',
            //            '$."Detail"',cast(ifnull(json_value(properties3,'$."Detail"'),'{}') as json),
            //            '$."Detail"."Id"',1)
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":"jim8","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext2.ToJson().ShouldBe("""{"Id":null,"Name":"jimjim88","Detail":{"Id":2,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext3.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":null,"Age":null},"Detail2":null}""");
        }

        [Test]
        public void Test_Json_Along_CaseCade_Custome_All_Update()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();
            //独占Json列
            //自定义列
            //全量更新
            var ext = new Ext
            {
                Name = "jim",
                Detail = new Detail
                {
                    Id = 1,
                    Addr = "addr"
                },
                Detail2 = new Detail
                {
                    Addr = "addr"
                }
            };
            var update = db.Update<Person>()
                 .SetExpr(i => new Person
                 {
                     Name = "tom",
                     Ext = ext
                 })
                 .SetColumnExpr(i => i.Ext2, ext)
                 .SetColumnExpr(i => i.Ext3, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                     }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        name = 'tom',
            //        properties = '{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}',
            //        properties2 = '{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}',
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."Name"','jim',
            //            '$."Detail"',cast(ifnull(json_value(properties3,'$."Detail"'),'{}') as json),
            //            '$."Detail"."Id"',1)
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext2.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext3.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":null,"Age":null},"Detail2":null}""");
        }
        #endregion

        #region 非独占json
        [Table("test")]
        class Person2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "properties", Key = "ext")]
            public Ext Ext { get; set; }

            [JsonStore(Bucket = "properties2", Key = "ext")]
            public Ext Ext2 { get; set; }

            [JsonStore(Bucket = "properties3", Key = "ext")]
            public Ext Ext3 { get; set; }
        }

        [Test]
        public void Test_Json_NotAlong_AllUpdate()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();

            var ext = new Ext
            {
                Name = "jim",
                Detail = new Detail
                {
                    Id = 1,
                    Addr = "addr"
                },
                Detail2 = new Detail
                {
                    Addr = "addr"
                }
            };
            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Ext = ext
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}' as json))
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":null,"Detail":null,"Detail2":null}""");
        }

        [Test]
        public void Test_Json_NotAlong_CaseCade_Custome_Part_Update()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();
            //独占json列
            //自定义类
            //局部更新
            var update = db.Update<Person2>()
                 .SetExpr(() => new Person2
                 {
                     Name = "tom",
                     Ext = new Ext
                     {
                         Name = "jim",
                         Detail = new Detail
                         {
                             Id = 1,
                             Addr = "addr"
                         },
                         Detail2 = new Detail
                         {
                             Addr = "addr"
                         }
                     }
                 })
                 .SetColumnExpr(i => i.Ext2, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                         Addr = "addr"
                     },
                     Detail2 = new Detail
                     {
                         Addr = "addr"
                     }
                 })
                 .SetColumnExpr(i => i.Ext3, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                     }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        name = 'tom',
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"','jim',
            //            '$."ext"."Detail"',cast(ifnull(json_value(properties,'$."ext"."Detail"'),'{}') as json),
            //            '$."ext"."Detail"."Id"',1,
            //            '$."ext"."Detail"."Addr"','addr',
            //            '$."ext"."Detail2"',cast(ifnull(json_value(properties,'$."ext"."Detail2"'),'{}') as json),
            //            '$."ext"."Detail2"."Addr"','addr'),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"','jim',
            //            '$."ext"."Detail"',cast(ifnull(json_value(properties2,'$."ext"."Detail"'),'{}') as json),
            //            '$."ext"."Detail"."Id"',1,
            //            '$."ext"."Detail"."Addr"','addr',
            //            '$."ext"."Detail2"',cast(ifnull(json_value(properties2,'$."ext"."Detail2"'),'{}') as json),
            //            '$."ext"."Detail2"."Addr"','addr'),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"','jim',
            //            '$."ext"."Detail"',cast(ifnull(json_value(properties3,'$."ext"."Detail"'),'{}') as json),
            //            '$."ext"."Detail"."Id"',1)
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext2.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext3.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":null,"Age":null},"Detail2":null}""");
        }

        [Test]
        public void Test_Json_NotAlong_CaseCade_Custome_Part_Update_WithParam()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();
            //独占json列
            //自定义类
            //局部更新
            var update = db.Update<Person2>()
                 .SetExpr(i => new Person2
                 {
                     Name = "@" + i.Name + "tom",
                     Ext = new Ext
                     {
                         Name = "jim" + i.Ext.Name + i.Name.Length.ToString(),
                         Detail = new Detail
                         {
                             Id = 1 + i.Ext.Detail.Id,
                             Addr = "addr"
                         },
                         Detail2 = new Detail
                         {
                             Addr = "addr"
                         }
                     }
                 })
                 .SetColumnExpr(i => i.Ext2, i => new Ext
                 {
                     Name = "jim" + i.Ext.Name + i.Name.Length.ToString(),
                     Detail = new Detail
                     {
                         Id = 1 + i.Ext.Detail.Id,
                         Addr = "addr"
                     },
                     Detail2 = new Detail
                     {
                         Addr = "addr"
                     }
                 })
                 .SetColumnExpr(i => i.Ext3, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                     }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        name = concat_ws('',concat_ws('','@',name),'tom'),
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"',concat_ws('',concat_ws('','jim',json_value(json_value(properties,'$."ext"'),'$."Name"' returning char)),convert(length(name),char)),
            //            '$."ext"."Detail"',cast(ifnull(json_value(properties,'$."ext"."Detail"'),'{}') as json),
            //            '$."ext"."Detail"."Id"',1 + (json_value(json_value(json_value(properties,'$."ext"'),'$."Detail"'),'$."Id"' returning signed)),
            //            '$."ext"."Detail"."Addr"','addr',
            //            '$."ext"."Detail2"',cast(ifnull(json_value(properties,'$."ext"."Detail2"'),'{}') as json),
            //            '$."ext"."Detail2"."Addr"','addr'),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"',concat_ws('',concat_ws('','jim',json_value(json_value(properties,'$."ext"'),'$."Name"' returning char)),convert(length(name),char)),
            //            '$."ext"."Detail"',cast(ifnull(json_value(properties2,'$."ext"."Detail"'),'{}') as json),
            //            '$."ext"."Detail"."Id"',1 + (json_value(json_value(json_value(properties,'$."ext"'),'$."Detail"'),'$."Id"' returning signed)),
            //            '$."ext"."Detail"."Addr"','addr',
            //            '$."ext"."Detail2"',cast(ifnull(json_value(properties2,'$."ext"."Detail2"'),'{}') as json),
            //            '$."ext"."Detail2"."Addr"','addr'),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"','jim',
            //            '$."ext"."Detail"',cast(ifnull(json_value(properties3,'$."ext"."Detail"'),'{}') as json),
            //            '$."ext"."Detail"."Id"',1)
            //    where id = 1;
            //    """);
            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":"jim8","Detail":{"Id":null,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext2.ToJson().ShouldBe("""{"Id":null,"Name":"jimjim88","Detail":{"Id":null,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext3.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":null,"Age":null},"Detail2":null}""");
        }

        [Test]
        public void Test_Json_NotAlong_CaseCade_Custome_All_Update()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50),properties json,properties2 json,properties3 json)");
            db.Insert<Person2>().SetColumnExpr(i => i.Name, "lisa").ExecuteIdentity();
            //独占Json列
            //自定义列
            //全量更新
            var ext = new Ext
            {
                Name = "jim",
                Detail = new Detail
                {
                    Id = 1,
                    Addr = "addr"
                },
                Detail2 = new Detail
                {
                    Addr = "addr"
                }
            };
            var update = db.Update<Person2>()
                 .SetExpr(() => new Person2
                 {
                     Name = "tom",
                     Ext = ext
                 })
                 .SetColumnExpr(i => i.Ext2, ext)
                 .SetColumnExpr(i => i.Ext3, i => new Ext
                 {
                     Name = "jim",
                     Detail = new Detail
                     {
                         Id = 1,
                     }
                 })
                 .Where(i => i.Id == 1);
            var sql = update.ToSql();
            //sql.ShouldBe("""
            //    update test set
            //        name = 'tom',
            //        properties = json_set(ifnull(properties,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}' as json)),
            //        properties2 = json_set(ifnull(properties2,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties2,'$."ext"'),'{}') as json),
            //            '$."ext"',cast('{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}' as json)),
            //        properties3 = json_set(ifnull(properties3,json_object()),
            //            '$."ext"',cast(ifnull(json_value(properties3,'$."ext"'),'{}') as json),
            //            '$."ext"."Name"','jim',
            //            '$."ext"."Detail"',cast(ifnull(json_value(properties3,'$."ext"."Detail"'),'{}') as json),
            //            '$."ext"."Detail"."Id"',1)
            //    where id = 1;
            //    """);

            update.ExecuteAffrows().ShouldBe(1);
            var p = db.Select<Person2>().Where(i => i.Id == 1).FirstOrDefault();
            p.Ext.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext2.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":"addr","Age":null},"Detail2":{"Id":null,"Addr":"addr","Age":null}}""");
            p.Ext3.ToJson().ShouldBe("""{"Id":null,"Name":"jim","Detail":{"Id":1,"Addr":null,"Age":null},"Detail2":null}""");
        }
        #endregion
    }
}
