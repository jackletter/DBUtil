﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.JsonTests.AnyAllTests
{
    [TestFixture]
    internal class ListAnyAllComplexTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [JsonStore(Bucket = "ext")]
            public List<Student> Students { get; set; }
            [JsonStore(Bucket = "scores")]
            public List<double> Scores { get; set; }
        }
        public class Student
        {
            public string Name { get; set; }
            public int Age { get; set; }
        }

        [Table("t_cat")]
        public class Category
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [JsonStore(Bucket = "exts")]
            public List<CategoryExt> Exts { get; set; }
        }
        public class CategoryExt
        {
            public int Id { get; set; }
            public double Score { get; set; }
            public List<CategoryOther> Others { get; set; }
        }
        public class CategoryOther
        {
            public int Id { get; set; }
            public int Year { get; set; }
            public List<Other2> Other2s { get; set; }
        }
        public class Other2
        {
            public int Id { get; set; }
            public List<int> StuIds { get; set; }
        }

        [Table("t_testarr")]
        public class TestArr
        {
            public int Id { get; set; }
            [JsonStore(Bucket = "exts")]
            public List<TestArrExt> Exts { get; set; }
        }
        public class TestArrExt
        {
            public List<int> Scores { get; set; }
            public int[] StuIds { get; set; }
            public IList<int> StuIds2 { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""
                create table test(id int primary key auto_increment,name varchar(50),ext json,scores json);
                insert into test(name,ext,scores) values
                ('LaoWang','[{"Name":"tom","Age":18},{"Name":"tom2","Age":19}]','[80,75,90]'),
                ('LaoLi','[{"Name":"lisa","Age":20},{"Name":"lisa2","Age":16}]','[40,50,30]'),
                ('王明','[{"Name":"王明","Age":35},{"Name":"小明","Age":16}]','[80,50,100]'),
                ('刘备','[{"Name":"刘备","Age":40},{"Name":"关羽","Age":36}]','[99,80,70]');
                """);

            DropTableIfExist("t_cat");
            db.ExecuteSql("""
                create table t_cat(id int primary key auto_increment,name varchar(50),exts json);
                insert into t_cat(name,exts) values
                ('中国','[{"Id":1,"Score":99.5,"Others":[{"Id":1,"Year":1990,"Other2s":[{"Id":1,"StuIds":[2,5,9]}]}]}]'),
                ('河南','[{"Id":2,"Score":85,"Others":[{"Id":2,"Year":1989,"Other2s":[{"Id":2,"StuIds":[2,10,3]}]}]}]'),
                ('郑州','[{"Id":3,"Score":75,"Others":[{"Id":3,"Year":1975,"Other2s":[{"Id":3,"StuIds":[2,6,9]}]}]}]'),
                ('洛阳','[{"Id":4,"Score":69.5,"Others":[{"Id":4,"Year":1996,"Other2s":[{"Id":4,"StuIds":[1,5,9]}]}]}]');
                """);

            //t_testarr
            DropTableIfExist("t_testarr");
            db.ExecuteSql("""
                create table t_testarr(id int primary key auto_increment,exts json);
                insert into t_testarr(exts) values
                ('[{"Scores":[53,23,96,45],"StuIds":[5,6],"StuIds2":[1,2,6,10]}]'),
                ('[{"Scores":[96,43,85,65],"StuIds":[5,10,8],"StuIds2":[1,2,6,10]}]'),
                ('[{"Scores":[86,76,62,77],"StuIds":[4,5,6],"StuIds2":[1,2,6,10]}]'),
                ('[{"Scores":[50,45,38,45],"StuIds":[5,8,10],"StuIds2":[1,2,6,10]}]');
                """);
        }

        [Test]
        public void TestAnySimple()
        {
            var select = db.Select<Person>().Where(i => i.Students.Any(i => i.Age > 18));
            var sql = select.ToSqlList();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where 0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i.Age` int path '$.Age')) t where t.`i.Age` > 18 limit 1);
            //    """);
            var json = select.ToList().ToJson();
            json.ShouldBe("""[{"Id":1,"Name":"LaoWang","Students":[{"Name":"tom","Age":18},{"Name":"tom2","Age":19}],"Scores":[80,75,90]},{"Id":2,"Name":"LaoLi","Students":[{"Name":"lisa","Age":20},{"Name":"lisa2","Age":16}],"Scores":[40,50,30]},{"Id":3,"Name":"王明","Students":[{"Name":"王明","Age":35},{"Name":"小明","Age":16}],"Scores":[80,50,100]},{"Id":4,"Name":"刘备","Students":[{"Name":"刘备","Age":40},{"Name":"关羽","Age":36}],"Scores":[99,80,70]}]""");

        }

        [Test]
        public void TestAnyDirectAndSimple()
        {
            var select = db.Select<Person>().Where(i => i.Scores.Any(i => i > 60) && i.Students.Any(i => i.Age > 18));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where (0<(select 1 from json_table(json_value(t.scores,'$'),'$[*]' columns(`i` decimal(30,15) path '$')) t where t.`i` > 60 limit 1)) and (0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i.Age` int path '$.Age')) t where t.`i.Age` > 18 limit 1));
            //    """);
            var json = select.ToList().ToJson();
            json.ShouldBe("""[{"Id":1,"Name":"LaoWang","Students":[{"Name":"tom","Age":18},{"Name":"tom2","Age":19}],"Scores":[80,75,90]},{"Id":3,"Name":"王明","Students":[{"Name":"王明","Age":35},{"Name":"小明","Age":16}],"Scores":[80,50,100]},{"Id":4,"Name":"刘备","Students":[{"Name":"刘备","Age":40},{"Name":"关羽","Age":36}],"Scores":[99,80,70]}]""");
        }

        [Test]
        public void TestAnyDisturb()
        {
            var score = 60f;
            var select = db.Select<Person>().Where(i => i.Id > 1 && i.Scores.Any(i => i > score && i + 10 < 90) && i.Name.StartsWith("刘") && i.Students.Any(i => i.Age > 18));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where (((t.id > 1) and (0<(select 1 from json_table(json_value(t.scores,'$'),'$[*]' columns(`i` decimal(30,15) path '$')) t where (t.`i` > 60) and ((t.`i` + 10) < 90) limit 1))) and (t.name like '刘%')) and (0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i.Age` int path '$.Age')) t where t.`i.Age` > 18 limit 1));
            //    """);
            var json = select.ToList().ToJson();
            json.ShouldBe("""[{"Id":4,"Name":"刘备","Students":[{"Name":"刘备","Age":40},{"Name":"关羽","Age":36}],"Scores":[99,80,70]}]""");
        }

        [Test]
        public void TestAnyNested()
        {
            var select = db.Select<Category>().Where(i => i.Exts.Any(i => i.Others.Any(j => j.Year > 1980 && j.Other2s.Any(k => k.Id > 1 && k.StuIds.Any(p => p == 5)))));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.exts,'$') `Exts`
            //    from t_cat t
            //    where 0<(select 1 from json_table(json_value(t.exts,'$'),'$[*]' columns(`i.Others` json path '$.Others')) t where 0<(select 1 from json_table(t.`i.Others`,'$[*]' columns(`j.Year` int path '$.Year',`j.Other2s` json path '$.Other2s')) t where (t.`j.Year` > 1980) and (0<(select 1 from json_table(t.`j.Other2s`,'$[*]' columns(`k.Id` int path '$.Id',`k.StuIds` json path '$.StuIds')) t where (t.`k.Id` > 1) and (0<(select 1 from json_table(t.`k.StuIds`,'$[*]' columns(`p` int path '$')) t where t.`p` = 5 limit 1)) limit 1)) limit 1) limit 1);
            //    """);
            var list = select.ToList();
            list.ToJson().ShouldBe("""[{"Id":4,"Name":"洛阳","Exts":[{"Id":4,"Score":69.5,"Others":[{"Id":4,"Year":1996,"Other2s":[{"Id":4,"StuIds":[1,5,9]}]}]}]}]""");
        }
        [Test]
        public void TestAnyArrIndex()
        {
            var select = db.Select<TestArr>().Where(i => i.Exts.Any(j => j.Scores[2] > 60) || i.Exts.Any(j => j.StuIds[0] == 5 && j.StuIds2[2] == 10));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.exts,'$') `Exts`
            //    from t_testarr t
            //    where (0<(select 1 from json_table(json_value(t.exts,'$'),'$[*]' columns(`j.Scores` json path '$.Scores')) t where (json_value(t.`j.Scores`,'$[2]' returning signed)) > 60 limit 1)) or (0<(select 1 from json_table(json_value(t.exts,'$'),'$[*]' columns(`j.StuIds` json path '$.StuIds',`j.StuIds2` json path '$.StuIds2')) t where ((json_value(t.`j.StuIds`,'$[0]' returning signed)) = 5) and ((json_value(t.`j.StuIds2`,'$[2]' returning signed)) = 10) limit 1));
            //    """);
            var list = select.ToList();
            list.ToJson().ShouldBe("""[{"Id":1,"Exts":[{"Scores":[53,23,96,45],"StuIds":[5,6],"StuIds2":[1,2,6,10]}]},{"Id":2,"Exts":[{"Scores":[96,43,85,65],"StuIds":[5,10,8],"StuIds2":[1,2,6,10]}]},{"Id":3,"Exts":[{"Scores":[86,76,62,77],"StuIds":[4,5,6],"StuIds2":[1,2,6,10]}]}]""");
        }

        [Test]
        public void TestDefaultDbType()
        {
            db.Setting.SetGetDefaultDbtype(type =>
            {
                if (type == typeof(string)) return "varchar(50)";
                return null;
            });
            var sql = db.Select<Person>().Where(i => i.Students.Any(i => i.Name.StartsWith("王"))).ToSqlList();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where 0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i.Name` varchar(50) path '$.Name')) t where t.`i.Name` like '王%' limit 1);
            //    """);
        }
    }
}
