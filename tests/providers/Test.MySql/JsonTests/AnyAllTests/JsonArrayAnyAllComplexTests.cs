﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Nodes;

namespace Test.MySql.JsonTests.AnyAllTests
{
    [TestFixture]
    internal class JsonArrayAnyAllComplexTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [JsonStore(Bucket = "ext")]
            public JsonArray Students { get; set; }
            [JsonStore(Bucket = "scores")]
            public JsonArray Scores { get; set; }
        }

        [Table("t_cat")]
        public class Category
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [JsonStore(Bucket = "exts")]
            public JsonArray Exts { get; set; }
        }

        [Table("t_testarr")]
        public class TestArr
        {
            public int Id { get; set; }
            [JsonStore(Bucket = "exts")]
            public JsonArray Exts { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""
                create table test(id int primary key auto_increment,name varchar(50),ext json,scores json);
                insert into test(name,ext,scores) values
                ('LaoWang','[{"Name":"tom","Age":18},{"Name":"tom2","Age":19}]','[80,75,90]'),
                ('LaoLi','[{"Name":"lisa","Age":20},{"Name":"lisa2","Age":16}]','[40,50,30]'),
                ('王明','[{"Name":"王明","Age":35},{"Name":"小明","Age":16}]','[80,50,100]'),
                ('刘备','[{"Name":"刘备","Age":40},{"Name":"关羽","Age":36}]','[99,80,70]');
                """);

            DropTableIfExist("t_cat");
            db.ExecuteSql("""
                create table t_cat(id int primary key auto_increment,name varchar(50),exts json);
                insert into t_cat(name,exts) values
                ('中国','[{"Id":1,"Score":99.5,"Others":[{"Id":1,"Year":1990,"Other2s":[{"Id":1,"StuIds":[2,5,9]}]}]}]'),
                ('河南','[{"Id":2,"Score":85,"Others":[{"Id":2,"Year":1989,"Other2s":[{"Id":2,"StuIds":[2,10,3]}]}]}]'),
                ('郑州','[{"Id":3,"Score":75,"Others":[{"Id":3,"Year":1975,"Other2s":[{"Id":3,"StuIds":[2,6,9]}]}]}]'),
                ('洛阳','[{"Id":4,"Score":69.5,"Others":[{"Id":4,"Year":1996,"Other2s":[{"Id":4,"StuIds":[1,5,9]}]}]}]');
                """);

            //t_testarr
            DropTableIfExist("t_testarr");
            db.ExecuteSql("""
                create table t_testarr(id int primary key auto_increment,exts json);
                insert into t_testarr(exts) values
                ('[{"Scores":[53,23,96,45],"StuIds":[5,6],"StuIds2":[1,2,6,10]}]'),
                ('[{"Scores":[96,43,85,65],"StuIds":[5,10,8],"StuIds2":[1,2,6,10]}]'),
                ('[{"Scores":[86,76,62,77],"StuIds":[4,5,6],"StuIds2":[1,2,6,10]}]'),
                ('[{"Scores":[50,45,38,45],"StuIds":[5,8,10],"StuIds2":[1,2,6,10]}]');
                """);
        }

        [Test]
        public void TestAnySimple()
        {
            var select = db.Select<Person>().Where(i => i.Students.Any(i => i.AsObject()["Age"].GetValue<int>() > 18));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where 0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i` json path '$')) t where (json_value(t.`i`,'$."Age"' returning signed)) > 18 limit 1);
            //    """);
            var json = select.ToList().ToJson();
            json.ShouldBe("""[{"Id":1,"Name":"LaoWang","Students":[{"Age":18,"Name":"tom"},{"Age":19,"Name":"tom2"}],"Scores":[80,75,90]},{"Id":2,"Name":"LaoLi","Students":[{"Age":20,"Name":"lisa"},{"Age":16,"Name":"lisa2"}],"Scores":[40,50,30]},{"Id":3,"Name":"王明","Students":[{"Age":35,"Name":"王明"},{"Age":16,"Name":"小明"}],"Scores":[80,50,100]},{"Id":4,"Name":"刘备","Students":[{"Age":40,"Name":"刘备"},{"Age":36,"Name":"关羽"}],"Scores":[99,80,70]}]""");

        }

        [Test]
        public void TestAnyDirectAndSimple()
        {
            var select = db.Select<Person>().Where(i => i.Scores.Any(i => i.GetValue<double>() > 60) && i.Students.Any(i => i.AsObject()["Age"].To<int>() > 18));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where (0<(select 1 from json_table(json_value(t.scores,'$'),'$[*]' columns(`i` json path '$')) t where (json_value(t.`i`,'$' returning double)) > 60 limit 1)) and (0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i` json path '$')) t where convert(json_value(t.`i`,'$."Age"'),decimal) > 18 limit 1));
            //    """);
            var json = select.ToList().ToJson();
            json.ShouldBe("""[{"Id":1,"Name":"LaoWang","Students":[{"Age":18,"Name":"tom"},{"Age":19,"Name":"tom2"}],"Scores":[80,75,90]},{"Id":3,"Name":"王明","Students":[{"Age":35,"Name":"王明"},{"Age":16,"Name":"小明"}],"Scores":[80,50,100]},{"Id":4,"Name":"刘备","Students":[{"Age":40,"Name":"刘备"},{"Age":36,"Name":"关羽"}],"Scores":[99,80,70]}]""");
        }

        [Test]
        public void TestAnyDisturb()
        {
            var score = 60f;
            var select = db.Select<Person>().Where(i => i.Id > 1 && i.Scores.Any(i => i.To<double>() > score && i.GetValue<double>() + 10 < 90) && i.Name.StartsWith("刘") && i.Students.Any(i => i.AsObject()["Age"].GetValue<int>() > 18));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where (((t.id > 1) and (0<(select 1 from json_table(json_value(t.scores,'$'),'$[*]' columns(`i` json path '$')) t where (convert(t.`i`,decimal(30,15)) > 60) and (((json_value(t.`i`,'$' returning double)) + 10) < 90) limit 1))) and (t.name like '刘%')) and (0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i` json path '$')) t where (json_value(t.`i`,'$."Age"' returning signed)) > 18 limit 1));
            //    """);
            var json = select.ToList().ToJson();
            json.ShouldBe("""[{"Id":4,"Name":"刘备","Students":[{"Age":40,"Name":"刘备"},{"Age":36,"Name":"关羽"}],"Scores":[99,80,70]}]""");
        }

        [Test]
        public void TestAnyNested()
        {
            var select = db.Select<Category>().Where(i => i.Exts.Any(i => i["Others"].AsArray().Any(j => j["Year"].GetValue<int>() > 1980 && j["Other2s"].AsArray().Any(k => k["Id"].GetValue<int>() > 1 && k["StuIds"].AsArray().Any(p => p.GetValue<int>() == 5)))));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.exts,'$') `Exts`
            //    from t_cat t
            //    where 0<(select 1 from json_table(json_value(t.exts,'$'),'$[*]' columns(`i` json path '$')) t where 0<(select 1 from json_table(json_value(t.`i`,'$."Others"'),'$[*]' columns(`j` json path '$')) t where ((json_value(t.`j`,'$."Year"' returning signed)) > 1980) and (0<(select 1 from json_table(json_value(t.`j`,'$."Other2s"'),'$[*]' columns(`k` json path '$')) t where ((json_value(t.`k`,'$."Id"' returning signed)) > 1) and (0<(select 1 from json_table(json_value(t.`k`,'$."StuIds"'),'$[*]' columns(`p` json path '$')) t where (json_value(t.`p`,'$' returning signed)) = 5 limit 1)) limit 1)) limit 1) limit 1);
            //    """);
            var list = select.ToList();
            list.ToJson().ShouldBe("""[{"Id":4,"Name":"洛阳","Exts":[{"Id":4,"Score":69.5,"Others":[{"Id":4,"Year":1996,"Other2s":[{"Id":4,"StuIds":[1,5,9]}]}]}]}]""");
        }
        [Test]
        public void TestAnyArrIndex()
        {
            var select = db.Select<TestArr>().Where(i => i.Exts.Any(j => j["Scores"].AsArray()[2].GetValue<double>() > 60) || i.Exts.Any(j => j["StuIds"].AsArray()[0].GetValue<int>() == 5 && j["StuIds2"].AsArray()[2].GetValue<int>() == 10));
            var sql = select.ToSql();
            //sql.ShouldBe("""
            //    select t.`Id`,json_value(t.exts,'$') `Exts`
            //    from t_testarr t
            //    where (0<(select 1 from json_table(json_value(t.exts,'$'),'$[*]' columns(`j` json path '$')) t where (json_value(json_value(t.`j`,'$."Scores"'),'$[2]' returning double)) > 60 limit 1)) or (0<(select 1 from json_table(json_value(t.exts,'$'),'$[*]' columns(`j` json path '$')) t where ((json_value(json_value(t.`j`,'$."StuIds"'),'$[0]' returning signed)) = 5) and ((json_value(json_value(t.`j`,'$."StuIds2"'),'$[2]' returning signed)) = 10) limit 1));
            //    """);
            var list = select.ToList();
            list.ToJson().ShouldBe("""[{"Id":1,"Exts":[{"Scores":[53,23,96,45],"StuIds":[5,6],"StuIds2":[1,2,6,10]}]},{"Id":2,"Exts":[{"Scores":[96,43,85,65],"StuIds":[5,10,8],"StuIds2":[1,2,6,10]}]},{"Id":3,"Exts":[{"Scores":[86,76,62,77],"StuIds":[4,5,6],"StuIds2":[1,2,6,10]}]}]""");
        }

        [Test]
        public void TestDefaultDbType()
        {
            //这里的 SetGetDefaultDbtype 实际没起作用
            db.Setting.SetGetDefaultDbtype(type =>
            {
                if (type == typeof(string)) return "varchar(50)";
                return null;
            });
            var sql = db.Select<Person>().Where(i => i.Students.Any(i => i["Name"].ToString().StartsWith("王"))).ToSqlList();
            //sql.ShouldBe("""
            //    select t.id `Id`,t.name `Name`,json_value(t.ext,'$') `Students`,json_value(t.scores,'$') `Scores`
            //    from test t
            //    where 0<(select 1 from json_table(json_value(t.ext,'$'),'$[*]' columns(`i` json path '$')) t where convert(json_value(t.`i`,'$."Name"'),char) like '王%' limit 1);
            //    """);
        }
    }
}
