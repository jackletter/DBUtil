﻿using System;
using System.IO;
using System.Text;
using DotNetCommon.Extensions;
using System.Diagnostics;
using NUnit.Framework;
using System.Threading.Tasks;
using DBUtil;

namespace Test.MySql.ManageTests
{
    [TestFixture]
    public sealed class ManageTests : TestBase
    {
        #region 测试生成insert语句 GeneInsertSql
        [Test]
        public void GeneInsertSqlTest()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"CREATE TABLE test (
	t_bigint BIGINT NULL,
	t_bigint_unsigned BIGINT UNSIGNED NULL,
	t_binary BINARY(255) NULL,
	t_bit BIT NULL,
	t_blob BLOB NULL,
	t_bool BOOL NULL,
	t_char CHAR NULL,
	t_date DATE NULL,
	t_datetime DATETIME NULL,
	t_decimal DECIMAL NULL,
	t_double DOUBLE NULL,
	t_double_precision DOUBLE PRECISION NULL,
	t_enum ENUM('reg','green','blue') NULL,
	t_float FLOAT NULL,
	t_int INT NULL,
	t_int_unsigned INT UNSIGNED NULL,
	t_integer INTEGER NULL,
	t_integer_unsigned INTEGER UNSIGNED NULL,
	t_long_varbinary LONG VARBINARY NULL,
	t_long_varchar LONG VARCHAR NULL,
	t_longblob LONGBLOB NULL,
	t_longtext LONGTEXT NULL,
	t_mediumblob MEDIUMBLOB NULL,
	t_mediumint MEDIUMINT NULL,
	t_mediumint_unsigned MEDIUMINT UNSIGNED NULL,
	t_mediumtext MEDIUMTEXT NULL,
	t_numeric NUMERIC NULL,
	t_real REAL NULL,
	t_set SET('red','green','blue') NULL,
	t_smallint SMALLINT NULL,
	t_smallint_unsigned SMALLINT UNSIGNED NULL,
	t_text TEXT NULL,
	t_time TIME NULL,
	t_timestamp TIMESTAMP NULL,
	t_tinyblob TINYBLOB NULL,
	t_tinyint TINYINT NULL,
	t_tinyint_unsigned TINYINT UNSIGNED NULL,
	t_tinytext TINYTEXT NULL,
	t_varbinary VARBINARY(100) NULL,
	t_varchar VARCHAR(100) NULL,
	t_year YEAR NULL,
	t_json json NULL
)");
            var res = db.Insert("test",
                new
                {
                    t_bigint = 1,
                    t_bigint_unsigned = 2,
                    t_binary = Encoding.UTF8.GetBytes("t_binary"),
                    t_bit = 0,
                    t_blob = Encoding.UTF8.GetBytes("t_blob"),
                    t_bool = true,
                    t_char = 'c',
                    t_date = DateTime.Now.ToCommonDateString(),
                    t_datetime = DateTime.Now,
                    t_decimal = 12.354515,
                    t_double = 12.3645,
                    t_double_precision = 14.023,
                    t_enum = "blue",
                    t_float = 0.56326,
                    t_int = 3,
                    t_int_unsigned = 4,
                    t_integer = 5,
                    t_integer_unsigned = 6,
                    t_long_varbinary = Encoding.UTF8.GetBytes("t_long_varbinary"),
                    t_long_varchar = "t_long_varchar",
                    t_longblob = Encoding.UTF8.GetBytes("t_longblob"),
                    t_longtext = "t_longtext",
                    t_mediumblob = Encoding.UTF8.GetBytes("t_mediumblob"),
                    t_mediumint = 7,
                    t_mediumint_unsigned = 8,
                    t_mediumtext = "t_mediumtext",
                    t_numeric = 9.69,
                    t_real = 10.23,
                    t_set = "red,blue",
                    t_smallint = 11,
                    t_smallint_unsigned = 12,
                    t_text = "t_text",
                    t_time = DateTime.Now.ToCommonTimeString(),
                    t_tinyblob = Encoding.UTF8.GetBytes("t_tinyblob"),
                    t_tinyint = 13,
                    t_tinyint_unsigned = 14,
                    t_tinytext = "t_tinytext",
                    t_varbinary = Encoding.UTF8.GetBytes("t_varbinary"),
                    t_varchar = "t_varchar",
                    t_year = DateTime.Now.Year,
                    t_json = new { name = "小明", age = 20 }.ToJson()
                }.ToDictionary()).ExecuteAffrows();
            Assert.IsTrue(res == 1);
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = Path.Combine(path, "out.sql");
            var count = db.Manage.GenerateInsertSqlFile("test", path);
            Debug.WriteLine($"生成的sql文件:{path}");
            Assert.IsTrue(count == 1);
        }

        [Test]
        public void GeneInsertSqlStringTest()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key,name varchar(50))");
            db.Insert("test", new { id = 1, name = "i'am xiaoming,what's it?" }.ToDictionary()).ExecuteAffrows();
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = Path.Combine(path, "out.sql");
            db.Manage.GenerateInsertSqlFile("test", path);
        }
        #endregion

        [Test]
        public async Task Show()
        {
            var conn = "Server=rm-uf675652wh920otgtao.mysql.rds.aliyuncs.com;Database=modelmanagement;Uid=modelmanagement;Pwd=NrlKPRsXeyHNnCr3IX;Charset=utf8mb4;";
            var db = DBFactory.CreateDB(DBType.MYSQL, conn);
            var count = db.Manage.GenerateInsertSqlFile("apilog_innerapi", "d:\\apilog_innerapi.sql");
        }

        [Test]
        public void Show2()
        {
            //var sql = "show create table student;show create table t4;";
            //var ds = db.SelectDataSet(sql);
            //int i = 0;

        }
    }
}
