﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;

namespace Test.MySql.Inserts
{
    [TestFixture]
    internal class InsertBatchTests : TestBase
    {
        #region model
        [Table("t_person")]
        public class Person
        {
            [PrimaryKey]
            public int Id { get; set; }
            public string Name1 { get; set; }
            public string Name2 { get; set; }
            public string Name3 { get; set; }
            public string Name4 { get; set; }
            public string Name5 { get; set; }
            public string Name6 { get; set; }
            public string Name7 { get; set; }
            public string Name8 { get; set; }
            public string Name9 { get; set; }
            public string Name10 { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("t_person");
            db.ExecuteSql(
                """
                create table t_person(
                    Id int primary key auto_increment,
                    Name1 varchar(50),
                    Name2 varchar(50),
                    Name3 varchar(50),
                    Name4 varchar(50),
                    Name5 varchar(50),
                    Name6 varchar(50),
                    Name7 varchar(50),
                    Name8 varchar(50),
                    Name9 varchar(50),
                    Name10 varchar(50)
                )
                """);
        }

        [Test]
        public void Test()
        {
            //大批量的插入, insert语句插入的行数本身并没有限制, 但是传递个server的包大小有: max_allowed_packet
            var count = 10000 * 100;
            var list = Enumerable.Range(1, count).Select(i => new Person
            {
                Id = i,
                Name1 = "jack_" + i,
                Name2 = "jack_" + i,
                Name3 = "jack_" + i,
                Name4 = "jack_" + i,
                Name5 = "jack_" + i,
                Name6 = "jack_" + i,
                Name7 = "jack_" + i,
                Name8 = "jack_" + i,
                Name9 = "jack_" + i,
                Name10 = "jack_" + i,
            }).ToList();
            try
            {
                var r = db.Insert(list).ExecuteAffrows();
                throw new Exception("如此大量的数据插入竟然没有报错!");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("""Error submitting 153MB packet; ensure 'max_allowed_packet' is greater than 153MB.""");
            }
            //分批插入

        }

        [Test]
        public void TestBatchDictionary()
        {
            //大批量的插入, insert语句插入的行数本身并没有限制, 但是传递个server的包大小有: max_allowed_packet
            var count = 11;
            var list = Enumerable.Range(1, count).Select(i => new Person
            {
                Id = i,
                Name1 = "Name1_" + i,
                Name2 = "Name2_" + i,
                Name3 = "Name3_" + i,
                Name4 = "Name4_" + i,
                Name5 = "Name5_" + i,
                Name6 = "Name6_" + i,
                Name7 = "Name7_" + i,
                Name8 = "Name8_" + i,
                Name9 = "Name9_" + i,
                Name10 = "Name10_" + i,
            }.ToDictionary().RemoveFluent("Id")).ToList();
            var oneSql = db.Insert("t_person", list).BatchOption(5).ToSql();
            var oneSql2 = db.Insert("t_person", list).BatchOption(5).ToSql(DBUtil.EnumInsertToSql.ExecuteIdentity);
            var multiSql = db.Insert("t_person", list).BatchOption(5).ToSqlMulti();
            var multiSql2 = db.Insert("t_person", list).BatchOption(5).ToSqlMulti(DBUtil.EnumInsertToSql.ExecuteIdentity);
            Console.WriteLine("ok");
        }

        [Test]
        public void TestLargeBatchDictionary()
        {
            //大批量的插入, insert语句插入的行数本身并没有限制, 但是传递个server的包大小有: max_allowed_packet
            var count = 10000 * 100;
            var list = Enumerable.Range(1, count).Select(i => new Person
            {
                Id = i,
                Name1 = "Name1_" + i,
                Name2 = "Name2_" + i,
                Name3 = "Name3_" + i,
                Name4 = "Name4_" + i,
                Name5 = "Name5_" + i,
                Name6 = "Name6_" + i,
                Name7 = "Name7_" + i,
                Name8 = "Name8_" + i,
                Name9 = "Name9_" + i,
                Name10 = "Name10_" + i,
            }.ToDictionary().RemoveFluent("Id")).ToList();
            var st = new Stopwatch();
            for (int i = 0; i < 10; i++)
            {
                st.Restart();
                var multiSql2 = db.Insert("t_person", list).BatchOption(1000).ToSqlMulti(DBUtil.EnumInsertToSql.ExecuteIdentity);
                Console.WriteLine($"第{i + 1}次耗时: {st.ElapsedMilliseconds} ms");
                st.Stop();
            }
            Console.WriteLine("ok");
        }

        [Test]
        public void TestBatchDictionarySetIgnore()
        {
            //大批量的插入, insert语句插入的行数本身并没有限制, 但是传递个server的包大小有: max_allowed_packet
            var count = 11;
            var list = Enumerable.Range(1, count).Select(i => new Person
            {
                Id = i,
                Name1 = "Name1_" + i,
                Name2 = "Name2_" + i,
                Name3 = "Name3_" + i,
                Name4 = "Name4_" + i,
                Name5 = "Name5_" + i,
                Name6 = "Name6_" + i,
                Name7 = "Name7_" + i,
                Name8 = "Name8_" + i,
                Name9 = "Name9_" + i,
                Name10 = "Name10_" + i,
            }.ToDictionary().RemoveFluent("Id")).ToList();
            var multiSql2 = db.Insert("t_person", list).BatchOption(5)
                .SetColumn("Name1", "jack")
                .ToSqlMulti(DBUtil.EnumInsertToSql.ExecuteIdentity);
            var dt = db.Insert("t_person", list).BatchOption(5)
                .SetColumn("Name1", "jack")
                .ToDataTable();
            Console.WriteLine("ok");
        }

        [Test]
        public void TestBatchEntity()
        {
            //大批量的插入, insert语句插入的行数本身并没有限制, 但是传递个server的包大小有: max_allowed_packet
            var count = 11;
            var list = Enumerable.Range(1, count).Select(i => new Person
            {
                Id = i,
                Name1 = "jack_" + i,
                Name2 = "jack_" + i,
                Name3 = "jack_" + i,
                Name4 = "jack_" + i,
                Name5 = "jack_" + i,
                Name6 = "jack_" + i,
                Name7 = "jack_" + i,
                Name8 = "jack_" + i,
                Name9 = "jack_" + i,
                Name10 = "jack_" + i,
            }).ToList();
            var multiSql = db.Insert(list).BatchOption(5).ToSqlMulti();
            var multi2Sql = db.Insert(list).BatchOption(5).ToSqlMulti(DBUtil.EnumInsertToSql.ExecuteInserted);

            Console.WriteLine("ok");
        }
    }
}
