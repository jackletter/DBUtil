﻿using DBUtil;
using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Inserts.InsertDto
{
    [TestFixture]
    internal class Index : TestBase
    {
        #region model NormalUser
        [Table("t_user")]
        public class NormalUser
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }

            [Column("addr")]
            public string Addr { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }
        }

        [Table("t_user")]
        public class NormalUserIdentity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }

            [Column("addr")]
            public string Addr { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }
        }
        #endregion

        [Test]
        public void TestToSql_Custom()
        {
            db.ExecuteSql(db.Manage.DropTableIfExistSql("t_user"));
            db.ExecuteSql("create table t_user(id int auto_increment primary key,name varchar(50),age int,birth datetime,addr varchar(200))");

            var sql = "";
            //插入一条
            var insert = db.Insert<NormalUser>()
                .SetColumnExpr(i => i.Id, 1)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");
            sql = insert.ToSql();
            sql.ShouldBe("insert into t_user(id,age,name) values(1,20,'小明');");

            //ToDataTable
            var dt = insert.ToDataTable();
            dt.TableName.ShouldBe("t_user");
            dt.Rows.Count.ShouldBe(1);
            dt.Columns.Count.ShouldBe(3);
            dt.Rows[0]["name"].ShouldBe("小明");
            dt.Rows[0]["age"].ShouldBe(20);

            var r = insert.ExecuteAffrows();
            r.ShouldBe(1);

            //ExecuteInserted
            insert = db.Insert<NormalUser>()
                .SetColumnExpr(i => i.Id, 2)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");
            sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(id,age,name) values(2,20,'小明');
select id `Id`,name `Name`,age `Age`,addr `Addr`,birth `Birth` from t_user where id = 2;");
            var d = insert.ExecuteInserted();
            d.ShouldNotBeNull();
            d.Id.ShouldBe(2);
            d.Age.ShouldBe(20);
            d.Name.ShouldBe("小明");
            d.Addr.ShouldBeNull();
            d.Birth.ShouldBeNull();

            //with SetColumn
            insert = db.Insert<NormalUser>()
                .SetColumnExpr(i => i.Id, 3)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");
            insert.SetColumn("age", 25);
            sql = insert.ToSql();
            sql.ShouldBe("insert into t_user(id,name,age) values(3,'小明',25);");
            r = insert.ExecuteAffrows();
            r.ShouldBe(1);

            //with RemoveAllColumnsExcept
            insert = db.Insert<NormalUser>()
                .SetColumnExpr(i => i.Id, 4)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");
            insert.SetColumn("age", 25);
            insert.OnlyColumns("id", "name");
            sql = insert.ToSql();
            sql.ShouldBe("insert into t_user(id,name) values(4,'小明');");
            r = insert.ExecuteAffrows();
            r.ShouldBe(1);

            //with SetProp
            insert = db.Insert<NormalUser>()
                .SetColumnExpr(i => i.Id, 5)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");
            insert.SetColumn("age", 25);
            insert.OnlyColumns("id", "name");
            insert.SetColumnExpr(i => i.Name, "newname");
            insert.SetColumnExpr(i => i.Age, 23);
            sql = insert.ToSql();
            sql.ShouldBe("insert into t_user(id,name,age) values(5,'newname',23);");
            r = insert.ExecuteAffrows();
            r.ShouldBe(1);

            //only SetProp
            insert = db.Insert<NormalUser>()
                .SetColumnExpr(i => i.Id, 6)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");
            insert.SetColumnExpr(i => i.Addr, "127.0.0.1");
            insert.SetColumnExpr(i => i.Age, 40);
            sql = insert.ToSql();
            sql.ShouldBe("insert into t_user(id,name,addr,age) values(6,'小明','127.0.0.1',40);");
            r = insert.ExecuteAffrows();
            r.ShouldBe(1);

            //with RemoveAllPropsExcept
            insert = db.Insert<NormalUser>()
                .SetColumnExpr(i => i.Id, 7)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");
            insert.OnlyColumnsExpr(i => new { i.Id, i.Age, i.Name });
            sql = insert.ToSql();
            sql.ShouldBe("insert into t_user(id,age,name) values(7,20,'小明');");
            r = insert.ExecuteAffrows();
            r.ShouldBe(1);

            //validate error
            try
            {
                sql = insert.ToSql(EnumInsertToSql.ExecuteIdentity);
                throw new Exception("error");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("非自增主键或已声明插入自增列,无法获取自动生成的主键值!");
            }
        }

        [Test]
        public void TestToSql_Identity()
        {
            var sql = "";
            //插入一条
            var insert = db.Insert<NormalUserIdentity>()
                .SetColumnExpr(i => i.Id, 1)
                .SetColumnExpr(i => i.Age, 20)
                .SetColumnExpr(i => i.Name, "小明");

            //ToSql
            sql = insert.ToSql();
            sql.ShouldBe("insert into t_user(id,age,name) values(1,20,'小明');");

            //ExecuteIdentity
            sql = insert.ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(id,age,name) values(1,20,'小明');
select last_insert_id();");

            //ExecuteInserted
            sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(id,age,name) values(1,20,'小明');
select id `Id`,name `Name`,age `Age`,addr `Addr`,birth `Birth` from t_user where id=last_insert_id();");

            //set InsertIdentity and ExecuteInserted
            insert.InsertIdentity();
            sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(id,age,name) values(1,20,'小明');
select id `Id`,name `Name`,age `Age`,addr `Addr`,birth `Birth` from t_user where id = 1;");

            //validate error
            try
            {
                //声明插入自增id,则不能调用 ExecuteIdentity
                insert.InsertIdentity();
                sql = insert.ToSql(EnumInsertToSql.ExecuteIdentity);
                throw new Exception("error");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("非自增主键或已声明插入自增列,无法获取自动生成的主键值!");
            }

            //先去掉 set InsertIdentity 的影响
            //setremove的优先级高,不受主键策略影响
            insert.SwitchInsertIdentity(false);
            insert.SetColumn("id", 100);
            sql = insert.ToSql();
            sql.ShouldBe(@"insert into t_user(age,name,id) values(20,'小明',100);");
        }

        [Test]
        public void TestNoSet()
        {
            var sql = "";
            //插入一条
            var insert = db.Insert<NormalUser>().SetColumnExpr(i => i.Age, 20);
            insert.Type.ShouldBe(EnumInsertBuilderType.InsertByDto);
            sql = insert.ToSql();
            sql.ShouldBe(@"insert into t_user(age) values(20);");
        }

        [Test]
        public void TestRawString()
        {
            db.ExecuteSql(db.Manage.DropTableIfExistSql("t_user"));
            db.ExecuteSql("create table t_user(id int auto_increment primary key,name varchar(50),age int,birth datetime,addr varchar(200))");
            db.ExecuteSql("""
                insert into t_user(id,name) values(1,'jack');
                """);

            db.Insert<NormalUser>().SetColumnExpr(i => i.Name, new RawString(db.Select<NormalUser>().Where(i => i.Id == 1).ToSqlFirstOrDefault(i => i.Name))).ExecuteAffrows();
            //
            //insert into t_user(name) values((select t.name
            //from t_user t
            //where t.id = 1
            //limit 1));
            var json = db.Select<NormalUser>().ToList().ToJson();
            json.ShouldBe("""[{"Id":1,"Name":"jack","Age":null,"Addr":null,"Birth":null},{"Id":2,"Name":"jack","Age":null,"Addr":null,"Birth":null}]""");
        }
    }
}
