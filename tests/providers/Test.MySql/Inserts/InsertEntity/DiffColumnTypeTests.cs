﻿using DBUtil;
using DBUtil.Attributes;
using DBUtil.Builders;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Test.MySql.Inserts.InsertEntity
{
    [TestFixture]
    public class DiffColumnTypeTests : TestBase
    {
        #region TestBit
        [Table("test")]
        internal class TestBitEntity : TestBase
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("bit1")]
            public bool Bit1 { get; set; }
            [Column("bit8", TypeName = "bit(8)")]
            public sbyte Bit8 { get; set; }
            [Column("bit16")]
            public ushort Bit16 { get; set; }
            [Column("bit32")]
            public uint Bit32 { get; set; }
            [Column("bit64")]
            public ulong Bit64 { get; set; }
            [Column("bitint")]
            public int BitNormal { get; set; }
        }
        [Test]
        public void TestBit()
        {
            DropTable("test");
            db.ExecuteSql(@"
create table test(
    id int auto_increment primary key,
    name varchar(50),
    bit1 bit(1),
    bit8 bit(8),
    bit16 bit(16),
    bit32 bit(32),
    bit64 bit(64),
    bitint bit(32)
)");

            var insert = db.Insert<TestBitEntity>().SetEntity(new TestBitEntity
            {
                Name = "小明",
                Bit1 = true,
                Bit8 = 0x12,
                Bit16 = 0xAB1C,
                Bit32 = 0x1234_1201,
                Bit64 = 0xAB_CD_EF_12,
                BitNormal = 18
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into test(name,bit1,bit8,bit16,bit32,bit64,bitint) values('小明',1,18,43804,305402369,2882400018,18);
select id `Id`,name `Name`,bit1 `Bit1`,bit8 `Bit8`,bit16 `Bit16`,bit32 `Bit32`,bit64 `Bit64`,bitint `BitNormal` from test where id=last_insert_id();");

            var row = insert.ExecuteInserted();
            row.Id.ShouldBe(1);
            row.Name.ShouldBe("小明");
            row.Bit1.ShouldBe(true);
            row.Bit8.ShouldBe((sbyte)0x12);
            row.Bit16.ShouldBe((ushort)0xAB1C);
            row.Bit32.ShouldBe((uint)0x1234_1201);
            row.Bit64.ShouldBe(0xAB_CD_EF_12);
            row.BitNormal.ShouldBe(18);
        }
        #endregion

        #region TestEnumSet
        [Table("test")]
        public class TestEnumSetEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("enumtest")]
            public EnumTest EnumTest { get; set; }
            [Column("enumset")]
            public EnumSet EnumSet { get; set; }
        }
        public enum EnumTest
        {
            A = 1,
            B = 2,
            C = 3
        }

        [Flags]
        public enum EnumSet
        {
            A = 1,
            B = 2,
            C = 4,
            D = 8
        }
        [Test]
        public void TestEnumSet()
        {
            DropTable("test");
            db.ExecuteSql(@"
create table test(
    id int auto_increment primary key,
    name varchar(50),
    enumtest enum('A','B','C'),
    enumset set('A','B','C','D')
)");

            var insert = db.Insert<TestEnumSetEntity>().SetEntity(new TestEnumSetEntity
            {
                Name = "小明",
                EnumTest = EnumTest.C,
                EnumSet = EnumSet.A | EnumSet.C
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into test(name,enumtest,enumset) values('小明',3,5);
select id `Id`,name `Name`,enumtest `EnumTest`,enumset `EnumSet` from test where id=last_insert_id();");
            var row = insert.ExecuteInserted();

            row.Id.ShouldBe(1);
            row.Name.ShouldBe("小明");
            Assert.IsTrue(row.EnumTest == EnumTest.C);
            Assert.IsTrue(row.EnumSet.Contains(EnumSet.A));
            Assert.IsTrue(row.EnumSet.Contains(EnumSet.C));
        }
        #endregion

        #region TestDateTime
        [Table("test")]
        public class TestDateTimeEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("`datetime`")]
            public DateTime DateTime { get; set; }
            [Column("`timespan`")]
            public TimeSpan TimeSpan { get; set; }
            [Column("`datetimeoffset`")]
            public DateTimeOffset DateTimeOffset { get; set; }
        }
        [Test]
        public void TestDateTime()
        {
            DropTable("test");
            db.ExecuteSql(@"
create table test(
    id int auto_increment primary key,
    name varchar(50),
    `datetime` datetime,
    `timespan` time,
    `datetimeoffset` datetime
)");
            var insert = db.Insert<TestDateTimeEntity>().SetEntity(new TestDateTimeEntity
            {
                Name = "小明",
                DateTime = DateTime.Parse("1991-01-02"),
                TimeSpan = TimeSpan.FromSeconds(3000),
                DateTimeOffset = DateTimeOffset.Parse("1991-01-02 01:02:03 +08:00")
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into test(name,`datetime`,`timespan`,`datetimeoffset`) values('小明','1991-01-02','00:50:00','1991-01-02 01:02:03+08:00');
select id `Id`,name `Name`,`datetime` `DateTime`,`timespan` `TimeSpan`,`datetimeoffset` `DateTimeOffset` from test where id=last_insert_id();");
            var row = insert.ExecuteInserted();

            row.Id.ShouldBe(1);
            row.Name.ShouldBe("小明");
            Assert.IsTrue(row.DateTime == DateTime.Parse("1991-01-02"));
            Assert.IsTrue(row.TimeSpan == TimeSpan.FromSeconds(3000));
            Assert.IsTrue(row.DateTimeOffset == DateTimeOffset.Parse("1991-01-02 01:02:03 +08:00"));
        }
        #endregion

        #region TestBinary
        [Table("test")]
        public class TestBinaryEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("bytearray")]
            public byte[] ByteArray { get; set; }

            [Column("`longtext`")]
            public string LongText { get; set; }
        }
        [Test]
        public void TestBinary()
        {
            DropTable("test");
            db.ExecuteSql(@"
create table test(
    id int auto_increment primary key,
    name varchar(50),
    `bytearray` varbinary(50),
    `longtext` longtext
)");
            var insert = db.Insert<TestBinaryEntity>().SetEntity(new TestBinaryEntity
            {
                Name = "小明",
                ByteArray = Encoding.ASCII.GetBytes("abc"),
                LongText = "小明，你好!"
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("insert into test(name,bytearray,`longtext`) values('小明',0x616263,'小明，你好!');\r\nselect id `Id`,name `Name`,bytearray `ByteArray`,`longtext` `LongText` from test where id=last_insert_id();");
            var row = insert.ExecuteInserted();

            row.Id.ShouldBe(1);
            row.Name.ShouldBe("小明");
            row.ByteArray.ShouldBe(Encoding.ASCII.GetBytes("abc"));
            row.LongText.ShouldBe("小明，你好!");
        }
        #endregion

        #region TestJson
        [Table("test")]
        public class TestJsonEntity
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            public string Name { get; set; }

            [JsonStore(Bucket = "logs")]
            [Column("logs")]
            public List<LogAction> Logs { get; set; }
        }

        public class LogAction
        {
            public int Id { get; set; }
            public string Type { get; set; }
            public string Message { get; set; }
            public DateTime CreateTime { get; set; }
        }
        [Test]
        public void TestJson()
        {
            DropTable("test");
            db.ExecuteSql(@"
create table test(
    id int auto_increment primary key,
    name varchar(50),
    logs text
)");
            var dt = DateTime.Parse("2023-01-28");
            var insert = db.Insert<TestJsonEntity>().SetEntity(new TestJsonEntity
            {
                Id = 1,
                Name = "小明",
                Logs = new List<LogAction>
                {
                    new LogAction
                    {
                        Id =1,
                        Type="add",
                        Message="测试消息",
                        CreateTime=dt,
                    },
                    new LogAction
                    {
                        Id=2,
                        Type="update",
                        Message="测试更新",
                        CreateTime=dt
                    }
                }
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into test(id,`Name`,logs) values(1,'小明','[{""Id"":1,""Type"":""add"",""Message"":""测试消息"",""CreateTime"":""2023-01-28T00:00:00.0000000""},{""Id"":2,""Type"":""update"",""Message"":""测试更新"",""CreateTime"":""2023-01-28T00:00:00.0000000""}]');
select id `Id`,`Name`,json_value(logs,'$') `Logs` from test where id = 1;");
            var row = insert.ExecuteInserted();
            row.Id.ShouldBe(1);
            row.Name.ShouldBe("小明");
            row.Logs.Count.ShouldBe(2);
            row.Logs[0].Id.ShouldBe(1);
            row.Logs[0].Type.ShouldBe("add");
            row.Logs[0].Message.ShouldBe("测试消息");
            row.Logs[0].CreateTime.ShouldBe(dt);
            row.Logs[1].Id.ShouldBe(2);
            row.Logs[1].Type.ShouldBe("update");
            row.Logs[1].Message.ShouldBe("测试更新");
            row.Logs[1].CreateTime.ShouldBe(dt);
        }
        #endregion

        #region TestJsonColumn
        [Table("test")]
        public class TestJsonColumnEntity
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            public string Name { get; set; }

            [JsonStore(Key = "age", Bucket = "properties")]
            [Column("age")]
            public int Age { get; set; }

            [JsonStore(Key = "addr", Bucket = "properties")]
            [Column("addr")]
            public string Addr { get; set; }

            [JsonStore(Key = "birth", Bucket = "properties2")]
            [Column("birth")]
            public DateTime? Birth { get; set; }

            [JsonStore(Key = "createTime", Bucket = "properties")]
            [Column("create_time")]
            public DateTime CreateTime { get; set; }
        }
        [Test]
        public void TestJsonColumn()
        {
            DropTable("test");
            db.ExecuteSql(@"
create table test(
    id int auto_increment primary key,
    name varchar(50),
    properties json,
    properties2 json
)");
            var insert = db.Insert<TestJsonColumnEntity>().SetEntity(new TestJsonColumnEntity
            {
                Id = 1,
                Name = "小明",
                Addr = "天命",
                Age = 18,
                CreateTime = DateTime.Parse("2023-01-28"),
                Birth = null
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe("""
                insert into test(id,`Name`,properties,properties2) values(1,'小明',json_object('age',18,'addr','天命','createTime','2023-01-28T00:00:00.0000000'),json_object('birth',null));
                select id `Id`,`Name`,json_value(properties,'$."age"' returning signed) `Age`,json_value(properties,'$."addr"' returning char) `Addr`,convert(json_value(properties2,'$."birth"'),datetime(6)) `Birth`,convert(json_value(properties,'$."createTime"'),datetime(6)) `CreateTime` from test where id = 1;
                """);

            var row = insert.ExecuteInserted();
            row.Id.ShouldBe(1);
            row.Name.ShouldBe("小明");
            row.Addr.ShouldBe("天命");
            row.Age.ShouldBe(18);
            row.CreateTime.ShouldBe(DateTime.Parse("2023-01-28"));
            row.Birth.ShouldBeNull();
            var dt = insert.ToDataTable();
        }
        #endregion

        #region TestBoolean
        [Table("test")]
        public class TestBooleanEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("active")]
            public bool Active { get; set; }
        }
        [Test]
        public void TestBoolean()
        {
            DropTable("test");
            db.ExecuteSql(@"create table test(id int auto_increment primary key,active bit(1))");

            var insert = db.Insert<TestBooleanEntity>().SetEntity(new TestBooleanEntity
            {
                Active = true,
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into test(active) values(1);
select id `Id`,active `Active` from test where id=last_insert_id();");
            var d = insert.ExecuteInserted();
            d.Id.ShouldBe(1);
            d.Active.ShouldBeTrue();
            insert = db.Insert<TestBooleanEntity>().SetEntity(new TestBooleanEntity
            {
                Active = false,
            });
            sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into test(active) values(0);
select id `Id`,active `Active` from test where id=last_insert_id();");
            d = insert.ExecuteInserted();
            d.Id.ShouldBe(2);
            d.Active.ShouldBeFalse();
        }
        #endregion

        #region TestChar
        [Table("test")]
        public class TestCharEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("chara1")]
            public char Chara1 { get; set; }
            [Column("chara2")]
            public char Chara2 { get; set; }
        }
        [Test]
        public void TestChar()
        {
            DropTable("test");
            db.ExecuteSql(@"create table test(id int auto_increment primary key,chara1 char(10),chara2 varchar(50))");

            var insert = db.Insert<TestCharEntity>().SetEntity(new TestCharEntity
            {
                Chara1 = 'A',
                Chara2 = 'a',
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into test(chara1,chara2) values('A','a');
select id `Id`,chara1 `Chara1`,chara2 `Chara2` from test where id=last_insert_id();");
            var d = insert.ExecuteInserted();
            d.Id.ShouldBe(1);
            d.Chara1.ShouldBe('A');
            d.Chara2.ShouldBe('a');
        }
        #endregion
    }
}