﻿using DBUtil;
using DBUtil.Attributes;
using DBUtil.Builders;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Test.MySql.Inserts.InsertEntity
{
    [TestFixture]
    internal class IgnoreTests : TestBase
    {
        [Table("t_user")]
        public class IgnoreUser
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [IgnoreInsert]
            [Column("age")]
            public int? Age { get; set; }
            [Column("addr")]
            public string Addr { get; set; }
            [IgnoreWrite]
            [Column("birth")]
            public DateTime? Birth { get; set; }
        }

        [Test]
        public void ModelIgnoreTest()
        {
            DropTable("t_user");
            db.ExecuteSql(@"create table t_user(
            id int primary key,
            name varchar(50),
            age int,
            addr varchar(50),
            birth datetime
)");

            //插入一条
            var insert = db.Insert<IgnoreUser>().SetEntity(new IgnoreUser()
            {
                Id = 1,
                Age = 20,
                Name = "小明",
                Addr = "天明路",
                Birth = DateTime.Parse("1996-01-02")
            });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(id,name,addr) values(1,'小明','天明路');
select id `Id`,name `Name`,age `Age`,addr `Addr`,birth `Birth` from t_user where id = 1;");
            var user = insert.ExecuteInserted();
            user.Id.ShouldBe(1);
            user.Name.ShouldBe("小明");
            user.Addr.ShouldBe("天明路");
            user.Birth.ShouldBeNull();
            user.Age.ShouldBeNull();

            //插入2条
            insert = db.Insert<IgnoreUser>().SetEntity(new[]{new IgnoreUser()
            {
                Id = 2,
                Age = 20,
                Name = "小红",
                Addr = "天明路",
                Birth = DateTime.Parse("1996-01-02")
            }, new IgnoreUser
            {
                Id = 3,
                Age = 22,
                Name = "小刚",
                Addr = "天明路",
                Birth = DateTime.Parse("1996-01-02")
            } });
            sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(id,name,addr) values
    (2,'小红','天明路'),
    (3,'小刚','天明路');
select id `Id`,name `Name`,age `Age`,addr `Addr`,birth `Birth` from t_user where id in (2,3);");
            var users = insert.ExecuteInsertedList();
            users.Count.ShouldBe(2);
            users[0].Id.ShouldBe(2);
            users[0].Name.ShouldBe("小红");
            users[0].Addr.ShouldBe("天明路");
            users[0].Birth.ShouldBeNull();
            users[0].Age.ShouldBeNull();

            users[1].Id.ShouldBe(3);
            users[1].Name.ShouldBe("小刚");
            users[1].Addr.ShouldBe("天明路");
            users[1].Birth.ShouldBeNull();
            users[1].Age.ShouldBeNull();
        }
    }
}
