﻿using DBUtil;
using DBUtil.Attributes;
using DBUtil.Builders;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Test.MySql.Inserts.InsertEntity
{
    [TestFixture]
    internal class KeyStrategyTests : TestBase
    {
        [Table("t_user")]
        public class User2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }
        [Test]
        public void IdentityTest()
        {
            //entity
            var sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Name`,`Age`) values('刘备',0);
select last_insert_id();");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Name`,`Age`) values('刘备',0);
select `Id`,`Name`,`Age` from t_user where `Id`=last_insert_id();");
            sql = db.Insert<User2>().SetEntity(new[]{new User2
            {
                Name = "刘备"
            },new User2
            {
                Name="关羽"
            } }).ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Name`,`Age`) values
    ('刘备',0),
    ('关羽',0);
select last_insert_id()+1;");
            sql = db.Insert<User2>().SetEntity(new[]{new User2
            {
                Name = "刘备"
            },new User2
            {
                Name="关羽"
            } }).ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Name`,`Age`) values
    ('刘备',0),
    ('关羽',0);
select `Id`,`Name`,`Age` from t_user where `Id`>=last_insert_id() order by `Id` limit 2;");

            //expression
            sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Name`) values('刘备');
select last_insert_id();");
            sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Name`) values('刘备');
select `Id`,`Name`,`Age` from t_user where `Id`=last_insert_id();");
            //dto
            sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Name`) values('刘备');
select last_insert_id();");
            sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Name`) values('刘备');
select `Id`,`Name`,`Age` from t_user where `Id`=last_insert_id();");
        }

        [Test]
        public void IdentityInsertIdentityTest()
        {
            //entity
            var sql = db.Insert<User2>().SetEntity(new User2
            {
                Id = 2,
                Name = "刘备"
            }).InsertIdentity().ToSql();
            sql.ShouldBe(@"insert into t_user(`Id`,`Name`,`Age`) values(2,'刘备',0);");
            try
            {
                sql = db.Insert<User2>().SetEntity(new User2
                {
                    Id = 2,
                    Name = "刘备"
                }).InsertIdentity().ToSql(EnumInsertToSql.ExecuteIdentity);
                throw new Exception("error");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("非自增主键或已声明插入自增列,无法获取自动生成的主键值!");
            }

            sql = db.Insert<User2>().SetEntity(new User2
            {
                Id = 2,
                Name = "刘备"
            }).InsertIdentity().ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Id`,`Name`,`Age`) values(2,'刘备',0);
select `Id`,`Name`,`Age` from t_user where `Id` = 2;");
            try
            {
                sql = db.Insert<User2>().SetEntity([new User2
                {
                    Id = 2,
                    Name = "刘备"
                },new User2
                {
                    Id = 3,
                    Name="关羽"
                } ]).InsertIdentity().ToSql(EnumInsertToSql.ExecuteIdentity);
                throw new Exception("error");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("非自增主键或已声明插入自增列,无法获取自动生成的主键值!");
            }
            sql = db.Insert<User2>().SetEntity(new[]{new User2
            {
                Id = 2,
                Name = "刘备"
            },new User2
            {
                Id = 3,
                Name="关羽"
            } }).InsertIdentity().ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Id`,`Name`,`Age`) values
    (2,'刘备',0),
    (3,'关羽',0);
select `Id`,`Name`,`Age` from t_user where `Id` in (2,3);");

            //expression
            try
            {
                sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").SetColumnExpr(i => i.Id, 2).InsertIdentity().ToSql(EnumInsertToSql.ExecuteIdentity);
                throw new Exception("error");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("非自增主键或已声明插入自增列,无法获取自动生成的主键值!");
            }
            sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").SetColumnExpr(i => i.Id, 2).InsertIdentity().ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Name`,`Id`) values('刘备',2);
select `Id`,`Name`,`Age` from t_user where `Id` = 2;");
            //dto
            try
            {
                sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").SetColumnExpr(i => i.Id, 2).InsertIdentity().ToSql(EnumInsertToSql.ExecuteIdentity);
                throw new Exception("error");
            }
            catch (Exception ex)
            {
                ex.Message.ShouldBe("非自增主键或已声明插入自增列,无法获取自动生成的主键值!");
            }
            sql = db.Insert<User2>().SetColumnExpr(i => i.Name, "刘备").SetColumnExpr(i => i.Id, 2).InsertIdentity().ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(`Name`,`Id`) values('刘备',2);
select `Id`,`Name`,`Age` from t_user where `Id` = 2;");
        }
    }
}
