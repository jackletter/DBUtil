﻿using DBUtil;
using DBUtil.Attributes;
using DBUtil.Builders;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Inserts.InsertEntity
{
    [TestFixture]
    internal class MultiPrimaryTests : TestBase
    {
        [Table("t_user")]
        public class UserEntity
        {
            [PrimaryKey]
            [Column("pri1")]
            public int Pri1 { get; set; }
            [PrimaryKey]
            [Column("pri2")]
            public string Pri2 { get; set; }
            [Column("age")]
            public int? Age { get; set; }
            [Column("addr")]
            public string Addr { get; set; }
        }

        [Test]
        public void TestOne()
        {
            DropTable("t_user");
            db.ExecuteSql(@"create table t_user(pri1 int,pri2 varchar(50),age int,addr varchar(50),primary key(pri1,pri2))");

            var insert = db.Insert<UserEntity>().SetEntity(new UserEntity
            {
                Pri1 = 1,
                Pri2 = "test",
                Addr = "天命",
                Age = 20
            });
            var sql = insert.ToSql();
            sql.ShouldBe(@"insert into t_user(pri1,pri2,age,addr) values(1,'test',20,'天命');");
            var r = insert.ExecuteAffrows();
            r.ShouldBe(1);
            TruncateTable("t_user");

            sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(pri1,pri2,age,addr) values(1,'test',20,'天命');
select pri1 `Pri1`,pri2 `Pri2`,age `Age`,addr `Addr` from t_user where (pri1,pri2) in ((1,'test'));");
            var d = insert.ExecuteInserted();
            d.Pri1.ShouldBe(1);
            d.Pri2.ShouldBe("test");
            d.Addr.ShouldBe("天命");
            d.Age.ShouldBe(20);
            try
            {
                insert.ToSql(EnumInsertToSql.ExecuteIdentity);
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("使用联合主键(pri1,pri2),无法获取插入后的自增主键值");
            }
            try
            {
                db.Insert<UserEntity>()
                    .SetColumnExpr(i => i.Pri1, 1)
                    .SetColumnExpr(i => i.Addr, "天命")
                    .SetColumnExpr(i => i.Age, 20)
                    .ToSql(EnumInsertToSql.ExecuteInserted);
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("使用联合主键时必须插入所有主键值");
            }
        }

        [Test]
        public void TestTwoRow()
        {
            DropTable("t_user");
            db.ExecuteSql(@"create table t_user(pri1 int,pri2 varchar(50),age int,addr varchar(50),primary key(pri1,pri2))");

            var insert = db.Insert<UserEntity>().SetEntity(new[]{new UserEntity
            {
                Pri1 = 1,
                Pri2 = "test",
                Addr = "天命",
                Age = 20
            },new UserEntity
            {
                Pri1 = 2,
                Pri2 = "test",
                Addr = "天命",
                Age = 20
            } });
            var sql = insert.ToSql();
            sql.ShouldBe(@"insert into t_user(pri1,pri2,age,addr) values
    (1,'test',20,'天命'),
    (2,'test',20,'天命');");
            var r = insert.ExecuteAffrows();
            r.ShouldBe(2);
            TruncateTable("t_user");

            sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(pri1,pri2,age,addr) values
    (1,'test',20,'天命'),
    (2,'test',20,'天命');
select pri1 `Pri1`,pri2 `Pri2`,age `Age`,addr `Addr` from t_user where (pri1,pri2) in ((1,'test'),(2,'test'));");
            var d2 = insert.ExecuteInsertedList();
            d2.Count.ShouldBe(2);
            d2[0].Pri1.ShouldBe(1);
            d2[0].Pri2.ShouldBe("test");
            d2[0].Addr.ShouldBe("天命");
            d2[0].Age.ShouldBe(20);
            d2[1].Pri1.ShouldBe(2);
            d2[1].Pri2.ShouldBe("test");
            d2[1].Addr.ShouldBe("天命");
            d2[1].Age.ShouldBe(20);
            try
            {
                insert.ToSql(EnumInsertToSql.ExecuteIdentity);
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("使用联合主键(pri1,pri2),无法获取插入后的自增主键值");
            }
        }
    }
}
