﻿using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Inserts.InsertEntity
{
    [TestFixture]
    internal class NoKeyTests : TestBase
    {
        [Table("t_user")]
        public class User6
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }
        [Test]
        public void Test()
        {
            DropTable("t_user");
            db.ExecuteSql(@"create table t_user(id int,name varchar(50),age int)");

            var insert = db.Insert<User6>().SetEntity(new[]{new User6
            {
                Id = 1,
                Age = 20,
                Name = "小明"
            }, new User6
            {
                Id = 2,
                Age = 21,
                Name = "小刚"
            } });
            var sql = insert.ToSql();
            sql.ShouldBe(@"insert into t_user(`Id`,`Name`,`Age`) values
    (1,'小明',20),
    (2,'小刚',21);");

            Should.Throw(() =>
            {
                db.Insert<User6>().SetEntity(new User6
                {
                    Id = 3,
                    Age = 22,
                }).ExecuteIdentity();
            }, typeof(Exception)).Message.ShouldContain("未定义主键,无法获取插入后的自增主键值");

            Should.Throw(() =>
            {
                db.Insert<User6>().SetEntity(new[]{new User6
                {
                    Id = 4,
                    Age = 23,
                    Name = "消防"
                }, new User6
                {
                    Id = 5,
                    Name = "刘备",
                    Age = 32
                } }).ExecuteInserted();
            }, typeof(Exception)).Message.ShouldContain("未定义主键,无法查询插入后的数据");
        }
    }
}
