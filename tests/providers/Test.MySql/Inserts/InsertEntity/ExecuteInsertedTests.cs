﻿using DBUtil;
using DBUtil.Attributes;
using DBUtil.Builders;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Inserts.InsertEntity
{
    [TestFixture]
    internal class ExecuteInsertedTests : TestBase
    {
        [Table("t_user")]
        public class User4
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("create_id")]
            public int? CreateUserId { get; set; }
            [Column("create_time")]
            public DateTime? CreateDate { get; set; }
            [Column("update_id")]
            public int? UpdateUserId { get; set; }
            [Column("update_time")]
            public DateTime? UpdateDate { get; set; }
        }

        [Test]
        public void Test()
        {
            DropTable("t_user");
            db.ExecuteSql(@"create table t_user(
            id int auto_increment primary key,
            name varchar(50),
            create_id int,
            create_time datetime,
            update_id int,
            update_time datetime
)");
            var insert = db.Insert<User4>().SetEntity(new[]{new User4()
            {
                Id = 2,
                Name = "小红",
                CreateDate = DateTime.Parse("1998-01-02"),
                CreateUserId = 100,
                UpdateUserId=3,
                UpdateDate=DateTime.Parse("1998-01-02"),
            }, new User4
            {
                Id = 3,
                Name = "小刚",
            } });
            var sql = insert.ToSql(EnumInsertToSql.ExecuteInserted);
            sql.ShouldBe(@"insert into t_user(name,create_id,create_time,update_id,update_time) values
    ('小红',100,'1998-01-02',3,'1998-01-02'),
    ('小刚',null,null,null,null);
select id `Id`,name `Name`,create_id `CreateUserId`,create_time `CreateDate`,update_id `UpdateUserId`,update_time `UpdateDate` from t_user where id>=last_insert_id() order by id limit 2;");
            var users = insert.ExecuteInsertedList();
            users.Count.ShouldBe(2);

            users[0].Id.ShouldBe(1);
            users[0].Name.ShouldBe("小红");
            users[0].CreateUserId.ShouldBe(100);
            users[0].CreateDate.ShouldBe(DateTime.Parse("1998-01-02"));
            users[0].UpdateUserId.ShouldBe(3);
            users[0].UpdateDate.ShouldBe(DateTime.Parse("1998-01-02"));

            users[1].Id.ShouldBe(2);
            users[1].Name.ShouldBe("小刚");
            users[1].CreateUserId.ShouldBeNull();
            users[1].CreateDate.ShouldBeNull();
            users[1].UpdateUserId.ShouldBeNull();
            users[1].UpdateDate.ShouldBeNull();
        }
    }
}
