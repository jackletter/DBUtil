﻿using DBUtil;
using DBUtil.Attributes;
using DBUtil.Builders;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Inserts.InsertEntity
{
    [TestFixture]
    internal class SetRemoveTests : TestBase
    {
        [Table("t_user")]
        public class User2
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }
        [Test]
        public void Test()
        {
            var now = DateTime.Parse("2023-01-23 21:12:13.123456");
            var sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumn("create_time", now).SetColumn("create_userid", 123).ToSql();
            sql.ShouldBe(@"insert into t_user(`Name`,`Age`,create_time,create_userid) values('刘备',0,'2023-01-23 21:12:13.123456',123);");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumn("create_time", now).SetColumn("create_userid", 123).IgnoreColumns("Name").ToSql();
            sql.ShouldBe(@"insert into t_user(`Age`,create_time,create_userid) values(0,'2023-01-23 21:12:13.123456',123);");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumn("create_time", now).SetColumn("create_userid", 123).OnlyColumns("Name", "create_userid").ToSql();
            sql.ShouldBe(@"insert into t_user(`Name`,create_userid) values('刘备',123);");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumnExpr(i => i.Name, "关羽").SetColumnExpr(i => i.Id, 123).ToSql();
            sql.ShouldBe(@"insert into t_user(`Age`,`Name`,`Id`) values(0,'关羽',123);");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumnExpr(i => i.Name, "关羽").SetColumnExpr(i => i.Age, 40).OnlyColumnsExpr(i => i.Age).ToSql();
            sql.ShouldBe(@"insert into t_user(`Age`) values(40);");
        }

        [Test]
        public void TestReturn()
        {
            var now = DateTime.Parse("2023-01-23 21:12:13.123456");
            var sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumn("create_time", now).SetColumn("create_userid", 123).ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Name`,`Age`,create_time,create_userid) values('刘备',0,'2023-01-23 21:12:13.123456',123);
select last_insert_id();");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumn("create_time", now).SetColumn("create_userid", 123).IgnoreColumns("Name").ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Age`,create_time,create_userid) values(0,'2023-01-23 21:12:13.123456',123);
select last_insert_id();");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumn("create_time", now).SetColumn("create_userid", 123).OnlyColumns("Name", "create_userid").ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Name`,create_userid) values('刘备',123);
select last_insert_id();");

            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumnExpr(i => i.Name, "关羽").SetColumnExpr(i => i.Id, 123).ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Age`,`Name`,`Id`) values(0,'关羽',123);
select last_insert_id();");
            sql = db.Insert<User2>().SetEntity(new User2
            {
                Name = "刘备"
            }).SetColumnExpr(i => i.Name, "关羽").SetColumnExpr(i => i.Age, 40).OnlyColumnsExpr(i => i.Age).ToSql(EnumInsertToSql.ExecuteIdentity);
            sql.ShouldBe(@"insert into t_user(`Age`) values(40);
select last_insert_id();");
        }
    }
}
