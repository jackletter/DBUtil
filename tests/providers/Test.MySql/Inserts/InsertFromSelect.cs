﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Inserts
{
    [TestFixture]
    internal class InsertFromSelect : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id", Order = 1)]
            public int Id { get; set; }

            [Column("name", Order = 3)]
            public string Name { get; set; }

            [Column("age", Order = 2)]
            public int Age { get; set; }
        }

        [Table("test_json")]
        public class PersonJson
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [JsonStore(Bucket = "ext", Key = "age")]
            public int Age { get; set; }

            [JsonStore(Bucket = "ext", Key = "status")]
            public List<EnumTest> States { get; set; }

            [JsonStore(Bucket = "ext2")]
            public Person Leader { get; set; }
        }
        public enum EnumTest
        {
            None, Active, NoActive
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int primary key auto_increment,name varchar(50),age int)");
            db.ExecuteSql("insert into test(name,age) values('jack',20),('tom',18);");

            DropTableIfExist("test_json");
            db.ExecuteSql("create table test_json(id int primary key auto_increment,name varchar(50),ext json,ext2 json)");
            db.ExecuteSql("insert into test_json(name,ext,ext2) values('jack','{\"age\":18,\"status\":[0,1]}','{\"id\":2,\"name\":\"ja\",\"age\":16}'),('tom','{\"age\":20,\"status\":[1]}','{\"id\":3,\"name\":\"to\",\"age\":18}');");
        }

        [Test]
        public void Simple()
        {
            var sql = db.Select<Person>().Where(i => i.Age > 10).AsInsert(i => new Person
            {
                Name = i.Name,
                Age = i.Age + 10
            }).ToSql();
            sql.ShouldBe("""
                insert into test(name,age) select t.name `Name`,t.age + 10 `Age`
                from test t
                where t.age > 10;
                """);

            try
            {
                db.Select<Person>().Where(i => i.Age > 10).AsInsert(i => i).ToSql();
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("不能在 AsInsert 方法中使用");
            }
        }

        [Test]
        public void SimpleJson()
        {
            var sql = db.Select<PersonJson>().Where(i => i.Age > 10).AsInsert(i => new PersonJson
            {
                Name = i.Name,
                Age = i.Age + 10,
                States = i.States,
                Leader = i.Leader,
            }).ToSql();
            sql.ShouldBe("""
                insert into test_json(name,ext,ext2)select t.`Name`,json_object('age',t.`Age`,'status',t.`States`),t.`Leader` from (
                select t.name `Name`,(json_value(t.ext,'$."age"' returning signed)) + 10 `Age`,json_value(t.ext,'$."status"') `States`,json_value(t.ext2,'$') `Leader`
                from test_json t
                where (json_value(t.ext,'$."age"' returning signed)) > 10
                ) t
                """);
        }
    }
}
