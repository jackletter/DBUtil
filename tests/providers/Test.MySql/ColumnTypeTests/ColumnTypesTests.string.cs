﻿using DBUtil.Attributes;
using NUnit.Framework;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace Test.MySql.ColumnTypeTests
{
    [TestFixture]
    internal class ColumnTypesTestsString : TestBase
    {
        #region model
        public class Person
        {
            [PrimaryKey(KeyStrategy =KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            public string Name { get; set; }
        }
        #endregion

        [Test]
        public void Test()
        {
            var select =  db.Select<Person>().Where(i => i.Name.Length > 0);
            var sql = select.ToSqlList();
        }
    }
}
