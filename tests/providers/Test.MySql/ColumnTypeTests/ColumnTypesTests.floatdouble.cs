﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using NUnit.Framework;
using Shouldly;

namespace Test.MySql.ColumnTypeTests
{
    [TestFixture]
    internal class ColumnTypesTestsFloatDouble : TestBase
    {
        #region 模型
        [Table("test")]
        public class Model
        {
            /// <summary>
            /// float
            /// </summary>
            public float? t_float { get; set; }

            /// <summary>
            /// double
            /// </summary>
            public double? t_double { get; set; }
        }
        #endregion

        [SetUp]
        public void Setup()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"
create table test(
   t_float float,
   t_double double
)");
            db.ExecuteSql(@"
insert into test.test (t_float,t_double) values(123.123,123.12345678901);");
        }

        [Test]
        public void TestDefaultParse()
        {
            //mysql中 float 占4个字节，精度是6位, double 占8个字节，精度是16位；
            var dic = db.SelectDictionary("select * from test");
            dic["t_float"].GetType().ShouldBe(typeof(float));
            dic["t_double"].GetType().ShouldBe(typeof(double));

            dic["t_float"].ShouldBe(123.123f);
            dic["t_double"].ShouldBe(123.12345678901d);
        }

        [Test]
        public void TestOrmReadWrite()
        {
            var model = db.SelectModel<Model>("select * from test");
            model.t_float.ShouldBe(123.123f);
            model.t_double.ShouldBe(123.12345678901d);
            TruncateTable("test");

            var sql = db.Insert<Model>().SetEntity(model).ToSql();
            sql.ShouldBe("insert into test(`t_float`,`t_double`) values(123.123,123.12345678901);");

            db.Insert<Model>().SetEntity(model).ExecuteAffrows();
            model = db.SelectModel<Model>("select * from test");
            model.t_float.ShouldBe(123.123f);
            model.t_double.ShouldBe(123.12345678901d);
        }
    }
}
