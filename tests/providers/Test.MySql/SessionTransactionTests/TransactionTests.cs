﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Linq;

namespace Test.MySql.SessionTransactionTests
{
    [TestFixture]
    internal sealed class TransactionTests : TestBase
    {
        [Test]
        public void NormalTest()
        {
            DropTableIfExist("test");
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50))");
            db.RunInTransaction(() =>
            {
                var i = db.Insert("test", new { name = "小明" }.ToDictionary()).ExecuteIdentity<int>();
                i.ShouldBe(1);
            });
            var name = db.SelectScalar<string>("select name from test where id=1");
            name.ShouldBe("小明");
            db.RunInTransaction(() =>
            {
                var i = db.Insert("test", new { name = "小红" }.ToDictionary()).ExecuteIdentity<int>();
                i.ShouldBe(2);
            });
            name = db.SelectScalar<string>("select name from test where id=2");
            name.ShouldBe("小红");
            try
            {
                db.RunInTransaction(() =>
                {
                    var i = db.Insert("test", new { name = "小刚" }.ToDictionary()).ExecuteIdentity<int>();
                    i.ShouldBe(3);
                    throw new Exception();
                });
            }
            catch { }
            name = db.SelectScalar<string>("select name from test where id=3");
            name.ShouldBeNull();
            db.RunInTransaction(() =>
            {
                var i = db.Insert("test", new { name = "小刚" }.ToDictionary()).ExecuteIdentity<int>();
                i.ShouldBe(4);
            });
            name = db.SelectScalar<string>("select name from test where id=4");
            name.ShouldBe("小刚");
        }

        [Test]
        public void LikeNestedTest()
        {
            db.ExecuteSql(db.Manage.DropTableIfExistSql("test"));
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50))");
            //嵌套
            try
            {
                db.RunInTransaction(() =>
                {
                    var i = db.Insert("test", new { name = "小明" }.ToDictionary()).ExecuteIdentity<int>();
                    i.ShouldBe(1);
                    db.RunInTransaction(() =>
                    {
                        var i = db.Insert("test", new { name = "小红" }.ToDictionary()).ExecuteIdentity<int>();
                        i.ShouldBe(2);
                    });
                    var name = db.SelectScalar<string>("select name from test where id=2");
                    name.ShouldBe("小红");
                    throw new Exception();
                });
            }
            catch { }
            var name = db.SelectScalar<int>("select count(1) from test");
            name.ShouldBe(0);

            //嵌套时隔离级别不能提升
            db.RunInTransaction(() =>
            {
                try
                {
                    db.RunInTransaction(() =>
                    {
                    }, System.Data.IsolationLevel.Serializable);
                }
                catch (Exception ex)
                {
                    ex.Message.ShouldContain("新的事务隔离级别不能大于已开启的事务隔离级别");
                }
                db.IsTransaction.ShouldBe(true);
                db.IsSession.ShouldBe(true);
            });
            db.IsTransaction.ShouldBeFalse();
            db.IsSession.ShouldBeFalse();
            //嵌套用户手动处理了异常
            db.ExecuteSql(db.Manage.DropTableIfExistSql("test"));
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50))");

            db.RunInTransaction(() =>
            {
                var i = db.Insert("test", new { name = "小明" }.ToDictionary()).ExecuteIdentity<int>();
                i.ShouldBe(1);
                db.RunInTransaction(() =>
                {
                    i = db.Insert("test", new { name = "小红" }.ToDictionary()).ExecuteIdentity<int>();
                    i.ShouldBe(2);
                });
                db.RunInTransaction(() =>
                {
                    i = db.Insert("test", new { name = "小刚" }.ToDictionary()).ExecuteIdentity<int>();
                    i.ShouldBe(3);
                    try
                    {
                        db.RunInTransaction(() =>
                        {
                            //将会被回滚
                            i = db.Insert("test", new { name = "tom" }.ToDictionary()).ExecuteIdentity<int>();
                            i.ShouldBe(4);
                            throw new Exception("自定义");
                        });
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ShouldContain("自定义");
                    }
                    //这里的不会被回滚
                });

                //将不会查到里面回滚的
                db.SelectScalar<string>("select name from test where id=4").ShouldBeNull();
                db.SelectScalar<string>("select name from test where id=3").ShouldBe("小刚");
            });
            db.SelectScalar<string>("select name from test where id=4").ShouldBeNull();
            db.SelectScalar<string>("select name from test where id=3").ShouldBe("小刚");
            db.SelectScalar<string>("select name from test where id=2").ShouldBe("小红");
            db.SelectScalar<string>("select name from test where id=1").ShouldBe("小明");
        }

        [Test]
        public void NoAffectTest()
        {
            db.ExecuteSql(db.Manage.DropTableIfExistSql("test"));
            db.ExecuteSql("create table test(id int auto_increment primary key,name varchar(50))");
            //嵌套
            try
            {
                db.RunInTransaction(() =>
                {
                    var i = db.Insert("test", new { name = "小明" }.ToDictionary()).ExecuteIdentity<int>();
                    i.ShouldBe(1);
                    try
                    {
                        db.RunInTransaction(() =>
                        {
                            var i = db.Insert("test", new { name = "小红" }.ToDictionary()).ExecuteIdentity<int>();
                            i.ShouldBe(2);
                            throw new Exception("自定义异常");
                        });
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ShouldContain("自定义异常");
                        throw;
                    }
                    var name = db.SelectScalar<string>("select name from test where id=2");
                    name.ShouldBe("小红");
                    db.IsTransaction.ShouldBe(true);
                });
            }
            catch (Exception ex) { ex.Message.ShouldContain("自定义异常"); }
            db.IsTransaction.ShouldBeFalse();
            var name = db.SelectScalar<int>("select count(1) from test");
            name.ShouldBe(0);
        }
    }
}
