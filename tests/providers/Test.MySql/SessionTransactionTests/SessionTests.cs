﻿using NUnit.Framework;
using Shouldly;

namespace Test.MySql.SessionTransactionTests
{
    [TestFixture]
    internal sealed class SessionTests : TestBase
    {
        [Test]
        public void CrossNestedTest()
        {
            db.RunInSession(() =>
            {
                //这里面是长连接
                //一个会话变量 @ii=10
                db.IsSession.ShouldBe(true);
                db.ExecuteSql("set @ii=10");
                var i = db.SelectScalar<int>("select @ii");
                i.ShouldBe(10);
                db.RunInNoSession(() =>
                {
                    //这里不再有长连接
                    //不会有会话变量 @ii
                    db.IsSession.ShouldBe(false);
                    var i = db.SelectScalar<int?>("select @ii");
                    i.ShouldBe(null);
                    db.ExecuteSql("set @ii=20");
                    i = db.SelectScalar<int?>("select @ii");
                    i.ShouldBe(null);
                    db.RunInSession(() =>
                    {
                        //这里有长连接
                        //一个新的会话变量 @ii=11
                        db.IsSession.ShouldBe(true);
                        var i = db.SelectScalar<int?>("select @ii");
                        i.ShouldBe(null);
                        db.ExecuteSql("set @ii=11");
                        i = db.SelectScalar<int>("select @ii");
                        i.ShouldBe(11);
                    });
                    //还是没有会话变量
                    i = db.SelectScalar<int?>("select @ii");
                    i.ShouldBe(null);
                });
                //还是原来的会话变量
                i = db.SelectScalar<int>("select @ii");
                i.ShouldBe(10);
            });
        }

        [Test]
        public void TestOtherAffect()
        {
            db.RunInSession(() =>
            {
                db.IsSession.ShouldBe(true);
                db.ExecuteSql("set @ii=20");
                db.SelectScalar<int>("select @ii").ShouldBe(20);
            });

            //不受上面代码的影响
            db.RunInSession(() =>
            {
                db.IsSession.ShouldBe(true);
                var i = db.SelectScalar<int?>("select @ii");
                i.ShouldBe(null);
            });
        }

        [Test]
        public void TestInherit()
        {
            db.RunInSession(() =>
            {
                db.IsSession.ShouldBe(true);
                var i = db.SelectScalar<int?>("select @ii");
                i.ShouldBe(null);
                db.ExecuteSql("set @ii=20");
                db.SelectScalar<int>("select @ii").ShouldBe(20);
                db.RunInSession(() =>
                {
                    db.SelectScalar<int>("select @ii").ShouldBe(20);
                });
            });
        }
    }
}
