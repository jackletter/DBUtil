﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using DBUtil;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;

namespace Test.MySql
{
    [TestFixture]
    public sealed class CommonTests : TestBase
    {
        #region 测试OpenTest
        [Test]
        public void OpenTest()
        {
            var res = db.OpenTest();
            res.ShouldNotBeNull();
            res.Success.ShouldBe(true);
        }
        #endregion        

        #region 测试批量执行sql语句

        [Test]
        public void TestBatchSqlExecuteError()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key,name varchar(50))");
            var sql = @"insert into test(id,name) values(1,'小明');
insert into test(id,name) values(2,'小王');";
            db.ExecuteSql(sql);
            sql = @"insert into test(id,name) values(3,'小孙');
insert into test(id,name) values(3,'小周');";
            try
            {
                db.ExecuteSql(sql);
            }
            catch { }
            var count = db.SelectDataTable("select * from test").Rows.Count;
            Assert.IsTrue(count == 3);
        }

        [Test]
        public void TestBatchSqlError()
        {
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key,name varchar(50))");
            var sql = @"insert into test(id,name) values(1,'小明');
insert into test(id,name) values(2,'小王');";
            db.ExecuteSql(sql);
            sql = @"insert into test(id,name) values(3,'小孙');
insert into test(id,name) values(3,'小周);";
            try
            {
                db.ExecuteSql(sql);
            }
            catch (Exception ex) { }
            var count = db.SelectDataTable("select * from test").Rows.Count;
            Assert.IsTrue(count == 3);
        }
        #endregion

        [Test]
        public void TestMonitor()
        {
            DotNetCommon.Logger.LoggerFactory.SetLogger(ctx =>
            {
                Console.WriteLine($"{ctx.Time.ToCommonStamp2String()} {ctx.CategoryName} {ctx.Message} {ctx.Exception?.Message}");
            }, true);
            DropTableIfExist("test");
            db.ExecuteSql(@"create table test(id int primary key,name varchar(50))");
            //db.InsertBatch(InsertBatchItem.Create("test", new { id = 1, name = "小明" }.ToDictionary())
            //    , InsertBatchItem.Create("test", new { id = 12, name = "小红" }.ToDictionary()));
            //var id = db.Generator.NewId("test", "id");

        }

        [Test]
        public async Task TestTimeout()
        {
            //await db.RunInTransactionAsync(async () =>
            //{
            //    await db.InsertAsync("testtable", new { id = 123456 }.ToDictionary());
            //    //5分钟
            //    await Task.Delay(1001 * 60 * 3);
            //    //await Task.Delay(1000 * 6);
            //    await db.InsertAsync("testtable", new { id = 789456 }.ToDictionary());
            //});
            //Assert.IsTrue(true);
        }

        [Test]
        public void TestColumnType()
        {
            PrepareColumnTypes();
            var dt = db.SelectDataTable("select * from test");

        }

        [Test]
        public void TestReader()
        {
            DropTableIfExist("category");
            db.ExecuteSql(@"create table category(id int primary key,categoryName varchar(200),parentId int)");
            db.Insert<Category>().SetEntity(new[]
            {
                new Category{Id=1,CategoryName="小明",ParentId=0},
                new Category{Id=2,CategoryName="夏红",ParentId=1 }
            }).ExecuteAffrows();
            db.SelectDataReader(reader =>
            {
                while (reader.RawReader.Read())
                {
                    var vals = new object[reader.RawReader.FieldCount];
                    reader.RawReader.GetValues(vals);
                }
            }, "select * from category");
        }

        [Test]
        public void InsertBigData()
        {
            //for (int i = 0; i < 100; i++)
            //{
            //    var p = Process.Start(@"D:\Applications\nodejs\node.exe", @"D:\jackletter\mockbigdata\index.js");
            //    p.WaitForExit();
            //    var text = File.ReadAllText(@"D:\jackletter\mockbigdata\testFile.sql");
            //    //执行sql插入
            //    db.ExecuteSql(text);
            //}
        }

        [Test]
        public void TestSelect()
        {
            DropTableIfExist("category");
            db.ExecuteSql(@"create table category(id int primary key,categoryName varchar(200),parentId int)");
            db.Insert<Category>().SetEntity(new[]
            {
                new Category{Id=1,CategoryName="小明",ParentId=0},
                new Category{Id=2,CategoryName="夏红",ParentId=1 }
            }).ExecuteAffrows();
            var sql = "select * from category limit 10;select count(1) from category;";
            var (c1, c2) = db.SelectMultiple(sql,
                 reader => reader.ReadList<Category>(),
                 reader => reader.ReadScalar<int>()
                 );

            int k = 0;
        }

        [Test]
        public async Task TestSelectAsync()
        {
            var sql = "select * from category limit 10;select count(1) from category;";
            var (c1, c2) = await db.SelectMultipleAsync(sql,
                 reader => reader.ReadListAsync<Category>(),
                 reader => reader.ReadScalarAsync<int>()
                 );

            int k = 0;
        }

        public class Category
        {
            public int Id { get; set; }
            public string CategoryName { get; set; }
            public int ParentId { get; set; }
        }

        [Test]
        public void TestSelectValueTuple()
        {
            var model = db.SelectModel<(string name, int age, DateTime birth)>("select '小明' name,18 age,cast('2023-06-14 17:49:01' as datetime) birth");
            model.name.ShouldBe("小明");
            model.age.ShouldBe(18);
            model.birth.ShouldBe(DateTime.Parse("2023-06-14 17:49:01"));
            var model2 = db.SelectModel<(string name, int age, DateTime? birth)>("select '小明' name,18 age,null birth");
            model2.name.ShouldBe("小明");
            model2.age.ShouldBe(18);
            model2.birth.ShouldBe(null);

            //超长
            var model3 = db.SelectModel<(
                string name1, string name2, string name3, string name4, string name5, string name6, string name7,
                string name8, string name9, string name10, string name11, string name12, string name13, string name14,
                string name15, string name16, string name17, string name18, string name19, string name20, string name21,
                string name22, string name23, string name24, string name25, string name26, string name27, string name28,
                string name29, string name30)>(@"select 
'name1' name1,'name2' name2,'name3' name3,'name4' name4,'name5' name5,'name6' name6,'name7' name7,
'name8' name8,'name9' name9,'name10' name10,'name11' name11,'name12' name12,'name13' name13,'name14' name14,
'name15' name15,'name16' name16,'name17' name17,'name18' name18,'name19' name19,'name20' name20,'name21' name21,
'name22' name22,'name23' name23,'name24' name24,'name25' name25,'name26' name26,'name27' name27,'name28' name28,
'name29' name29,'name30' name30");
            model3.name1.ShouldBe("name1");
            model3.name30.ShouldBe("name30");
        }

        [Test]
        public void TestSelectTuple()
        {
            var model = db.SelectModel<Tuple<string, int, DateTime>>("select '小明' name,18 age,cast('2023-06-14 17:49:01' as datetime) birth");
            model.Item1.ShouldBe("小明");
            model.Item2.ShouldBe(18);
            model.Item3.ShouldBe(DateTime.Parse("2023-06-14 17:49:01"));
            var model2 = db.SelectModel<Tuple<string, int, DateTime?>>("select '小明' name,18 age,null birth");
            model2.Item1.ShouldBe("小明");
            model2.Item2.ShouldBe(18);
            model2.Item3.ShouldBe(null);

            //超长
            var model3 = db.SelectModel<
                Tuple<string, string, string, string, string, string, string,
                Tuple<string, string, string, string, string, string, string,
                Tuple<string, string, string, string, string, string, string,
                Tuple<string, string, string, string, string, string, string,
                Tuple<string, string>>>>>>(@"select 
'name1' name1,'name2' name2,'name3' name3,'name4' name4,'name5' name5,'name6' name6,'name7' name7,
'name8' name8,'name9' name9,'name10' name10,'name11' name11,'name12' name12,'name13' name13,'name14' name14,
'name15' name15,'name16' name16,'name17' name17,'name18' name18,'name19' name19,'name20' name20,'name21' name21,
'name22' name22,'name23' name23,'name24' name24,'name25' name25,'name26' name26,'name27' name27,'name28' name28,
'name29' name29,'name30' name30");
            model3.Item1.ShouldBe("name1");
            model3.Rest.Item1.ShouldBe("name8");
            model3.Rest.Rest.Item1.ShouldBe("name15");
            model3.Rest.Rest.Rest.Item1.ShouldBe("name22");
            model3.Rest.Rest.Rest.Rest.Item1.ShouldBe("name29");
            model3.Rest.Rest.Rest.Rest.Item2.ShouldBe("name30");
        }
    }
}
