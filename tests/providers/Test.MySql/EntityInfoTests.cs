﻿using DBUtil;
using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql
{

    [TestFixture]
    internal class EntityInfoTests : TestBase
    {
        #region model
        public class BaseEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public virtual int Id { get; set; }
        }

        public class AuditEntity : BaseEntity
        {
            [Column("create_id")]
            public int CreateUserId { get; set; }
            [Column("create_time")]
            public DateTime CreateTime { get; set; }
            public int UpdateUserId { get; set; }
            public DateTime? UpdateTime { get; set; }
            public int DeleteUserId { get; set; }
            public DateTime? DeleteTime { get; set; }
        }

        [Table("t_person", Schema = "")]
        public class PersonEntity : AuditEntity
        {
            [Column("name")]
            public string Name { get; set; }
            public int Age { get; set; }

            [JsonStore(Bucket = "properties")]
            public PersonExt Properties { get; set; }

            [JsonStore(Bucket = "properties2")]
            public PersonExt Properties2 { get; set; }

            [JsonStore(Bucket = "properties", Key = "title")]
            [Column("title")]
            public string Title { get; set; }

            public class PersonExt
            {
                public int Score { get; set; }
                public string Addr { get; set; }
            }
        }


        [Table("t_student")]
        public class StudentEntity : AuditEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Custom)]
            [Column("id2")]
            public override int Id { get; set; }
            public string Name { get; set; }
        }
        #endregion

        #region model1
        public abstract class BaseEntityK<TPrimaryKey>
        {
            [Column("t_id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public virtual TPrimaryKey Id { get; set; }
        }
        public class PersonEntityK : BaseEntityK<int> { }
        #endregion

        private Func<DBAccess, Type, EntityInfo> GetEntityInfo;
        [SetUp]
        public void SetUp()
        {
            var methodInfo = typeof(DBAccess).GetMethods(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Where(i => i.Name == "GetEntityInfo" && i.ToString() == "DBUtil.EntityInfo GetEntityInfo(System.Type)").First();
            GetEntityInfo = (db, type) => methodInfo.Invoke(db, new object[] { type }) as EntityInfo;
        }

        [Test]
        public void NormalTest()
        {
            //BaseEntity
            var entityInfo = GetEntityInfo(db, typeof(BaseEntity));
            entityInfo = entityInfo.Clone();
            entityInfo.ShouldNotBeNull();
            entityInfo.TableNamePure.ShouldBe("BaseEntity");
            entityInfo.SchemaNamePure.ShouldBeNull();
            entityInfo.TableNameSeg.ShouldBe("`BaseEntity`");
            entityInfo.Type.ShouldBe(typeof(BaseEntity));
            entityInfo.TypeClassFullName.ShouldBe(typeof(BaseEntity).GetClassFullName());

            entityInfo.PrimaryKeyColumn.ShouldNotBeNull();
            entityInfo.PrimaryKeyColumn.PropNamePure.ShouldBe(nameof(BaseEntity.Id));

            entityInfo.EntityPropertyInfos.Count.ShouldBe(1);
            var col = entityInfo.EntityPropertyInfos[0];
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("id");
            col.PropNamePure.ShouldBe(nameof(BaseEntity.Id));
            col.PropNameSeg.ShouldBe($"`{nameof(BaseEntity.Id)}`");
            col.IsColumnNameSegEqualPropNameSeg.ShouldBe(false);
            col.DefaultValue.ShouldBe(default(int));
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeTrue();
            col.JsonBucket.ShouldBeNull();
            col.JsonKey.ShouldBeNull();
            col.Order.ShouldBe(-1);
            col.PrimaryKeyStrategy.ShouldBe(KeyStrategy.Identity);
            col.PropertyInfo.ShouldBe(typeof(BaseEntity).GetProperty("Id"));
            col.Type.ShouldBe(typeof(int));
            col.PropNamePure.ShouldBe(nameof(BaseEntity.Id));
            col.TypeClassFullName.ShouldBe(typeof(int).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(int).GetTypeCode());
        }

        [Test]
        public void TestNormalAudit()
        {
            var entityInfo = GetEntityInfo(db, typeof(AuditEntity));
            entityInfo = entityInfo.Clone();
            entityInfo.ShouldNotBeNull();
            entityInfo.TableNamePure.ShouldBe("AuditEntity");
            entityInfo.SchemaNamePure.ShouldBeNull();
            entityInfo.TableNameSeg.ShouldBe("`AuditEntity`");
            entityInfo.Type.ShouldBe(typeof(AuditEntity));
            entityInfo.TypeClassFullName.ShouldBe(typeof(AuditEntity).GetClassFullName());

            entityInfo.PrimaryKeyColumn.ShouldNotBeNull();
            entityInfo.PrimaryKeyColumn.PropNamePure.ShouldBe(nameof(AuditEntity.Id));

            //entityInfo.EntityPropertyInfos.Count.ShouldBe(7);
            //id
            var col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == "Id");
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("id");
            col.PropNamePure.ShouldBe(nameof(AuditEntity.Id));
            col.PropNameSeg.ShouldBe($"`{nameof(AuditEntity.Id)}`");
            col.DefaultValue.ShouldBe(default(int));
            col.IsColumnNameSegEqualPropNameSeg.ShouldBe(false);
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeTrue();
            col.JsonBucket.ShouldBeNull();
            col.JsonKey.ShouldBeNull();
            col.Order.ShouldBe(-1);
            col.PrimaryKeyStrategy.ShouldBe(KeyStrategy.Identity);
            col.PropertyInfo.ShouldBe(typeof(AuditEntity).GetProperty("Id"));
            col.Type.ShouldBe(typeof(int));
            col.PropNamePure.ShouldBe(nameof(AuditEntity.Id));
            col.TypeClassFullName.ShouldBe(typeof(int).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(int).GetTypeCode());

            //CreateTime
            col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == "CreateTime");
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("create_time");
            col.PropNamePure.ShouldBe(nameof(AuditEntity.CreateTime));
            col.PropNameSeg.ShouldBe($"`{nameof(AuditEntity.CreateTime)}`");
            col.DefaultValue.ShouldBe(default(DateTime));
            col.IsColumnNameSegEqualPropNameSeg.ShouldBe(false);
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeFalse();
            col.JsonBucket.ShouldBeNull();
            col.JsonKey.ShouldBeNull();
            col.Order.ShouldBe(-1);
            col.PropertyInfo.ShouldBe(typeof(AuditEntity).GetProperty("CreateTime"));
            col.Type.ShouldBe(typeof(DateTime));
            col.PropNamePure.ShouldBe(nameof(AuditEntity.CreateTime));
            col.TypeClassFullName.ShouldBe(typeof(DateTime).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(DateTime).GetTypeCode());

            //CreateUserId
            col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == "CreateUserId");
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("create_id");
            col.PropNamePure.ShouldBe(nameof(AuditEntity.CreateUserId));
            col.DefaultValue.ShouldBe(default(int));
            col.IsColumnNameSegEqualPropNameSeg.ShouldBe(false);
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeFalse();
            col.JsonBucket.ShouldBeNull();
            col.JsonKey.ShouldBeNull();
            col.Order.ShouldBe(-1);
            col.PropertyInfo.ShouldBe(typeof(AuditEntity).GetProperty("CreateUserId"));
            col.Type.ShouldBe(typeof(int));
            col.PropNamePure.ShouldBe(nameof(AuditEntity.CreateUserId));
            col.TypeClassFullName.ShouldBe(typeof(int).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(int).GetTypeCode());

            //UpdateTime
            col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == "UpdateTime");
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("`UpdateTime`");
            col.PropNamePure.ShouldBe(nameof(AuditEntity.UpdateTime));
            col.DefaultValue.ShouldBe(default(DateTime?));
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeTrue();
            col.IsPrimaryKey.ShouldBeFalse();
            col.JsonBucket.ShouldBeNull();
            col.JsonKey.ShouldBeNull();
            col.Order.ShouldBe(-1);
            col.PropertyInfo.ShouldBe(typeof(AuditEntity).GetProperty("UpdateTime"));
            col.Type.ShouldBe(typeof(DateTime?));
            col.PropNamePure.ShouldBe(nameof(AuditEntity.UpdateTime));
            col.TypeClassFullName.ShouldBe(typeof(DateTime?).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(DateTime).GetTypeCode());
        }

        [Test]
        public void TestNormalStudent()
        {
            var entityInfo = GetEntityInfo(db, typeof(StudentEntity));
            entityInfo = entityInfo.Clone();
            entityInfo.ShouldNotBeNull();
            entityInfo.TableNamePure.ShouldBe("t_student");
            entityInfo.SchemaNamePure.ShouldBeNull();
            entityInfo.TableNameSeg.ShouldBe("t_student");
            entityInfo.Type.ShouldBe(typeof(StudentEntity));
            entityInfo.TypeClassFullName.ShouldBe(typeof(StudentEntity).GetClassFullName());

            entityInfo.PrimaryKeyColumn.ShouldNotBeNull();
            entityInfo.PrimaryKeyColumn.PropNamePure.ShouldBe(nameof(StudentEntity.Id));

            //entityInfo.EntityPropertyInfos.Count.ShouldBe(8);
            //id
            var col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == "Id");
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("id2");
            col.PropNamePure.ShouldBe(nameof(StudentEntity.Id));
            col.PropNameSeg.ShouldBe($"`{nameof(StudentEntity.Id)}`");
            col.IsColumnNameSegEqualPropNameSeg.ShouldBe(false);
            col.DefaultValue.ShouldBe(default(int));
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeTrue();
            col.JsonBucket.ShouldBeNull();
            col.JsonKey.ShouldBeNull();
            col.Order.ShouldBe(-1);
            col.PrimaryKeyStrategy.ShouldBe(KeyStrategy.Custom);
            col.PropertyInfo.ShouldBe(typeof(StudentEntity).GetProperty("Id"));
            col.Type.ShouldBe(typeof(int));
            col.PropNamePure.ShouldBe(nameof(StudentEntity.Id));
            col.TypeClassFullName.ShouldBe(typeof(int).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(int).GetTypeCode());
        }

        [Test]
        public void TestNormalPerson()
        {
            var entityInfo = GetEntityInfo(db, typeof(PersonEntity));
            entityInfo = entityInfo.Clone();
            entityInfo.ShouldNotBeNull();
            entityInfo.TableNamePure.ShouldBe("t_person");
            entityInfo.SchemaNamePure.ShouldBeNull();
            entityInfo.TableNameSeg.ShouldBe("t_person");
            entityInfo.Type.ShouldBe(typeof(PersonEntity));
            entityInfo.TypeClassFullName.ShouldBe(typeof(PersonEntity).GetClassFullName());

            entityInfo.PrimaryKeyColumn.ShouldNotBeNull();
            entityInfo.PrimaryKeyColumn.PropNamePure.ShouldBe(nameof(PersonEntity.Id));

            //Properties
            var col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == nameof(PersonEntity.Properties));
            col.JsonBucket.ShouldBe("properties");
            col.JsonKey.ShouldBeNull();
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("`Properties`");
            col.PropNamePure.ShouldBe(nameof(PersonEntity.Properties));
            col.PropNameSeg.ShouldBe($"`{nameof(PersonEntity.Properties)}`");
            col.DefaultValue.ShouldBe(default(PersonEntity.PersonExt));
            col.IsColumnNameSegEqualPropNameSeg.ShouldBe(true);
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeFalse();
            col.Order.ShouldBe(-1);
            col.PropertyInfo.ShouldBe(typeof(PersonEntity).GetProperty("Properties"));
            col.Type.ShouldBe(typeof(PersonEntity.PersonExt));
            col.PropNamePure.ShouldBe(nameof(PersonEntity.Properties));
            col.TypeClassFullName.ShouldBe(typeof(PersonEntity.PersonExt).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(object).GetTypeCode());

            //Properties2
            col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == nameof(PersonEntity.Properties2));
            col.JsonBucket.ShouldBe("properties2");
            col.JsonKey.ShouldBeNull();
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("`Properties2`");
            col.PropNamePure.ShouldBe(nameof(PersonEntity.Properties2));
            col.DefaultValue.ShouldBe(default(PersonEntity.PersonExt));
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeFalse();
            col.Order.ShouldBe(-1);
            col.PropertyInfo.ShouldBe(typeof(PersonEntity).GetProperty("Properties2"));
            col.Type.ShouldBe(typeof(PersonEntity.PersonExt));
            col.PropNamePure.ShouldBe(nameof(PersonEntity.Properties2));
            col.TypeClassFullName.ShouldBe(typeof(PersonEntity.PersonExt).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(object).GetTypeCode());

            //Title
            col = entityInfo.EntityPropertyInfos.First(i => i.PropNamePure == nameof(PersonEntity.Title));
            col.JsonBucket.ShouldBe("properties");
            col.JsonKey.ShouldBe("title");
            col.ShouldNotBeNull();
            col.ColumnNameSeg.ShouldBe("title");
            col.PropNamePure.ShouldBe(nameof(PersonEntity.Title));
            col.DefaultValue.ShouldBe(default(string));
            col.IsColumnNameSegEqualPropNameSeg.ShouldBe(false);
            col.IsColumn.ShouldBeTrue();
            col.IsIgnoreInsert.ShouldBeFalse();
            col.IsIgnoreSelect.ShouldBeFalse();
            col.IsIgnoreUpdate.ShouldBeFalse();
            col.IsNullAble.ShouldBeFalse();
            col.IsPrimaryKey.ShouldBeFalse();
            col.Order.ShouldBe(-1);
            col.PropertyInfo.ShouldBe(typeof(PersonEntity).GetProperty("Title"));
            col.Type.ShouldBe(typeof(string));
            col.PropNamePure.ShouldBe(nameof(PersonEntity.Title));
            col.TypeClassFullName.ShouldBe(typeof(string).GetClassFullName());
            col.TypeName.ShouldBeNull();
            col.TypeCode.ShouldBe(typeof(string).GetTypeCode());
        }

        [Test]
        public void TestGenericBase()
        {
            var entityInfo = GetEntityInfo(db, typeof(PersonEntityK));
            entityInfo.ShouldNotBeNull();
            entityInfo.EntityPropertyInfos.ShouldNotBeEmpty();
            entityInfo.EntityPropertyInfos[0].PropNamePure.ShouldBe(nameof(PersonEntityK.Id));
            entityInfo.EntityPropertyInfos[0].ColumnNameSeg.ShouldBe("t_id");
            entityInfo.EntityPropertyInfos[0].PrimaryKeyStrategy.ShouldBe(KeyStrategy.Identity);
        }
    }
}
