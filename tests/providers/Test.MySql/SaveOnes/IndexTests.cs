﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.SaveOnes
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            public int Age { get; set; }

            [JsonStore(Bucket = "ext")]
            public Ext Ext { get; set; }
        }

        public class Ext
        {
            public string QQ { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
        }

        #endregion

        [SetUp]
        public void SetUp()
        {
            db.ExecuteSql(db.Manage.DropTableIfExistSql("test"));
            db.ExecuteSql("create table test(id int primary key auto_increment,name varchar(50),age int,ext json)");
            db.ExecuteSql("""
                insert into test(name,age,ext) values('jack',20,'{"QQ":"123456789","Phone":"13123457896","Email":"123456789@qq.com"}')
                """);
        }

        [Test]
        public void TestSimple()
        {
            var builder = db.SaveOne(new Person
            {
                Id = 1,
                Name = "jack2",
                Age = 23,
                Ext = new Ext
                {
                    QQ = "12345",
                    Phone = "152123456789",
                    Email = "12345@qq.com"
                }
            });
            var sql = builder.ToSql();
            sql.ShouldBe("""
                insert into test(id,name,`Age`,ext) values(1,'jack2',23,'{"QQ":"12345","Phone":"152123456789","Email":"12345@qq.com"}') on duplicate key update 
                    name = 'jack2',
                    `Age` = 23,
                    ext = '{"QQ":"12345","Phone":"152123456789","Email":"12345@qq.com"}';
                """);
            var ret = builder.ExecuteAffrows();
            ret.ShouldBe(2);
            var person = db.Select<Person>().FirstOrDefault();
            var json = person.ToJson();
            json.ShouldBe("""
                {"Id":1,"Name":"jack2","Age":23,"Ext":{"QQ":"12345","Phone":"152123456789","Email":"12345@qq.com"}}
                """);
        }

        [Test]
        public void TestAsTableName()
        {
            var builder = db.SaveOne(new Person
            {
                Id = 1,
                Name = "jack2",
                Age = 23,
                Ext = new Ext
                {
                    QQ = "12345",
                    Phone = "152123456789",
                    Email = "12345@qq.com"
                }
            }).AsTable(i => "test2");
            var sql = builder.ToSql();
            sql.ShouldBe("""
                insert into test2(id,name,`Age`,ext) values(1,'jack2',23,'{"QQ":"12345","Phone":"152123456789","Email":"12345@qq.com"}') on duplicate key update 
                    name = 'jack2',
                    `Age` = 23,
                    ext = '{"QQ":"12345","Phone":"152123456789","Email":"12345@qq.com"}';
                """);
        }

        [Test]
        public void TestSetIgnore()
        {
            var builder = db.SaveOne(new Person
            {
                Id = 1,
                Name = "jack2",
                Age = 23,
                Ext = new Ext
                {
                    QQ = "12345",
                    Phone = "152123456789",
                    Email = "12345@qq.com"
                }
            }).SetColumn(i => i.Age, 30).SetColumnForInsert(i => i.Name, "jackinsert").SetColumnForUpdate(i => i.Name, "jackupdate");
            var sql = builder.ToSql();
            sql.ShouldBe("""
                insert into test(id,`Age`,name,ext) values(1,30,'jackinsert','{"QQ":"12345","Phone":"152123456789","Email":"12345@qq.com"}') on duplicate key update 
                    `Age` = 30,
                    name = 'jackupdate',
                    ext = '{"QQ":"12345","Phone":"152123456789","Email":"12345@qq.com"}';
                """);
        }

        [Test]
        public void TestError_NoPrimaryKey()
        {
            try
            {
                var builder = db.SaveOne<Ext>();
                throw new Exception();
            }
            catch (Exception ex)
            {
                ex.Message.ShouldContain("没有配置主键, 不能用于 SaveOneBuilder");
            }
        }
    }
}
