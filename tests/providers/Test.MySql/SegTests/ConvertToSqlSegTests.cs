﻿using DBUtil;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Test.MySql.SegTests
{
    [TestFixture]
    internal sealed class ConvertToSqlSegTests : TestBase
    {
        [Test]
        public void TestNormal()
        {
            //数字
            db.ConvertToSqlSeg(1).Data.ShouldBe("1");
            db.ConvertToSqlSeg(1.123456).Data.ShouldBe("1.123456");
            //字符串
            db.ConvertToSqlSeg("刘备").Data.ShouldBe("'刘备'");
            db.ConvertToSqlSeg("' or 1=1 -- ").Data.ShouldBe("'\\' or 1=1 -- '");
            //dbnull or null
            db.ConvertToSqlSeg(null).Data.ShouldBe("null");
            db.ConvertToSqlSeg(DBNull.Value).Data.ShouldBe("null");
            //RawString
            db.ConvertToSqlSeg(new RawString("user()")).Data.ShouldBe("user()");
            //枚举
            db.ConvertToSqlSeg(TaskStatus.Running).Data.ShouldBe(((int)TaskStatus.Running).ToString());
            //bool
            db.ConvertToSqlSeg(true).Data.ShouldBe("1");
            db.ConvertToSqlSeg(false).Data.ShouldBe("0");
            //guid
            db.ConvertToSqlSeg(Guid.Parse("10adf41f2e974a289025b5355d473eca")).Data.ShouldBe("'10adf41f2e974a289025b5355d473eca'");
            db.ConvertToSqlSeg(Guid.Parse("10adf41f2e974a289025b5355d473eca"), "D").Data.ShouldBe("'10adf41f-2e97-4a28-9025-b5355d473eca'");

            //datetime
            //mysql 最多支持6位小数 超出的自动截断
            db.ConvertToSqlSeg(DateTime.Parse("2020-01-02 01:02:03.123456")).Data.ShouldBe("'2020-01-02 01:02:03.123456'");
            db.ConvertToSqlSeg(DateTime.Parse("2020-01-02 01:02:03.123000")).Data.ShouldBe("'2020-01-02 01:02:03.123'");
            db.ConvertToSqlSeg(DateTime.Parse("2020-01-02 01:02:03.0000")).Data.ShouldBe("'2020-01-02 01:02:03'");
            db.ConvertToSqlSeg(DateTime.Parse("2020-01-02 01:00:00.000000")).Data.ShouldBe("'2020-01-02 01:00:00'");
            db.ConvertToSqlSeg(DateTime.Parse("2020-01-02")).Data.ShouldBe("'2020-01-02'");
            db.ConvertToSqlSeg(DateTime.Parse("2020-01-02 01:02:03.123456"), "yyyy/MM/dd").Data.ShouldBe("'2020/01/02'");
            //datetimeoffset
            db.ConvertToSqlSeg(DateTimeOffset.Parse("2020-01-02 01:02:03.123456789 +08:00")).Data.ShouldBe("'2020-01-02 01:02:03.123456+08:00'");
            db.ConvertToSqlSeg(DateTimeOffset.Parse("2020-01-02 01:02:03.000 +08:00")).Data.ShouldBe("'2020-01-02 01:02:03+08:00'");
            db.ConvertToSqlSeg(DateTimeOffset.Parse("2020-01-02 +08:00")).Data.ShouldBe("'2020-01-02+08:00'");
            //timespan
            db.ConvertToSqlSeg(DateTime.Parse("2022-01-02 01:02:03.1234567") - DateTime.Parse("2022-01-01")).Data.ShouldBe("'25:02:03.1234567'");
            db.ConvertToSqlSeg(DateTime.Parse("2022-01-02 01:02:03.1234567") - DateTime.Parse("2022-01-02")).Data.ShouldBe("'01:02:03.1234567'");
            //datetonly timeonly
            db.ConvertToSqlSeg(new DateOnly(2020, 1, 2)).Data.ShouldBe("'2020-01-02'");
            db.ConvertToSqlSeg(new TimeOnly(1, 2, 3, 123)).Data.ShouldBe("'01:02:03.1230000'");

            //二进制
            db.ConvertToSqlSeg(new byte[] { 0x12, 0x13 }).Data.ShouldBe("0x1213");
            db.ConvertToSqlSeg(new sbyte[] { 0x12, 0x13 }).Data.ShouldBe("0x1213");
            db.ConvertToSqlSeg(new List<sbyte> { 0x12, 0x13 }).Data.ShouldBe("0x1213");
            //集合或数组
            db.ConvertToSqlSeg(new[] { 1, 2, 3 }).Data.ShouldBe("(1,2,3)");
            db.ConvertToSqlSeg(new[] { "小明", "' or 1=1 -- " }).Data.ShouldBe("('小明','\\' or 1=1 -- ')");
            db.ConvertToSqlSeg(new List<object> { new RawString("user()"), "xiaoming" }).Data.ShouldBe("(user(),'xiaoming')");
            //元组
            db.ConvertToSqlSeg((1, 2)).Data.ShouldBe("(1,2)");
            db.ConvertToSqlSeg(new[] { (1, 2), (1, 3) }).Data.ShouldBe("((1,2),(1,3))");
        }
    }
}