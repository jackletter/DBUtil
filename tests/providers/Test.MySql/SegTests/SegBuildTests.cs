﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.SegTests
{
    [TestFixture]
    internal sealed class SegBuildTests : TestBase
    {
        #region model
        public class BaseEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
        }
        [Table("student")]
        public class StudentEntity : BaseEntity
        {
            public string SNO { get; set; }
            public string Name { get; set; }
            public int KlassId { get; set; }
        }
        #endregion
        [Test]
        public void TestTable()
        {
            db.TableSeg<StudentEntity>().ShouldBe("student");
            db.TableSeg<StudentEntity>("t").ShouldBe("student t");
            db.TableSeg<StudentEntity>("t", old => old + "_202201").ShouldBe("student_202201 t");
        }

        [Test]
        public void TestWhereInt()
        {
            var id = 1;
            //单个
            db.WhereSeg<int>(i => i == id && i > 10 || new[] { 1, 2, 3 }.Contains(i)).ShouldBe("((i = 1) and (i > 10)) or (i in (1,2,3))");
            db.WhereSeg<int>(i => new int[] { }.Contains(i)).ShouldBe("i in ()");
            db.WhereSeg<int>(_ => new int[] { }.Contains(_)).ShouldBe(" in ()");
            //多个参数
            db.WhereSeg<int, int, int>((i, j, k) => new int[] { 1, 2 }.Contains(i) || j == 10 && k + 5 < 10).ShouldBe("(i in (1,2)) or ((j = 10) and ((k + 5) < 10))");

            //前缀
            string sql = string.Empty;
            db.WhereSeg<int>(age => age >= 10, "t").ShouldBe("t.age >= 10");
            db.WhereSeg<int?>(age => age.Value >= 10, "t").ShouldBe("t.age >= 10");
            db.WhereSeg<int?, string, PersonEntity>((age, name, p) => age.Value >= 10 && name.StartsWith("王") && p.Id > 5, ["t", "t", "t"]).ShouldBe("((t.age >= 10) and (t.name like '王%')) and (t.p.`Id` > 5)");

            //函数
            //https://dev.mysql.com/doc/refman/8.0/en/mathematical-functions.html
        }

        [Test]
        public void TestWhereString()
        {
            var sql = db.WhereSeg<StudentEntity>(t => t.Name.StartsWith("王"));
            sql.ShouldBe("t.`Name` like '王%'");
            sql = db.WhereSeg<StudentEntity>(_ => _.Name.StartsWith("王"));
            sql.ShouldBe("`Name` like '王%'");
        }

        [Test]
        public void TestWhereDateTime()
        {
            var sql = "";
            var start = DateTime.Parse("2023-01-01");
            var end = DateTime.Parse("2023-01-19");
            sql = db.WhereSeg<DateTime>(dt => dt >= start && dt <= end);
            sql.ShouldBe("(dt >= '2023-01-01') and (dt <= '2023-01-19')");
        }

        public enum EnumTest
        {
            Open = 1,
            Close = 2,
            Dead = 3
        }
        [Flags]
        public enum EnumTest2
        {
            Open = 1,
            Close = 2,
            Dead = 4
        }
        [Test]
        public void TestWhereEnum()
        {
            var sql = "";
            sql = db.WhereSeg<EnumTest>(type => type == EnumTest.Close);
            sql.ShouldBe("type = 2");
            sql = db.WhereSeg<EnumTest>(type => new[] { EnumTest.Close, EnumTest.Dead }.Contains(type), "t");
            sql.ShouldBe("t.type in (2,3)");
            sql = db.WhereSeg<EnumTest2>(type => type.ContainsAny(EnumTest2.Close | EnumTest2.Dead));
            sql.ShouldBe("type & 6 > 0");
            sql = db.WhereSeg<EnumTest2>(type => type.Contains(EnumTest2.Close | EnumTest2.Dead), "t");
            sql.ShouldBe("t.type & 6 = 6");
        }

        public class PersonEntity
        {
            public int Id { get; set; }
            [JsonStore(Bucket = "properties", Key = "name")]
            [Column("name")]
            public string Name { get; set; }
        }

        [Test]
        public void TestJsonStore()
        {
            string seg = string.Empty;
            seg = db.ColumnSeg<PersonEntity>(i => i.Name);
            seg.ShouldBe(@"json_value(i.properties,'$.""name""' returning char)");
            seg = db.ColumnSeg<PersonEntity>(i => new { OtherName = i.Name });
            seg.ShouldBe(@"json_value(i.properties,'$.""name""' returning char) `OtherName`");
            seg = db.ColumnSeg<PersonEntity>(i => new { OtherName = i.Name, i.Name, i.Id });
            seg.ShouldBe(@"json_value(i.properties,'$.""name""' returning char) `OtherName`,json_value(i.properties,'$.""name""' returning char) `Name`,i.`Id`");
            seg = db.ColumnSeg<PersonEntity>(_ => new { OtherName = _.Name, _.Name, _.Id });
            seg.ShouldBe(@"json_value(properties,'$.""name""' returning char) `OtherName`,json_value(properties,'$.""name""' returning char) `Name`,`Id`");
            seg = db.ColumnSeg<PersonEntity>(_ => new { Age = 10, OtherAge = _.Id });
            seg.ShouldBe("10 `Age`,`Id` `OtherAge`");
        }
    }
}
