﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.MySql.SegTests
{
    [TestFixture]
    internal sealed class UserSchemaSegTests : TestBase
    {
        [Test]
        public void Test()
        {
            var str = db.GetCurrentUserSqlSeg();
            db.GetCurrentUserSqlSeg().ShouldBe("user()");
            db.GetCurrentUserSqlSeg(true).ShouldBe("reverse(substring(reverse(user()),instr(reverse(user()),'@')+1))");
            db.GetCurrentLoginUserSqlSeg().ShouldBe("user()");
            db.GetCurrentLoginUserSqlSeg(true).ShouldBe("reverse(substring(reverse(user()),instr(reverse(user()),'@')+1))");
            db.GetCurrentDataBaseSqlSeg().ShouldBe("database()");
            db.GetCurrentSchemaSqlSeg().ShouldBe("database()");
            db.GetCurrentDataBaseVersionSqlSeg().ShouldBe("version()");

            db.UserName.ShouldBe("root@localhost");
            db.LoginUserName.ShouldBe("root@localhost");
            db.DBName.ShouldBe("test");
            db.SchemaName.ShouldBe("test");
            db.DBVersion.ShouldNotBeNullOrWhiteSpace();
        }
    }
}
