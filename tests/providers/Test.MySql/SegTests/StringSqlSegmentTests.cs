﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.MySql.SegTests
{
    [TestFixture]
    internal class StringSqlSegmentTests : TestBase
    {
        [Test]
        public void GetLengthTest()
        {
            var res = "";
            res = db.StringSqlSegment.GetLength("name");
            res.ShouldBe("length(name)");
            res = db.StringSqlSegment.GetLength("name", true);
            res.ShouldBe("length(ifnull(name,''))");
            res = db.StringSqlSegment.GetLength("'小明'");
            res.ShouldBe("length('小明')");
            res = db.StringSqlSegment.GetLength("'小明'", true);
            res.ShouldBe("length(ifnull('小明',''))");
        }
    }
}
