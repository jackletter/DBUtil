﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Text.Json.Nodes;

namespace Test.MySql.SegTests
{
    [TestFixture]
    public class WhereSegTests : TestBase
    {
        #region model
        public class Person
        {
            public int p_int { get; set; }
            public int? p_int_null { get; set; }
            public string p_str { get; set; }

            [JsonStore(Bucket = "ext")]
            public JsonObject p_jsonobj { get; set; }
            [JsonStore(Bucket = "ext", Key = "key")]
            public JsonObject p_jsonobj_key { get; set; }

            [JsonStore(Bucket = "ext")]
            public JsonObject p_jsonarr { get; set; }
            [JsonStore(Bucket = "ext", Key = "key")]
            public JsonObject p_jsonarr_key { get; set; }

            [JsonStore(Bucket = "ext", Key = "person")]
            public Person JPerson { get; set; }
        }

        #endregion

        [Test]
        public void TestModel()
        {
            string seg = null;
            //model.prop int
            seg = db.WhereSeg<Person>(i => i.p_int > 0);
            seg.ShouldBe("i.`p_int` > 0");
            seg = db.WhereSeg<Person>(i => (i.p_int_null ?? 3) > 0);
            seg.ShouldBe("ifnull(i.`p_int_null`,3) > 0");

            //model.prop string
            seg = db.WhereSeg<Person>(i => (i.p_str == "小明" ? 1 : 0) > 0);
            seg.ShouldBe("(if((i.`p_str` = '小明'),1,0)) > 0");
        }

        [Test]
        public void TestModelJsonObject()
        {
            string seg = null;
            seg = db.WhereSeg<Person>(i => i.p_jsonobj["age"].GetValue<int>() > 18);
            seg.ShouldBe("(json_value(json_value(i.ext,'$'),'$.\"age\"' returning signed)) > 18");
            seg = db.WhereSeg<Person>(i => i.p_jsonobj["age"].To<int>() > 18);
            seg.ShouldBe("convert(json_value(json_value(i.ext,'$'),'$.\"age\"'),decimal) > 18");

            seg = db.WhereSeg<Person>(i => i.p_jsonobj_key["ext"]["age"].GetValue<int>() > 18);
            seg.ShouldBe("(json_value(json_value(json_value(i.ext,'$.\"key\"'),'$.\"ext\"'),'$.\"age\"' returning signed)) > 18");
        }

        [Test]
        public void TestModelJsonArray()
        {
            string seg = null;
            seg = db.WhereSeg<Person>(i => i.p_jsonarr.ContainsAny(new { name = "jack" }.ToJsonObject()));
            seg.ShouldBe("json_overlaps(json_value(i.ext,'$'),'{\"name\":\"jack\"}')");
            seg = db.WhereSeg<Person>(i => i.p_jsonarr_key[0].ToJsonObject().ContainsKey("name"));
            seg.ShouldBe("json_contains_path(cast(json_value(json_value(i.ext,'$.\"key\"'),'$[0]') as json),'one','$.\"name\"')");
        }

        [Test]
        public void TestModelJsonModel()
        {
            string seg = null;
            seg = db.WhereSeg<Person>(i => i.JPerson.p_int == 1);
            seg.ShouldBe("(json_value(json_value(i.ext,'$.\"person\"'),'$.\"p_int\"' returning signed)) = 1");
            seg = db.WhereSeg<Person>(i => i.JPerson.p_str.StartsWith("王"));
            seg.ShouldBe(
                """
                (json_value(json_value(i.ext,'$."person"'),'$."p_str"' returning char)) like '王%'
                """);
        }
    }
}
