﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.SegTests
{
    [TestFixture]
    internal class CaseSegTests : TestBase
    {
        public enum EnumSex
        {
            Male,
            FeMale
        }
        [Table("t_person")]
        public class PersonEntity
        {
            [Column("id")]
            public int Id { get; set; }
            [Column("age")]
            public int Age { get; set; }
            [Column("classs_id")]
            public int ClassId { get; set; }
            public EnumSex Sex { get; set; }
        }
        public class KlassEntity
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        [Test]
        public void TestSelectNoEntity()
        {
            string sql = "";
            sql = db.CaseSeg("score")
                .WhenSeg(1).Then("高")
                .WhenSeg(2).Then("中")
                .WhenSeg(3).Then("低")
                .EndAs("desc");
            sql.ShouldBe(@"case score
  when 1 then '高'
  when 2 then '中'
  when 3 then '低' end desc");
        }

        [Test]
        public void TestSelectWithEntity()
        {
            string sql = "";
            sql = db.CaseSeg<PersonEntity>(t => t.Sex)
                .WhenSeg(EnumSex.Male).Then("男")
                .WhenSeg(EnumSex.FeMale).Then("女")
                .ElseSeg("未知")
                .EndAs("性别");
            sql.ShouldBe(@"case t.`Sex`
  when 0 then '男'
  when 1 then '女'
  else '未知' end 性别");

            sql = db.CaseSeg()
                .WhenSeg<PersonEntity, KlassEntity>((t, c) => t.Id == c.Id).Then<PersonEntity>(p => p.Id)
                .WhenSeg<PersonEntity>(t => t.ClassId == 0).Then<PersonEntity>(t => t.Age)
                .ElseSeg<KlassEntity>(c => c.Name)
                .EndAs("测试");
            sql.ShouldBe(@"case 
  when t.id = c.`Id` then p.id
  when t.classs_id = 0 then t.age
  else c.`Name` end 测试");
        }

        [Test]
        public void TestUpdateCase()
        {
            string sql = $"""
                update {db.TableSeg<PersonEntity>()} set {db.ColumnPureSeg<PersonEntity>(_ => _.Sex)}={db.CaseSeg<PersonEntity>(_ => _.Id)
                .WhenSeg(1).Then("男")
                .WhenSeg(2).Then("女")
                .End()}
                where {db.WhereSeg<PersonEntity>(_ => new[] { 1, 2 }.Contains(_.Id))}
                """;
            sql.ShouldBe(@"update t_person set `Sex`=case id
  when 1 then '男'
  when 2 then '女' end
where id in (1,2)");
        }

        [Test]
        public void TestUpdateCaseList()
        {
            var list = new List<PersonEntity>()
            {
                new PersonEntity{Id=1,Age=10},
                new PersonEntity{Id=2,Age=20},
            };
            string sql = $"""
                update {db.TableSeg<PersonEntity>()} set {db.ColumnPureSeg<PersonEntity>(_ => _.Age)}={db.CaseListSeg(list, _ => _.Id, _ => _.Age)}
                where {db.WhereSeg<PersonEntity>(_ => list.Select(i => i.Id).Contains(_.Id))}
                """;
            sql.ShouldBe(@"update t_person set age=case id
  when 1 then 10
  when 2 then 20 end
where id in (1,2)");
        }
    }
}
