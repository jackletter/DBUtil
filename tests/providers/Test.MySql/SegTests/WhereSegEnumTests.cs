﻿using DBUtil.Attributes;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Test.MySql.SegTests
{
    [TestFixture]
    public class WhereSegEnumTests : TestBase
    {
        #region model
        public class Person
        {
            #region 枚举
            public EnumSex p_enum_int { get; set; }
            public EnumSex? p_enum_int_null { get; set; }

            [Column(TypeName = "varchar(50)")]
            public EnumSex p_enum_string { get; set; }

            [Column(TypeName = "varchar(50)")]
            public EnumSex? p_enum_string_null { get; set; }

            public EnumFlag p_enum_flag { get; set; }
            public EnumFlag? p_enum_flag_null { get; set; }

            [Column(TypeName = "varchar(200)")]
            public EnumFlag p_enum_flag_string { get; set; }
            [Column(TypeName = "varchar(200)")]
            public EnumFlag? p_enum_flag_string_null { get; set; }
            #endregion
        }

        public enum EnumSex
        {
            Male = 1,
            FeMale = 2,
        }

        [Flags]
        public enum EnumFlag
        {
            Active = 1,
            Close = 2,
            Open = 4,
        }
        #endregion

        [Test]
        public void TestModelEnum()
        {
            string seg = null;
            //model.prop enum
            seg = db.WhereSeg<Person>(i => i.p_enum_int == EnumSex.FeMale);
            seg.ShouldBe("i.`p_enum_int` = 2");
            seg = db.WhereSeg<Person>(i => i.p_enum_int_null.Value == EnumSex.FeMale);
            seg.ShouldBe("i.`p_enum_int_null` = 2");

            //model.prop enum enum2string
            seg = db.WhereSeg<Person>(i => i.p_enum_string != EnumSex.FeMale);
            seg.ShouldBe("i.`p_enum_string` <> 'FeMale'");

            //model.prop enum flag
            seg = db.WhereSeg<Person>(i => i.p_enum_flag == EnumFlag.Open);
            seg.ShouldBe("i.`p_enum_flag` = 4");
            seg = db.WhereSeg<Person>(i => i.p_enum_flag != EnumFlag.Open);
            seg.ShouldBe("i.`p_enum_flag` <> 4");

            //model.prop enum flag enum2string
            seg = db.WhereSeg<Person>(i => i.p_enum_flag_string == EnumFlag.Open);
            seg.ShouldBe("i.`p_enum_flag_string` = 'Open'");
            seg = db.WhereSeg<Person>(i => i.p_enum_flag_string != EnumFlag.Open);
            seg.ShouldBe("i.`p_enum_flag_string` <> 'Open'");
        }

        [Test]
        public void TestModelEnumFlagContains()
        {
            string seg = null;
            //model.prop enum flag Contains
            seg = db.WhereSeg<Person>(i => i.p_enum_flag.ContainsAny(EnumFlag.Open));
            seg.ShouldBe("i.`p_enum_flag` & 4 > 0");
            seg = db.WhereSeg<Person>(i => i.p_enum_flag.ContainsAny(EnumFlag.Open | EnumFlag.Close));
            seg.ShouldBe("i.`p_enum_flag` & 6 > 0");
            seg = db.WhereSeg<Person>(i => i.p_enum_flag.Contains(EnumFlag.Open | EnumFlag.Close));
            seg.ShouldBe("i.`p_enum_flag` & 6 = 6");

            //model.prop enum flag string Contains
            seg = db.WhereSeg<Person>(i => i.p_enum_flag_string.ContainsAny(EnumFlag.Open));
            seg.ShouldBe("concat_ws('',concat_ws('',', ',i.`p_enum_flag_string`),',') like '%, Open,%'");
            seg = db.WhereSeg<Person>(i => i.p_enum_flag_string.ContainsAny(EnumFlag.Open | EnumFlag.Close));
            seg.ShouldBe("(concat_ws('',concat_ws('',', ',i.`p_enum_flag_string`),',') like '%, Close,%' or concat_ws('',concat_ws('',', ',i.`p_enum_flag_string`),',') like '%, Open,%')");
            seg = db.WhereSeg<Person>(i => i.p_enum_flag_string.Contains(EnumFlag.Open | EnumFlag.Close));
            seg.ShouldBe("(concat_ws('',concat_ws('',', ',i.`p_enum_flag_string`),',') like '%, Close,%' and concat_ws('',concat_ws('',', ',i.`p_enum_flag_string`),',') like '%, Open,%')");
        }

        [Test]
        public void TestModelEnum2StringAffter()
        {
            string seg = null;
            //model.prop enum enum2string 对其他的影响
            seg = db.WhereSeg<Person>(i => i.p_enum_string == EnumSex.FeMale && i.p_enum_int == EnumSex.Male);
            seg.ShouldBe("(i.`p_enum_string` = 'FeMale') and (i.`p_enum_int` = 1)");
            seg = db.WhereSeg<Person>(i => i.p_enum_int == EnumSex.Male && i.p_enum_string == EnumSex.FeMale);
            seg.ShouldBe("(i.`p_enum_int` = 1) and (i.`p_enum_string` = 'FeMale')");
        }

        [Test]
        public void TestModelEnumContains()
        {
            string seg = null;
            //model.prop enum Contains
            seg = db.WhereSeg<Person>(i => new[] { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_int));
            seg.ShouldBe("i.`p_enum_int` in (2,1)");
            seg = db.WhereSeg<Person>(i => new List<EnumSex> { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_int));
            seg.ShouldBe("i.`p_enum_int` in (2,1)");

            seg = db.WhereSeg<Person>(i => new EnumSex?[] { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_int));
            seg.ShouldBe("i.`p_enum_int` in (2,1)");
            seg = db.WhereSeg<Person>(i => new List<EnumSex?> { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_int));
            seg.ShouldBe("i.`p_enum_int` in (2,1)");

            //model.prop enum enum2string Contains
            seg = db.WhereSeg<Person>(i => new[] { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_string));
            seg.ShouldBe("i.`p_enum_string` in ('FeMale','Male')");
            seg = db.WhereSeg<Person>(i => new List<EnumSex> { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_string));
            seg.ShouldBe("i.`p_enum_string` in ('FeMale','Male')");

            seg = db.WhereSeg<Person>(i => new EnumSex?[] { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_string));
            seg.ShouldBe("i.`p_enum_string` in ('FeMale','Male')");
            seg = db.WhereSeg<Person>(i => new List<EnumSex?> { EnumSex.FeMale, EnumSex.Male }.Contains(i.p_enum_string));
            seg.ShouldBe("i.`p_enum_string` in ('FeMale','Male')");
        }

        [Test]
        public void WhereSegEnumStringDirect()
        {
            var seg = db.WhereSeg<EnumSex>(sex => sex == EnumSex.Male, enum2String: true);
            seg.ShouldBe("sex = 'Male'");

            var seg2 = db.WhereSeg<EnumSex>(sex => sex == EnumSex.Male, prefix: "t", enum2String: true);
            seg2.ShouldBe("t.sex = 'Male'");

            var seg3 = db.WhereSeg<EnumSex?>(sex => sex.Value == EnumSex.Male, prefix: "t", enum2String: true);
            seg3.ShouldBe("t.sex = 'Male'");

            var seg4 = db.WhereSeg<EnumSex, int, EnumFlag>((sex, age, flag) => sex == EnumSex.Male && age < 18 && flag.ContainsAny(EnumFlag.Close | EnumFlag.Active), prefixs: ["t", "t2", "t3"], enum2Strings: [false, false, true]);
            seg4.ShouldBe("((t.sex = 1) and (t2.age < 18)) and ((concat_ws('',concat_ws('',', ',t3.flag),',') like '%, Active,%' or concat_ws('',concat_ws('',', ',t3.flag),',') like '%, Close,%'))");
        }
    }
}
