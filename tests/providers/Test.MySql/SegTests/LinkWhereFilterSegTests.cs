﻿using NUnit.Framework;
using Shouldly;

namespace Test.MySql.SegTests
{
    [TestFixture]
    internal sealed class LinkWhereFilterSegTests : TestBase
    {
        [Test]
        public void Test()
        {
            db.LinkWhereFilterSeg(null).ShouldBe("where 1=1");
            db.LinkWhereFilterSeg("").ShouldBe("where 1=1");
            db.LinkWhereFilterSeg("  ").ShouldBe("where 1=1");
            db.LinkWhereFilterSeg("  \t ").ShouldBe("where 1=1");
            db.LinkWhereFilterSeg("\t and age>10").ShouldBe("where 1=1 and age>10");
            db.LinkWhereFilterSeg("\t oR age>10").ShouldBe("where 1=1 oR age>10");
            db.LinkWhereFilterSeg("\t age>10").ShouldBe("where age>10");
        }
    }
}
