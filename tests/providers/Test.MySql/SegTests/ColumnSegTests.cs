﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.SegTests
{
    [TestFixture]
    internal class ColumnSegTests : TestBase
    {
        #region model
        public class BaseEntity
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
        }
        [Table("student")]
        public class StudentEntityNoColumnAttr : BaseEntity
        {
            public string SNO { get; set; }
            public string Name { get; set; }
            public int KlassId { get; set; }
        }

        [Table("test")]
        public class PersonEntity
        {
            [Column("id")]
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            public int Id { get; set; }
            [Column("name")]
            public string Name { get; set; }
            [Column("`#age`")]
            public int Age { get; set; }
            [Column("Addr")]
            public string Addr { get; set; }
            public float Score { get; set; }
        }

        [Table("teacher")]
        public class TeacherEntity : BaseEntity
        {
            public string SNO { get; set; }
            public string Name { get; set; }
        }
        #endregion

        [Test]
        public void TestConvert2PropName()
        {
            db.ColumnSeg<StudentEntityNoColumnAttr>().ShouldBe("`SNO`,`Name`,`KlassId`,`Id`");
            db.ColumnSeg<StudentEntityNoColumnAttr>(t => t).ShouldBe("t.`SNO`,t.`Name`,t.`KlassId`,t.`Id`");
            db.ColumnSeg<StudentEntityNoColumnAttr>(t => new { t.Id, t.Name }).ShouldBe("t.`Id`,t.`Name`");
            db.ColumnSeg<StudentEntityNoColumnAttr>(_ => new { _.Id, _.Name }).ShouldBe("`Id`,`Name`");
            db.ColumnSeg<StudentEntityNoColumnAttr>(t => new { tid = t.Id, tname = t.Name }).ShouldBe("t.`Id` `tid`,t.`Name` `tname`");
        }

        [Test]
        public void Test3PureColumn()
        {
            db.ColumnPureSeg<PersonEntity>(i => i.Name).ShouldBe("name");
            db.ColumnPureSeg<PersonEntity>().ShouldBe("id,name,`#age`,Addr,`Score`");
            db.ColumnPureSeg<PersonEntity>(i => i).ShouldBe("i.id,i.name,i.`#age`,i.Addr,i.`Score`");
            db.ColumnPureSeg<PersonEntity>(t => new { t.Id, t.Name }).ShouldBe("t.id,t.name");
            db.ColumnPureSeg<StudentEntityNoColumnAttr>(_ => new { _.Id, _.Name }).ShouldBe("`Id`,`Name`");
            db.ColumnPureSeg<StudentEntityNoColumnAttr>(t => new { tid = t.Id, tname = t.Name }).ShouldBe("t.`Id`,t.`Name`");

            var seg = db.ColumnPureSeg<PersonEntity>(t => new { t.Id, t.Name, Age = t.Age > 18 ? 30 : 18 });
            seg.ShouldBe("t.id,t.name,if((t.`#age` > 18),30,18) `Age`");

            var seg2 = db.ColumnPureSeg<PersonEntity>(t => new PersonEntity { Id = t.Id, Name = t.Name, Age = t.Age > 18 ? 30 : 18 });
            seg2.ShouldBe("t.id,t.name,if((t.`#age` > 18),30,18) `Age`");

            var seg3 = db.ColumnPureSeg<PersonEntity>(t => new PersonEntity { Id = t.Id, Name = t.Name, Age = t.Age > 18 ? 30 : 18 });
            seg3.ShouldBe("t.id,t.name,if((t.`#age` > 18),30,18) `Age`");
        }

        [Test]
        public void TestMulti()
        {
            var sql = db.ColumnSeg<StudentEntityNoColumnAttr, TeacherEntity>((s, t) => new
            {
                Id = s.Id,
                Name = s.Name,
                TeacherName = t.Name,
                Desc = "(name=" + s.Name + ",teacher=" + t.Name + ")"
            });
            sql.ShouldBe("s.`Id`,s.`Name`,t.`Name` `TeacherName`,concat_ws('',concat_ws('',concat_ws('',concat_ws('','(name=',s.`Name`),',teacher='),t.`Name`),')') `Desc`");
        }
    }
}
