﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Deletes.DeleteExpression
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Table("t_user")]
        public class TUser
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }

            [Column("addr")]
            public string Addr { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }
        }
        [Test]
        public void Test()
        {
            var sql = "";
            sql = db.Delete<TUser>().Where(i => i.Id == 1).ToSql();
            sql.ShouldBe("delete from t_user where id = 1;");
            sql = db.Delete<TUser>().Where(i => i.Id == 1).Where("age>18").Where("age<60").ToSql();
            sql.ShouldBe("delete from t_user where (id = 1) and (age>18) and (age<60);");
            sql = db.Delete<TUser>().Where(i => i.Id == 1).Where("age>18").Where("age<60")
                .Where(i => i.Name.StartsWith("王"))
                .ToSql();
            sql.ShouldBe("delete from t_user where (id = 1) and (age>18) and (age<60) and (name like '王%');");
            sql = db.Delete<TUser>().Where(i => i.Id == 1).Where("age>18").Where("age<60")
                .Where(i => i.Name.StartsWith("王"))
                .Where(i => i.Name.EndsWith("小"))
                .ToSql();
            sql.ShouldBe("delete from t_user where (id = 1) and (age>18) and (age<60) and (name like '王%') and (name like '%小');");
            sql = db.Delete<TUser>().WhereSeg<int>(id => id == 1).ToSql();
            sql.ShouldBe("delete from t_user where id = 1;");
            sql = db.Delete<TUser>().WhereSeg<int, string>((id, name) => id == 1 && name.StartsWith('王')).ToSql();
            sql.ShouldBe("delete from t_user where (id = 1) and (name like '王%');");
        }
    }
}
