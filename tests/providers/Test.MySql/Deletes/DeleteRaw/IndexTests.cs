﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Deletes.DeleteRaw
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Table("t_user")]
        public class TUser
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }

            [Column("addr")]
            public string Addr { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }
        }
        [Test]
        public void Test()
        {
            var sql = "";
            sql = db.Delete("t_user").Where("id=1").ToSql();
            sql.ShouldBe("delete from t_user where id=1;");
            sql = db.Delete("t_user").Where("age>18").Where("age<60").ToSql();
            sql.ShouldBe("delete from t_user where (age>18) and (age<60);");
            sql = db.Delete("t_user").AsTableIf(true, old => old + "_user")
                .WhereSeg<int>(id => id == 1)
                .ToSql();
            sql.ShouldBe("delete from t_user_user where id = 1;");
        }
    }
}
