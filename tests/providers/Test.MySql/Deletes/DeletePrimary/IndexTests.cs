﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.MySql.Deletes.DeletePrimary
{
    [TestFixture]
    internal class IndexTests : TestBase
    {
        [Table("t_user")]
        public class TUser
        {
            [PrimaryKey]
            [Column("id")]
            public int Id { get; set; }

            [Column("name")]
            public string Name { get; set; }

            [Column("age")]
            public int? Age { get; set; }

            [Column("addr")]
            public string Addr { get; set; }

            [Column("birth")]
            public DateTime? Birth { get; set; }
        }

        [Table("t_user_student")]
        public class TUserStudent
        {
            [PrimaryKey]
            public int UserId { get; set; }

            [PrimaryKey]
            public int StudentId { get; set; }

            [Column("name")]
            public string Name { get; set; }
        }
        [Test]
        public void TestSimple()
        {
            var sql = "";
            sql = db.DeleteByPrimary<TUser>(1).ToSql();
            sql.ShouldBe("delete from t_user where id = 1;");
            sql = db.DeleteByPrimary<TUser>(1).Where("age>18").Where("age<60").ToSql();
            sql.ShouldBe("delete from t_user where id = 1 and ((age>18) and (age<60));");
            sql = db.DeleteByPrimary<TUser>(1).Where("age>18").Where("age<60")
                .Where(i => i.Name.StartsWith("王"))
                .ToSql();
            sql.ShouldBe("delete from t_user where id = 1 and ((age>18) and (age<60) and (name like '王%'));");
            sql = db.DeleteByPrimary<TUser>(1).Where("age>18").Where("age<60")
                .Where(i => i.Name.StartsWith("王"))
                .Where(i => i.Name.EndsWith("小"))
                .ToSql();
            sql.ShouldBe("delete from t_user where id = 1 and ((age>18) and (age<60) and (name like '王%') and (name like '%小'));");
        }

        [Test]
        public void TestMultiPrimary()
        {
            var sql = "";
            sql = db.DeleteByPrimary<TUserStudent>((1, 1)).ToSql();
            sql.ShouldBe("delete from t_user_student where (`UserId`,`StudentId`) = (1,1);");
            sql = db.DeleteByPrimary<TUserStudent>(new[] { (1, 1), (1, 2) }).Where("age>18").Where("age<60").ToSql();
            sql.ShouldBe("delete from t_user_student where (`UserId`,`StudentId`) in ((1,1),(1,2)) and ((age>18) and (age<60));");
        }

        [Test]
        public void TestArray()
        {
            var sql = "";
            sql = db.DeleteByPrimary<TUser>(new[] { 1, 2, 3 }).ToSql();
            sql.ShouldBe("delete from t_user where id in (1,2,3);");
            sql = db.DeleteByPrimary<TUser>(new[] { 1, 2, 3 }).Where("age>18").Where("age<60").ToSql();
            sql.ShouldBe("delete from t_user where id in (1,2,3) and ((age>18) and (age<60));");
            sql = db.DeleteByPrimary<TUser>(new[] { 1, 2, 3 }).Where("age>18").Where("age<60")
                .Where(i => i.Name.StartsWith("王"))
                .ToSql();
            sql.ShouldBe("delete from t_user where id in (1,2,3) and ((age>18) and (age<60) and (name like '王%'));");
            sql = db.DeleteByPrimary<TUser>(1).Where("age>18").Where("age<60")
                .Where(i => i.Name.StartsWith("王"))
                .Where(i => i.Name.EndsWith("小"))
                .ToSql();
            sql.ShouldBe("delete from t_user where id = 1 and ((age>18) and (age<60) and (name like '王%') and (name like '%小'));");
        }
    }
}
