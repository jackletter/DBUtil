﻿using DBUtil.Attributes;
using NUnit.Framework;
using Shouldly;
using System.ComponentModel.DataAnnotations.Schema;
using DotNetCommon.Extensions;
using System.Text.Json.Nodes;

namespace Test.MySql
{
    [TestFixture]
    internal class IfNullUseTests : TestBase
    {
        #region model
        [Table("test")]
        public class Person
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            public int? Age { get; set; }
            public string Addr { get; set; }
            public EnumTest? Test { get; set; }
        }

        public enum EnumTest
        {
            Open = 1, Close = 2
        }

        [Table("test2")]
        public class PersonJson
        {
            [PrimaryKey(KeyStrategy = KeyStrategy.Identity)]
            [Column("id")]
            public int Id { get; set; }

            [JsonStore(Bucket = "ext", Key = "Age")]
            public int? Age { get; set; }
            [JsonStore(Bucket = "ext", Key = "Addr")]
            public string Addr { get; set; }
            [JsonStore(Bucket = "ext", Key = "Test")]
            public EnumTest? Test { get; set; }
            [JsonStore(Bucket = "ext2")]
            public JsonObject Properties { get; set; }
        }
        #endregion

        [SetUp]
        public void SetUp()
        {
            DropTableIfExist("test");
            db.ExecuteSql("""create table test(id int primary key auto_increment,age int,test enum('Open','Close'),addr varchar(50))""");
            db.ExecuteSql("""
                insert into test(age,test,addr) values
                    (20,'Open','addr'),
                    (null,null,'    ');
                """);

            DropTableIfExist("test2");
            db.ExecuteSql("""create table test2(id int primary key auto_increment,ext json,ext2 json)""");
            db.ExecuteSql("""
                insert into test2(ext,ext2) values
                    ('{"Age":20,"Addr":"addr","Test":1}','{"id":1}'),
                    (null,null);
                """);
        }

        #region TestNoJson
        [Test]
        public void TestNoJson()
        {
            var update = db.Update<Person>()
                .SetColumnExpr(i => i.Age, i => i.Age.IfNullUse(0) + 1)
                .SetColumnExpr(i => i.Test, i => i.Test.IfNullUse(EnumTest.Open))
                .SetColumnExpr(i => i.Addr, i => i.Addr.IfNullOrWhiteSpaceUse("init") + "_2")
                .Where(i => i.Id > 0);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test set
                    `Age` = ifnull(`Age`,0) + 1,
                    `Test` = ifnull(`Test`,1),
                    `Addr` = concat_ws('',if((`Addr` is null or length(trim(`Addr`)) = 0),'init',`Addr`),'_2')
                where id > 0;
                """);

            update.ExecuteAffrows().ShouldBe(2);
            var p = db.Select<Person>().ToList();
            p[0].Age.ShouldBe(21);
            p[0].Test.ShouldBe(EnumTest.Open);
            p[0].Addr.ShouldBe("addr_2");
            p[1].Age.ShouldBe(1);
            p[1].Test.ShouldBe(EnumTest.Open);
            p[1].Addr.ShouldBe("init_2");
        }
        #endregion

        #region TestJson
        [Test]
        public void TestJson()
        {
            var update = db.Update<PersonJson>()
                .SetColumnExpr(i => i.Age, i => i.Age.IfNullUse(0) + 1)
                .SetColumnExpr(i => i.Test, i => i.Test.IfNullUse(EnumTest.Open))
                .SetColumnExpr(i => i.Addr, i => i.Addr.IfNullOrWhiteSpaceUse("init") + "_2")
                .SetColumnExpr(i => i.Properties, i => i.Properties.SetFluent("id", i.Properties["id"].GetValue<int?>().IfNullUse(0) + 1))
                .Where(i => i.Id > 0);
            var sql = update.ToSql();
            sql.ShouldBe("""
                update test2 set
                    ext = json_set(ifnull(ext,json_object()),
                        '$."Age"',(ifnull(json_value(ext,'$."Age"' returning signed),0)) + 1,
                        '$."Test"',ifnull(json_value(ext,'$."Test"' returning signed),1),
                        '$."Addr"',concat_ws('',if((json_value(ext,'$."Addr"' returning char) is null or length(trim(json_value(ext,'$."Addr"' returning char))) = 0),'init',json_value(ext,'$."Addr"' returning char)),'_2')),
                    ext2 = json_set(ifnull(json_value(ext2,'$'),'{}'),'$."id"',(ifnull(json_value(json_value(ext2,'$'),'$."id"' returning signed),0)) + 1)
                where id > 0;
                """);

            update.ExecuteAffrows().ShouldBe(2);
            var p = db.Select<PersonJson>().ToList();
            p[0].Age.ShouldBe(21);
            p[0].Test.ShouldBe(EnumTest.Open);
            p[0].Addr.ShouldBe("addr_2");
            p[0].Properties["id"].GetValue<int?>().ShouldBe(2);
            p[1].Age.ShouldBe(1);
            p[1].Test.ShouldBe(EnumTest.Open);
            p[1].Addr.ShouldBe("init_2");
            p[1].Properties["id"].GetValue<int?>().ShouldBe(1);
        }
        #endregion
    }
}
