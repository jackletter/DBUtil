﻿using DBUtil;
using DotNetCommon.Extensions;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Test.MySql
{
    [TestFixture]
    internal class WriteMonitorTests : TestBase
    {
        [Test]
        public void Test()
        {
            db = DBFactory.CreateDB(DBType.MYSQL, "Server=127.0.0.1;Database=test;Uid=root;Pwd=123456;", DBSetting.NewInstance()
                .SetSqlMonitor(arg =>
                {
                    Console.WriteLine(arg);
                    return Task.CompletedTask;
                })
                .SetWriteMonitor(arg =>
                {
                    Console.WriteLine(arg);
                    return Task.CompletedTask;
                }));
            db.ExecuteSql(db.Manage.DropTableIfExistSql("test5"));
            db.ExecuteSql("create table test5(id int primary key,name varchar(50))");

            var i = db.Insert("test5", new { id = 1, name = "小明" }.ToDictionary())
                .ExecuteAffrows();
            Console.WriteLine("ok");

        }
    }
}
