using DBUtil;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
var app = builder.Build();
DBUtil.DBFactory.AddDBSupport<DBUtil.Provider.MySql.MySqlDBFactory>();
app.MapControllers();
app.Run();
