﻿using DBUtil;
using Microsoft.AspNetCore.Mvc;

namespace WebTests.Controllers
{
    [Route("[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private static ILogger<TestController> _logger;
        private static DBAccess db = null;

        static TestController()
        {
            db = DBFactory.CreateDB(DBType.MYSQL, "Server=127.0.0.1;Database=test;Uid=root;Pwd=123456;", DBSetting.NewInstance().SetSqlMonitor(arg =>
            {
                _logger.LogInformation(arg.ToString());
                return Task.CompletedTask;
            }));
        }

        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
        }

        public object Get()
        {
            var dt = db.SelectDataTable(@"select * from information_schema.TABLES t
where t.TABLE_SCHEMA ='test'");
            db.SelectDataTable(@"select * from information_schema.TABLES t
where t.TABLE_SCHEMA ='test'");
            db.SelectDataTable(@"select * from information_schema.TABLES t
where t.TABLE_SCHEMA ='test'");
            return new { Id = 1, Name = "小明", Age = 18 };
        }
    }
}
