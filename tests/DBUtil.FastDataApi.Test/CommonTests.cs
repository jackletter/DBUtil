﻿using DBUtil.Provider.SqlServer.MetaData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using DotNetCommon.Extensions;

namespace DBUtil.FastData.Test
{
    [TestClass]
    public sealed class CommonTests : TestBase
    {
        private void PrepareData()
        {
            db.Manage.DropTableIfExist("test");
            db.ExecuteSql(@"create table test(
    col_bigint bigint primary key,
    col_binary binary(50),
    col_bit bit,
    col_char char(50),
    col_date date,
    col_datetime datetime,
    col_datetime2 datetime2(7),
    col_datetimeoffset datetimeoffset,
    col_decimal decimal,
    col_float float,
    col_geography geography,
    col_geometry geometry,
    col_hierarchyid hierarchyid,
    col_image image,
    col_int int,
    col_money money,
    col_nchar nchar(50),
    col_ntext ntext,
    col_numeric numeric(10,2),
    col_nvarchar nvarchar(50),
    col_real real,
    col_smalldatetime smalldatetime,
    col_smallint smallint,
    col_smallmoney smallmoney,
    col_sql_variant sql_variant,
    col_text text,
    col_time time(7),
    col_timestamp timestamp,
    col_tinyint tinyint,
    col_uniqueidentifier uniqueidentifier,
    col_varbinary varbinary(max),
    col_varchar varchar(50),
    col_xml xml
)");
            var res = db.InsertBatch(InsertBatchItem.Create("test", new
            {
                col_bigint = 1,
                col_binary = Encoding.UTF8.GetBytes("jack"),
                col_bit = 0,
                col_char = "jack haha",
                col_date = DateTime.Now,
                col_datetime = DateTime.Now,
                col_datetime2 = DateTime.Now,
                col_datetimeoffset = DateTime.Now,
                col_decimal = 15.2,
                col_float = 152.3,
                col_geography = ("geography::STGeomFromText('POLYGON ((-122.358 47.653, -122.348 47.649, -122.348 47.658, -122.358 47.658, -122.358 47.653))', 4326)", false),
                col_geometry = ("geometry::STGeomFromText('POLYGON ((0 0, 150 0, 150 150, 0 150, 0 0))', 4326)", false),
                col_hierarchyid = "/1/2/",
                col_image = Encoding.UTF8.GetBytes("abc"),
                col_int = 15,
                col_money = 30.23,
                col_nchar = "kolp",
                col_ntext = "小明啊",
                col_numeric = 15.6542,
                col_nvarchar = "和奇偶就氨基酸的",
                col_real = 15.63,
                col_smalldatetime = DateTime.Now,
                col_smallint = 45,
                col_smallmoney = 452.363,
                col_sql_variant = "string in sql_variant",
                col_text = "col_text",
                col_time = DateTime.Now,
                col_timestamp = ("default", false),
                col_tinyint = 15,
                col_uniqueidentifier = Guid.NewGuid().ToString(),
                col_varbinary = Encoding.UTF8.GetBytes("jiko"),
                col_varchar = "kosadas",
                col_xml = "<person name='小明'>info</person>"
            }.ToDictionary()));
            Assert.IsTrue(res == 1);
        }

        [TestMethod]
        public void TestQuery()
        {
            PrepareData();
            var api = new DBUtil.FastData.Api(db);
            var res = api.QueryData(new DBUtil.FastData.QueryCommand()
            {
                TableName = "Test",
                Tag = "Test",
                Columns = new List<DBUtil.FastData.ColumnRequest>()
                {
                    new DBUtil.FastData.ColumnRequest()
                    {
                        Name="col_bigint"
                    },
                    new DBUtil.FastData.ColumnRequest()
                    {
                        Name="col_nvarchar"
                    }
                }
            });
        }
    }
}
